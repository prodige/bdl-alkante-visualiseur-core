#!/bin/bash
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

sed -i "s|\"version\".*|\"version\": \"$1\",|" ${SCRIPTDIR}/projects/visualiseur-core/package.json;
ng build visualiseur-core;
if [ $? -ne 0 ]; then
  echo "ERROR during ng build visualiseur-core"
  exit 1
else
  cd dist/visualiseur-core;
  gitpkg publish --registry git@gitlab.alkante.al:angular/bdl-alkante-visualiseur-core.git
fi
