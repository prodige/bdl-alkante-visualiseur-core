import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnauthorizedDisconnectedComponent } from './unauthorized-disconnected.component';

describe('UnauthorizedDisconnectedComponent', () => {
  let component: UnauthorizedDisconnectedComponent;
  let fixture: ComponentFixture<UnauthorizedDisconnectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthorizedDisconnectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthorizedDisconnectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
