import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ElementRef,
  AfterViewInit,
} from "@angular/core";
import { Subscription } from "rxjs";
import { VisualiseurCoreService } from "../../projects/visualiseur-core/src/lib/visualiseur-core.service";
import { FeatureCollection } from "../../projects/visualiseur-core/src/lib/models/context";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HelpingModalComponent } from "../../projects/visualiseur-core/src/lib/modals/helping-modal/helping-modal.component";
import { LocalStorageService } from "ngx-webstorage";
import { DrawLayerService } from "../../projects/visualiseur-core/src/lib/services/draw-layer.service";
import { InterrogatingService } from "../../projects/visualiseur-core/src/lib/map/services/interrogating.service";
import { FontAwesomeSymbolDefinitionService } from "../../projects/visualiseur-core/src/lib/map/extensions/font-awesome-symbol-definition.service";
import { WmsTimeService } from "../../projects/visualiseur-core/src/lib/services/wms-time.service";
import { FavoriteZonesManageService } from "../../projects/visualiseur-core/src/lib/services/favorite-zones-manage.service";
import { MapService } from "../../projects/visualiseur-core/src/lib/map/map.service";

import LayerGroup from "ol/src/layer/Group";
import { Style, Fill, Stroke, Icon } from "ol/src/style";
import CircleStyle from "ol/src/style/Circle";
import FontSymbol from "ol-ext/style/FontSymbol";
import FillPattern from "ol-ext/style/FillPattern";
import StrokePattern from "ol-ext/style/StrokePattern";
import { InterrogatingTooltipButtonComponent } from "../../projects/visualiseur-core/src/lib/map/buttons/interrogating-tooltip/interrogating-tooltip.component";
// tslint:disable-next-line: max-line-length

import GeoJSON from "ol/src/format/GeoJSON.js";
import Feature from "ol/src/Feature";
import { createEmpty, extend } from "ol/src/extent";
import VectorLayer from "ol/src/layer/Vector";
import Map from "ol/src/Map";
import { MapIdService } from "../../projects/visualiseur-core/src/lib/map/map-id.service";
import ImageSource from "ol/source/Image";
import ImageLayer from "ol/layer/Image";
import BaseLayer from "ol/layer/Base";

@Component({
  selector: "alk-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  map: Map;
  informativeDisplayLayerBeenSet = false;

  oldScreenWidth: number;
  isNavCollapsed = false;
  context: Promise<FeatureCollection> | null = null;
  contextSubscription: Subscription = new Subscription();
  title = "bdl-alkante-visualiseur-core";
  infoLink = "#";
  helpingContent: string;
  resume: string;
  favoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> = [];

  sessionStorageSubscription: Subscription = new Subscription();
  localStorageSubscription: Subscription = new Subscription();
  FAVORITE_STORAGE_KEY = "favoriteZones";
  HELP_STORAGE_KEY = "isHelpPillVisible";
  isHelpPillVisible;
  isContextLoading = true;

  drawLayerFeatureSubscription: Subscription = new Subscription();
  drawLayerFeature = true;
  userConnectionSubscription: Subscription = new Subscription();
  isConnected = false;
  rootLayerGroupSubscription: Subscription = new Subscription();
  rootLayerGroup: LayerGroup;
  isMpaSheetAvailable = false;

  downloadableRootLayerGroup: LayerGroup;
  bufferGeneralNameIndexSubscription: Subscription = new Subscription();

  mapId = "visualiseur";

  mapToolSubscription: Subscription = new Subscription();

  wmsLayers: Array<ImageLayer<ImageSource>> = [];
  tools: FeatureCollection["properties"]["extension"]["Tools"];
  layout: FeatureCollection["properties"]["extension"]["Layout"];
  navOpened = true;
  selectedTool: string;

  resultSearching = false;
  resultSearchingSubscription: Subscription = new Subscription();
  tooltipResultSearchingSubscription: Subscription = new Subscription();
  resultAvailable = false;
  resultAvailableSubscription: Subscription = new Subscription();

  toastResponse: string = null;
  @ViewChild("copyToast", { read: null, static: true }) copyToast: ElementRef;
  @ViewChild(InterrogatingTooltipButtonComponent, { read: null, static: true })
  tooltipButton: InterrogatingTooltipButtonComponent;

  queryFeature: any;
  geojson = new GeoJSON();
  mapReadySubscription: Subscription = new Subscription();

  informativeDisplayLayerSubscription: Subscription = new Subscription();
  informativeDisplayLayer: VectorLayer;

  helpingDisplay: boolean;

  constructor(
    public mapService: MapService,
    public coreService: VisualiseurCoreService,
    public wmsService: WmsTimeService,
    public drawLayerService: DrawLayerService,
    private modalService: NgbModal,
    private localStorage: LocalStorageService,
    private interrogatingService: InterrogatingService,
    private favoriteZonesManage: FavoriteZonesManageService,
    private mapIdService: MapIdService
  ) {
    this.isHelpPillVisible = this.localStorage.retrieve(this.HELP_STORAGE_KEY);
  }

  ngOnInit() {
    this.oldScreenWidth = window.innerWidth;

    // On commence par mettre à jour les données de la base de la carte
    this.mapService.setMapId("visualiseur", this.mapId || "main");
    this.mapService.mapZoom = 25;
    this.mapService.mapCenter = [48.117266, -1.6777926];

    /* this.map = this.mapService.getMap(this.mapService.getMapId(this.mapId)); */

    this.drawLayerFeatureSubscription.add(
      this.drawLayerService.getHasFeature().subscribe((bool) => {
        setTimeout(() => {
          this.drawLayerFeature = !bool;
        }, 0);
      })
    );

    this.localStorageSubscription.add(
      this.localStorage.observe(this.HELP_STORAGE_KEY).subscribe((visible) => {
        this.isHelpPillVisible = visible;
      })
    );

    this.bufferGeneralNameIndexSubscription.add(
      this.drawLayerService.getBufferGeneralNameIndex().subscribe((index) => {
        this.rootLayerGroupSubscription.add(
          this.coreService.getRootLayerGroup().subscribe((layerGroup) => {
            this.rootLayerGroup = layerGroup;
            this.isMpaSheetAvailable = this.searchForMpaSheet(
              this.rootLayerGroup
            )
              ? true
              : false;
            this.downloadableRootLayerGroup =
              this.coreService.getDownloadableRootLayerGroup(
                new LayerGroup({ layer: layerGroup.getLayers() })
              );
            this.drawLayerService.checkIfIndexExists(index, layerGroup);
          })
        );
      })
    );

    this.resultAvailableSubscription.add(
      this.interrogatingService
        .getResultOfInterrogation()
        .subscribe((result) => {
          if (result && result.totalResults > 0) {
            this.resultAvailable = true;
          } else {
            this.resultAvailable = false;
          }
        })
    );

    this.resultSearchingSubscription.add(
      this.interrogatingService.getResultSearching().subscribe((result) => {
        this.resultSearching = result;
        if (!this.resultSearching) {
          setTimeout(() => {
            if (!this.resultAvailable && this.copyToast) {
              this.toastResponse = `Il n'y a pas de résultats pour cette recherche`;
              this.copyToast.nativeElement.className = "toast show";
              setTimeout(() => {
                this.copyToast.nativeElement.className =
                  this.copyToast.nativeElement.className.replace("show", "");
              }, 2000);
            }
          }, 500);
        }
      })
    );

    this.tooltipResultSearchingSubscription.add(
      this.interrogatingService
        .getTooltipResultSearching()
        .subscribe((result) => {
          this.resultSearching = result;
        })
    );

    this.mapToolSubscription.add(
      this.coreService.getMapTool().subscribe(
        (tool) => {
          this.selectedTool = tool;
        },
        (error) => console.error(error)
      )
    );

    const subscription = this.favoriteZonesManage
      .getSessionFavoriteZones()
      .subscribe((sessionFZ) => {
        if (sessionFZ) {
          this.favoriteZones = sessionFZ;
        }
      });

    this.contextSubscription.add(
      this.coreService.getContext().subscribe(
        (context) => {
          if (!context) {
            /* Promise.reject(null); */
            return false;
          }
          this.context = Promise.resolve(context);

          this.helpingContent = context.properties.extension.MapDescription;

          if (
            ["blue", "grey", "red", "default"].includes(
              context.properties.extension.Layout.Theme
            )
          ) {
            document.body.classList.add(
              context.properties.extension.Layout.Theme
            );
          } else {
            document.body.classList.add("default");
          }
          this.title = context.properties.title;
          document.title = this.title;
          this.infoLink = context.properties.extension.MapMetadata
            ? context.properties.extension.MapMetadata.links.via.href
            : null;
          this.helpingContent = context.properties.extension.MapDescription;
          this.helpingDisplay = context.properties.extension.MapHelpDisplay;
          this.resume = context.properties.subtitle;
          this.mapService.setFeatureTools(context.properties.extension.Tools);
          this.tools = context.properties.extension.Tools;
          this.layout = context.properties.extension.Layout;
          this.queryFeature = context.properties.extension.queryFeature;
        },
        (error) => console.error(error)
      )
    );

    this.wmsLayers = this.setDefaultWmsLayerForPlayer(
      this.rootLayerGroup
    ) as ImageLayer<ImageSource>[];
    this.wmsService.setWmsLayers(this.wmsLayers as any);
    this.setDefaultAnnotationStyle();
  }

  ngAfterViewInit() {
    if (this.tools && this.tools.InfoTooltip) {
      setTimeout(() => {
        this.coreService.setAnnotatingSnapping(false);
        /* this.tooltipButton.interrogationTooltip(); */
        this.coreService.setMapTool("toggleTooltip");
        this.coreService.setInterrogatingOption("desktop");
        if (
          this.helpingContent &&
          !this.isHelpPillVisible &&
          this.helpingDisplay
        ) {
          this.openHelpModal();
        }
      }, 3000);
    }

    this.mapReadySubscription.add(
      this.mapService
        .getMapReady(this.mapIdService.getId() || this.mapId || "main")
        .subscribe((map) => {
          if (!this.informativeDisplayLayerBeenSet && this.queryFeature) {
            const featuresToDisplay: Array<Feature<any>> = [];
            if (this.informativeDisplayLayer) {
              const extent = createEmpty();

              this.queryFeature.features.forEach((feature) => {
                const featureGenerated = this.geojson.readFeature(
                  feature
                ) as unknown as Feature<any>;
                if (featureGenerated && featureGenerated.getGeometry()) {
                  extend(extent, featureGenerated.getGeometry().getExtent());
                  featuresToDisplay.push(featureGenerated);
                }
              });
              map.getView().fit(extent, { size: map.getSize(), maxZoom: 15 });
              this.mapService.setMapFitEvent(true);
            }

            this.informativeDisplayLayerSubscription.add(
              this.mapService
                .getInformativeDisplayLayer(map)
                .subscribe((informativeDisplayLayer) => {
                  this.informativeDisplayLayer = informativeDisplayLayer;
                  if (
                    this.informativeDisplayLayer &&
                    featuresToDisplay.length !== 0
                  ) {
                    this.informativeDisplayLayerBeenSet = true;
                    this.informativeDisplayLayer.getSource().clear();
                    this.informativeDisplayLayer
                      .getSource()
                      .addFeatures(featuresToDisplay);
                  }
                })
            );
          }
        })
    );
  }

  ngOnDestroy() {
    this.drawLayerFeatureSubscription.unsubscribe();
    this.mapToolSubscription.unsubscribe();
    this.localStorageSubscription.unsubscribe();
    this.sessionStorageSubscription.unsubscribe();
    this.contextSubscription.unsubscribe();
    this.resultAvailableSubscription.unsubscribe();
    this.resultSearchingSubscription.unsubscribe();
    this.userConnectionSubscription.unsubscribe();
    this.informativeDisplayLayerSubscription.unsubscribe();
  }

  searchForMpaSheet(rootLayerGroup: LayerGroup): Array<BaseLayer> {
    const mpaSheets = [];
    rootLayerGroup
      .getLayers()
      .getArray()
      .forEach((layer) => {
        if (layer.get("class") === "LayerGroup") {
          this.searchForMpaSheet(layer).forEach((mpaSheet) => {
            mpaSheets.push(mpaSheet);
          });
        } else {
          if (
            layer.get("layerIdentifier") &&
            layer.get("layerIdentifier").includes("amp_sheet")
          ) {
            mpaSheets.push(layer);
          }
        }
      });
    return mpaSheets;
  }

  updateTools(): void {
    if (this.oldScreenWidth > 991 && window.innerWidth < 992) {
      this.modalService.dismissAll();
      this.coreService.setMapTool("");
      this.coreService.setInterrogatingOption("mobile");
    } else if (this.oldScreenWidth < 992 && window.innerWidth > 991) {
      this.coreService.setInterrogatingOption("desktop");
    }
    this.oldScreenWidth = window.innerWidth;
  }

  openHelpModal() {
    const modalRef = this.modalService.open(HelpingModalComponent, {
      size: "lg",
      centered: true,
      windowClass: "window-xl",
      backdrop: "static",
      keyboard: false,
    });

    if (modalRef.componentInstance.hideHelpContent) {
      modalRef.componentInstance.hideHelpContent
        .get("checkboxInput")
        .setValue(this.isHelpPillVisible);
    }

    modalRef.result.then((close) => {
      this.isHelpPillVisible = close;
      this.localStorage.store(this.HELP_STORAGE_KEY, this.isHelpPillVisible);
    });
  }

  setDefaultWmsLayerForPlayer(
    layerGroup: LayerGroup
  ): Array<ImageLayer<ImageSource>> {
    const layers = [];
    if (layerGroup) {
      layerGroup
        .getLayers()
        .getArray()
        .forEach((layer) => {
          if (layer.get("class") === "LayerGroup") {
            this.setDefaultWmsLayerForPlayer(layer);
          } else {
            if (layer.get("wmstimeMode") !== undefined) {
              layers.push(layer);
            }
          }
        });
    }
    return layers;
  }

  setDefaultAnnotationStyle(): void {
    // tslint:disable-next-line: no-unused-expression
    new FontAwesomeSymbolDefinitionService();

    // Defaut definition of annotating's tool point
    const symbol = {
      glyph: "fa-circle",
      fontSize: "0.3",
      fontStyle: "normal",
      color: "rgba(1, 149, 254, 0.85)",
      fill: new Fill({
        color: "rgba(1, 149, 254, 0.85)",
      }),
      stroke: new Stroke({
        color: "rgba(1, 149, 254, 0.85)",
        width: 2,
      }),
      radius: 20,
    };
    const defaultPoint = new Style({
      image: new FontSymbol(symbol),
    });
    this.drawLayerService.setPointStyle(defaultPoint);

    let defaultPattern = null;
    const emptyPattern = {
      empty: {
        width: 10,
        height: 10,
        lines: [],
        stroke: 0,
        fill: true,
      },
    };
    FillPattern.prototype.patterns = Object.assign(
      emptyPattern,
      FillPattern.prototype.patterns
    );
    for (const [key, value] of Object.entries(FillPattern.prototype.patterns)) {
      if (key === "empty") {
        defaultPattern = {
          name: key,
          data: value,
          pattern: new FillPattern({ pattern: key }).getImage().toDataURL(),
        };
      }
    }

    // Defaut definition of annotating's tool linestring
    const defaultLineString = new Style({
      stroke: new StrokePattern({
        width: 3,
        pattern: "empty",
        ratio: 2,
        color: "rgba(1, 149, 254, 0.85)",
        offset: 0,
        scale: 0,
        fill: new Fill({
          color: "rgba(1, 149, 254, 0.85)",
        }),
        size: 7,
        spacing: 10,
        angle: 0,
      }),
    });
    this.drawLayerService.setLineStringStyle(defaultLineString);
    this.drawLayerService.setLineStringStyleOptions({
      settedStyle: {
        size: 7,
        color: "rgba(1, 149, 254, 0.85)",
        background: "rgba(1, 149, 254, 0.85)",
        pattern: defaultPattern,
        text: null,
      },
    });

    // Defaut definition of annotating's tool polygon
    const defaultPolygon = new Style({
      fill: new FillPattern({
        pattern: "empty",
        ratio: 2,
        icon: new Icon({
          src: "data/target.png",
        }),
        color: "rgba(1, 149, 254, 0.85)",
        offset: 0,
        scale: 0,
        fill: new Fill({
          color: "rgba(0, 0, 0, 0.35)",
        }),
        size: 7,
        spacing: 10,
        angle: 0,
      }),
      stroke: new Stroke({
        color: "rgba(1, 149, 254, 0.85)",
        width: 3,
      }),
    });
    this.drawLayerService.setPolygonStyle(defaultPolygon);
    this.drawLayerService.setPolygonStyleOptions({
      settedStyle: {
        size: 7,
        color: "rgba(1, 149, 254, 0.85)",
        background: "rgba(0, 0, 0, 0.35)",
        pattern: defaultPattern,
        text: null,
      },
    });

    // style par défault pour la selection et le tracé
    const fill = new Fill({
      color: "rgba(1, 149, 254, 0.85)",
    });
    const stroke = new Stroke({
      // color: '#3399CC',
      color: "rgba(0, 0, 0, 0.85)",
      width: 3.25,
    });
    const defaultStyle = new Style({
      image: new CircleStyle({
        fill,
        stroke,
        radius: 5,
      }),
      fill,
      stroke,
    });

    this.drawLayerService.setDefaultDrawStyle(defaultStyle);
    this.drawLayerService.setDefaultSelectStyle(defaultStyle);
  }

  isNavOpened() {
    this.navOpened = !this.navOpened;
  }
}
