import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, registerLocaleData } from '@angular/common';
import { RouterModule } from '@angular/router';

import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr-FR', localeFrExtra);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { DragulaModule } from 'ng2-dragula';
import { VisualiseurCoreModule } from '../../projects/visualiseur-core/src/lib/visualiseur-core.module';
import { MapModule } from '../../projects/visualiseur-core/src/lib/map/map.module';

import { NavToolsService } from '../../projects/visualiseur-core/src/lib/services/nav-tools.service';

// Jquery
import { NotFoundComponent } from './not-found/not-found.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { UnauthorizedDisconnectedComponent } from './unauthorized-disconnected/unauthorized-disconnected.component';
import { EnvServiceProvider } from './env.service.provider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    BootstrapComponent,
    NotFoundComponent,
    UnauthorizedComponent,
    UnauthorizedDisconnectedComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    MapModule,
    VisualiseurCoreModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    DragulaModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
  ],
  providers: [
    NavToolsService,
    EnvServiceProvider
  ],
  bootstrap: [BootstrapComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
