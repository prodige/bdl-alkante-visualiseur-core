import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UrlManageCoreService } from '../../projects/visualiseur-core/src/lib/url-manage-core.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { UnauthorizedDisconnectedComponent } from './unauthorized-disconnected/unauthorized-disconnected.component';
import { CguDisplayComponent } from '../../projects/visualiseur-core/src/lib/cgu-display/cgu-display.component';

const routes: Routes = [
  {
    path: '401',
    pathMatch: 'prefix',
    component: UnauthorizedDisconnectedComponent
  },
  {
    path: '403',
    pathMatch: 'prefix',
    component: UnauthorizedComponent
  },
  /* {
    path: '403/:account/:context',
    pathMatch: 'prefix',
    component: UnauthorizedComponent
  }, */
  {
    path: '404',
    pathMatch: 'prefix',
    component: NotFoundComponent
  },
  {
    path: '404/:message',
    pathMatch: 'prefix',
    component: NotFoundComponent
  },
  /* {
    path: '404/:account/:context',
    pathMatch: 'prefix',
    component: NotFoundComponent
  }, */
  {
    path: 'cgu-display',
    pathMatch: 'prefix',
    component: CguDisplayComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    component: AppComponent,
    resolve: {
      context: UrlManageCoreService
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), BrowserModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
