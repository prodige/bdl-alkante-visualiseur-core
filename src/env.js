(function (window) {
  $ENV = window.__env = window.__env || {};

  /** Décommenter uniquement les lignes à surcharger */

  // API url
  // $ENV.serverUrl = 'https://carto-test.application-prodige-mig.fr/';

  // $ENV.serverUrl = 'https://carto.sigloire.fr/';
  // n_sre_eolien_r52.map

  // $ENV.serverUrl = 'https://carto.e-sweet-carto.fr/';
  // carte_terrains_prospection_esweet.map
  
  // $ENV.serverUrl = 'https://carto.oisethd-sig.fr/';
  // carte_smothd_epci.map
  // carte_smothd_departement.map

  // $ENV.serverUrl = 'https://carto-preprod.doterr.fr/';
  // bdcarto_all_layers.map

  // $ENV.serverUrl = 'https://carto-xp-pf-dgitm.application-prodige-mig.fr/';
  // carte_test_ilo_ign.map
  // carte_interactive_irve_en_service_des_aires_du_rrn.map

  $ENV.serverUrl = 'https://carto.picto-occitanie.fr/';
  // visualiseur_de_donnees_publiques.map

  // $ENV.serverUrl = 'https://carto.nexafrika.com/';
  // layers/ce1bda0f-0d76-47dc-b82c-63f0a8efc1af.map

  // $ENV.serverUrl = 'https://frontcarto-prodige41.alkante.al:19020/';


  // $ENV.serverUrl = 'https://carto.datara.gouv.fr/';
  // PRFB_Forestiers_engages_pour_la_biodiversite.map // dreal_nature_paysage_r82.map


  // $ENV.serverUrl = 'https://carto.sigloire.fr/';
  // n_gemapi_syndicats_r52.map


  // $ENV.serverUrl = 'https://carto-test.atlasante.fr/';

  // $ENV.serverUrl = 'https://carto.geoguyane.fr/';
  // Carte_Lidar_PVA_Images_Leve_Topo.map

  $ENV.serverUrl = 'https://carto.datara.gouv.fr/';
  // r_gestion_crise_d69.map

  // $ENV.serverUrl = 'https://frontcarto-amp.alkante.al:19010/';
  // maia.map // panache.map

  // $ENV.serverProj = 'https://admincarto-prodige41.alkante.al:19010/';


  // $ENV.serverUrl = 'https://test-carto.amp.milieumarinfrance.fr/';
  // $ENV.serverMpa = 'https://test-www.amp.milieumarinfrance.fr/';
  // 1/france.map

  $ENV.nearestPoint = {
    active: true,
    uuid: '46694f45-726c-4555-8cc4-f508898f07af'
  }

  // $ENV.interrogationPointPrecision = 5;

  // $ENV.interrogationTooltipPrecision = 5;

  // $ENV.defaultMousePositionProjection = "EPSG:4326";

  // $ENV.availableProjections = {
  //   "4326":  {
  //     "key": "EPSG:4326",
  //     "label": "WGS84",
  //     "proj": +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
  //     "bounds": [-180.0000, -90.0000, 180.0000, 90.0000]
  //   }
  // };

}(this));
