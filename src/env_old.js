(function (window) {
  $ENV = window.__env = window.__env || {};

  /** Décommenter uniquement les lignes à surcharger */

  // API url
  // $ENV.serverUrl = 'https://carto.geospm.com/';
  // $ENV.serverUrl = 'https://carto-test.atlasante.fr/';
  //$ENV.serverUrl = 'https://frontcarto-amp.alkante.al:19010/'; 
  // $ENV.serverUrl = 'https://carto.datara.gouv.fr/';
  // $ENV.serverUrl = 'https://frontcarto-prodige41.alkante.al:19020/';
  // $ENV.serverProj = 'https://admincarto-prodige41.alkante.al:19010/';

  // $ENV.interrogationPointPrecision = 5;

  // $ENV.interrogationTooltipPrecision = 5;

  // $ENV.defaultMousePositionProjection = "EPSG:4326";

  // $ENV.availableProjections = {
  //   "4326":  {
  //     "key": "EPSG:4326",
  //     "label": "WGS84",
  //     "proj": +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
  //     "bounds": [-180.0000, -90.0000, 180.0000, 90.0000]
  //   }
  // };
  /**
  $ENV.serverUrl = 'https://carto.prodige.internal/';
  $ENV.apiHost = 'https://metier.prodige.internal/app_dev.php/';
  $ENV.apiImport = '';
  $ENV.serverProj = 'https://admincarto.prodige.internal/';
  $ENV.idAppliMetier = 2;
  $ENV.catalogueUrl = 'https://catalogue-test.atlasante.fr';
  $ENV.uuidCalculUcd = '5e0951a9-b095-41a9-b89c-9c124b4bf1ed';
  $ENV.uuidCalculFiness = 'bd0d9364-3846-43b3-a3f3-873cefd92c6b';
  $ENV.apiRespireSiren = 'https://www-test.atlasante.fr/scripts/prodigedist/annu_api.php?service=getSirenUserByProfil&profil=246';//*/

  // mig
  //**
  $ENV.serverUrl = 'https://carto-test.application-prodige-mig.fr/';
  $ENV.apiHost = 'https://metier-prodige42.alkante.al/app_dev.php/';
  $ENV.apiImport = '';
  $ENV.serverProj = 'https://admincarto-test.application-prodige-mig.fr/';
  $ENV.idAppliMetier = 2;
  $ENV.catalogueUrl = 'https://catalogue-test.application-prodige-mig.fr';
  $ENV.uuidCalculUcd = '5e0951a9-b095-41a9-b89c-9c124b4bf1ed';
  $ENV.uuidCalculFiness = 'bd0d9364-3846-43b3-a3f3-873cefd92c6b';
  $ENV.apiRespireSiren = 'https://www-test.atlasante.fr/scripts/prodigedist/annu_api.php?service=getSirenUserByProfil&profil=246';//*/

  /*  atlasante
  $ENV.serverUrl = 'https://carto-test.atlasante.fr/';
  $ENV.apiHost = 'https://metier-test.atlasante.fr/app_dev.php/';
  $ENV.apiImport = '';
  $ENV.serverProj = 'https://admincarto-test.atlasante.fr/';
  $ENV.idAppliMetier = 2;
  $ENV.catalogueUrl = 'https://catalogue-test.atlasante.fr';
  $ENV.uuidCalculUcd = '5e0951a9-b095-41a9-b89c-9c124b4bf1ed';
  $ENV.uuidCalculFiness = 'bd0d9364-3846-43b3-a3f3-873cefd92c6b';
  $ENV.apiRespireSiren = 'https://www-test.atlasante.fr/scripts/prodigedist/annu_api.php?service=getSirenUserByProfil&profil=246';//*/
  // $ENV.interrogationTooltipPrecision = 5;
  
  /*  virtualbox
  $ENV.serverUrl = 'https://carto-prodige42.alkante.al/';
  $ENV.apiHost = 'https://metier-prodige42.alkante.al/app_dev.php/';
  $ENV.apiImport = '';
  $ENV.serverProj = 'https://admincarto-prodige42.alkante.al/';
  $ENV.idAppliMetier = 2;
  $ENV.catalogueUrl = 'https://catalogue-prodige42.alkante.al';
  $ENV.uuidCalculUcd = '5e0951a9-b095-41a9-b89c-9c124b4bf1ed';
  $ENV.uuidCalculFiness = 'bd0d9364-3846-43b3-a3f3-873cefd92c6b';
  $ENV.apiRespireSiren = 'https://www-prodige42.alkante.al/scripts/prodigedist/annu_api.php?service=getSirenUserByProfil&profil=246';//*/

  /**
  $ENV.serverUrl = 'https://carto.prodige.internal/';
  $ENV.apiHost = 'https://metier.prodige.internal/app_dev.php/';
  $ENV.apiImport = '';
  $ENV.serverProj = 'https://admincarto.prodige.internal/';
  $ENV.idAppliMetier = 2;
  $ENV.catalogueUrl = 'https://catalogue.prodige.internal';
  $ENV.uuidCalculUcd = '5e0951a9-b095-41a9-b89c-9c124b4bf1ed';
  $ENV.uuidCalculFiness = 'bd0d9364-3846-43b3-a3f3-873cefd92c6b';
  $ENV.apiRespireSiren = 'https://www-test.atlasante.fr/scripts/prodigedist/annu_api.php?service=getSirenUserByProfil&profil=246';//*/
  // $ENV.defaultMousePositionProjection = "EPSG:4326"; 

  // $ENV.availableProjections = {
  //   "4326":  {
  //     "key": "EPSG:4326",
  //     "label": "WGS84",
  //     "proj": +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
  //     "bounds": [-180.0000, -90.0000, 180.0000, 90.0000]
  //   }
  // };

  //$ENV.enabledExportShp = true;

 // $ENV.cartorExportUrlDev =  "http://carto.prodige.internal/"; //TODO: pour les dev

}(this));
