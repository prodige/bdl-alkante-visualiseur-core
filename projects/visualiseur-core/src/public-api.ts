/*
 * Public API Surface of visualiseur-core
 */

export * from './lib/visualiseur-core.service';
export * from './lib/visualiseur-core.component';
export * from './lib/visualiseur-core.module';
export * from './lib/map/map.module';

/* Export des interfaces */
export { ConnexionConfig } from './lib/models/connexion-config';
export { FilterLayer } from './lib/models/filter-layer';
export { OptionChart } from './lib/models/option-chart';
export { OptionImpression } from './lib/models/option-impression';

/** Export des pipe */
export { InformationSheetFilterPipe } from './lib/pipes/information-sheet-filter.pipe';
export { NumberToLabelPipe } from './lib/pipes/number-to-label.pipe';
export { LegendFilterPipe } from './lib/pipes/legend-filter.pipe';

/* Export des composants */
export { VisualiseurCoreComponent } from './lib/visualiseur-core.component';
export { OlMapComponent } from './lib/map/ol-map/ol-map.component';
export { NavProjectComponent } from './lib/nav-project/nav-project.component';
export { ConnexionButtonComponent } from './lib/connexion-button/connexion-button.component';
export { HeaderComponent } from './lib/action-button-dropdown/header/header.component';
export { ItemComponent } from './lib/action-button-dropdown/item/item.component';
export { AnnotatingButtonComponent } from './lib/action-buttons/annotating-button/annotating-button.component';
export { DownloadingButtonComponent } from './lib/action-buttons/downloading-button/downloading-button.component';
export { DrawingButtonComponent } from './lib/action-buttons/drawing-button/drawing-button.component';
export { InterrogatingButtonComponent } from './lib/action-buttons/interrogating-button/interrogating-button.component';
export { LocatingButtonComponent } from './lib/action-buttons/locating-button/locating-button.component';
export { MeasuringButtonComponent } from './lib/action-buttons/measuring-button/measuring-button.component';
export { OpeningButtonComponent } from './lib/action-buttons/opening-button/opening-button.component';
export { PrintingButtonComponent } from './lib/action-buttons/printing-button/printing-button.component';
export { SavingButtonComponent } from './lib/action-buttons/saving-button/saving-button.component';
export { SharingButtonComponent } from './lib/action-buttons/sharing-button/sharing-button.component';
export { AddLayerComponent } from './lib/layers-legend/sidebars/add-layer/add-layer.component';
export { LayerLegendLiteComponent } from './lib/layers-legend/layer-legend-lite/layer-legend-lite.component';
export { DrawingBoxRatioComponent } from './lib/map/tools/annotating-tools/drawing-box-ratio/drawing-box-ratio.component';
export { MeasuringToolsComponent } from './lib/map/tools/measuring-tools/measuring-tools.component';
export { DrawingPolygonComponent } from './lib/map/tools/annotating-tools/drawing-polygon/drawing-polygon.component';
export { DrawingLinestringComponent } from './lib/map/tools/annotating-tools/drawing-linestring/drawing-linestring.component';
export { DrawingSymbolComponent } from './lib/map/tools/annotating-tools/drawing-symbol/drawing-symbol.component';
export { InsertTextComponent } from './lib/map/tools/annotating-tools/insert-text/insert-text.component';
export { BuffersComponent } from './lib/map/tools/annotating-tools/buffers/buffers.component';
export { EditDrawsComponent } from './lib/map/tools/annotating-tools/edit-draws/edit-draws.component';
export { StyleDrawsComponent } from './lib/map/tools/annotating-tools/style-draws/style-draws.component';
export { ExportDrawsComponent } from './lib/map/tools/annotating-tools/export-draws/export-draws.component';
export { InterrogatingPointComponent } from './lib/map/tools/interrogating-tools/interrogating-point/interrogating-point.component';
// tslint:disable-next-line: max-line-length
export { InterrogatingRectangleComponent } from './lib/map/tools/interrogating-tools/interrogating-rectangle/interrogating-rectangle.component';
export { InterrogatingCircleComponent } from './lib/map/tools/interrogating-tools/interrogating-circle/interrogating-circle.component';
export { InterrogatingPolygonComponent } from './lib/map/tools/interrogating-tools/interrogating-polygon/interrogating-polygon.component';
// tslint:disable-next-line: max-line-length
export { InterrogatingGrantedRequestComponent } from './lib/map/tools/interrogating-tools/interrogating-granted-request/interrogating-granted-request.component';
export { InterrogatingTooltipComponent } from './lib/map/tools/interrogating-tools/interrogating-tooltip/interrogating-tooltip.component';
export { InterrogatingTooltipButtonComponent } from './lib/map/buttons/interrogating-tooltip/interrogating-tooltip.component';
/* export { LayersTooltipComponent } from './lib/map-tools/nav-tools/layers-tooltip/layers-tooltip.component'; */
export { ResearchComponent } from './lib/map-tools/research/research.component';
export { MobileSidebarComponent } from './lib/layers-legend/sidebars/mobile-sidebar/mobile-sidebar.component';
export { ResultMobileComponent } from './lib/interrogating-results/result-mobile/result-mobile.component';
export { SidebarComponent } from './lib/layers-legend/sidebars/sidebar/sidebar.component';
export { ZoomInComponent } from './lib/map/buttons/zoom-in/zoom-in.component';
export { ZoomOutComponent } from './lib/map/buttons/zoom-out/zoom-out.component';
export { MobileInterrogationComponent } from './lib/map/buttons/mobile-interrogation/mobile-interrogation.component';
export { ZoomRestoreComponent } from './lib/map/buttons/zoom-restore/zoom-restore.component';
export { ButtonFullScreenComponent } from './lib/map/buttons/full-screen/full-screen.component';
export { ResultBottomBoxComponent } from './lib/interrogating-results/result-bottom-box/result-bottom-box.component';
export { MapVisualisationComponent } from './lib/map-tools/map-visualisation/map-visualisation.component';
export { ResultButtonComponent } from './lib/map-tools/result-button/result-button.component';
export { LogoComponent } from './lib/map-tools/logo/logo.component';
export { CopyrightComponent } from './lib/map-tools/copyright/copyright.component';
export { ScaleLineComponent } from './lib/map/controls/scale-line/scale-line.component';
export { MousePositionComponent } from './lib/map/controls/mouse-position/mouse-position.component';
// tslint:disable-next-line: max-line-length
export { CoordinateProjectionSelectorComponent } from './lib/map/controls/coordinate-projection-selector/coordinate-projection-selector.component';
export { MapScaleDropdownComponent } from './lib/map-tools/map-scale-dropdown/map-scale-dropdown.component';
export { BaseMapDropdownComponent } from './lib/map-tools/base-map-dropdown/base-map-dropdown.component';
export { LayersLegendComponent } from './lib/layers-legend/layers-legend/layers-legend.component';
export { ActionButtonsComponent } from './lib/action-buttons/action-buttons.component';
export { WmsTimeComponent } from './lib/map-tools/wms-time/wms-time.component';
export { ResultContentComponent } from './lib/interrogating-results/result-modal/result-content/result-content.component';
export { CguDisplayComponent } from './lib/cgu-display/cgu-display.component';
export { PresetZonesContainerComponent } from './lib/modals/locating-preset-zones-modal/preset-zones-container/preset-zones-container.component';
export { PresetZoneSelectorComponent } from './lib/modals/locating-preset-zones-modal/preset-zones-container/preset-zone-selector/preset-zone-selector.component';
export { PresetZoneFirstSelectorComponent } from './lib/modals/locating-preset-zones-modal/preset-zones-container/preset-zone-first-selector/preset-zone-first-selector.component';
export { ChoseServerComponent } from './lib/modals/add-layer-modal/chose-server/chose-server.component';
export { GeolocationButtonComponent } from './lib/map-tools/geolocation-button/geolocation-button.component';

export { Parser } from './lib/parser-requetes-attributaires';

/* Export modals */
export { ResultModalComponent } from './lib/interrogating-results/result-modal/result-modal.component';
export { SharingModalComponent } from './lib/modals/sharing-modal/sharing-modal.component';
export { PrintingExportModalComponent } from './lib/modals/printing-export-modal/printing-export-modal.component';
export { PrintingPdfModalComponent } from './lib/modals/printing-pdf-modal/printing-pdf-modal.component';
export { PrintingImageModalComponent } from './lib/modals/printing-image-modal/printing-image-modal.component';
export { DownloadingModalComponent } from './lib/modals/downloading-modal/downloading-modal.component';
export { OpeningModalComponent } from './lib/modals/opening-modal/opening-modal.component';
export { SavingModalComponent } from './lib/modals/saving-modal/saving-modal.component';
export { LocatingManageModalComponent } from './lib/modals/locating-manage-modal/locating-manage-modal.component';
export { LocatingAddModalComponent } from './lib/modals/locating-add-modal/locating-add-modal.component';
export { LocatingPresetZonesModalComponent } from './lib/modals/locating-preset-zones-modal/locating-preset-zones-modal.component';
export { MeasuringModalComponent } from './lib/map/tools/measuring-tools/measuring-modal/measuring-modal.component';
export { InterrogatingModalComponent } from './lib/map/tools/interrogating-tools/interrogating-modal/interrogating-modal.component';
export { AnnotatingModalComponent } from './lib/map/tools/annotating-tools/annotating-modal/annotating-modal.component';
export { PreEditingModalComponent } from './lib/map/tools/editing-tools/pre-editing-modal/pre-editing-modal.component';
export { EditingModalComponent } from './lib/map/tools/editing-tools/pre-editing-modal/editing-modal/editing-modal.component';
export { SharingContentModalComponent } from './lib/modals/sharing-modal/sharing-content-modal/sharing-content-modal.component';
export { OpeningContentModalComponent } from './lib/modals/opening-modal/opening-content-modal/opening-content-modal.component';
export { GenerateStyleDrawsModalComponent } from './lib/map/tools/annotating-tools/style-draws/style-draws-modal/generate-style-draws-modal/generate-style-draws-modal.component';
export { MergeFicheModalComponent } from './lib/modals/merge-fiche-modal/merge-fiche-modal.component';
export { InterrogatingGrantedRequestModalComponent } from './lib/modals/interrogating-granted-request-modal/interrogating-granted-request-modal.component';
export { InterrogatingBufferModalComponent } from './lib/modals/interrogating-buffer-modal/interrogating-buffer-modal.component';
export { ExportLegendModalComponent } from './lib/modals/printing-export-modal/export-legend-modal/export-legend-modal.component';
export { ImportFileModalComponent } from './lib/modals/import-file-modal/import-file-modal.component';
export { HelpingModalComponent } from './lib/modals/helping-modal/helping-modal.component';
export { AddLayerModalComponent } from './lib/modals/add-layer-modal/add-layer-modal.component';
export { StyleDrawsModalComponent } from './lib/map/tools/annotating-tools/style-draws/style-draws-modal/style-draws-modal.component';
export { InsertTextModalComponent } from './lib/map/tools/annotating-tools/insert-text/insert-text-modal/insert-text-modal.component';
export { BuffersModalComponent } from './lib/map/tools/annotating-tools/buffers/buffers-modal/buffers-modal.component';
export { ResultModalFicheComponent } from './lib/interrogating-results/result-modal-fiche/result-modal-fiche.component';
export { ModalConfirmComponent } from './lib/modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component';
export { SearchMpaModalComponent } from './lib/modals/search-mpa-modal/search-mpa-modal.component';
export { InterrogatingMailingModalComponent } from './lib/modals/interrogating-mailing-modal/interrogating-mailing-modal.component';

/* Export des Services */
export { MapService } from './lib/map/map.service';
export { MapIdService } from './lib/map/map-id.service';
export { DrawLayerService } from './lib/services/draw-layer.service';
export { FavoriteZonesManageService } from './lib/services/favorite-zones-manage.service';
export { InterrogatingService } from './lib/map/services/interrogating.service';
export { UserConnectionService } from './lib/services/user-connection.service';
export { EditingToolService } from './lib/services/editing-tool.service';
export { EnvCoreService } from './lib/env.core.service';
export { EnvCoreServiceProvider } from './lib/env.core.service.provider';
export { UrlProxyService } from './lib/services/url-proxy.service';
export { WmsTimeService } from './lib/services/wms-time.service';
export { MousePositionProjectionService } from './lib/map/mouse-position-projection.service';
export { UrlManageCoreService } from './lib/url-manage-core.service';
export { PresetZonesSelectorService } from './lib/services/preset-zones-selector.service';
export { FontAwesomeSymbolDefinitionService } from './lib/map/extensions/font-awesome-symbol-definition.service';
export { NavToolsService } from './lib/services/nav-tools.service';
export { AddLayerServersService } from './lib/services/add-layer-servers.service';

/* Export manquant */
export { AttributesDrawComponent } from './lib/map/tools/annotating-tools/edit-draws/attributes-draw/attributes-draw.component';
export { IndeterminateDirective } from './lib/map/directives/indeterminate.directive';
export { RemoveDrawComponent } from './lib/map/tools/annotating-tools/edit-draws/remove-draw/remove-draw.component';
export { MoveDrawComponent } from './lib/map/tools/annotating-tools/edit-draws/move-draw/move-draw.component';
export { ModifyDrawComponent } from './lib/map/tools/annotating-tools/edit-draws/modify-draw/modify-draw.component';
export { DrawingCircleComponent } from './lib/map/tools/annotating-tools/drawing-circle/drawing-circle.component';
export { AlertConfirmPromptModalComponent } from './lib/modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component';
export { ChartComponent } from './lib/chart/chart.component';

export * from './lib/map/extensions/ol.extensions';

export { VisualiseurCoreModule } from './lib/visualiseur-core.module';
export { MapModule } from './lib/map/map.module';
