import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alk-visualiseur-core',
  template: `
    <ng-content></ng-content>
  `,
  styles: []
})
export class VisualiseurCoreComponent implements OnInit {


  constructor() { }

  ngOnInit() {}

}
