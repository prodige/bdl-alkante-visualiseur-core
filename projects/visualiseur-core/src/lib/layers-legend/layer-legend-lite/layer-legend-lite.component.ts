import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {InterrogatingService} from '../../map/services/interrogating.service';
import TileLayer from 'ol/layer/Tile';
import ImageLayer from 'ol/layer/Image';
import LayerGroup from 'ol/layer/Group';
import VectorLayer from 'ol/layer/Vector';
import {DragulaService} from 'ng2-dragula';
import {NgbActiveModal, NgbDropdownConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FavoriteZonesManageService} from '../../services/favorite-zones-manage.service';
import {VisualiseurCoreService} from '../../visualiseur-core.service';
import {MapIdService} from '../../map/map-id.service';
import {Subscription} from 'rxjs';
import {MapService} from '../../map/map.service';
import {LayersLegendComponent} from '../layers-legend/layers-legend.component';

@Component({
  selector: 'alk-layer-legend-lite',
  templateUrl: './layer-legend-lite.component.html',
  styleUrls: ['./layer-legend-lite.component.scss']
})
export class LayerLegendLiteComponent extends  LayersLegendComponent implements OnInit, OnDestroy {
  @Input() mapId: string;

  rootLayerGroup: LayerGroup;

  subRootLayerGroup: Subscription;

  layerReady = false;

  legends = [];

  subGetMapReady: Subscription = null;

  moveMap = false;

  @Input() showLegendTitle: false;

  constructor(
    public mapService: MapService,
    public coreService: VisualiseurCoreService,
    public modalService: NgbModal,
    public config: NgbDropdownConfig,
    public dragulaService: DragulaService,
    public interrogatingService: InterrogatingService,
    public mapIdService: MapIdService,
    public favoriteZonesManage: FavoriteZonesManageService,
    public favoriteZonesManageService: FavoriteZonesManageService,
    public activeModal: NgbActiveModal
  ) {
    super(mapService, coreService, null, modalService, config, dragulaService, interrogatingService,
      mapIdService, favoriteZonesManage, favoriteZonesManageService, activeModal);
  }

  ngOnInit(): void {
    this.mode = 'Légende';
    this.subRootLayerGroup = this.coreService.getRootLayerGroup().subscribe(
      (rootLayerGroup: LayerGroup) => {
        this.layerReady = false;
        this.rootLayerGroup = rootLayerGroup;

        this.legends = [];
        this.allLayerLegendInArray(this.rootLayerGroup);

        setTimeout(() => {
          this.layerReady = true;
        }, 100);

      }
    );

    this.subGetMapReady = this.mapService.getMapReady(this.mapId || 'visualiseur')
      .subscribe((map) => {


        map.on('moveend', () => {
          // (Septembre 2022) Pour mettre à jour les filtres, en attendant une version plus opti du visualiseur core
          this.moveMap = true;
          setTimeout(() => {
            this.moveMap = false;
          }, 25);
        });
      });

  }

  ngOnDestroy() {
    if (this.subRootLayerGroup) {
      this.subRootLayerGroup.unsubscribe();
    }

    if (this.subGetMapReady){
      this.subGetMapReady.unsubscribe();
    }


    this.legends = [];
  }

  /**
   * Récupère les  layers avec des légendes et les mets dans le tableau legends
   *
   */
  private allLayerLegendInArray(rootLayerGroup: LayerGroup): void {

    rootLayerGroup.getLayers().forEach(layer => {
      if (layer.get('class') === 'LayerGroup') {
        this.allLayerLegendInArray(layer as LayerGroup);
      } else if ( ['ImageLayer', 'TileLayer', 'VectorLayer'].indexOf(layer.get('class')) > -1
        && layer.get('legends') instanceof  Array && layer.get('legends').length > 0) {

        // copie des légendes sans référence avec ajout du layer associé
        const tabLegends = layer.get('legends');
        const copyLegends = tabLegends.map(legend => Object.assign({layer}, legend));

        this.legends = this.legends.concat(copyLegends);
      }
    });
  }


}
