import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayerLegendLiteComponent } from './layer-legend-lite.component';

describe('LayerLegendLiteComponent', () => {
  let component: LayerLegendLiteComponent;
  let fixture: ComponentFixture<LayerLegendLiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayerLegendLiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayerLegendLiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
