import {
  Component,
  OnInit,
  ContentChild,
  HostListener,
  Input,
  Host,
  Optional,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MapIdService } from "../../../map/map-id.service";
import { VisualiseurCoreService } from "../../../visualiseur-core.service";

@Component({
  selector: "alk-add-layer",
  templateUrl: "./add-layer.component.html",
  styleUrls: ["./add-layer.component.scss"],
})
export class AddLayerComponent implements OnInit {
  @Input() mapId;
  @ContentChild("modal", {read: null, static: true}) modal: Component;

  constructor(
    public mapIdService: MapIdService,
    public modalService: NgbModal,
    public coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {}

  @HostListener("click", ["$event"])
  click(event: MouseEvent): void {
    this.coreService.setMapTool("");
    if (this.modal) {
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "lg",
        centered: true,
        windowClass: "window-xl",
        container: "#modals",
      });
      if (this.mapId && modalRef.componentInstance.setMapId) {
        modalRef.componentInstance.setMapId(this.mapId || "main");
      }
    }
  }
}
