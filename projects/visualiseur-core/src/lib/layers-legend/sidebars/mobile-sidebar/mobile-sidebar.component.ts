import {
  Component,
  OnInit,
  Input,
  Host,
  Optional,
  OnDestroy,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { Subscription } from "rxjs";
import { VisualiseurCoreService } from "../../../visualiseur-core.service";
import { MapService } from "../../../map/map.service";
import { MapIdService } from "../../../map/map-id.service";

import Map from "ol/Map";
import LayerGroup from "ol/layer/Group";
import { FeatureCollection } from "../../../models/context";
import { LayersLegendComponent } from "../../layers-legend/layers-legend.component";

@Component({
  selector: "alk-mobile-sidebar",
  templateUrl: "./mobile-sidebar.component.html",
  styleUrls: ["./mobile-sidebar.component.scss"],
})
export class MobileSidebarComponent implements OnInit, OnDestroy {
  mapId: string;

  resolutionChange = false;
  isLegendVisible = false;

  @ViewChild(LayersLegendComponent, {read: null, static: true}) collapsedEmit: LayersLegendComponent;
  public isCollapsed = this.coreService.isApplicationInLiteMode ? false : true;

  public sideBarLegendModeSubscription: Subscription;
  public mode:
    | "Couches"
    | "Légende"
    | "Information"
    | "Tools"
    | "Open"
    | "Share";

  contextSubscription: Subscription;
  layout: FeatureCollection["properties"]["extension"]["Layout"];

  public coreServiceSubscription: Subscription;
  public rootLayerGroup: LayerGroup;

  public featureToolsSubscription: Subscription;
  public tools: FeatureCollection["properties"]["extension"]["Tools"];

  constructor(
    public mapService: MapService,
    public mapIdService: MapIdService,
    public coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "main";

    this.sideBarLegendModeSubscription = this.mapService
      .getSideBarLegendMode()
      .subscribe((mode) => {
        this.mode = mode;
      });

    this.contextSubscription = this.coreService.getContext().subscribe(
      (context) => {
        if (!context) {
          return false;
        }
        this.layout = context.properties.extension.Layout;
      },
      (error) => console.error(error)
    );

    this.coreServiceSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup: LayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      });

    this.featureToolsSubscription = this.mapService
      .getFeatureTools()
      .subscribe((newTools) => {
        this.tools = newTools;
      });
  }

  ngOnDestroy() {
    if (this.featureToolsSubscription) {
      this.featureToolsSubscription.unsubscribe();
    }
    if (this.coreServiceSubscription) {
      this.coreServiceSubscription.unsubscribe();
    }
    if (this.sideBarLegendModeSubscription) {
      this.sideBarLegendModeSubscription.unsubscribe();
    }
  }

  openMenu() {
    this.changeMode("Information");
    this.isCollapsed = !this.isCollapsed;
  }

  emittedCollapsed(event) {
    this.isCollapsed = event;
  }

  changeMode(
    mode: "Couches" | "Légende" | "Information" | "Tools" | "Open" | "Share"
  ): void {
    this.mapService.setSideBarLegendMode(mode);
    this.mode = mode;
  }

  verifyLayerGroupVisibility(
    layerGroup: LayerGroup,
    mapId: string
  ): [boolean, boolean, boolean, boolean] {
    return this.mapService.verifyLayerGroupVisibility(layerGroup, mapId);
  }
}
