import {
  Component,
  OnInit,
  Input,
  Host,
  Optional,
  OnDestroy,
  EventEmitter,
  Output,
  ViewChild,
} from "@angular/core";
import { Subscription, Subject } from "rxjs";
import { VisualiseurCoreService } from "../../visualiseur-core.service";
import { MapService } from "../../map/map.service";
import { MapIdService } from "../../map/map-id.service";
import {
  NgbDropdownConfig,
  NgbModal,
  NgbModalRef,
  NgbActiveModal,
} from "@ng-bootstrap/ng-bootstrap";

import Feature from "ol/format/Feature";
import GeoJSON from "ol/format/GeoJSON";
import Map from "ol/Map";
import BaseLayer from "ol/layer/Base";
import VectorLayer from "ol/layer/Vector";
import LayerGroup from "ol/layer/Group";
import { Style } from "ol/style";
import { DragulaService, Group } from "ng2-dragula";
import { StyleDrawsModalComponent } from "../../map/tools/annotating-tools/style-draws/style-draws-modal/style-draws-modal.component";
// tslint:disable-next-line: max-line-length
import { GenerateStyleDrawsModalComponent } from "../../map/tools/annotating-tools/style-draws/style-draws-modal/generate-style-draws-modal/generate-style-draws-modal.component";
import { debounceTime } from "rxjs/operators";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";
import { HttpClient } from "@angular/common/http";
import { InterrogatingBufferModalComponent } from "../../modals/interrogating-buffer-modal/interrogating-buffer-modal.component";
import {
  InterrogatingService,
  LayerQueryFieldType,
} from "../../map/services/interrogating.service";
import { FeatureCollection } from "../../models/context";
import { FavoriteZonesManageService } from "../../services/favorite-zones-manage.service";
import { MeasuringModalComponent } from "../../map/tools/measuring-tools/measuring-modal/measuring-modal.component";
import { LocatingPresetZonesModalComponent } from "../../modals/locating-preset-zones-modal/locating-preset-zones-modal.component";
import { AlertConfirmPromptModalComponent } from "../../modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import { Geometry, MultiPoint } from "ol/geom";

import VectorSource from "ol/source/Vector";
import ImageSource from "ol/source/Image";

@Component({
  selector: "alk-layers-legend",
  templateUrl: "./layers-legend.component.html",
  styleUrls: ["./layers-legend.component.scss"],
})
export class LayersLegendComponent implements OnInit, OnDestroy {
  @Input() newRootLayerGroupEE: EventEmitter<LayerGroup>;
  @Input() screenMode: "mobile" | "desktop" = "desktop";
  mapId: string;

  resolutionChange = false;
  isLegendVisible = this.coreService.isApplicationInLiteMode;
  public isCollapsed = this.coreService.isApplicationInLiteMode ? false : true;

  public sideBarLegendModeSubscription: Subscription;
  public mode:
    | "Couches"
    | "Légende"
    | "Information"
    | "Tools"
    | "Open"
    | "Share";

  drawLayerSubscription: Subscription;
  bufferLayerGroupSubscription: Subscription;
  interrogationSubscription: Subscription;
  contextSubscription: Subscription;
  title: string;
  resume: string;
  helpingContent: string;
  toolsMenu: FeatureCollection["properties"]["extension"]["Tools"];

  favoriteZonesSubscription: Subscription;
  favoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> = [];

  measuringModal: NgbModalRef;

  @Output() emitter: EventEmitter<boolean> = new EventEmitter();
  collapsedEmit = true;

  public filterValue$: Subject<string> = new Subject();
  public filterValue = "";
  filterValueSubscription: Subscription;

  map: Map;

  subs = new Subscription();
  dragulaLayersGroup: Group;
  isDragEnabled = true;
  dragging = false;

  rootLayerGroup: LayerGroup;
  public tools: FeatureCollection["properties"]["extension"]["Tools"];
  public featureToolsSubscription: Subscription;

  @ViewChild(AlertConfirmPromptModalComponent, {read: null, static: true})
  confirmModal: AlertConfirmPromptModalComponent;

  constructor(
    public mapService: MapService,
    public coreService: VisualiseurCoreService,
    public http: HttpClient,
    public modalService: NgbModal,
    public config: NgbDropdownConfig,
    public dragulaService: DragulaService,
    public interrogatingService: InterrogatingService,
    public mapIdService: MapIdService,
    public favoriteZonesManage: FavoriteZonesManageService,
    public favoriteZonesManageService: FavoriteZonesManageService,
    public activeModal: NgbActiveModal
  ) {
    if (this.config) {
      // customize default values of dropdowns used by this component tree
      this.config.container = "body";
      // config.display = 'dynamic';
      this.config.placement = "bottom-left bottom-right top-left top-right";
      this.config.autoClose = true;
    }
  }

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;
    });

    this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup: LayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      });

    this.contextSubscription = this.coreService
      .getContext()
      .subscribe((context) => {
        if (!context) {
          return false;
        }
        this.title = context.properties.title;
        this.resume = context.properties.subtitle;
        this.toolsMenu = context.properties.extension.Tools;
        this.helpingContent = context.properties.extension.MapDescription;
        this.helpingContent = String(this.helpingContent)
          .replace(/&quot;/g, '"')
          .replace(/&#39;/g, "'")
          .replace(/&lt;/g, "<")
          .replace(/&gt;/g, ">")
          .replace(/&amp;/g, "&");
      });

    this.favoriteZonesSubscription = this.favoriteZonesManage
      .getSessionFavoriteZones()
      .subscribe((sessionFZ) => {
        if (sessionFZ) {
          this.favoriteZones = sessionFZ;
        }
      });

    this.sideBarLegendModeSubscription = this.mapService
      .getSideBarLegendMode()
      .subscribe((mode) => {
        this.mode = mode;
      });

    // On initialise une surveillance avec tempo sur le champs de filtre
    this.filterValue = "";
    this.filterValueSubscription = this.filterValue$
      .pipe(debounceTime(300))
      .subscribe((searchTextValue) => {
        if (this.filterValue !== searchTextValue && this.rootLayerGroup) {
          this.filterValue = searchTextValue;
          this.changeLayerFilterVisibility(this.rootLayerGroup, 0);
        }
      });

    if (this.dragulaService.find("BaseLayers_desktop")) {
      this.dragulaLayersGroup = this.dragulaService.createGroup(
        "BaseLayers_mobile",
        {
          moves: (
            el: Element,
            container: Element,
            handle: Element,
            sibling: Element
          ) => {
            return this.isDragEnabled;
          },
          direction: "vertical",
          mirrorContainer: document.body,
          revertOnSpill: true,
          removeOnSpill: false,
        }
      );
    } else {
      this.dragulaLayersGroup = this.dragulaService.createGroup(
        "BaseLayers_desktop",
        {
          moves: (
            el: Element,
            container: Element,
            handle: Element,
            sibling: Element
          ) => {
            return this.isDragEnabled;
          },
          direction: "vertical",
          mirrorContainer: document.body,
          revertOnSpill: true,
          removeOnSpill: false,
        }
      );
    }

    this.subs.add(
      this.dragulaService.drag().subscribe(({ name, el, source }) => {
        this.dragging = true;
      })
    );
    this.subs.add(
      this.dragulaService.dragend().subscribe(({ name, el }) => {
        this.dragging = false;
      })
    );

    // You can also get all events, not limited to a particular group
    this.subs.add(
      this.dragulaService
        .drop()
        .subscribe(({ name, el, target, source, sibling }) => {
          this.dragging = false;
        })
    );

    this.featureToolsSubscription = this.mapService
      .getFeatureTools()
      .subscribe((newTools) => {
        this.tools = newTools;
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    if (this.subs) {
      this.subs.unsubscribe();
    }
    if (this.favoriteZonesSubscription) {
      this.favoriteZonesSubscription.unsubscribe();
    }
    if (this.bufferLayerGroupSubscription) {
      this.bufferLayerGroupSubscription.unsubscribe();
    }
    if (this.interrogationSubscription) {
      this.interrogationSubscription.unsubscribe();
    }
    if (this.drawLayerSubscription) {
      this.drawLayerSubscription.unsubscribe();
    }
    if (this.filterValueSubscription) {
      this.filterValueSubscription.unsubscribe();
    }
    if (this.sideBarLegendModeSubscription) {
      this.sideBarLegendModeSubscription.unsubscribe();
    }
    if (this.featureToolsSubscription) {
      this.featureToolsSubscription.unsubscribe();
    }

    this.dragulaService.destroy("BaseLayers_desktop");
    this.dragulaService.destroy("BaseLayers_mobile");
  }

  openPresetZonesTool() {
    this.emitter.emit(this.collapsedEmit);
    this.modalService.dismissAll();
    const modalRef = this.modalService.open(LocatingPresetZonesModalComponent, {
      size: "sm",
      windowClass: "window-tool",
      container: "#visualiseur",
    });
    if (modalRef.componentInstance.setMapId) {
      modalRef.componentInstance.setMapId(this.mapIdService.getId() || "main");
      modalRef.componentInstance.setScreenMode("mobile");
    }
  }

  addZone(zoneName: string) {
    /* this.map = this.mapService.getMap(this.mapIdService.getId()); */

    const viewport = this.map.getView().calculateExtent(this.map.getSize());
    const minLong = viewport[0];
    const minLat = viewport[1];
    const maxLong = viewport[2];
    const maxLat = viewport[3];

    this.favoriteZonesManageService.addZone(zoneName, false, {
      minLongitude: minLong,
      minLatitude: minLat,
      maxLongitude: maxLong,
      maxLatitude: maxLat,
    });
  }

  openMeasuringTool(measuringOption: string) {
    this.emitter.emit(this.collapsedEmit);
    if (!this.measuringModal) {
      this.coreService.setMapTool("mesurer");
      this.modalService.dismissAll();
      this.measuringModal = this.modalService.open(MeasuringModalComponent, {
        size: "sm",
        windowClass: "window-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });

      this.measuringModal.result.then((close) => {
        this.measuringModal = null;
        this.coreService.setMapTool("");
      });
    }
    this.coreService.setMeasuringOption(measuringOption);
  }

  changeOpacity(
    layer: VectorLayer<VectorSource<Geometry>>,
    opacity: number
  ): void {
    layer.setOpacity(opacity);
    layer.changed();
  }

  deleteLayer(
    baseLayer: LayerGroup | VectorLayer<VectorSource<Geometry>>,
    parentLayerGroup: LayerGroup,
    mapId: string
  ): void {
    this.confirmModal.openModal().then((result) => {
      if (result) {
        (baseLayer as VectorLayer<VectorSource<Geometry>>).setMap(null);
        baseLayer.changed();
        // this.map[mapId].changed();
        parentLayerGroup.getLayers().remove(baseLayer);
        parentLayerGroup.changed();
        // this.activeModal.close('save');
      }
    });
  }

  updateLayers(
    layers: Array<LayerGroup | VectorLayer<VectorSource<Geometry>>>,
    parentLayerGroup: LayerGroup,
    mapId: string
  ): void {
    const layerCollection = parentLayerGroup.getLayers();
    layerCollection.clear();
    layers.forEach((layer, idx) => {
      layerCollection.insertAt(idx, layer);
    });

    const zIndex = { value: 9999 };
    this.reassignLayerZIndex(this.rootLayerGroup, zIndex);
    this.verifyLayerGroupVisibility(this.rootLayerGroup, mapId);
  }

  applyLayerGroupVisibility(
    layerGroup: LayerGroup,
    visibilityWanted: boolean,
    mapId: string
  ): void {
    layerGroup
      .getLayers()
      .getArray()
      .forEach(
        (baseLayer: LayerGroup | VectorLayer<VectorSource<Geometry>>) => {
          if (baseLayer instanceof LayerGroup) {
            return this.applyLayerGroupVisibility(
              baseLayer,
              visibilityWanted,
              mapId
            );
          } else if (!(baseLayer.get("baseLayer") === true)) {
            baseLayer.setVisible(visibilityWanted);
            baseLayer.set("visibleChecked", visibilityWanted);
            baseLayer.changed();
          }
        }
      );
    // layerGroup.set('visible', visibilityWanted );
    layerGroup.set("visibleChecked", visibilityWanted);
    layerGroup.set("allChildrenHaveSameVisibility", true);
  }

  reassignLayerZIndex(layerGroup: LayerGroup, zIndex): void {
    // console.groupCollapsed( layerGroup.get('name') );
    layerGroup
      .getLayers()
      .getArray()
      .forEach(
        (
          baseLayer: LayerGroup | VectorLayer<VectorSource<Geometry>>,
          idx,
          tab
        ) => {
          if (baseLayer instanceof LayerGroup) {
            this.reassignLayerZIndex(baseLayer, zIndex);
          } else {
            if (!!baseLayer.get("baseLayer")) {
              baseLayer.setZIndex(0);
            } else {
              baseLayer.setZIndex(zIndex.value--);
            }
            baseLayer.changed();
          }
        }
      );
  }

  toggleBaseLayerVisibility(
    eventTarget: HTMLInputElement,
    baseLayer: BaseLayer,
    mapId: string
  ): void {
    const checkbox = eventTarget as HTMLInputElement;

    if (baseLayer instanceof LayerGroup) {
      // Il faut récupérer la checkbox liée à l'état de visibilité du groupe
      baseLayer.set("visibleChecked", checkbox.checked);
      // baseLayer.set('visible', checkbox.checked );
      this.applyLayerGroupVisibility(baseLayer, checkbox.checked, mapId);
    } else {
      baseLayer.set("visibleChecked", checkbox.checked);
      // baseLayer.set('visible', checkbox.checked );
      baseLayer.setVisible(checkbox.checked);
      baseLayer.changed();
    }
    this.verifyLayerGroupVisibility(this.rootLayerGroup, mapId);
  }

  verifyLayerGroupVisibility(
    layerGroup: LayerGroup,
    mapId: string
  ): [boolean, boolean, boolean, boolean] {
    return this.mapService.verifyLayerGroupVisibility(layerGroup, mapId);
  }

  changeLayerFilterVisibility(layerGroup: LayerGroup, zIndex): number {
    let nbFilteredVisible = 0;
    layerGroup
      .getLayers()
      .getArray()
      .forEach(
        (
          baseLayer: LayerGroup | VectorLayer<VectorSource<Geometry>>,
          idx,
          tab
        ) => {
          if (baseLayer instanceof LayerGroup) {
            let nbSubFilteredVisible = this.changeLayerFilterVisibility(
              baseLayer,
              zIndex
            );
            if (
              this.filterValue &&
              (baseLayer.get("name") as string)
                .toLocaleLowerCase()
                .includes(this.filterValue.toLocaleLowerCase())
            ) {
              nbSubFilteredVisible++;
            }
            baseLayer.set("filterVisibility", !!nbSubFilteredVisible);
            nbFilteredVisible += nbSubFilteredVisible;
          } else if (
            baseLayer instanceof VectorLayer ||
            (baseLayer as any) instanceof ImageLayer ||
            (baseLayer as any) instanceof TileLayer
          ) {
            const filterVisible =
              !this.filterValue ||
              (baseLayer.get("name") as string)
                .toLocaleLowerCase()
                .includes(this.filterValue.toLocaleLowerCase()) ||
              (!!baseLayer.get("legends") &&
                (baseLayer.get("legends") as Array<any>).some((legend) => {
                  return (legend.label as string)
                    .toLocaleLowerCase()
                    .includes(this.filterValue.toLocaleLowerCase());
                }));
            baseLayer.set("filterVisibility", filterVisible);
            nbFilteredVisible +=
              filterVisible && !(baseLayer.get("baseLayer") === true) ? 1 : 0;
          }
        }
      );
    return nbFilteredVisible;
  }

  filterLayer($event: KeyboardEvent, element: HTMLInputElement): void {
    this.filterValue$.next(element.value);
  }

  loadLayerData(
    baseLayer: ImageLayer<ImageSource>,
    parentLayerGroup: LayerGroup
  ): void {
    this.isCollapsed = !this.isCollapsed;
    this.interrogatingService.setResultSearching(true);
    const layerQueryUrl = (baseLayer.getSource() as any).getUrl();
    const body = `<?xml version="1.0" encoding="UTF-8"?>
      <GetFeature service="WFS" version="2.0.0" outputformat="geojson" xmlns="http://www.opengis.net/wfs/2.0" xmlns:cw="http://www.someserver.com/cw" xmlns:fes="http://www.opengis.net/ogc/1.1" xmlns:gml="http://www.opengis.net/gml/3.2">
        <Query typeNames="${baseLayer.get(
          "layerName"
        )}" styles="default"></Query>
      </GetFeature>`;
    this.http.post(layerQueryUrl, body).subscribe((response) => {
      let totalResults = 0;
      let layerResults = 0;
      // Tableau dans lequel on stockera les données récupèrées par la requête
      const layerData = [];
      // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
      const layerQueryFields = baseLayer.get("LayerQuery")
        .Fields as Array<LayerQueryFieldType>;
      // On extrait de la liste des colonnes précédentes les clés
      const layerQueryFieldsNames = layerQueryFields.map(
        (layerQueryField) => layerQueryField.FieldName
      );

      // On parcours enfin les réponse pour ne garder que les données qui nous intéresse
      (response as any).features.forEach((featureInResponse) => {
        totalResults++;
        layerResults++;
        const datas = Object.entries(
          featureInResponse.properties as { [key: string]: any }
        )
          .filter(([key, value]) => layerQueryFieldsNames.includes(key))
          .reduce((accum, [key, value]) => {
            accum[key] = value;
            return accum;
          }, {});

        layerData.push(
          Object.assign({}, datas, {
            feature: featureInResponse,
          })
        );
      });

      const validResults = {
        layerName: baseLayer.get("name"),
        layer: baseLayer,
        layerQueryFields,
        data: layerData,
        nbResults: layerResults,
      };

      const resultOfInterrogation = {
        featureOfRequest: null,
        layersResults: [validResults],
        totalResults,
      };

      this.interrogatingService.setResultOfInterrogation(resultOfInterrogation);

      this.interrogatingService.setResultSearching(false);
      this.interrogatingService.changeResultOfInterrogation();
      if (totalResults > 0) {
        this.interrogatingService.openResultCurrentBox();
      }
    });
  }

  selectFromBuffer(
    baseLayer: VectorLayer<VectorSource<Geometry>>,
    parentLayerGroup: LayerGroup
  ): void {
    const modalRef = this.modalService.open(InterrogatingBufferModalComponent, {
      size: "lg",
      centered: true,
      windowClass: "window-xl",
      container: "#modals",
      keyboard: false,
    });
    this.map = this.mapService.getMap(this.mapIdService.getId());
    modalRef.componentInstance.setMode("buffers");
    modalRef.componentInstance.setMapProjection(
      this.map.getView().getProjection()
    );
    modalRef.componentInstance.setSelectedFeaturesOrLayer(baseLayer);
    modalRef.componentInstance.setLayers(this.rootLayerGroup);
  }

  changeStyleLayer(
    baseLayer: VectorLayer<VectorSource<Geometry>>,
    parentLayerGroup: LayerGroup
  ): void {
    if (baseLayer.get("isDrawLayer")) {
      const modalRef = this.modalService.open(StyleDrawsModalComponent, {
        size: "lg",
        centered: true,
        windowClass: "window-map-tool",
        container: "#modals",
        keyboard: false,
      });
    } else {
      const modalRef = this.modalService.open(
        GenerateStyleDrawsModalComponent,
        {
          size: "lg",
          centered: true,
          windowClass: "window-map-tool",
          container: "#modals",
          keyboard: false,
        }
      );
      if (!baseLayer.get("isDrawLayer")) {
        if (baseLayer.getSource().getFeatures().length <= 1) {
          if (
            baseLayer.getSource().getFeatures()[0].getGeometry().getType() ===
            "MultiPoint"
          ) {
            const features = [];
            (baseLayer.getSource().getFeatures()[0].getGeometry() as MultiPoint)
              .getCoordinates()
              .forEach((point, index) => {
                const test = {
                  type: "Feature",
                  geometry: {
                    type: baseLayer
                      .getSource()
                      .getFeatures()[0]
                      .getGeometry()
                      .getType(),
                    coordinates: point,
                  },
                };
                features.push(new GeoJSON().readFeature(test));
                features[index].setStyle(
                  baseLayer.getStyle() ? baseLayer.getStyle() : new Style()
                );
              });
            modalRef.componentInstance.setSelectedFeatures(features);
            modalRef.componentInstance.setVectorLayer(baseLayer);
          } else {
            modalRef.componentInstance.setSelectedFeature(
              baseLayer.getSource().getFeatures()[0]
            );
            modalRef.componentInstance.geometryType = "Polygon";
          }
        } else {
          modalRef.componentInstance.setSelectedFeatures(
            baseLayer.getSource().getFeatures()
          );
          modalRef.componentInstance.setVectorLayer(baseLayer);
        }
      }
    }
  }
}
