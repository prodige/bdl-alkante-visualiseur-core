import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayersLegendComponent } from './layers-legend.component';

describe('LayersLegendComponent', () => {
  let component: LayersLegendComponent;
  let fixture: ComponentFixture<LayersLegendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayersLegendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayersLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
