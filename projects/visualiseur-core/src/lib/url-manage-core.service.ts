import './map/extensions/ol.extensions';

import {  HttpErrorResponse, HttpClient } from '@angular/common/http';
import { FeatureCollection } from './models/context';
import { Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { EnvCoreService } from './env.core.service';
import { VisualiseurCoreService } from './visualiseur-core.service';

// Observable RxJS
import { mergeMap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UrlManageCoreService implements Resolve<FeatureCollection> {

  private countRedirection = 0;
  
  constructor(
    private router: Router,
    private http: HttpClient,
    private environment: EnvCoreService,
    private coreService: VisualiseurCoreService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FeatureCollection> {
    const segments = route.url.slice();
    const { length } = segments;
    if ( length >= 2 ) {
        const account = segments.shift().toString();
        const contextPath = segments.join('/');
        
        this.environment.mapName = contextPath;
        
        const queryString = (
          Object.entries(route.queryParams).length
          ? '&'
          : ''
        ) +
        Object.entries(route.queryParams).reduce(( tabQuery, [key, val] ) => {
          // avoid problems while failing CAS ticket
          if (key !== 'ticket') {
            tabQuery.push(`${encodeURIComponent(key)}=${encodeURIComponent(val)}`);
          }
          return tabQuery;
        }, []).join('&');

        return this.http.jsonp(
          `${this.environment.serverUrl}carto/context?account=${account}&contextPath=/${contextPath}${ queryString }`,
          'callback'
        ).pipe(
          mergeMap( (context: FeatureCollection) => {
            if (context.code && context.success === false && context.code === 403) {
              this.router.navigate([`/403`]);
              return of(null);
            } else if (context.code && context.success === false && context.code === 401) {
              this.router.navigate([`/401`], {state: {oldUrl: state.url}});
              return of(null);
            }
            this.coreService.setContext(context);
            return of(context) ;
          }),
          catchError((err: HttpErrorResponse) => {
            if ( this.countRedirection > 2 ) {
              return;
            }
            console.count('Redirection');
            this.countRedirection++;

            if (err.status === 403) {
              this.router.navigate([`/403`]);
            } else if (err.status === 401) {
              this.router.navigate([`/401`]);
            } else {
              this.router.navigate([`/404`]);
            }
            console.error(err);
            return of(null);
          })
        );
    } else if ( length === 0 ) {
      if ( this.countRedirection > 2 ) {
        return;
      }
      console.count('Redirection');
      this.countRedirection++;
      this.router.navigateByUrl('404/NotFound');
    } else {
      return null;
    }
  }

}
