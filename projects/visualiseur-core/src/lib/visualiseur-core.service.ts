import "./map/extensions/ol.extensions";
import { EnvCoreService } from "./env.core.service";
import { MousePositionProjectionService } from "./map/mouse-position-projection.service";

import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { FeatureCollection } from "./models/context";
import { Injectable, Inject } from "@angular/core";
import {ReplaySubject, Observable, of, BehaviorSubject} from 'rxjs';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Router,
} from "@angular/router";

// Observable RxJS
import { mergeMap, catchError } from "rxjs/operators";

// MAP Openlayers
import BaseLayer from "ol/layer/Base";
import LayerGroup from "ol/layer/Group";

// TileLayers & Cie
import TileLayer from "ol/layer/Tile";
import { Options as TileLayerOptions } from "ol/layer/BaseTile";
import BingMaps from "ol/source/BingMaps";
import { Options as BingMapsOptions } from "ol/source/BingMaps";
import TileArcGISRest from "ol/source/TileArcGISRest";
import { Options as TileArcGISRestOptions } from "ol/source/TileArcGISRest";
import TileDebug from "ol/source/TileDebug";
import { Options as TileDebugOptions } from "ol/source/TileDebug";
import TileJSON from "ol/source/TileJSON";
import { Options as TileJSONOptions } from "ol/source/TileJSON";
import TileWMS from "ol/source/TileWMS";
import { Options as TileWMSOptions } from "ol/source/TileWMS";
import UTFGrid from "ol/source/UTFGrid";
import { Options as UTFGridOptions } from "ol/source/UTFGrid";
import VectorTile from "ol/source/VectorTile";
import { Options as VectorTileOptions } from "ol/source/VectorTile";
import WMTS from "ol/source/WMTS";
import { Options as WMTSOptions } from "ol/source/WMTS";
import XYZ from "ol/source/XYZ";
import { Options as XYZOptions } from "ol/source/XYZ";
import CartoDB from "ol/source/CartoDB";
import { Options as CartoDBOptions } from "ol/source/CartoDB";
import OSM from "ol/source/OSM";
import { Options as OSMOptions } from "ol/source/OSM";
import Stamen from "ol/source/Stamen";
import { Options as StamenOptions } from "ol/source/Stamen";
import Zoomify from "ol/source/Zoomify";
import { Options as ZoomifyOptions } from "ol/source/Zoomify";

// ImageLayers & Cie
import ImageLayer from "ol/layer/Image";

import ImageArcGISRest from "ol/source/ImageArcGISRest";
import { Options as ImageArcGISRestOptions } from "ol/source/ImageArcGISRest";
import ImageCanvasSource from "ol/source/ImageCanvas";
import { Options as ImageCanvasSourceOptions } from "ol/source/ImageCanvas";
import ImageMapGuide from "ol/source/ImageMapGuide";
import { Options as ImageMapGuideOptions } from "ol/source/ImageMapGuide";
import Static from "ol/source/ImageStatic";
import { Options as StaticOptions } from "ol/source/ImageStatic";
import ImageWMS from "ol/source/ImageWMS";
import { Options as ImageWMSOptions } from "ol/source/ImageWMS";
import RasterSource from "ol/source/Raster";
import { Options as RasterSourceOptions } from "ol/source/Raster";

// VectorLayers & Cie
import VectorLayer from "ol/layer/Vector";
import { Options as VectorLayerOptions } from "ol/layer/BaseVector";
import AnimatedCluster from "ol-ext/layer/AnimatedCluster";
import VectorSource from "ol/source/Vector";
import { Options as VectorSourceOptions } from "ol/source/Vector";
import { Options as SourceOptions } from "ol/source/Source";
import Cluster from "ol/source/Cluster";

import WMTSTileGrid from "ol/tilegrid/WMTS";
import Projection from "ol/proj/Projection";
import TileGrid from "ol/tilegrid/TileGrid";

import { bbox as bboxStrategy } from "ol/loadingstrategy";
import GeoJSON from "ol/format/GeoJSON";
import WFS from "ol/format/WFS";
import GML2 from "ol/format/GML2";
import GML3 from "ol/format/GML3";
import { UrlProxyService } from "./services/url-proxy.service";
import Collection from "ol/Collection";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { Style, Fill, Stroke, Icon, Text } from "ol/style";
import FontSymbol from "ol-ext/style/FontSymbol";
import FillPattern from "ol-ext/style/FillPattern";
import StrokePattern from "ol-ext/style/StrokePattern";

import proj4 from "proj4";
import { register } from "ol/proj/proj4";
import { get as getProjection, ProjectionLike } from "ol/proj";

import {
  ClusterService,
  ClusterStyleOption,
} from "../lib/services/cluster.service";

import MapboxVectorLayer from "ol/layer/MapboxVector";
import { Geometry } from "ol/geom";
import { Layer } from "ol/layer";

interface BaseLayerConfig {
  id: string;
  class: string; // 'ImageLayer' | 'TileLayer';;
  isDrawLayer?: boolean;
  isAddTextLayer?: boolean;
  options: {
    opacity: number;
    visible: boolean;
    source: {
      [param: string]: any;
    };
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}
interface LayerGroupConfig {
  class: "LayerGroup";
  isDrawLayer?: boolean;
  layers: Array<BaseLayerConfig | LayerGroupConfig>;
  options: {
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}

@Injectable({
  providedIn: "root",
})
export class VisualiseurCoreService implements Resolve<FeatureCollection> {
  public isApplicationInLiteMode = false;

  private context: ReplaySubject<FeatureCollection> =
    new ReplaySubject<FeatureCollection>(1);

  /** copie sans référence */
  private contextCopy$ = new BehaviorSubject<FeatureCollection>(null);

  private mapTool: ReplaySubject<string> = new ReplaySubject<string>(1);
  // tslint:disable-next-line:max-line-length
  private favoriteZones: ReplaySubject<
    Array<FeatureCollection["GeobookmarkConfig"]>
  > = new ReplaySubject<Array<FeatureCollection["GeobookmarkConfig"]>>(1);
  private measuringOption: ReplaySubject<string> = new ReplaySubject<string>(1);
  private extractingOption: ReplaySubject<string> = new ReplaySubject<string>(
    1
  );
  private annotatingOption: ReplaySubject<string> = new ReplaySubject<string>(
    1
  );
  private annotatingSnapping: ReplaySubject<boolean> =
    new ReplaySubject<boolean>(1);
  private interrogatingOption: ReplaySubject<"mobile" | "desktop"> =
    new ReplaySubject<"mobile" | "desktop">(1);

  private nextLayerIdx: number;
  private nextLayerIdx$: ReplaySubject<number> = new ReplaySubject<number>(1);
  private updatedRootLayerGroup: ReplaySubject<boolean> =
    new ReplaySubject<boolean>(1);
  private rootLayerGroup: ReplaySubject<LayerGroup> =
    new ReplaySubject<LayerGroup>(1);
  private baseLayers: ReplaySubject<Array<BaseLayer>> = new ReplaySubject<
    Array<BaseLayer>
  >(1);
  private mapProjection: Projection = null;
  private mapProjection$: ReplaySubject<Projection> =
    new ReplaySubject<Projection>(1);

  private isContextLoading = false;
  private startZIndex = 9999;
  private startAddLayerZIndex = 10000;

  private countRedirection = 0;
  private contextExtensionInfo: any = null;

  httpOptions = {
    withCredentials: true,
  };

  private cguRead: boolean = false;

  constructor(
    private router: Router,
    private urlProxyService: UrlProxyService,
    private http: HttpClient,
    private mousePositionProjectionService: MousePositionProjectionService,
    private environment: EnvCoreService,
    private clusterService: ClusterService
  ) {
    this.annotatingSnapping.next(false);
    interface ProjectionDef {
      key: string;
      label: string;
      proj: string;
      bounds: [number, number, number, number];
    }
    // On enregistre les différentes projections auprès de proj4
    Object.values(this.environment.availableProjections).forEach(
      (projection: ProjectionDef) => {
        proj4.defs(projection.key, projection.proj);
      }
    );

    // On fourni proj4 mis à jour avec les projections défini dans l'environnement de l'application à openlayers
    register(proj4);

    // On défini pour chacune des projections leurs bounds
    Object.values(environment.availableProjections).forEach(
      (projection: ProjectionDef) => {
        getProjection(projection.key).setExtent(projection.bounds);
      }
    );

    // ! - Ne pas supprimer, important pour le chargement de la carte.
    this.mousePositionProjectionService
      .get()
      .toPromise()
      .then((projection: ProjectionLike) => {})
      .catch((errorGetProjection) => {});
    // Le service s'auto abonne au mise à jour du contexte pour pouvoir mettre à jours les variables qui en découlent
    this.getContext().subscribe((context) => {
      if (!context) {
        return false;
      }

      this.isContextLoading = true;
      this.contextExtensionInfo = context.properties.extension;
      const regexProj = new RegExp(/^urn:ogc:def:crs:(.*)$/i);
      const contextMapProjection = regexProj.exec(context.crs.properties.name);
      this.setMapProjection(getProjection(contextMapProjection[1]));

      if (
        context &&
        context.properties &&
        context.properties.extension &&
        context.properties.extension.layers
      ) {
        const generatedLayersFromContext = this.generateLayersAndLayerGroups(
          context.properties.extension.layers
        );
        this.rootLayerGroup.next(generatedLayersFromContext as LayerGroup);
      } else {
        this.rootLayerGroup.next(null);
      }

      this.isContextLoading = false;
    });

    this.getRootLayerGroup().subscribe((rootLayerGroup: LayerGroup) => {
      this.baseLayers.next(
        this.extractBaseLayersFromLayerGroup(rootLayerGroup)
      );
    });
    this.measuringOption.next("length");
    this.extractingOption.next("rect");
    this.mapTool.next("");

    this.setNextLayerIdx(0);
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<FeatureCollection> {
    const segments = route.url.slice();
    const { length } = segments;
    if (length >= 2) {
      const account = segments.shift().toString();
      const contextPath = segments.join("/");

      const queryString =
        (Object.entries(route.queryParams).length ? "&" : "") +
        Object.entries(route.queryParams)
          .reduce((tabQuery, [key, val]) => {
            // avoid problems while failing CAS ticket
            if (key !== "ticket") {
              tabQuery.push(
                `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
              );
            }
            return tabQuery;
          }, [])
          .join("&");

      return this.http
        .jsonp(
          `${this.environment.serverUrl}carto/context?account=${account}&contextPath=/${contextPath}${queryString}`,
          "callback"
        )
        .pipe(
          mergeMap((context: FeatureCollection) => {
            if (
              context.code &&
              context.success === false &&
              context.code === 403
            ) {
              this.router.navigate([`/403`]);
              return of(null);
            } else if (
              context.code &&
              context.success === false &&
              context.code === 401
            ) {
              this.router.navigate([`/401`], { state: { oldUrl: state.url } });
              return of(null);
            }
            this.setContext(context);
            return of(context);
          }),
          catchError((err: HttpErrorResponse) => {
            if (this.countRedirection > 2) {
              return;
            }
            console.count("Redirection");
            this.countRedirection++;

            if (err.status === 403) {
              // this.router.navigate([`/403/${account}/${contextPath}`]);
              this.router.navigate([`/403`]);
            } else if (err.status === 401) {
              // this.router.navigate([`/401/${account}/${contextPath}`]);
              this.router.navigate([`/401`]);
            } else {
              // this.router.navigate([`/404/${account}/${contextPath}`]);
              this.router.navigate([`/404`]);
            }
            console.error(err);
            return of(null);
          })
        );
    } else if (length === 0) {
      if (this.countRedirection > 2) {
        return;
      }
      console.count("Redirection");
      this.countRedirection++;
      this.router.navigateByUrl("404/NotFound");
    } else {
      return null;
    }
  }

  getClusterService() {
    return this.clusterService;
  }

  getContext(): Observable<FeatureCollection> {
    return this.context;
  }

  setContext(context: FeatureCollection): void {
    this.context.next(context);
    this.contextCopy$.next(Object.assign({}, context));
  }

  /**
   * @return une copie du context sans référence
   */
  getContextCopy$(): Observable<FeatureCollection>{
    return this.contextCopy$;
  }

  getMapTool(): Observable<string> {
    return this.mapTool;
  }

  setMapTool(tool: string): void {
    this.mapTool.next(tool);
  }

  getFavoriteZones(): Observable<
    Array<FeatureCollection["GeobookmarkConfig"]>
  > {
    return this.favoriteZones;
  }

  setFavoriteZones(fav: Array<FeatureCollection["GeobookmarkConfig"]>) {
    this.favoriteZones.next(fav);
  }

  getMeasuringOption(): Observable<string> {
    return this.measuringOption;
  }

  setMeasuringOption(opt: string): void {
    this.measuringOption.next(opt);
  }

  getExtractingOption(): Observable<string> {
    return this.extractingOption;
  }

  setExtractingOption(opt: string): void {
    this.extractingOption.next(opt);
  }

  getInterrogatingOption(): Observable<"mobile" | "desktop"> {
    return this.interrogatingOption;
  }

  setInterrogatingOption(opt: "mobile" | "desktop"): void {
    this.interrogatingOption.next(opt);
  }

  getAnnotatingSnapping(): Observable<boolean> {
    return this.annotatingSnapping;
  }

  setAnnotatingSnapping(snap: boolean): void {
    this.annotatingSnapping.next(snap);
  }

  getAnnotatingOption(): Observable<string> {
    return this.annotatingOption;
  }

  setAnnotatingOption(opt: string): void {
    this.annotatingOption.next(opt);
  }

  getRootLayerGroup(): Observable<LayerGroup> {
    return this.rootLayerGroup;
  }

  getUpdatedRootLayerGroup(): Observable<boolean> {
    return this.updatedRootLayerGroup;
  }

  updateRootLayerGroup(): void {
    this.updatedRootLayerGroup.next(true);
  }

  setRootLayerGroup(layerGroup: LayerGroup): void {
    this.rootLayerGroup.next(layerGroup);
  }

  getBaseLayers(): Observable<Array<BaseLayer>> {
    return this.baseLayers;
  }

  getMapProjection(): Observable<Projection> {
    return this.mapProjection$;
  }

  setMapProjection(mapProjection: Projection): void {
    this.mapProjection = mapProjection;
    this.mapProjection$.next(mapProjection);
  }

  setNextLayerIdx(nb: number): void {
    this.nextLayerIdx = nb;
    this.nextLayerIdx$.next(nb);
  }

  getNextLayerIdx(): Observable<number> {
    return this.nextLayerIdx$;
  }

  getCguRead(): boolean {
    return this.cguRead;
  }

  setCguRead(readOk: boolean) {
    this.cguRead = readOk;
  }

  getDownloadableRootLayerGroup(rootLayerGroup: LayerGroup): LayerGroup {
    const newRootLayerGroup = new LayerGroup({
      layers: null,
    });
    const collection: Collection<BaseLayer> = new Collection<BaseLayer>();

    rootLayerGroup
      .getLayers()
      .getArray()
      .forEach((rootLayer) => {
        if (rootLayer.get("class") === "LayerGroup") {
          let layerDownloaded = this.getDownloadableRootLayerGroup(
            new LayerGroup({ layers: (rootLayer as LayerGroup).getLayers() })
          );
          if (layerDownloaded.getLayers().getArray().length !== 0) {
            layerDownloaded
              .getLayers()
              .getArray()
              .forEach((layer) => {
                collection.push(layer);
              });
          }
        } else if (rootLayer.get("LayerDownload")) {
          collection.push(rootLayer);
        }
      });

    if (collection) {
      newRootLayerGroup.setLayers(collection);
    }

    return newRootLayerGroup;
  }

  private extractBaseLayersFromLayerGroup: (
    layerGroup: LayerGroup
  ) => Array<BaseLayer> = (layerGroup) => {
    const baseLayers = [];
    if (layerGroup) {
      layerGroup.getLayers().forEach((subLayer: BaseLayer) => {
        if (subLayer instanceof LayerGroup) {
          baseLayers.push(...this.extractBaseLayersFromLayerGroup(subLayer));
        } else if (subLayer.get("baseLayer")) {
          baseLayers.push(subLayer);
        }
      });
    }
    return baseLayers;
  };

  private extractBaseLayersFromContext: (
    subContext: any,
    onlyBaseLayers?: false | null | true
  ) => Array<any> = (subContext, onlyBaseLayers = null) => {
    const baseLayers = [];
    if (subContext) {
      if (
        subContext.layers &&
        subContext.layers instanceof Array &&
        subContext.layers.length
      ) {
        subContext.layers.forEach((subLayer) => {
          const subLayers = this.extractBaseLayersFromContext(
            subLayer,
            onlyBaseLayers
          );
          baseLayers.push(...subLayers);
        });
      } else if (
        (onlyBaseLayers &&
          subContext.extension &&
          subContext.extension.baseLayer) ||
        (!onlyBaseLayers &&
          (!subContext.extension || !subContext.extension.baseLayer))
      ) {
        baseLayers.push(subContext);
      }
    }
    return baseLayers;
  };

  // tslint:disable-next-line:max-line-length
  contextLayersAndLayerGroups(
    rootLayers: Collection<BaseLayer>
  ):
    | BaseLayerConfig
    | LayerGroupConfig
    | Array<BaseLayerConfig | LayerGroupConfig> {
    const newLayerConfig = [];
    if (rootLayers && rootLayers.getArray()) {
      if (rootLayers.getArray() instanceof Array) {
        rootLayers.getArray().forEach((rootLayer: BaseLayer) => {
          const rootLayerVal = rootLayer.getProperties()
            ? rootLayer.getProperties()
            : "";

          if (rootLayerVal && rootLayer["getProdigeType"]) {
            const className = rootLayer["getProdigeType"]();
            const layerConfig = {
              class: className,
              options: {
                opacity: rootLayer.getOpacity(),
                visible: rootLayer.getVisible(),
                source: {},
              },
              extension: {
                isDrawLayer: rootLayerVal.isDrawLayer
                  ? rootLayerVal.isDrawLayer
                  : undefined,
                isAddTextLayer: rootLayerVal.isAddTextLayer
                  ? rootLayerVal.isAddTextLayer
                  : undefined,
                isAddArrowLayer: rootLayerVal.isAddArrowLayer
                  ? rootLayerVal.isAddArrowLayer
                  : undefined,
                isInterrogable: rootLayerVal.isInterrogable
                  ? rootLayerVal.isInterrogable
                  : undefined,
                layerIdx: rootLayerVal.layerIdx,
                imageDataUrl: rootLayerVal.imageDataUrl
                  ? rootLayerVal.imageDataUrl
                  : undefined,
                rootUrl: rootLayerVal.rootUrl
                  ? rootLayerVal.rootUrl
                  : undefined,
                selectedDate: rootLayerVal.selectedDate
                  ? rootLayerVal.selectedDate
                  : undefined,
                name: rootLayerVal.name,
                namespace: rootLayerVal.namespace
                  ? rootLayerVal.namespace
                  : undefined,
                metadataUrl: rootLayerVal.metadataUrl
                  ? rootLayerVal.metadataUrl
                  : null,
                mapName: rootLayerVal.mapName ? rootLayerVal.mapName : null,
                layerName: rootLayerVal.layerName
                  ? rootLayerVal.layerName
                  : null,
                minscaledenominator: rootLayerVal.minscaledenominator
                  ? rootLayerVal.minscaledenominator
                  : null,
                maxscaledenominator: rootLayerVal.maxscaledenominator
                  ? rootLayerVal.maxscaledenominator
                  : null,
                displayClass: rootLayerVal.displayClass,
                filterVisibility: rootLayerVal.filterVisibility,
                depth: rootLayerVal.depth,
                forceOpacityControl: rootLayerVal.forceOpacityControl,
                LayerDownload: rootLayerVal.LayerDownload,
                LayerLegendScaleDisplay: rootLayerVal.LayerLegendScaleDisplay,
                baseLayer: rootLayerVal.baseLayer,
                defaultbaseLayer: rootLayerVal.defaultbaseLayer,
                LayerQuery: rootLayerVal.LayerQuery,
                legendUrl: null,
                legends: rootLayerVal.legends,
                style: rootLayerVal.style ? rootLayerVal.style : null,
                clusterOptions: null,
                settedStyle: rootLayerVal.settedStyle
                  ? rootLayerVal.settedStyle
                  : null,
              },
              zIndex: rootLayerVal.zIndex,
            };

            if (!layerConfig.extension.legends) {
              layerConfig.extension.legendUrl = rootLayerVal.legendUrl;
            }
            switch (className) {
              case "LayerGroup":
                const layerGroup = {
                  class: "LayerGroup",
                  options: {
                    opacity: rootLayerVal.opacity,
                    visible: rootLayerVal.visible,
                  },
                  extension: {
                    id: rootLayerVal.id,
                    name: rootLayerVal.name,
                    open: rootLayerVal.open,
                    isBufferLayerGroup: rootLayerVal.isBufferLayerGroup
                      ? rootLayerVal.isBufferLayerGroup
                      : false,
                    isDrawLayerGroup: rootLayerVal.isDrawLayerGroup
                      ? rootLayerVal.isDrawLayerGroup
                      : false,
                  },
                  layers: this.contextLayersAndLayerGroups(rootLayerVal.layers),
                };
                newLayerConfig.push(layerGroup);
                break;
              case "TileLayer":
                if (
                  rootLayerVal.source.values_ &&
                  rootLayerVal.source.getProdigeType
                ) {
                  switch (rootLayerVal.source.getProdigeType()) {
                    case "BingMaps":
                      break;
                    case "TileArcGISRest":
                      break;
                    case "TileDebug":
                      break;
                    case "TileJSON":
                      break;
                    case "TileWMS":
                      layerConfig.options.source = {
                        class: "TileWMS",
                        options: {
                          url: rootLayerVal.source.urls[0],
                          params: {
                            LAYERS: rootLayerVal.source.params_.LAYERS,
                          },
                        },
                      };
                      break;
                    case "UTFGrid":
                      break;
                    case "VectorTile":
                      break;
                    case "WMTS":
                      let tileGridOpts = {};
                      if (rootLayerVal.source.tileGrid.origin_) {
                        tileGridOpts = {
                          origin: rootLayerVal.source.tileGrid.origin_,
                          resolutions:
                            rootLayerVal.source.tileGrid.getResolutions(),
                          matrixIds:
                            rootLayerVal.source.tileGrid.getMatrixIds(),
                        }
                      } else {
                        tileGridOpts = {
                          origins: rootLayerVal.source.tileGrid.origins_,
                          resolutions:
                            rootLayerVal.source.tileGrid.getResolutions(),
                          matrixIds:
                            rootLayerVal.source.tileGrid.getMatrixIds(),
                        }
                      }
                      layerConfig.options.source = {
                        class: "WMTS",
                        options: {
                          url: rootLayerVal.source.urls[0],
                          layer: rootLayerVal.source.layer_,
                          matrixSet: rootLayerVal.source.matrixSet_,
                          format: rootLayerVal.source.format_,
                          style: rootLayerVal.source.style_,
                          tileGrid: {
                            class: "WMTSTileGrid",
                            options: tileGridOpts,
                          },
                        },
                      };
                      break;
                    case "XYZ":
                      break;
                    case "CartoDB":
                      break;
                    case "OSM":
                      layerConfig.options.source = {
                        class: "OSM",
                        options: {
                          url: rootLayerVal.source.urls[0],
                          attributions: rootLayerVal.source.attributions_
                            ? rootLayerVal.source.attributions_
                            : "",
                          opaque: rootLayerVal.source.opaque_
                            ? rootLayerVal.source.opaque_
                            : "",
                        },
                      };
                      break;
                    case "Stamen":
                      break;
                    case "Zoomify":
                      break;
                    default:
                  }
                }
                newLayerConfig.push(layerConfig);
                break;
              case "ImageLayer":
                if (
                  rootLayerVal.source.values_ &&
                  rootLayerVal.source.getProdigeType
                ) {
                  switch (rootLayerVal.source.getProdigeType()) {
                    case "ImageArcGISRest":
                      break;
                    case "ImageCanvasSource":
                      break;
                    case "ImageMapGuide":
                      break;
                    case "Static":
                      break;
                    case "ImageWMS":
                      layerConfig.options.source = {
                        class: "ImageWMS",
                        options: {
                          url: rootLayerVal.source.url_,
                          params: {
                            FORMAT: rootLayerVal.source.params_.FORMAT,
                            LAYERS: rootLayerVal.source.params_.LAYERS,
                            VERSION: rootLayerVal.source.params_.VERSION,
                            FILTER: rootLayerVal.source.params_.FILTER
                              ? rootLayerVal.source.params_.FILTER
                              : "",
                          },
                        },
                      };
                      break;
                    case "RasterSource":
                      break;
                    default:
                  }
                }
                newLayerConfig.push(layerConfig);
                break;
              case "VectorLayer":
                if (
                  rootLayerVal.source.values_ &&
                  rootLayerVal.source.getProdigeType
                ) {
                  switch (rootLayerVal.source.getProdigeType()) {
                    case "VectorSource":
                      let vectorSourceFormat = "GeoJSON";
                      if (typeof rootLayerVal.source.format_ == "object") {
                        if (
                          rootLayerVal.source.format_.constructor == GeoJSON ||
                          rootLayerVal.source.format_ instanceof GeoJSON
                        ) {
                          vectorSourceFormat = "GeoJSON";
                        } else {
                          if (rootLayerVal.source.getProdigeType) {
                            vectorSourceFormat =
                              rootLayerVal.source.getProdigeType();
                          }
                        }
                      }
                      if (vectorSourceFormat) {
                        switch (vectorSourceFormat) {
                          case "WFS":
                          case "VectorSource":
                            const wfsOptions = rootLayerVal.source.values_
                              .wfs_options
                              ? rootLayerVal.source.values_.wfs_options
                              : null;
                            const finalUrl = decodeURIComponent(
                              rootLayerVal.source.url_()
                            );
                            let urlCleaned = null;
                            if (finalUrl.includes("proxy?proxyUrl")) {
                              urlCleaned = finalUrl.split("proxyUrl=")[1];
                            } else {
                              urlCleaned = finalUrl;
                            }
                            //removing unnecessary queryParams
                            const urlObj = new URL(urlCleaned);
                            for (const param of [
                              "service",
                              "request",
                              "outputFormat",
                              "typename",
                              "srsname",
                              "bbox",
                            ]) {
                              urlObj.searchParams.delete(param);
                            }
                            urlCleaned = urlObj.toString();

                            const gmlFormat =
                              rootLayerVal.source.format_.gmlFormat_;
                            let gmlFormatStr = "GML2";
                            if (
                              typeof gmlFormat == "object" &&
                              "schemaLocation" in gmlFormat
                            ) {
                              gmlFormatStr =
                                gmlFormat.schemaLocation.indexOf("gml/3") != -1
                                  ? "GML3"
                                  : "GML2";
                            }
                            layerConfig.options.source = {
                              class: "VectorSource",
                              options: {
                                format: "WFS",
                                version: "1.1.0",
                                gmlFormat: gmlFormatStr,
                                typename: {
                                  name: wfsOptions ? wfsOptions.name : "",
                                  prefix: wfsOptions ? wfsOptions.prefix : "",
                                  ns: wfsOptions ? wfsOptions.ns : "",
                                },
                                url: urlCleaned,
                              },
                            };
                            if (layerConfig.extension.settedStyle) {
                              layerConfig.extension.settedStyle.geom =
                                rootLayerVal.source.getFeatures() &&
                                rootLayerVal.source.getFeatures()[0]
                                  ? rootLayerVal.source
                                      .getFeatures()[0]
                                      .getGeometry()
                                      .getType()
                                  : null;
                            }
                            break;
                          case "GeoJSON":
                            const writer = new GeoJSON();
                            let features = (
                              rootLayer as VectorLayer<VectorSource<Geometry>>
                            )
                              .getSource()
                              .getFeatures();
                            if (rootLayer.get("isBuffer")) {
                              const cleanFeatures = features.map((feature) => {
                                // const tmpFeature = new Feature(feature.values_.values_.geometry);
                                const tmpFeature = new Feature(
                                  feature.getGeometry()
                                );
                                tmpFeature.setProperties({
                                  style: feature.getStyle(),
                                  settedStyle: feature.get("settedStyle")
                                    ? feature.get("settedStyle")
                                    : null,
                                });
                                // tmpFeature.set('values_', feature.values_.values_.geometry);
                                // tmpFeature.setStyle( feature.getStyle() );
                                return tmpFeature;
                              });
                              features = cleanFeatures;
                            }
                            layerConfig.options.source = {
                              class: "VectorSource",
                              options: {
                                attributions: rootLayerVal.source.attributions_,
                                features: [],
                                format: "GeoJSON",
                                loader: rootLayerVal.source.loader_,
                              },
                              extension: {
                                geojson: writer.writeFeatures(features as any),
                              },
                            };
                            break;
                          default:
                            console.error(
                              "Format VectorSource non pris en charge ",
                              vectorSourceFormat
                            );
                            break;
                        }
                      }
                      break;
                    case "Cluster":
                      layerConfig.options.source = {
                        class: "Cluster",
                        options: {
                          url: rootLayerVal.source.getUrl(),
                          params: {
                            LAYERS: rootLayerVal.layerName,
                          },
                        },
                      };
                      layerConfig.extension.clusterOptions =
                        rootLayerVal.clusterOptions;

                      break;
                  }
                }
                newLayerConfig.push(layerConfig);
                break;
            }
          } else {
            console.error(" rootLayer ne respecte pas le format ", rootLayer);
          }
        });
      }
    }
    return newLayerConfig;
  }

  generateLayersAndLayerGroups: (
    layerOrLayerGroup:
      | BaseLayerConfig
      | LayerGroupConfig
      | Array<BaseLayerConfig | LayerGroupConfig>
  ) => BaseLayer | LayerGroup | Array<BaseLayer | LayerGroup> = (
    layerOrLayerGroup
  ) => {
    if (layerOrLayerGroup) {
      if (layerOrLayerGroup instanceof Array) {
        const layers = [];
        layerOrLayerGroup.forEach(
          (subLayerConfig: BaseLayerConfig | LayerGroupConfig) => {
            layers.push(this.generateLayersAndLayerGroups(subLayerConfig));
          }
        );
        // Nous avions un tableau en entrée, on retourne donc un tableau en sortie
        return layers;
      } else {
        let generatedLayer: BaseLayer | LayerGroup = null;
        if (
          this.nextLayerIdx === null ||
          this.nextLayerIdx <= layerOrLayerGroup.extension.layerIdx
        ) {
          this.setNextLayerIdx(layerOrLayerGroup.extension.layerIdx + 1);
        }

        switch (layerOrLayerGroup.class) {
          case "TileVector":
            generatedLayer = new MapboxVectorLayer({
              styleUrl: layerOrLayerGroup.extension.tiledStyleUrl
                ? layerOrLayerGroup.extension.tiledStyleUrl
                : "",
              accessToken: layerOrLayerGroup.options.source.options.url + "",
            });

            break;
          case "LayerGroup":
            const layerGroupOptions = Object.assign(
              {},
              layerOrLayerGroup.options,
              {
                layers: this.generateLayersAndLayerGroups(
                  (layerOrLayerGroup as LayerGroupConfig).layers
                ) as Array<any>,
              }
            );
            generatedLayer = new LayerGroup(layerGroupOptions);
            generatedLayer.set("class", layerOrLayerGroup.class);
            generatedLayer.set(
              "extension",
              layerOrLayerGroup.extension ? layerOrLayerGroup.extension : null
            );
            if (layerOrLayerGroup.extension) {
              const layerLayerGroupExtAttributes = Object.keys(
                layerOrLayerGroup.extension
              );
              layerLayerGroupExtAttributes.forEach((attributeName) => {
                generatedLayer.set(
                  attributeName,
                  layerOrLayerGroup.extension[attributeName]
                );
              });
            }
            break;
          case "TileLayer":
            // Gestion de la source
            let sourceTile = null;
            if (
              layerOrLayerGroup.options &&
              layerOrLayerGroup.options.source &&
              layerOrLayerGroup.options.source.class
            ) {
              switch (layerOrLayerGroup.options.source.class) {
                case "BingMaps":
                  const bingMapsOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new BingMaps(bingMapsOptions as BingMapsOptions);
                  break;
                case "TileArcGISRest":
                  const tileArcGISRestOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new TileArcGISRest(
                    tileArcGISRestOptions as TileArcGISRestOptions
                  );
                  break;
                case "TileDebug":
                  const tileDebugOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new TileDebug(
                    tileDebugOptions as TileDebugOptions
                  );
                  break;
                case "TileJSON":
                  const tileJSONOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new TileJSON(tileJSONOptions as TileJSONOptions);
                  break;
                case "TileWMS":
                  const tileWMSOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                      // url: this.urlProxyService.getProxyUrl(layerOrLayerGroup.options.source.options.url),
                      // crossOrigin: 'anonymous'
                    }
                  );
                  sourceTile = new TileWMS(tileWMSOptions as TileWMSOptions);
                  break;
                case "UTFGrid":
                  const uTFGridOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new UTFGrid(uTFGridOptions as UTFGridOptions);
                  break;
                case "VectorTile":
                  const vectorTileOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new VectorTile(
                    vectorTileOptions as VectorTileOptions
                  );
                  break;
                case "WMTS":
                  const tileGridOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options.tileGrid.options,
                    {
                      class: layerOrLayerGroup.options.source.options.tileGrid
                        .class
                        ? layerOrLayerGroup.options.source.options.tileGrid
                            .class
                        : "",
                    }
                  );
                  const tileGrid = new WMTSTileGrid(tileGridOptions);
                  const wMTSOptions = Object.assign(
                    {
                      // valeur par defaut
                      // tslint:disable-next-line:max-line-length
                      format: layerOrLayerGroup.options.source.options.format
                        ? layerOrLayerGroup.options.source.options.format
                        : "image/png",
                      projection: this.mapProjection.getCode(),
                    },
                    layerOrLayerGroup.options.source.options,
                    {
                      tileGrid,
                    }
                  );
                  sourceTile = new WMTS(wMTSOptions as WMTSOptions);
                  break;
                case "XYZ":
                  const xYZOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new XYZ(xYZOptions as XYZOptions);
                  break;
                case "CartoDB":
                  const cartoDBOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new CartoDB(cartoDBOptions as CartoDBOptions);
                  break;
                case "OSM":
                  const oSMOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      // projection: this.mapProjection.getCode()
                    }
                  );
                  sourceTile = new OSM(oSMOptions as OSMOptions);
                  break;
                case "Stamen":
                  const stamenOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new Stamen(stamenOptions as StamenOptions);
                  break;
                case "Zoomify":
                  const zoomifyOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceTile = new Zoomify(zoomifyOptions as ZoomifyOptions);
                  break;
                default:
                  console.error(
                    "Classe source non valide/disponible",
                    layerOrLayerGroup,
                    layerOrLayerGroup.options.source.class
                  );
                  return null;
              }
              // On ajoute à la source les propriétés misent en extension dans le contexte
              if (sourceTile && layerOrLayerGroup.options.source) {
                let propertyName;
                sourceTile.set("class", sourceTile.constructor.name);
                sourceTile.set(
                  "extension",
                  layerOrLayerGroup.options.source.extension
                    ? layerOrLayerGroup.options.source.extension
                    : null
                );
                if (sourceTile && layerOrLayerGroup.options.source.extension) {
                  for (propertyName of layerOrLayerGroup.options.source
                    .extension) {
                    sourceTile.set(
                      propertyName,
                      layerOrLayerGroup.options.source.extension[propertyName]
                    );
                  }
                }
              }
            } else {
              console.error(
                "Classe source non valide/disponible",
                layerOrLayerGroup,
                layerOrLayerGroup.options.source.class
              );
              return null;
            }
            const tileLayerOptions = Object.assign(
              {},
              layerOrLayerGroup.options,
              {
                class: layerOrLayerGroup.class ? layerOrLayerGroup.class : "",
                source: sourceTile,
                // tslint:disable-next-line:max-line-length
                zIndex:
                  layerOrLayerGroup.extension &&
                  !!layerOrLayerGroup.extension.baseLayer
                    ? 0
                    : !this.isContextLoading &&
                      layerOrLayerGroup.options.source.class === "WMTS"
                    ? this.startAddLayerZIndex++
                    : this.startZIndex--,
                /* surcharge: null, */
              }
            );
            generatedLayer = new TileLayer(tileLayerOptions);
            break;
          case "ImageLayer":
            // Gestion de la source
            let sourceImage = null;
            if (
              layerOrLayerGroup.options &&
              layerOrLayerGroup.options.source &&
              layerOrLayerGroup.options.source.class
            ) {
              switch (layerOrLayerGroup.options.source.class) {
                case "ImageArcGISRest":
                  const imageArcGISRestOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceImage = new ImageArcGISRest(
                    imageArcGISRestOptions as ImageArcGISRestOptions
                  );
                  break;
                case "ImageCanvasSource":
                  const imageCanvasOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceImage = new ImageCanvasSource(
                    imageCanvasOptions as ImageCanvasSourceOptions
                  );
                  break;
                case "ImageMapGuide":
                  const imageMapGuideOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceImage = new ImageMapGuide(
                    imageMapGuideOptions as ImageMapGuideOptions
                  );
                  break;
                case "Static":
                  const staticOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceImage = new Static(staticOptions as StaticOptions);
                  break;
                case "ImageWMS":
                  const imageWMSOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceImage = new ImageWMS(
                    imageWMSOptions as ImageWMSOptions
                  );
                  break;
                case "RasterSource":
                  const rasterOptions = Object.assign(
                    {},
                    layerOrLayerGroup.options.source.options,
                    {
                      /* surcharge: null, */
                      projection: this.mapProjection.getCode(),
                    }
                  );
                  sourceImage = new RasterSource(
                    rasterOptions as RasterSourceOptions
                  );
                  break;
                default:
                  console.error(
                    "Classe source non valide/disponible",
                    layerOrLayerGroup,
                    layerOrLayerGroup.options.source.class
                  );
                  return null;
              }
              // On ajoute à la source les propriétés misent en extension dans le contexte
              if (sourceImage && layerOrLayerGroup.options.source) {
                let propertyName;
                sourceImage.set("class", sourceImage.constructor.name);
                // tslint:disable-next-line:max-line-length
                sourceImage.set(
                  "extension",
                  layerOrLayerGroup.options.source.extension
                    ? layerOrLayerGroup.options.source.extension
                    : null
                );
                if (sourceImage && layerOrLayerGroup.options.source.extension) {
                  for (propertyName of layerOrLayerGroup.options.source
                    .extension) {
                    sourceImage.set(
                      propertyName,
                      layerOrLayerGroup.options.source.extension[propertyName]
                    );
                  }
                }
              }
            } else {
              console.error(
                "Classe source non valide/disponible",
                layerOrLayerGroup,
                layerOrLayerGroup.options.source.class
              );
              return null;
            }
            const imageLayerGroupOptions = Object.assign(
              {},
              layerOrLayerGroup.options,
              {
                class: layerOrLayerGroup.class ? layerOrLayerGroup.class : "",
                source: sourceImage,
                // tslint:disable-next-line:max-line-length
                // zIndex: (!this.isContextLoading && layerOrLayerGroup.options.source.class) === 'ImageWMS' ? this.startAddLayerZIndex++ : this.startZIndex--,
                zIndex:
                  layerOrLayerGroup.extension &&
                  !!layerOrLayerGroup.extension.baseLayer
                    ? 0
                    : !this.isContextLoading &&
                      layerOrLayerGroup.options.source.class === "ImageWMS"
                    ? this.startAddLayerZIndex++
                    : this.startZIndex--,
                /* surcharge: null, */
              }
            );
            generatedLayer = new ImageLayer(imageLayerGroupOptions);
            break;
          case "VectorLayer":
            // Gestion de la source
            let sourceVector: VectorSource<any> | Cluster = null;
            let vectorAdditionnalExtension: any = {};

            // is a named Style defined for the vector layer
            let namedStyle = null;
            if (
              layerOrLayerGroup.extension &&
              layerOrLayerGroup.extension.style
            ) {
              const styleName = layerOrLayerGroup.extension.style;
              if (styleName != null && styleName != "") {
                if (styleName in this.environment.namedStyles) {
                  namedStyle = this.environment.namedStyles[styleName];
                }
              }
            }

            let isClusterLayer = false;

            if (
              layerOrLayerGroup.options &&
              layerOrLayerGroup.options.source &&
              layerOrLayerGroup.options.source.class
            ) {
              switch (layerOrLayerGroup.options.source.class) {
                case "VectorSource":
                  let vectorSourceOptions = Object.assign({ wrapX: false });
                  switch (layerOrLayerGroup.options.source.options.format) {
                    case "WFS":
                      const options = layerOrLayerGroup.options.source.options;
                      const gmlClass =
                        options.gmlFormat &&
                        typeof options.gmlFormat === "string" &&
                        options.gmlFormat.indexOf("3") !== -1
                          ? new GML3()
                          : new GML2();
                      vectorSourceOptions = Object.assign(
                        { wrapX: false },
                        {
                          format: new WFS({
                            gmlFormat: gmlClass,
                          }),
                          url: (extent, resolution) => {
                            const typename =
                              options.typename.prefix !== ""
                                ? options.typename.prefix +
                                  ":" +
                                  options.typename.name
                                : options.typename.name;
                            const version = "1.1.0";

                            //const bboxStr = (this.contextExtensionInfo.wfsBbox ?
                            //  this.contextExtensionInfo.wfsBbox : this.mapProjection.extent_)
                            //  .join(',') + ',' + this.mapProjection.getCode();
                            const bboxStr =
                              (extent
                                ? extent
                                : this.mapProjection.getExtent()
                              ).join(",") +
                              "," +
                              this.mapProjection.getCode();
                            const urlOptions = [
                              ["service", "WFS"],
                              ["version", version],
                              ["request", "GetFeature"],
                              ["typename", typename],
                              ["srsname", this.mapProjection.getCode()],
                              ["bbox", bboxStr],
                            ];
                            console.log(
                              "VectorSourceCore assign context bbox : ",
                              bboxStr
                            );
                            // tslint:disable-next-line:max-line-length
                            return this.urlProxyService.getFinalUrl(
                              options.url,
                              urlOptions,
                              layerOrLayerGroup.extension.isLocalData
                                ? layerOrLayerGroup.extension.isLocalData
                                : false
                            );
                          },
                          extension: {
                            name: options.typename.name,
                            prefix: options.typename.prefix,
                            ns: options.typename.ns,
                          },
                          strategy: bboxStrategy,
                          projection: this.mapProjection.getCode(),
                        }
                      );
                      vectorAdditionnalExtension = {
                        wfs_options: {
                          name: options.typename.name,
                          prefix: options.typename.prefix,
                          ns: options.typename.ns,
                        },
                      };
                      if (
                        layerOrLayerGroup.extension.settedStyle &&
                        namedStyle == null
                      ) {
                        const settedStyle =
                          layerOrLayerGroup.extension.settedStyle;
                        if (settedStyle.icon) {
                          namedStyle = new Style({
                            image: new FontSymbol({
                              glyph: settedStyle.icon,
                              fontSize: settedStyle.size
                                ? settedStyle.size
                                : "",
                              fontStyle: "normal",
                              color: settedStyle.color
                                ? settedStyle.color
                                : "rgba(1, 149, 254, 0.85)",
                              fill: new Fill({
                                color: settedStyle.color
                                  ? settedStyle.color
                                  : "rgba(1, 149, 254, 0.85)",
                              }),
                              stroke: new Stroke({
                                color: "#fff",
                                width: 2,
                              }),
                              radius: 20,
                            }),
                            text: new Text({
                              text: settedStyle.text ? settedStyle.text : null,
                              textAlign: "center",
                              textBaseline: "middle",
                              // tslint:disable-next-line: max-line-length
                              font:
                                (settedStyle.size
                                  ? settedStyle.size + "px "
                                  : null) +
                                (settedStyle.font ? settedStyle.font : null),
                              fill: new Fill({
                                color: settedStyle.color
                                  ? settedStyle.color
                                  : "rgba(1, 149, 254, 0.85)",
                              }),
                            }),
                          });
                        } else if (settedStyle.svgIcon) {
                          namedStyle = new Style({
                            image: new Icon({
                              src: settedStyle.svgIcon,
                              scale: settedStyle.scale,
                            }),
                          });
                        } else if (
                          settedStyle.geom &&
                          settedStyle.geom === "LineString"
                        ) {
                          if (settedStyle.pattern) {
                            namedStyle = new Style({
                              stroke: new StrokePattern({
                                width: 8,
                                pattern: settedStyle.pattern
                                  ? settedStyle.pattern.name
                                  : null,
                                ratio: 2,
                                color: settedStyle.color
                                  ? settedStyle.color
                                  : "rgba(1, 149, 254, 0.85)",
                                offset: 0,
                                scale: parseInt(
                                  settedStyle.size ? settedStyle.size : "",
                                  10
                                ),
                                fill: new Fill({
                                  color: settedStyle.background,
                                }),
                                size: 1,
                                spacing: 10,
                                angle: 0,
                              }),
                              text: new Text({
                                text: settedStyle.text
                                  ? settedStyle.text
                                  : null,
                                textAlign: "center",
                                textBaseline: "middle",
                                // tslint:disable-next-line: max-line-length
                                font:
                                  (settedStyle.size
                                    ? settedStyle.size + "px "
                                    : null) +
                                  (settedStyle.font ? settedStyle.font : null),
                                fill: new Fill({
                                  color: settedStyle.color
                                    ? settedStyle.color
                                    : "rgba(1, 149, 254, 0.85)",
                                }),
                              }),
                            });
                          } else {
                            namedStyle = new Style({
                              stroke: new Stroke({
                                width: settedStyle.width,
                                color: settedStyle.color
                                  ? settedStyle.color
                                  : "rgba(1, 149, 254, 0.85)",
                              }),
                            });
                          }
                        } else if (
                          settedStyle.geom &&
                          settedStyle.geom === "Polygon"
                        ) {
                          namedStyle = new Style({
                            fill: new FillPattern({
                              pattern: settedStyle.pattern
                                ? settedStyle.pattern.name
                                : "empty",
                              ratio: 2,
                              icon: new Icon({
                                src: "data/target.png",
                              }),
                              color: settedStyle.color
                                ? settedStyle.color
                                : "rgba(1, 149, 254, 0.85)",
                              offset: 0,
                              scale: parseInt(
                                settedStyle.size ? settedStyle.size : ""
                              ),
                              fill: new Fill({
                                color: settedStyle.background
                                  ? settedStyle.background
                                  : "rgba(0, 0, 0, 0.35)",
                              }),
                              size: 1,
                              spacing: 10,
                              angle: 0,
                            }),
                            stroke: new Stroke({
                              color: settedStyle.color
                                ? settedStyle.color
                                : "rgba(1, 149, 254, 0.85)",
                              width: 3,
                            }),
                            text: new Text({
                              text: settedStyle.text ? settedStyle.text : null,
                              textAlign: "center",
                              textBaseline: "middle",
                              // tslint:disable-next-line: max-line-length
                              font:
                                (settedStyle.size
                                  ? settedStyle.size + "px "
                                  : null) +
                                (settedStyle.font ? settedStyle.font : null),
                              fill: new Fill({
                                color: settedStyle.color
                                  ? settedStyle.color
                                  : null,
                              }),
                            }),
                          });
                        }
                      }
                      break;
                    case "GeoJSON":
                      vectorSourceOptions = Object.assign(
                        { wrapX: false },
                        layerOrLayerGroup.options.source.options,
                        {
                          format: new GeoJSON(),
                          features: new GeoJSON().readFeatures(
                            layerOrLayerGroup.options.source.extension.geojson
                          ),
                        }
                      );
                      vectorSourceOptions.features.forEach((feature) => {
                        if (feature.get("settedStyle") && namedStyle == null) {
                          const settedStyle = feature.get("settedStyle");
                          if (feature.getGeometry().getType() === "Point") {
                            if (settedStyle.icon) {
                              feature.setStyle(
                                new Style({
                                  image: new FontSymbol({
                                    glyph: settedStyle.icon
                                      ? settedStyle.icon
                                      : null,
                                    fontSize: settedStyle.size
                                      ? settedStyle.size
                                      : "",
                                    fontStyle: "normal",
                                    color: settedStyle.color
                                      ? settedStyle.color
                                      : "rgba(1, 149, 254, 0.85)",
                                    fill: new Fill({
                                      color: settedStyle.color
                                        ? settedStyle.color
                                        : "rgba(1, 149, 254, 0.85)",
                                    }),
                                    stroke: new Stroke({
                                      color: "#fff",
                                      width: 2,
                                    }),
                                    radius: 20,
                                  }),
                                  text: new Text({
                                    text: feature.get("text")
                                      ? feature.get("text")
                                      : null,
                                    textAlign: "center",
                                    textBaseline: "middle",
                                    // tslint:disable-next-line: max-line-length
                                    font:
                                      (feature.get("size")
                                        ? feature.get("size") + "px "
                                        : null) +
                                      (feature.get("font")
                                        ? feature.get("font")
                                        : null),
                                    fill: new Fill({
                                      color: feature.get("color")
                                        ? feature.get("color")
                                        : "rgba(1, 149, 254, 0.85)",
                                    }),
                                  }),
                                })
                              );
                            } else if (settedStyle.svgIcon) {
                              feature.setStyle(
                                new Style({
                                  image: new Icon({
                                    src: settedStyle.svgIcon,
                                    scale: settedStyle.scale,
                                  }),
                                })
                              );
                            }
                          } else if (
                            feature.getGeometry().getType() === "LineString"
                          ) {
                            if (settedStyle.pattern) {
                              feature.setStyle(
                                new Style({
                                  stroke: new StrokePattern({
                                    width: 8,
                                    pattern: settedStyle.pattern
                                      ? settedStyle.pattern.name
                                      : null,
                                    ratio: 2,
                                    color: settedStyle.color
                                      ? settedStyle.color
                                      : "rgba(1, 149, 254, 0.85)",
                                    offset: 0,
                                    scale: parseInt(
                                      settedStyle.size ? settedStyle.size : "",
                                      10
                                    ),
                                    fill: new Fill({
                                      color: settedStyle.background
                                        ? settedStyle.background
                                        : "rgba(1, 149, 254, 0.85)",
                                    }),
                                    size: 1,
                                    spacing: 10,
                                    angle: 0,
                                  }),
                                  text: new Text({
                                    text: feature.get("text")
                                      ? feature.get("text")
                                      : null,
                                    textAlign: "center",
                                    textBaseline: "middle",
                                    // tslint:disable-next-line: max-line-length
                                    font:
                                      (feature.get("size")
                                        ? feature.get("size") + "px "
                                        : null) +
                                      (feature.get("font")
                                        ? feature.get("font")
                                        : null),
                                    fill: new Fill({
                                      color: feature.get("color")
                                        ? feature.get("color")
                                        : "rgba(1, 149, 254, 0.85)",
                                    }),
                                  }),
                                })
                              );
                            } else {
                              feature.setStyle(
                                new Style({
                                  stroke: new Stroke({
                                    width: settedStyle.width,
                                    color: settedStyle.color
                                      ? settedStyle.color
                                      : "rgba(1, 149, 254, 0.85)",
                                  }),
                                })
                              );
                            }
                          } else if (
                            feature.getGeometry().getType() === "Polygon"
                          ) {
                            feature.setStyle(
                              new Style({
                                fill: new FillPattern({
                                  pattern: settedStyle.pattern
                                    ? settedStyle.pattern.name
                                    : "empty",
                                  ratio: 2,
                                  icon: new Icon({
                                    src: "data/target.png",
                                  }),
                                  color: settedStyle.color
                                    ? settedStyle.color
                                    : "rgba(1, 149, 254, 0.85)",
                                  offset: 0,
                                  scale: parseInt(
                                    settedStyle.size ? settedStyle.size : "",
                                    10
                                  ),
                                  fill: new Fill({
                                    color: settedStyle.background
                                      ? settedStyle.background
                                      : "rgba(0, 0, 0, 0.35)",
                                  }),
                                  size: 1,
                                  spacing: 10,
                                  angle: 0,
                                }),
                                stroke: new Stroke({
                                  color: settedStyle.color
                                    ? settedStyle.color
                                    : "rgba(1, 149, 254, 0.85)",
                                  width: 3,
                                }),
                                text: new Text({
                                  text: feature.get("text")
                                    ? feature.get("text")
                                    : null,
                                  textAlign: "center",
                                  textBaseline: "middle",
                                  // tslint:disable-next-line: max-line-length
                                  font:
                                    (feature.get("size")
                                      ? feature.get("size") + "px "
                                      : null) +
                                    (feature.get("font")
                                      ? feature.get("font")
                                      : null),
                                  fill: new Fill({
                                    color: feature.get("color")
                                      ? feature.get("color")
                                      : "rgba(1, 149, 254, 0.85)",
                                  }),
                                }),
                              })
                            );
                          }
                        }
                      });
                      break;
                    default:
                      vectorSourceOptions = Object.assign(
                        { wrapX: false },
                        layerOrLayerGroup.options.source.options,
                        {
                          /* surcharge: null, */
                        }
                      );
                      break;
                  }

                  sourceVector = new VectorSource(
                    vectorSourceOptions as VectorSourceOptions
                  );
                  break;
                case "Cluster":
                  isClusterLayer = true;
                  let clusterOption: ClusterStyleOption = null;
                  if (
                    layerOrLayerGroup.extension &&
                    layerOrLayerGroup.extension.clusterOptions
                  ) {
                    clusterOption = layerOrLayerGroup.extension
                      .clusterOptions as ClusterStyleOption;
                  }

                  // vector source
                  const vectorSource = new VectorSource({
                    wrapX: false,
                    projection: this.mapProjection.getCode(),
                    format: new GeoJSON(),
                    url: layerOrLayerGroup.options.source.options.url,
                    loader: (extent, resolution, projection) => {
                      this.http
                        .get<FeatureCollection>(clusterOption.url, {
                          withCredentials: true,
                        })
                        .subscribe((result) => {
                          const features: Feature<Geometry>[] = [];
                          if (result) {
                            result.features.forEach((feature) => {
                              let coordinates = null;
                              switch (feature.geometry.type) {
                                case "MultiPoint":
                                  coordinates = feature.geometry.coordinates[0];
                                  break;
                                case "Point":
                                  coordinates = feature.geometry.coordinates;
                                  break;
                              }

                              if (coordinates) {
                                const featureTmp = new Feature({
                                  geometry: new Point(coordinates),
                                });

                                featureTmp.setProperties(feature.properties);
                                features.push(featureTmp);
                              }
                            });
                          }

                          vectorSource.addFeatures(features);
                        });
                    },
                  } as SourceOptions);

                  // clusterStyleService
                  this.clusterService.setOptions(clusterOption);
                  namedStyle = (feature) =>
                    this.clusterService.applyStyle(feature);

                  sourceVector = new Cluster({
                    source: vectorSource,
                    distance:
                      clusterOption && clusterOption.distance
                        ? clusterOption.distance
                        : 50,
                    url: layerOrLayerGroup.options.source.options.url,
                  } as VectorSourceOptions);

                  sourceVector.getUrl = () => vectorSource.getUrl();

                  // sourceVector.setUrl(layerOrLayerGroup.options.source.options.url);
                  layerOrLayerGroup.options.animationDuration =
                    clusterOption.transitionEffect ? 700 : 0;

                  this.clusterService.clearCache();
                  break;
                default:
                  console.error(
                    "Classe source non valide/disponible",
                    layerOrLayerGroup,
                    layerOrLayerGroup.options.source.class
                  );
                  return null;
              }
              // On ajoute à la source les propriétés misent en extension dans le contexte
              if (sourceVector && layerOrLayerGroup.options.source) {
                sourceVector.set("class", sourceVector.constructor.name);
                // tslint:disable-next-line:max-line-length
                sourceVector.set(
                  "extension",
                  layerOrLayerGroup.options.source.extension
                    ? layerOrLayerGroup.options.source.extension
                    : null
                );
                if (
                  sourceVector &&
                  layerOrLayerGroup.options.source.extension
                ) {
                  const layerLayerGroupExtAttributes = Object.keys(
                    layerOrLayerGroup.options.source.extension
                  );
                  layerLayerGroupExtAttributes.forEach((propertyName) => {
                    sourceVector.set(
                      propertyName,
                      layerOrLayerGroup.options.source.extension[propertyName]
                    );
                  });
                }
              }
              if (sourceVector && vectorAdditionnalExtension) {
                const extAttributes = Object.keys(vectorAdditionnalExtension);
                extAttributes.forEach((propertyName) => {
                  sourceVector.set(
                    propertyName,
                    vectorAdditionnalExtension[propertyName]
                  );
                });
              }
            } else {
              console.error(
                "Classe source non valide/disponible",
                layerOrLayerGroup,
                layerOrLayerGroup.options.source.class
              );
              return null;
            }
            const vectorLayerGroupOptions = Object.assign(
              {},
              layerOrLayerGroup.options,
              {
                // extent: layerOrLayerGroup.options.extent,
                class: layerOrLayerGroup.class ? layerOrLayerGroup.class : "",
                source: sourceVector,

                zIndex:
                  layerOrLayerGroup.extension &&
                  !!layerOrLayerGroup.extension.baseLayer
                    ? 0
                    : !this.isContextLoading &&
                      layerOrLayerGroup.options.source.options.format === "WFS"
                    ? this.startAddLayerZIndex++
                    : this.startZIndex--,

                /* surcharge: null, */
              }
            );
            // cas du WFS, style par défault à assigner ?

            if (isClusterLayer) {
              generatedLayer = new AnimatedCluster(vectorLayerGroupOptions);
            } else {
              generatedLayer = new VectorLayer(vectorLayerGroupOptions);
            }

            if (this.contextExtensionInfo.wfsBbox) {
              generatedLayer.setExtent(this.contextExtensionInfo.wfsBbox);
            } else {
              generatedLayer.setExtent(undefined);
            }

            const newMapProjection = this.mapProjection;
            newMapProjection.setWorldExtent(this.mapProjection.getExtent());
            generatedLayer.set("projection", newMapProjection);

            // named Style
            if (namedStyle != null) {
              (generatedLayer as VectorLayer<VectorSource<Geometry>>).setStyle(
                namedStyle
              );
            }

            break;
        }

        if (generatedLayer && layerOrLayerGroup.extension) {
          generatedLayer.setProperties(layerOrLayerGroup.extension);
        }

        return generatedLayer;
      }
    }
    return null;
  };

  getClassName(anObject: object): string {
    return anObject ? anObject.constructor.name : "";
  }

  getProjections(): Observable<any> {
    return this.http.get(
      `${this.environment.serverProj}api/combobox/lexProjection`,
      this.httpOptions
    );
  }
}
