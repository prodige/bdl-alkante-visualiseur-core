import {
  NgModule,
  ModuleWithProviders,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
} from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { VisualiseurCoreComponent } from "./visualiseur-core.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";

/* ========================= Action Buttons Import =========================*/
import { ItemComponent } from "./action-button-dropdown/item/item.component";
import { HeaderComponent } from "./action-button-dropdown/header/header.component";
import { ConnexionButtonComponent } from "./connexion-button/connexion-button.component";

/* ========================= Map Tools =========================*/
import { SidebarComponent } from "./layers-legend/sidebars/sidebar/sidebar.component";
import { MobileSidebarComponent } from "./layers-legend/sidebars/mobile-sidebar/mobile-sidebar.component";
import { ResultBottomBoxComponent } from "./interrogating-results/result-bottom-box/result-bottom-box.component";
import { ResultMobileComponent } from "./interrogating-results/result-mobile/result-mobile.component";
import { LayersLegendComponent } from "./layers-legend/layers-legend/layers-legend.component";
import { BaseMapDropdownComponent } from "./map-tools/base-map-dropdown/base-map-dropdown.component";
import { MapScaleDropdownComponent } from "./map-tools/map-scale-dropdown/map-scale-dropdown.component";
import { CopyrightComponent } from "./map-tools/copyright/copyright.component";
import { LogoComponent } from "./map-tools/logo/logo.component";
import { ResultButtonComponent } from "./map-tools/result-button/result-button.component";
import { WmsTimeComponent } from "./map-tools/wms-time/wms-time.component";
import { MapVisualisationComponent } from "./map-tools/map-visualisation/map-visualisation.component";
import { SharingButtonComponent } from "./action-buttons/sharing-button/sharing-button.component";
import { PrintingButtonComponent } from "./action-buttons/printing-button/printing-button.component";
import { DownloadingButtonComponent } from "./action-buttons/downloading-button/downloading-button.component";
import { OpeningButtonComponent } from "./action-buttons/opening-button/opening-button.component";
import { SavingButtonComponent } from "./action-buttons/saving-button/saving-button.component";
import { LocatingButtonComponent } from "./action-buttons/locating-button/locating-button.component";
import { MeasuringButtonComponent } from "./action-buttons/measuring-button/measuring-button.component";
import { InterrogatingButtonComponent } from "./action-buttons/interrogating-button/interrogating-button.component";
import { AnnotatingButtonComponent } from "./action-buttons/annotating-button/annotating-button.component";
import { DrawingButtonComponent } from "./action-buttons/drawing-button/drawing-button.component";
import { NavProjectComponent } from "./nav-project/nav-project.component";
import { PresetZonesContainerComponent } from "./modals/locating-preset-zones-modal/preset-zones-container/preset-zones-container.component";
import { PresetZoneSelectorComponent } from "./modals/locating-preset-zones-modal/preset-zones-container/preset-zone-selector/preset-zone-selector.component";
import { PresetZoneFirstSelectorComponent } from "./modals/locating-preset-zones-modal/preset-zones-container/preset-zone-first-selector/preset-zone-first-selector.component";
import { ChoseServerComponent } from "./modals/add-layer-modal/chose-server/chose-server.component";
import { CguDisplayComponent } from "./cgu-display/cgu-display.component";
import { ActionButtonsComponent } from "./action-buttons/action-buttons.component";
import { AddLayerComponent } from "./layers-legend/sidebars/add-layer/add-layer.component";
import { ResultContentComponent } from "./interrogating-results/result-modal/result-content/result-content.component";
import { ResearchComponent } from "./map-tools/research/research.component";
import { ZoomInComponent } from "./map/buttons/zoom-in/zoom-in.component";
import { OlMapComponent } from "./map/ol-map/ol-map.component";
import { DrawingPolygonComponent } from "./map/tools/annotating-tools/drawing-polygon/drawing-polygon.component";
import { MeasuringToolsComponent } from "./map/tools/measuring-tools/measuring-tools.component";
import { DrawingLinestringComponent } from "./map/tools/annotating-tools/drawing-linestring/drawing-linestring.component";
import { DrawingSymbolComponent } from "./map/tools/annotating-tools/drawing-symbol/drawing-symbol.component";
import { DrawingCircleComponent } from "./map/tools/annotating-tools/drawing-circle/drawing-circle.component";
import { GeolocationButtonComponent } from "./map-tools/geolocation-button/geolocation-button.component";

/* ========================= Modals =========================*/
import { ResultModalComponent } from "./interrogating-results/result-modal/result-modal.component";
import { SharingModalComponent } from "./modals/sharing-modal/sharing-modal.component";
import { PrintingExportModalComponent } from "./modals/printing-export-modal/printing-export-modal.component";
import { PrintingPdfModalComponent } from "./modals/printing-pdf-modal/printing-pdf-modal.component";
import { PrintingImageModalComponent } from "./modals/printing-image-modal/printing-image-modal.component";
import { DownloadingModalComponent } from "./modals/downloading-modal/downloading-modal.component";
import { OpeningModalComponent } from "./modals/opening-modal/opening-modal.component";
import { SavingModalComponent } from "./modals/saving-modal/saving-modal.component";
import { LocatingManageModalComponent } from "./modals/locating-manage-modal/locating-manage-modal.component";
import { LocatingAddModalComponent } from "./modals/locating-add-modal/locating-add-modal.component";
import { LocatingPresetZonesModalComponent } from "./modals/locating-preset-zones-modal/locating-preset-zones-modal.component";
import { MeasuringModalComponent } from "./map/tools/measuring-tools/measuring-modal/measuring-modal.component";
import { InterrogatingModalComponent } from "./map/tools/interrogating-tools/interrogating-modal/interrogating-modal.component";
import { AnnotatingModalComponent } from "./map/tools/annotating-tools/annotating-modal/annotating-modal.component";
import { PreEditingModalComponent } from "./map/tools/editing-tools/pre-editing-modal/pre-editing-modal.component";
import { EditingModalComponent } from "./map/tools/editing-tools/pre-editing-modal/editing-modal/editing-modal.component";
import { SharingContentModalComponent } from "./modals/sharing-modal/sharing-content-modal/sharing-content-modal.component";
import { OpeningContentModalComponent } from "./modals/opening-modal/opening-content-modal/opening-content-modal.component";
import { GenerateStyleDrawsModalComponent } from "./map/tools/annotating-tools/style-draws/style-draws-modal/generate-style-draws-modal/generate-style-draws-modal.component";
import { MergeFicheModalComponent } from "./modals/merge-fiche-modal/merge-fiche-modal.component";
import { InterrogatingGrantedRequestModalComponent } from "./modals/interrogating-granted-request-modal/interrogating-granted-request-modal.component";
import { InterrogatingBufferModalComponent } from "./modals/interrogating-buffer-modal/interrogating-buffer-modal.component";
import { ExportLegendModalComponent } from "./modals/printing-export-modal/export-legend-modal/export-legend-modal.component";
import { ImportFileModalComponent } from "./modals/import-file-modal/import-file-modal.component";
import { HelpingModalComponent } from "./modals/helping-modal/helping-modal.component";
import { AddLayerModalComponent } from "./modals/add-layer-modal/add-layer-modal.component";
import { StyleDrawsModalComponent } from "./map/tools/annotating-tools/style-draws/style-draws-modal/style-draws-modal.component";
import { InsertTextModalComponent } from "./map/tools/annotating-tools/insert-text/insert-text-modal/insert-text-modal.component";
import { BuffersModalComponent } from "./map/tools/annotating-tools/buffers/buffers-modal/buffers-modal.component";
import { ResultModalFicheComponent } from "./interrogating-results/result-modal-fiche/result-modal-fiche.component";
import { ModalConfirmComponent } from "./modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import { SearchMpaModalComponent } from "./modals/search-mpa-modal/search-mpa-modal.component";
import { InterrogatingMailingModalComponent } from "./modals/interrogating-mailing-modal/interrogating-mailing-modal.component";

/* ========================= Other modules =========================*/
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MapModule } from "./map/map.module";
import { NgxWebstorageModule } from "ngx-webstorage";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { DragulaModule } from "ng2-dragula";
import { TreeviewModule } from "ngx-treeview";
import { NgxBootstrapSliderModule } from "@marmotteintrepide/ngx-bootstrap-slider";
import { ColorPickerModule } from "ngx-color-picker";
/* import { NgxTextEditorModule } from 'ngx-text-editor'; */
import { AngularEditorModule } from "@kolkov/angular-editor";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";

import {ChartComponent} from './chart/chart.component';
import {InformationSheetFilterPipe } from './pipes/information-sheet-filter.pipe';
import {NumberToLabelPipe} from './pipes/number-to-label.pipe';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CommonModule,
    MapModule,
    HttpClientModule,
    HttpClientJsonpModule,
    NgxWebstorageModule.forRoot(),
    DragulaModule,
    NgxBootstrapSliderModule,
    TreeviewModule.forRoot(),
    ColorPickerModule,
    NgMultiSelectDropDownModule.forRoot(),
    /* NgxTextEditorModule */ AngularEditorModule,
  ],
  declarations: [
    ItemComponent,
    HeaderComponent,
    SidebarComponent,
    MobileSidebarComponent,
    ResultBottomBoxComponent,
    ResultMobileComponent,
    VisualiseurCoreComponent,
    ConnexionButtonComponent,
    LayersLegendComponent,
    BaseMapDropdownComponent,
    MapScaleDropdownComponent,
    CopyrightComponent,
    LogoComponent,
    ResultModalComponent,
    ResultButtonComponent,
    WmsTimeComponent,
    MapVisualisationComponent,
    SharingModalComponent,
    SharingButtonComponent,
    PrintingExportModalComponent,
    PrintingPdfModalComponent,
    PrintingImageModalComponent,
    PrintingButtonComponent,
    DownloadingModalComponent,
    DownloadingButtonComponent,
    OpeningModalComponent,
    OpeningButtonComponent,
    SavingModalComponent,
    SavingButtonComponent,
    LocatingManageModalComponent,
    LocatingAddModalComponent,
    LocatingPresetZonesModalComponent,
    LocatingButtonComponent,
    MeasuringModalComponent,
    MeasuringButtonComponent,
    InterrogatingModalComponent,
    InterrogatingButtonComponent,
    AnnotatingModalComponent,
    AnnotatingButtonComponent,
    PreEditingModalComponent,
    EditingModalComponent,
    DrawingButtonComponent,
    NavProjectComponent,
    PresetZonesContainerComponent,
    PresetZoneSelectorComponent,
    PresetZoneFirstSelectorComponent,
    SharingContentModalComponent,
    OpeningContentModalComponent,
    ChoseServerComponent,
    GenerateStyleDrawsModalComponent,
    MergeFicheModalComponent,
    InterrogatingGrantedRequestModalComponent,
    InterrogatingBufferModalComponent,
    ExportLegendModalComponent,
    ImportFileModalComponent,
    HelpingModalComponent,
    AddLayerModalComponent,
    StyleDrawsModalComponent,
    InsertTextModalComponent,
    BuffersModalComponent,
    ResultModalFicheComponent,
    CguDisplayComponent,
    ActionButtonsComponent,
    ModalConfirmComponent,
    AddLayerComponent,
    SearchMpaModalComponent,
    ResultContentComponent,
    ResearchComponent,
    InterrogatingMailingModalComponent,
    GeolocationButtonComponent,
    ChartComponent,
    InformationSheetFilterPipe,
    NumberToLabelPipe
  ],
  providers: [
    NgbActiveModal
  ],
  exports: [
    ItemComponent,
    HeaderComponent,
    SidebarComponent,
    MobileSidebarComponent,
    ResultBottomBoxComponent,
    ResultMobileComponent,
    VisualiseurCoreComponent,
    ConnexionButtonComponent,
    LayersLegendComponent,
    BaseMapDropdownComponent,
    MapScaleDropdownComponent,
    CopyrightComponent,
    LogoComponent,
    ResultModalComponent,
    ResultButtonComponent,
    WmsTimeComponent,
    MapVisualisationComponent,
    SharingModalComponent,
    SharingButtonComponent,
    PrintingExportModalComponent,
    PrintingPdfModalComponent,
    PrintingImageModalComponent,
    PrintingButtonComponent,
    DownloadingModalComponent,
    DownloadingButtonComponent,
    OpeningModalComponent,
    OpeningButtonComponent,
    SavingModalComponent,
    SavingButtonComponent,
    LocatingManageModalComponent,
    LocatingAddModalComponent,
    LocatingPresetZonesModalComponent,
    LocatingButtonComponent,
    MeasuringModalComponent,
    MeasuringButtonComponent,
    InterrogatingModalComponent,
    InterrogatingButtonComponent,
    AnnotatingModalComponent,
    AnnotatingButtonComponent,
    PreEditingModalComponent,
    EditingModalComponent,
    DrawingButtonComponent,
    NavProjectComponent,
    PresetZonesContainerComponent,
    PresetZoneSelectorComponent,
    PresetZoneFirstSelectorComponent,
    SharingContentModalComponent,
    OpeningContentModalComponent,
    ChoseServerComponent,
    GenerateStyleDrawsModalComponent,
    MergeFicheModalComponent,
    InterrogatingGrantedRequestModalComponent,
    InterrogatingBufferModalComponent,
    ExportLegendModalComponent,
    ImportFileModalComponent,
    HelpingModalComponent,
    AddLayerModalComponent,
    StyleDrawsModalComponent,
    InsertTextModalComponent,
    BuffersModalComponent,
    ResultModalFicheComponent,
    CguDisplayComponent,
    ActionButtonsComponent,
    ModalConfirmComponent,
    AddLayerComponent,
    SearchMpaModalComponent,
    ResultContentComponent,
    ResearchComponent,
    InterrogatingMailingModalComponent,
    GeolocationButtonComponent,
    ChartComponent,
    InformationSheetFilterPipe,
    NumberToLabelPipe
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class VisualiseurCoreModule {
  static forRoot(environment: any): ModuleWithProviders<any> {
    return {
      ngModule: VisualiseurCoreModule,
      providers: [
        ItemComponent,
        { provide: 'environment', useValue: environment },
      ]
    };
  }
}
