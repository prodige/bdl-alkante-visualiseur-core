import { Component, OnInit, OnDestroy } from '@angular/core';
import { FeatureCollection } from '../models/context';
import { VisualiseurCoreService } from '../visualiseur-core.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alk-nav-project',
  templateUrl: './nav-project.component.html',
  styleUrls: ['./nav-project.component.scss']
})
export class NavProjectComponent implements OnInit, OnDestroy {

  isCollapsed = false;
  context: FeatureCollection;
  contextSubscription: Subscription;

  navItems: [];

  constructor(private coreService: VisualiseurCoreService) { }

  ngOnInit() {
    this.contextSubscription = this.coreService.getContext().subscribe(
      context => {
        if(!context){
          return false;
        }
        this.context = context;

        this.navItems = context.properties.extension.MapRelatedProjects;

      }, error => console.error(error)
    );
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
  }

}
