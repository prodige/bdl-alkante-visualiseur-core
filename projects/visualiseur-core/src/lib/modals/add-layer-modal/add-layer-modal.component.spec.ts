import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLayerModalComponent } from './add-layer-modal.component';

describe('AddLayerModalComponent', () => {
  let component: AddLayerModalComponent;
  let fixture: ComponentFixture<AddLayerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLayerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLayerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
