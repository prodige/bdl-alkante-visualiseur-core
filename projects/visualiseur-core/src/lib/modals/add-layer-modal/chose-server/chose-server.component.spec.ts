import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoseServerComponent } from './chose-server.component';

describe('ChoseServerComponent', () => {
  let component: ChoseServerComponent;
  let fixture: ComponentFixture<ChoseServerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoseServerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoseServerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
