import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintingImageModalComponent } from './printing-image-modal.component';

describe('PrintingImageModalComponent', () => {
  let component: PrintingImageModalComponent;
  let fixture: ComponentFixture<PrintingImageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintingImageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintingImageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
