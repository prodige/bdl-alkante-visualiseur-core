import { DrawingBoxRatioComponent } from "./../../map/tools/annotating-tools/drawing-box-ratio/drawing-box-ratio.component";
import { MapService } from "./../../map/map.service";
import { FeatureCollection } from "./../../models/context";
import { VisualiseurCoreService } from "./../../visualiseur-core.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  NgbActiveModal,
  NgbModalRef,
  NgbModal,
} from "@ng-bootstrap/ng-bootstrap";
import { Subscription, from } from "rxjs";
import Collection from "ol/Collection";
import LayerGroup from "ol/layer/Group";
import BaseLayer from "ol/layer/Base";
import { Options as LayerGroupOptions } from "ol/layer/Group";
@Component({
  selector: "alk-printing-image-modal",
  templateUrl: "./printing-image-modal.component.html",
  styleUrls: ["./printing-image-modal.component.scss"],
})
export class PrintingImageModalComponent implements OnInit, OnDestroy {
  public fileFormat: "png" | "jpg" = "png";
  private mapId: string;
  private refModalOpened: NgbModalRef;
  private contextSubscription: Subscription;
  private newContext: FeatureCollection;
  private context: FeatureCollection;

  private rootLayers: any;
  private layersRootSubscription: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    public mapService: MapService,
    private coreService: VisualiseurCoreService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.mapService.mentionText = null;

    this.layersRootSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((root) => {
        this.rootLayers = root;
      });

    this.contextSubscription = this.coreService.getContext().subscribe(
      (context) => {
        if (!context) {
          return false;
        }
        this.context = context;
        this.newContext = Object.assign({}, this.context);
        // this.newContext = JSON.parse(JSON.stringify(this.context));
        this.mapService.mapTitlePrint = context.properties.title;
        this.mapService.mapSubtitlePrint = context.properties.subtitle;

        let rootLayerGroup: LayerGroup = this.rootLayerSearchRecursive(
          this.rootLayers.getLayers()
        );
        this.newContext.properties.extension.layers.layers =
          this.coreService.contextLayersAndLayerGroups(
            rootLayerGroup.getLayers()
          );
      },
      (error) => console.error(error)
    );
  }

  rootLayerSearchRecursive(
    root: Collection<BaseLayer>,
    layerGroup: LayerGroup = null
  ): LayerGroup {
    const layersList = [];
    root.getArray().forEach((rootLayer) => {
      if (rootLayer.get("class") === "LayerGroup") {
        rootLayer = this.rootLayerSearchRecursive(
          (rootLayer as unknown as LayerGroup).getLayers(),
          rootLayer as LayerGroup
        );
        if ((rootLayer as LayerGroup).getLayers().getArray().length !== 0) {
          layersList.push(rootLayer);
        }
      } else {
        if (rootLayer.get("visible") || rootLayer.get("visibleChecked")) {
          layersList.push(rootLayer);
        }
      }
    });
    return new LayerGroup({
      name:
        layerGroup && layerGroup.get("name") ? layerGroup.get("name") : null,
      layers: layersList,
    } as LayerGroupOptions);
  }

  selectFileFormat(format: "png" | "jpg") {
    this.fileFormat = format;
  }

  public startSelectExportArea() {
    if (!this.refModalOpened) {
      this.mapService.shapeRatioPrint = 1;
      this.coreService.setMapTool("drawShapeRatio");

      this.modalService.dismissAll();

      this.refModalOpened = this.modalService.open(DrawingBoxRatioComponent, {
        size: "sm",
        windowClass: "window-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });

      this.refModalOpened.componentInstance.setMapId(this.mapId || "main");
      this.refModalOpened.result.then(
        (close) => {
          this.refModalOpened = this.modalService.open(
            PrintingImageModalComponent,
            {
              size: "lg",
              centered: true,
              windowClass: "window-xl",
              container: "#modals",
            }
          );
          if (this.refModalOpened.componentInstance.setMapId) {
            this.refModalOpened.componentInstance.setMapId(
              this.mapId || "main"
            );
          }
        },
        (dismiss) => {}
      );
    }
  }

  public prepareImpressionRequest() {
    const modelImpress = {
      img: null,
      title: null,
      text: null,
      selected: true,
      ratio: {
        A3: 0.5,
        A4: 1,
      },
      file: "modele_img.tpl",
    };
    // On met à jour les valeurs pour les layers
    this.newContext.properties.extension.Layout.PrintModels.push(modelImpress);

    this.mapService.imageFormat = "A4";
    this.mapService.startImpressionRequest(
      this.fileFormat,
      this.newContext,
      modelImpress
    );
  }

  public setMapId(mapId: string): void {
    this.mapId = mapId;
  }

  ngOnDestroy(): void {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
      this.contextSubscription = null;
    }
    if (this.layersRootSubscription) {
      this.layersRootSubscription.unsubscribe();
      this.layersRootSubscription = null;
    }
  }
}
