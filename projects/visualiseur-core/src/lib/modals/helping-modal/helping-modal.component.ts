import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Inject,
  ElementRef,
} from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { VisualiseurCoreService } from "../../visualiseur-core.service";
import { Subscription } from "rxjs";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { LocalStorageService } from "ngx-webstorage";

@Component({
  selector: "alk-helping-modal",
  templateUrl: "./helping-modal.component.html",
  styleUrls: ["./helping-modal.component.scss"],
})
export class HelpingModalComponent implements OnInit, OnDestroy {
  HELP_STORAGE_KEY = "isHelpPillVisible";
  public hideHelpContent: FormGroup;
  @ViewChild("checkboxInput", {read: null, static: true}) checkboxInput;

  contextSubscription: Subscription;
  helpingContent: SafeHtml;

  constructor(
    public activeModal: NgbActiveModal,
    private coreService: VisualiseurCoreService,
    private formBuilder: FormBuilder,
    private localStorage: LocalStorageService,
    private sanitized: DomSanitizer
  ) {}

  ngOnInit() {
    this.contextSubscription = this.coreService.getContext().subscribe(
      (context) => {
        if (!context) {
          return false;
        }
        this.helpingContent = context.properties.extension.MapDescription;
        this.helpingContent = String(this.helpingContent)
          .replace(/&quot;/g, '"')
          .replace(/&#39;/g, "'")
          .replace(/&lt;/g, "<")
          .replace(/&gt;/g, ">")
          .replace(/&amp;/g, "&");
        this.helpingContent = this.sanitized.bypassSecurityTrustHtml(
          String(this.helpingContent)
        );
      },
      (error) => console.error(error)
    );
    this.hideHelpContent = this.formBuilder.group({
      checkboxInput: new FormControl(
        this.localStorage.retrieve(this.HELP_STORAGE_KEY) ? true : false,
        [Validators.required]
      ),
    });
    this.localStorage.observe(this.HELP_STORAGE_KEY).subscribe((isChecked) => {
      this.hideHelpContent
        .get("checkboxInput")
        .setValue(isChecked ? true : false);
    });
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
  }

  closing() {
    if (this.hideHelpContent.get("checkboxInput").value) {
      this.activeModal.close(true);
    } else {
      this.activeModal.close(false);
    }
  }
}
