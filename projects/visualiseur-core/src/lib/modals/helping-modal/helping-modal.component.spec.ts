import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpingModalComponent } from './helping-modal.component';

describe('HelpingModalComponent', () => {
  let component: HelpingModalComponent;
  let fixture: ComponentFixture<HelpingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
