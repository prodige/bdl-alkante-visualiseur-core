import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import {
  DataFeatureType,
  LayerDataResponseType,
} from "../../map/services/interrogating.service";
import { Feature } from "../../models/context";
import { Subscription } from "rxjs";
import { AlertConfirmPromptModalComponent } from "../alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import Collection from "ol/Collection";

@Component({
  selector: "alk-merge-fiche-modal",
  templateUrl: "./merge-fiche-modal.component.html",
  styleUrls: ["./merge-fiche-modal.component.scss"],
})
export class MergeFicheModalComponent implements OnInit {
  /** */
  public featureToMerges: Collection<Feature>;

  /** */
  public featureStructure: Feature = null;

  /** */
  public modalReady = false;

  /** */
  public currentLayerResults: LayerDataResponseType = null;

  /** */
  public currentSelectedFeature: DataFeatureType = null;

  /** */
  public editing = false;

  /** */
  public enumsSubscription: Subscription;

  /** */
  public enum: Array<string>;

  /** */
  public editionConfigSubscription: Subscription;

  /** */
  public editionConfig: any;

  /** */
  public layerEditionConfig: any = null;

  /** */
  public dropdownSettings = {};

  /** */
  public featuresDatas = {};

  /** */
  public numberChoice = [
    { label: "somme", value: "sum" },
    { label: "moyenne", value: "average" },
    { label: "min", value: "min" },
    { label: "max", value: "max" },
  ];

  public isNumericTypes = {
    "double precision": true,
    double: true,
    numeric: true,
    smallint: true,
    real: true,
    bigint: true,
    integer: true,
  };
  /** */
  @ViewChild(AlertConfirmPromptModalComponent, {read: null, static: true})
  confirmModal: AlertConfirmPromptModalComponent;

  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit() {
    this.currentLayerResults.layerQueryFields.forEach((field) => {
      const type = field && field.structure ? field.structure.data_type : null;

      this.featuresDatas[field.FieldName] = [];

      /** On propose de choisir les valeurs entre les features */
      this.featureToMerges.forEach((feature, index) => {
        if (
          typeof feature.get(field.FieldName) !== "undefined" &&
          field.FieldName !== "gid"
        ) {
          this.featuresDatas[field.FieldName].push({
            value: feature.get(field.FieldName),
            label: feature.get("gid")
              ? feature.get("gid")
              : "(nouvelle feature " + index + ")",
          });
        }
      });

      // pour les type numeriques on propose des choix suplémentaires
      if (this.isNumericTypes[type]) {
        this.numberChoice.forEach((choice) => {
          this.featuresDatas[field.FieldName].push({
            value: choice.label,
            label: null,
          });
        });
      }
    });
  }

  closeModal(isCancel): void {
    this.activeModal.close({ feature: this.currentSelectedFeature, isCancel });
  }

  saveAndCloseModal(): void {
    this.confirmModal.openModal().then((result) => {
      Object.entries(this.currentSelectedFeature).forEach(([key, value]) => {
        this.currentLayerResults.layerQueryFields.some((field) => {
          if (field.FieldName !== key) {
            return false;
          }
          const type =
            field && field.structure ? field.structure.data_type : null;
          if (this.isNumericTypes[type]) {
            let fctType = null;
            this.numberChoice.some((choice) => {
              fctType = choice.label === value ? choice.value : null;
              return !!fctType;
            });

            if (fctType) {
              const newValue = this.procesNumeric(fctType, field.FieldName);
              this.currentSelectedFeature[key] =
                newValue !== null ? newValue : value;
            }
          }

          return true;
        });
      });
      if (result) {
        this.activeModal.close({
          feature: this.currentSelectedFeature,
          isCancel: false,
        });
      }
    });
  }

  procesNumeric(
    fctType: "sum" | "average" | "min" | "max",
    fieldName: string
  ): number {
    const values = [];
    let result = null;
    this.featureToMerges.forEach((feature, index) => {
      if (typeof feature.get(fieldName) !== "undefined") {
        values.push(feature.get(fieldName) * 1.0);
      }
    });

    switch (fctType) {
      case "sum":
        result = values.reduce(
          (accumulator, currentValue) => accumulator + currentValue
        );
        break;
      case "average":
        result = values.reduce(
          (accumulator, currentValue) => accumulator + currentValue
        );
        result /= values.length;
        break;
      case "min":
        result = Math.min(...values);
        break;
      case "max":
        result = Math.max(...values);
        break;
      default:
        result = null;
    }

    return result;
  }
}
