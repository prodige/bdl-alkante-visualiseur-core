import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeFicheModalComponent } from './merge-fiche-modal.component';

describe('MergeFicheModalComponent', () => {
  let component: MergeFicheModalComponent;
  let fixture: ComponentFixture<MergeFicheModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeFicheModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeFicheModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
