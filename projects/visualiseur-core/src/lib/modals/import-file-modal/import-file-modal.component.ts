import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Host,
  Optional,
  OnDestroy,
} from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import Map from "ol/Map";
import { MapService } from "../../map/map.service";
import { MapIdService } from "../../map/map-id.service";
import { HttpClient } from "@angular/common/http";
import { EnvCoreService } from "../../env.core.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import {
  FileWfsReturned,
  FileReturned,
  BaseLayerConfig,
} from "../add-layer-modal/add-layer-modal.component";
import VectorLayer from "ol/layer/Vector";
import { saveAs } from "file-saver";
import GeoJSON from "ol/format/GeoJSON";
import { Geometry } from "ol/geom";
import VectorSource from "ol/source/Vector";

@Component({
  selector: "alk-import-file-modal",
  templateUrl: "./import-file-modal.component.html",
  styleUrls: ["./import-file-modal.component.scss"],
})
export class ImportFileModalComponent implements OnInit, OnDestroy {
  mapId: string | MapIdService;
  map: Map;
  layerFields = null;
  layerIdx = null;
  gidField = "gid";
  isLoading = false;

  loadingFailed = false;
  formImport: FormGroup;
  filesToUpload = new FormData();
  acceptedFileTypes =
    ".shp, .mif, .mid, .tab, .dat, .id, .map, .geojson, .kml, .csv, .shx, .dbf, .prj, .ods, .xls";

  postedFile: FileReturned | FileWfsReturned;

  csvColumns = [];
  editableVectorLayer: VectorLayer<VectorSource<Geometry>>;

  @ViewChild("labelImport", {read: null, static: true}) labelImport: ElementRef;

  constructor(
    private mapService: MapService,
    private http: HttpClient,
    public activeModal: NgbActiveModal,
    private environment: EnvCoreService,
    private mapIdService: MapIdService
  ) {
    this.formImport = new FormGroup({
      importFile: new FormControl("", Validators.required),
    });
  }

  ngOnInit(): void {
    this.map = this.mapService.getMap(
      this.mapIdService.getId() || this.mapId || "main"
    );
  }

  ngOnDestroy(): void {}

  closeModal(): void {}

  setMapId(id: string | MapIdService): void {
    this.mapId = id;
  }

  onFileChange(files: FileList): void {
    this.filesToUpload = new FormData();
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map((f) => f.name)
      .join(", ");
    Array.from(files).map((f) => {
      this.filesToUpload.append(f.name, f);
    });
  }

  loadFile(): void {
    this.loadingFailed = false;
    const mapExtent = this.map.getView().calculateExtent();
    this.isLoading = true;
    this.http
      .post(
        `${this.environment.serverUrl}core/upload/upload?${
          ["fr", "en"].includes(window.location.pathname.split("/")[1])
            ? window.location.pathname.split("/")[2]
            : window.location.pathname.split("/")[1]
        }`,
        this.filesToUpload,
        {
          params: {
            srsMap: this.map.getView().getProjection().getCode(),
            extentMinx: mapExtent[0].toString(),
            extentMiny: mapExtent[1].toString(),
            extentMaxx: mapExtent[2].toString(),
            extentMaxy: mapExtent[3].toString(),
            to_editing: "1",
          },
        }
      )
      .subscribe((response: any) => {
        this.isLoading = false;
        if (response.success) {
          this.postedFile = response as FileReturned;
          if (
            (response.data && response.type === "json") ||
            response.type === "geojson"
          ) {
            this.activeModal.close({
              source: response,
              gidField: this.gidField,
            });
          } else if (
            this.postedFile.filename &&
            this.postedFile.filename.split(".")[1] === "csv"
          ) {
            Object.values(this.postedFile.columns).forEach((column, key) => {
              this.csvColumns.push(key, column);
            });
          } else if (!this.postedFile.filename) {
            this.postedFile = response as FileWfsReturned;
            this.addLayerFromFile(this.postedFile);
            this.csvColumns = [];
          }
        } else {
          this.loadingFailed = true;
        }
      });
  }

  addLayerFromFile(file: FileWfsReturned): void {
    let finalUrl = "";

    const params = {
      service: "WFS",
      version: "1.1.0",
      request: "GetFeature",
      typename: null,
      srsname: this.map.getView().getProjection().getCode(),
      outputFormat: "geojson",
    };

    finalUrl = file.data.urlWfs ? file.data.urlWfs : "";
    params.typename = "temp_" + file.data.uniqueId;

    this.http.get(finalUrl, { params }).subscribe((response) => {
      this.activeModal.close({ source: response, gidField: this.gidField });
    });
  }

  public exportGeojsonExemple(): void {
    if (this.editableVectorLayer) {
      const features = this.editableVectorLayer.getSource().getFeatures();

      const feature = features.length ? features[0] : null;
      if (feature) {
        const formatGeojson = new GeoJSON();
        const dataToFile = {
          geojson: formatGeojson.writeFeaturesObject([feature]),
          crs:{
            type: "name",
            properties: {
              name: this.map.getView().getProjection().getCode(),
            },
          }
        }

        dataToFile.geojson.features.forEach((featureToSet, idx) => {
          Object.entries(featureToSet.properties).forEach(([key, value]) => {
            if (typeof value === "object" && value instanceof Array) {
              dataToFile.geojson.features[idx].properties[key] = value.join();
            }
          });
        });

        const file = new Blob(
          [
            //new Uint8Array([0xEF, 0xBB, 0xBF]), // UTF-8 BOM
            JSON.stringify(dataToFile),
          ],
          { type: "text/json" }
        );

        saveAs(file, "exemple.geojson");
      }
    }
  }
}
