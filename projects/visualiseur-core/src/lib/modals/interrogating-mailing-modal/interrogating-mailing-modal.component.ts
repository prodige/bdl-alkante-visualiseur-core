import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
  AfterViewInit,
  forwardRef,
} from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LayerDataResponseType } from "../../map/services/interrogating.service";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { Subscription } from "rxjs";
import { UserConnectionService } from "../../services/user-connection.service";
import { InterrogatingMailingService } from "../../services/interrogating-mailing.service";
import { AlertConfirmPromptModalComponent } from "../alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: "alk-interrogating-mailing-modal",
  templateUrl: "./interrogating-mailing-modal.component.html",
  styleUrls: ["./interrogating-mailing-modal.component.scss"],
})
export class InterrogatingMailingModalComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "0",
    maxHeight: "auto",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    defaultParagraphSeparator: "",
    defaultFontName: "",
    defaultFontSize: "",
    fonts: [
      { class: "arial", name: "Arial" },
      { class: "times-new-roman", name: "Times New Roman" },
      { class: "calibri", name: "Calibri" },
      { class: "comic-sans-ms", name: "Comic Sans MS" },
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: "redText",
        class: "redText",
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ],
    uploadUrl: "v1/image",
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: "top",
    toolbarHiddenButtons: [["bold", "italic"], ["fontSize"]],
  };
  mapName: string | number;
  mailingSubscription: Subscription;
  coreServiceSubscription: Subscription;
  isModelManage = false;
  httpOptions = {
    withCredentials: true,
  };
  currentLayerResults: LayerDataResponseType;
  mailingForm = new FormGroup({
    selectedLayer: new FormControl("", Validators.required),
    toField: new FormControl("", Validators.required),
    toMail: new FormControl("", Validators.required),
    fromField: new FormControl("", Validators.required),
    fromMail: new FormControl("", Validators.required),
    savedModel: new FormControl("", Validators.required),
    mailingObject: new FormControl("", Validators.required),
    mailingMessage: new FormControl("", Validators.required),
    mailingFields: new FormControl("", Validators.required),
    checkbox: new FormControl(false, Validators.required),
    modelName: new FormControl("", Validators.required),
  });
  toNames = [];
  toMails = [];
  mailingTool: any;
  savedModels = [];
  selectedModelId: any;
  selectedModel: any;
  @ViewChild("checkboxInput", { read: null, static: true })
  checkboxInput: ElementRef;
  editorPlaceholder = "";
  /* editorConfig = {
    editable: true,
    spellcheck: true,
    height: '200px',
    minHeight: '200px',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Tapez votre texte ici...',
    imageEndPoint: '',
    toolbar: [
        ['bold', 'italic', 'underline', 'orderedList', 'unorderedList'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['cut', 'copy', 'removeFormat', 'undo', 'redo']
    ]
  }; */
  showToastSendSuccess = false;
  showToastSendError = false;
  showToastTestSuccess = false;
  showToastTestError = false;

  // Récupération des élements nécessaires à la traduction
  @ViewChild("editorConfigPlaceholder", { read: null, static: true })
  editorConfigPlaceholder: ElementRef;
  // Récupération du composant de confirmation
  @ViewChild(AlertConfirmPromptModalComponent, { read: null, static: true })
  confirmModal: AlertConfirmPromptModalComponent;

  constructor(
    public mailingService: InterrogatingMailingService,
    private userConnectionService: UserConnectionService,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    this.mailingForm = this.formBuilder.group({
      selectedLayer: ["", Validators.required],
      toField: ["", Validators.required],
      toMail: ["", Validators.required],
      fromField: ["", Validators.required],
      fromMail: ["", Validators.required],
      savedModel: ["", Validators.required],
      mailingObject: ["", Validators.required],
      mailingMessage: ["", Validators.required],
      mailingFields: ["", Validators.required],
      checkbox: [false, Validators.required],
      modelName: ["", Validators.required],
    });
  }

  ngOnInit() {
    this.coreServiceSubscription = this.userConnectionService
      .getUserConfig()
      .subscribe((user) => {
        this.isModelManage =
          user.userInternet !== null ? !user.userInternet : null;
        this.mailingForm
          .get("fromField")
          .setValue(`${user.name} ${user.firstName}`);
        this.mailingForm.get("fromMail").setValue(user.email);
        this.mailingForm.get("mailingMessage").setValue("");
        this.mailingSubscription = this.mailingService
          .getModelList(this.currentLayerResults)
          .subscribe((list) => {
            this.savedModels = [];
            list.forEach((model) => {
              this.savedModels.push(model);
            });
          });
      });

    this.selectedModelId = "noPresetSelected";
    this.selectedModel = null;

    this.mailingForm.get("toField").setValue("noPresetSelected");
    this.mailingForm.get("toMail").setValue("noPresetSelected");
    this.mailingForm.get("savedModel").setValue("noPresetSelected");
  }

  ngAfterViewInit() {
    this.editorPlaceholder = (
      this.editorConfigPlaceholder.nativeElement as HTMLInputElement
    ).value;
  }

  ngOnDestroy() {
    if (this.coreServiceSubscription) {
      this.coreServiceSubscription.unsubscribe();
    }
    if (this.mailingSubscription) {
      this.mailingSubscription.unsubscribe();
    }
  }

  setSelectedLayer(layerResult: LayerDataResponseType) {
    this.currentLayerResults = layerResult;
    let mailingFields = "";
    layerResult.layerQueryFields.forEach((queryField, index) => {
      mailingFields += `${index === 0 ? "" : ", "}{$${
        queryField.FieldName
      }} : ${queryField.FieldAlias}`;
      this.toNames.push(queryField.FieldName);
      this.toMails.push(queryField.FieldName);
    });
    this.mailingForm.get("mailingFields").setValue(mailingFields);
  }

  setMailingTool(tool: any) {
    this.mailingTool = tool;
  }

  selectModel(modelId: any) {
    this.selectedModelId = modelId;
    const savedModels = this.savedModels.filter(
      (model) => model.pk_modele_id === modelId
    );
    this.selectedModel = savedModels.length > 0 ? savedModels[0] : null;
    if (this.selectedModel) {
      this.mailingForm
        .get("mailingObject")
        .setValue(this.selectedModel.msg_title);
      this.mailingForm
        .get("mailingMessage")
        .setValue(this.selectedModel.msg_body);
      this.mailingForm
        .get("modelName")
        .setValue(this.selectedModel.modele_name);
    }
  }

  updateModel() {
    this.mailingService
      .updateModel(
        this.mailingForm,
        this.currentLayerResults,
        this.savedModels,
        this.selectedModel
      )
      .subscribe((updatedModel) => {
        this.savedModels = [];
        this.mailingService
          .getModelList(this.currentLayerResults)
          .subscribe((list) => {
            (list as any).forEach((model) => {
              this.savedModels.push(model);
            });
            this.selectModel(updatedModel.pk_modele_id);
            this.mailingForm.get("checkbox").setValue(false);
          });
      });
  }

  deleteModel() {
    this.confirmModal.openModal().then((result) => {
      if (result) {
        this.mailingService
          .deleteModel(this.selectedModel)
          .subscribe((response) => {
            this.savedModels = [];
            this.mailingService
              .getModelList(this.currentLayerResults)
              .subscribe((list) => {
                (list as any).forEach((model) => {
                  this.savedModels.push(model);
                });
                this.selectModel("noPresetSelected");
                this.mailingForm.get("checkbox").setValue(false);
              });
          });
        this.activeModal.close("save");
      }
    });
  }

  sendMail(mode: string, form: FormGroup, results: LayerDataResponseType) {
    this.mailingService.mailing(mode, form, results).subscribe((response) => {
      if (mode === "test") {
        this.showToastTestSuccess = response.success;
        this.showToastTestError = !response.success;
      } else {
        this.showToastSendSuccess = response.success;
        this.showToastSendError = !response.success;
      }
      setTimeout(() => {
        this.showToastTestSuccess = false;
        this.showToastTestError = false;
        this.showToastSendSuccess = false;
        this.showToastSendError = false;
      }, 2000);
    });
  }
}
