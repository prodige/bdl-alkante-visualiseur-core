import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingMailingModalComponent } from './interrogating-mailing-modal.component';

describe('InterrogatingMailingModalComponent', () => {
  let component: InterrogatingMailingModalComponent;
  let fixture: ComponentFixture<InterrogatingMailingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingMailingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingMailingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
