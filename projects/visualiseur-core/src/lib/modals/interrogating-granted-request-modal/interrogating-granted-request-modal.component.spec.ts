import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingGrantedRequestModalComponent } from './interrogating-granted-request-modal.component';

describe('InterrogatingGrantedRequestModalComponent', () => {
  let component: InterrogatingGrantedRequestModalComponent;
  let fixture: ComponentFixture<InterrogatingGrantedRequestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingGrantedRequestModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingGrantedRequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
