import { EnvCoreService } from "./../../env.core.service";
import {
  Component,
  OnInit,
  Inject,
  AfterViewInit,
  Input,
  ViewChild,
  TemplateRef,
  ContentChild,
  ElementRef,
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  LayerQueryFieldType,
  InterrogatingService,
} from "../../map/services/interrogating.service";
import { Layer } from "ol/layer";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { Observable, Subject, merge, of, ReplaySubject } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  map,
  filter,
  switchMap,
  catchError,
} from "rxjs/operators";
import { Parser } from "../../parser-requetes-attributaires";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";
import { Geometry } from "ol/geom";
import VectorSource from "ol/source/Vector";
import { BaseLayer } from "ol/layer/Base";

// tslint:disable: max-line-length

type GrantedRequestOperators =
  | "COMMENCE PAR"
  | "NE CONTIENT PAS"
  | "CONTIENT"
  | "="
  | "<>"
  | ">="
  | ">"
  | "<="
  | "<";
type GrantedRequestLogicalOperators = "ET" | "OU";

type GrantedRequest = Array<
  | GrantedRequestRequest
  | GrantedRequestLogicalOperators
  | GrantedRequestSubRequest
>;
interface GrantedRequestRequest {
  fieldId: string;
  selectedOperator: GrantedRequestOperators;
  possibleValues: Array<string>;
  selectedValueId: string;
  searchingValues: boolean;
  errorOnSearchingValues: boolean;
  focus$: ReplaySubject<GrantedRequestRequest>;
  debouncedText$: Observable<string>;
  isOpen: boolean;
  isJustSelectItem: boolean;
}
interface GrantedRequestSubRequest {
  isNegative: boolean;
  subRequest: GrantedRequest;
}

@Component({
  selector: "alk-interrogating-granted-request-modal",
  templateUrl: "./interrogating-granted-request-modal.component.html",
  styleUrls: ["./interrogating-granted-request-modal.component.scss"],
})
export class InterrogatingGrantedRequestModalComponent
  implements OnInit, AfterViewInit
{
  mapHrefPath: string = null;
  modalReady = false;
  mapHrefPathNoRouter: string;

  // @ViewChildren('ngbTypeahed') ngbDistrict: QueryList<NgbTypeahead>;
  protected focusObservableList: Array<ReplaySubject<{ request: GrantedRequestRequest, forceUpdate }>> = [];
  focus$ = new Subject<GrantedRequestRequest>();
  click$ = new Subject<GrantedRequestRequest>();

  operators: Array<{ operatorId: string; operatorLabel: string }> = [
    { operatorId: "COMMENCE PAR", operatorLabel: `commence par` },
    { operatorId: "NE CONTIENT PAS", operatorLabel: `ne contient pas` },
    { operatorId: "CONTIENT", operatorLabel: `contient` },
    { operatorId: "=", operatorLabel: `est égal (=)` },
    { operatorId: "<>", operatorLabel: `est différent (<>)` },
    { operatorId: ">=", operatorLabel: `est supérieur ou égal (>=)` },
    { operatorId: ">", operatorLabel: `est strictement supérieur (>)` },
    { operatorId: "<=", operatorLabel: `est inférieur ou égal (<=)` },
    { operatorId: "<", operatorLabel: `est strictement inférieur (<)` },
  ];

  // Récupération des élements nécessaires à la traduction
  @ViewChild("LogicalOperatorLabelAnd", {read: null, static: true})
  logicalOperatorLabelAnd: ElementRef;
  @ViewChild("LogicalOperatorLabelOr", {read: null, static: true})
  logicalOperatorLabelOr: ElementRef;
  @ViewChild("LogicalOperatorLabelNot", {read: null, static: true})
  logicalOperatorLabelNot: ElementRef;
  // Et pour placeholders des lignes de requetes
  @ViewChild("QueryRowFieldPlaceholder", {read: null, static: true})
  queryRowFieldPlaceholder: ElementRef;
  @ViewChild("QueryRowOperatorPlaceholder", {read: null, static: true})
  queryRowOperatorPlaceholder: ElementRef;
  @ViewChild("QueryRowValuePlaceholder", {read: null, static: true})
  queryRowValuePlaceholder: ElementRef;
  // Et pour les opérateurs
  @ViewChild("OperatorLabelBeginWith", {read: null, static: true})
  operatorLabelBeginWith: ElementRef;
  @ViewChild("OperatorLabelDoesNotContain", {read: null, static: true})
  operatorLabelDoesNotContain: ElementRef;
  @ViewChild("OperatorLabelContain", {read: null, static: true}) operatorLabelContain: ElementRef;
  @ViewChild("OperatorLabelEqual", {read: null, static: true}) operatorLabelEqual: ElementRef;
  @ViewChild("OperatorLabelDifferent", {read: null, static: true})
  operatorLabelDifferent: ElementRef;
  @ViewChild("OperatorLabelGreaterThanOrEqual", {read: null, static: true})
  operatorLabelGreaterThanOrEqual: ElementRef;
  @ViewChild("OperatorLabelStrictlyGreaterThan", {read: null, static: true})
  operatorLabelStrictlyGreaterThan: ElementRef;
  @ViewChild("OperatorLabelLessOrEqual", {read: null, static: true})
  operatorLabelLessOrEqual: ElementRef;
  @ViewChild("OperatorLabelStrictlyLess", {read: null, static: true})
  operatorLabelStrictlyLess: ElementRef;
  @ViewChild("OperatorLabelDefault", {read: null, static: true}) operatorLabelDefault: ElementRef;

  protected readonly logicalOperators: { [operator: string]: string } = {
    ET: "ET",
    OU: "OU",
  };

  possibleLayers: Array<{
    layerId: string;
    layerName: string;
    url: string;
    layer: BaseLayer;
  }> = [];
  possibleFields: Array<LayerQueryFieldType> = [];

  request: {
    compiledGrantedRequest: string;
    subRequest: GrantedRequest;
    selectedLayer: VectorLayer<VectorSource<Geometry>>;
    selectedLayerId: string;
    limitNumberOfLines: boolean;
    maxNumberOfLines: number;
    isNotValid: boolean;
    requestSended: boolean;
  } = {
    compiledGrantedRequest: "",
    subRequest: [],
    selectedLayer: null,
    selectedLayerId: null,
    limitNumberOfLines: false,
    maxNumberOfLines: 10,
    isNotValid: true,
    requestSended: false,
  };
  latestRequestCreate: GrantedRequestRequest = null;

  constructor(
    protected http: HttpClient,
    protected activeModal: NgbActiveModal,
    protected router: Router,
    protected interrogatingService: InterrogatingService,
    protected environment: EnvCoreService
  ) { }

  ngOnInit() {
    const tabUrl = /\/([^\/]+)?\.map/.exec(this.router.url);
    if (tabUrl !== null && typeof tabUrl[1] !== "undefined") {
      this.mapHrefPath = tabUrl[1];
    } else {
      this.mapHrefPath = this.mapHrefPathNoRouter;
    }
  }

  ngAfterViewInit() {
    this.operators.map((opetaror) => {
      let operatorLabel = opetaror.operatorLabel;
      switch (opetaror.operatorId) {
        case "COMMENCE PAR":
          operatorLabel = (
            this.operatorLabelBeginWith.nativeElement as HTMLInputElement
          ).value;
          break;
        case "NE CONTIENT PAS":
          operatorLabel = (
            this.operatorLabelDoesNotContain.nativeElement as HTMLInputElement
          ).value;
          break;
        case "CONTIENT":
          operatorLabel = (
            this.operatorLabelContain.nativeElement as HTMLInputElement
          ).value;
          break;
        case "=":
          operatorLabel = (
            this.operatorLabelEqual.nativeElement as HTMLInputElement
          ).value;
          break;
        case "<>":
          operatorLabel = (
            this.operatorLabelDifferent.nativeElement as HTMLInputElement
          ).value;
          break;
        case ">=":
          operatorLabel = (
            this.operatorLabelGreaterThanOrEqual
              .nativeElement as HTMLInputElement
          ).value;
          break;
        case ">":
          operatorLabel = (
            this.operatorLabelStrictlyGreaterThan
              .nativeElement as HTMLInputElement
          ).value;
          break;
        case "<=":
          operatorLabel = (
            this.operatorLabelLessOrEqual.nativeElement as HTMLInputElement
          ).value;
          break;
        case "<":
          operatorLabel = (
            this.operatorLabelStrictlyLess.nativeElement as HTMLInputElement
          ).value;
          break;
        case null:
        default:
          operatorLabel = (
            this.operatorLabelDefault.nativeElement as HTMLInputElement
          ).value;
          break;
      }
      return {
        operatorId: opetaror.operatorId,
        operatorLabel,
      };
    });

    this.focus$.subscribe((item: GrantedRequestRequest) => {
      if (item && item.focus$) {
        item.isJustSelectItem = false;
        item.isOpen = false;
        item.focus$.next(item);
      }
    });
  }

  searchValue: (text$: Observable<string>) => Observable<string[]> = (
    text$
  ) => {
    if (this.latestRequestCreate !== null) {
      const element = this.latestRequestCreate;
      element.focus$ = new ReplaySubject<GrantedRequestRequest>(null);

      const focusInput$ = element.focus$.pipe(
        map(() => {
          return null;
        })
      );

      const debouncedText$ = (element.debouncedText$ = text$.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        map((term: string) => {
          return term;
        })
      ));

      this.latestRequestCreate = null;

      return merge(debouncedText$, focusInput$).pipe(
        filter((term) => {
          if (element.isJustSelectItem) {
            return false;
          }
          if (term !== null && term.length < 3) {
            return false;
          }
          return term !== null || !element.isOpen;
        }),
        switchMap((term) => {
          element.isOpen = true;
          if (term !== null) {
            element.selectedValueId = term;
          } else {
            term = element.selectedValueId;
          }
          /* if ( term === null || term === '' ) {
            return of([]);
          } else { */
          return this.getPossibleValues(element, term);
          /* } */
        }),
        map((results) => {
          return results;
        })
      );
    } else {
      return of([]);
    }
  };

  selectItem(event, element): void {
    element.selectedValueId = event.item;
    element.isJustSelectItem = true;
    element.isOpen = false;
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  blurSelection(element): void {
    if (element.isOpen) {
      element.isJustSelectItem = true;
      element.isOpen = false;
    }
  }

  updateCompiledGrantedRequest(subRequests?: GrantedRequest): string {
    let compiledGrantedRequestPart = "";
    if (!subRequests) {
      // Sur le premier passage
      this.request.isNotValid = false;
    }

    (subRequests ? subRequests : this.request.subRequest).forEach(
      (subRequest) => {
        if (this.isGrantedRequestRequest(subRequest)) {
          let selectedField = this.possibleFields.find(
            (possibleField) => possibleField.FieldName === subRequest.fieldId
          );
          let selectedOperator = this.operators.find(
            (operator) => operator.operatorId === subRequest.selectedOperator
          );
          let selectedValue = { value: subRequest.selectedValueId };
          // let selectedValue = subRequest.possibleValues.find( possibleValue => possibleValue.valueId === subRequest.selectedValueId );

          // Assignation de valeurs par défaut si non trouvé
          if (!selectedField) {
            const translatedQueryRowFieldPlaceholder = (
              this.queryRowFieldPlaceholder.nativeElement as HTMLInputElement
            ).value;
            selectedField = {
              FieldAlias: translatedQueryRowFieldPlaceholder,
            } as any;
            this.request.isNotValid = true;
          }
          if (!selectedOperator) {
            const translatedQueryRowOperatorPlaceholder = (
              this.queryRowOperatorPlaceholder.nativeElement as HTMLInputElement
            ).value;
            selectedOperator = {
              operatorLabel: translatedQueryRowOperatorPlaceholder,
            } as any;
            this.request.isNotValid = true;
          }
          if (subRequest.selectedValueId === null) {
            this.request.isNotValid = true;
          }
          if (selectedValue === null) {
            const translatedQueryRowValuePlaceholder = (
              this.queryRowValuePlaceholder.nativeElement as HTMLInputElement
            ).value;
            selectedValue = {
              value: translatedQueryRowValuePlaceholder,
            } as any;
            this.request.isNotValid = true;
          }

          // Concaténation de la ligne correspondante
          compiledGrantedRequestPart += `${selectedField.FieldAlias} ${selectedOperator.operatorLabel} ${selectedValue.value}`;
        } else if (this.isGrantedRequestSubRequest(subRequest)) {
          const translatedLogicalOperatorNot = (
            this.logicalOperatorLabelNot.nativeElement as HTMLInputElement
          ).value;
          compiledGrantedRequestPart +=
            `${subRequest.isNegative ? translatedLogicalOperatorNot : ""}(\n` +
            `${this.updateCompiledGrantedRequest(subRequest.subRequest)}` +
            `\n)`;
        } else if (this.isGrantedRequestLogicalOperators(subRequest)) {
          // ET ou OU
          const translatedLogicalOperatorAnd = (
            this.logicalOperatorLabelAnd.nativeElement as HTMLInputElement
          ).value;
          const translatedLogicalOperatorOr = (
            this.logicalOperatorLabelOr.nativeElement as HTMLInputElement
          ).value;
          compiledGrantedRequestPart += `\n ${
            subRequest === "ET"
              ? translatedLogicalOperatorAnd
              : translatedLogicalOperatorOr
          } \n`;
        }
      }
    );

    // Si on est sur l'appel original de la méthode et non l'un des appels récursif, on met à jours le texte
    if (!subRequests) {
      this.request.compiledGrantedRequest = compiledGrantedRequestPart;
    }

    return compiledGrantedRequestPart;
  }

  updateCompiledGrantedRequestWFS(
    subRequests?: GrantedRequest,
    recursiveHelper: { map: Map<string, string>; index: number } = null
  ): string {
    let compiledGrantedRequestPart = "";
    if (recursiveHelper === null) {
      recursiveHelper = {
        map: new Map<string, string>(),
        index: 1,
      };
    }

    (subRequests ? subRequests : this.request.subRequest).forEach(
      (subRequest) => {
        if (this.isGrantedRequestRequest(subRequest)) {
          let selectedField = this.possibleFields.find(
            (possibleField) => possibleField.FieldName === subRequest.fieldId
          );
          let selectedOperator = this.operators.find(
            (operator) => operator.operatorId === subRequest.selectedOperator
          );
          let selectedValue = { value: subRequest.selectedValueId };
          // let selectedValue = subRequest.possibleValues.find( possibleValue => possibleValue.valueId === subRequest.selectedValueId );

          // Assignation de valeurs par défaut si non trouvé
          if (!selectedField) {
            selectedField = { FieldAlias: "[#NOM_CHAMP#]" } as any;
          }
          if (!selectedOperator) {
            selectedOperator = { operatorLabel: "[#OPÉRATEUR#]" } as any;
          }
          if (selectedValue === null) {
            selectedValue = { value: "[#VALEUR#]" } as any;
          }

          let subRequestPart = "";
          let subRequestNameValue =
            `<PropertyName>${subRequest.fieldId}</PropertyName>` +
            `<Literal>${subRequest.selectedValueId}</Literal>`;
          switch (subRequest.selectedOperator) {
            case "<":
              subRequestPart = `<PropertyIsLessThan>${subRequestNameValue}</PropertyIsLessThan>`;
              break;
            case "<=":
              subRequestPart = `<PropertyIsLessThanOrEqualTo>${subRequestNameValue}</PropertyIsLessThanOrEqualTo>`;
              break;
            case "<>":
              subRequestPart = `<PropertyIsNotEqualTo>${subRequestNameValue}</PropertyIsNotEqualTo>`;
              break;
            case "=":
              subRequestPart = `<PropertyIsEqualTo>${subRequestNameValue}</PropertyIsEqualTo>`;
              break;
            case ">":
              subRequestPart = `<PropertyIsGreaterThan>${subRequestNameValue}</PropertyIsGreaterThan>`;
              break;
            case ">=":
              subRequestPart = `<PropertyIsGreaterThanOrEqualTo>${subRequestNameValue}</PropertyIsGreaterThanOrEqualTo>`;
              break;
            case "COMMENCE PAR":
              subRequestNameValue =
                `<PropertyName>${subRequest.fieldId}</PropertyName>` +
                `<Literal>${subRequest.selectedValueId}*</Literal>`;
              subRequestPart = `<PropertyIsLike matchCase="false" wildcard='*' singleChar='.' escape='!'>${subRequestNameValue}</PropertyIsLike>`;
              break;
            case "CONTIENT":
              subRequestNameValue =
                `<PropertyName>${subRequest.fieldId}</PropertyName>` +
                `<Literal>*${subRequest.selectedValueId}*</Literal>`;
              subRequestPart = `<PropertyIsLike matchCase="false" wildcard='*' singleChar='.' escape='!'>${subRequestNameValue}</PropertyIsLike>`;
              break;
            case "NE CONTIENT PAS":
              subRequestNameValue =
                `<PropertyName>${subRequest.fieldId}</PropertyName>` +
                `<Literal>*${subRequest.selectedValueId}*</Literal>`;
              subRequestPart = `<NOT><PropertyIsLike matchCase="false" wildcard='*' singleChar='.' escape='!'>${subRequestNameValue}</PropertyIsLike></NOT>`;
              break;
          }

          // Concaténation de la ligne correspondante
          // const phrase = `${selectedField.FieldName} ${selectedOperator.operatorLabel} ${selectedValue.value}`;
          recursiveHelper.map.set(
            " $" + recursiveHelper.index + " ",
            subRequestPart
          );
          compiledGrantedRequestPart += " $" + recursiveHelper.index + " ";
          recursiveHelper.index++;
        } else if (this.isGrantedRequestSubRequest(subRequest)) {
          compiledGrantedRequestPart +=
            `${subRequest.isNegative ? "NOT" : ""}(` +
            `${this.updateCompiledGrantedRequestWFS(
              subRequest.subRequest,
              recursiveHelper
            )}` +
            `\n)`;
        } else if (this.isGrantedRequestLogicalOperators(subRequest)) {
          compiledGrantedRequestPart += ` ${
            subRequest === "ET" ? "AND" : "OR"
          } `;
        }
      }
    );

    // Si on est sur l'appel original de la méthode et non l'un des appels récursif, on met à jours le texte
    if (!subRequests) {
      let parsedQuery = Parser.parse(compiledGrantedRequestPart);
      recursiveHelper.map.forEach( (value, key) => {
        parsedQuery = (parsedQuery as string).replace(key.replace( ' ', ''), ' ' + value + ' ');
      });

      return parsedQuery;
    }

    return compiledGrantedRequestPart;
  }

  selectLayerId(layerId: string): void {
    // Si la requête est vide, on peut changer la couche
    if (this.request.subRequest.length < 2) {
      this.request.selectedLayerId = layerId;
      const selectedLayer = this.possibleLayers.find(
        (possibleLayer) => possibleLayer.layerId === layerId
      );

      if (selectedLayer) {
        this.request.selectedLayer =
          selectedLayer.layer as unknown as VectorLayer<VectorSource<Geometry>>;
        // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
        this.possibleFields = (
          selectedLayer.layer as unknown as Layer<any>
        ).get("LayerQuery").Fields as Array<LayerQueryFieldType>;
        if (this.request.subRequest.length === 0) {
          // this.addOrInsertSubRequest();
          this.addOrInsertRequest();
        }
      }
    }
  }

  selectFieldId(fieldId: string, request: GrantedRequestRequest): void {
    request.fieldId = fieldId;
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();

    // En fonction du champs sélectionnée, retrouver la liste des valeurs possibles
    request.possibleValues = [];
    request.selectedValueId = null;
    request.searchingValues = false;
    request.errorOnSearchingValues = false;
  }

  toggleNegate(subRequest: GrantedRequestSubRequest): void {
    subRequest.isNegative = !subRequest.isNegative;
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  toggleLogicalOperator(
    index: number,
    parentSubRequest?: GrantedRequestSubRequest
  ): void {
    const parent: GrantedRequest = parentSubRequest
      ? parentSubRequest.subRequest
      : this.request.subRequest;
    // Si il y a déjà un élément dans la requête, on insère un opérateur logique avant
    if (
      this.isGrantedRequestLogicalOperators(parent[index]) &&
      ["ET", "OU"].indexOf(parent[index] as GrantedRequestLogicalOperators) !==
        -1
    ) {
      parent[index] = parent[index] === "ET" ? "OU" : "ET";
    }
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  selectOperator(
    request: GrantedRequestRequest,
    operator: GrantedRequestOperators
  ): void {
    request.selectedOperator = operator;
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  selectValue(request: GrantedRequestRequest, valueId: string): void {
    // request.selectedValueId = valueId;
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  addOrInsertRequest(
    parentSubRequest?: GrantedRequestSubRequest,
    logicalOperator: GrantedRequestLogicalOperators = "ET",
    index?: number
  ): void {
    const newRequest: GrantedRequestRequest = {
      fieldId: null,
      selectedOperator: null,
      possibleValues: [],
      selectedValueId: null,
      searchingValues: false,
      errorOnSearchingValues: false,
      focus$: null,
      debouncedText$: null,
      isOpen: false,
      isJustSelectItem: false,
    };

    this.latestRequestCreate = newRequest;

    const parent: GrantedRequest = parentSubRequest
      ? parentSubRequest.subRequest
      : this.request.subRequest;
    if (typeof index === "undefined") {
      // Si il y a déjà un élément dans la requête, on insère un opérateur logique avant
      if (parent.length > 0) {
        parent.push(logicalOperator);
      }
      parent.push(newRequest);
    } else {
      if (parent.length > 0 && index >= 0) {
        parent.splice(++index, 0, logicalOperator);
      }
      parent.splice(++index, 0, newRequest);
      if (index === 0 && parent.length > 1) {
        parent.splice(++index, 0, logicalOperator);
      }
    }
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  addOrInsertSubRequest(
    parentSubRequest?: GrantedRequestSubRequest,
    logicalOperator: GrantedRequestLogicalOperators = "ET",
    index?: number
  ): void {
    const newSubRequest: GrantedRequestSubRequest = {
      isNegative: false,
      subRequest: [],
    };

    const parent: GrantedRequest = parentSubRequest
      ? parentSubRequest.subRequest
      : this.request.subRequest;
    if (typeof index === "undefined") {
      // Si il y a déjà un élément dans la requête, on insère un opérateur logique avant
      if (parent.length > 0) {
        parent.push(logicalOperator);
      }
      parent.push(newSubRequest);
    } else {
      if (parent.length > 0 && index >= 0) {
        parent.splice(++index, 0, logicalOperator);
      }
      parent.splice(++index, 0, newSubRequest);
      if (index === 0 && parent.length > 1) {
        parent.splice(++index, 0, logicalOperator);
      }
    }
    this.addOrInsertRequest(newSubRequest);
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  removeRequestFromSubRequest(
    requestOrSubRequest: GrantedRequestRequest | GrantedRequestSubRequest,
    parentSubRequest?: GrantedRequestSubRequest
  ): void {
    if (this.request.subRequest && this.request.subRequest.length === 1) {
      return;
    }
    const parent: GrantedRequest = parentSubRequest
      ? parentSubRequest.subRequest
      : this.request.subRequest;
    let index = parent.indexOf(requestOrSubRequest);
    if (index !== -1) {
      parent.splice(index, 1);
      if (parent.length > 0 && index > 0) {
        parent.splice(--index, 1);
      } else if (parent.length > 0 && index === 0) {
        parent.splice(index, 1);
      }
    }
    this.updateCompiledGrantedRequest();
    this.updateCompiledGrantedRequestWFS();
  }

  isGrantedRequestLogicalOperators(
    toTest: unknown
  ): toTest is GrantedRequestLogicalOperators {
    return toTest === "ET" || toTest === "OU";
  }

  isGrantedRequestSubRequest(
    toTest: unknown
  ): toTest is GrantedRequestSubRequest {
    return (
      toTest &&
      (toTest as GrantedRequestSubRequest).isNegative !== undefined &&
      (toTest as GrantedRequestSubRequest).subRequest !== undefined
    );
  }

  isGrantedRequestRequest(toTest: unknown): toTest is GrantedRequestRequest {
    return (
      toTest &&
      (toTest as GrantedRequestRequest).fieldId !== undefined &&
      (toTest as GrantedRequestRequest).possibleValues !== undefined &&
      (toTest as GrantedRequestRequest).selectedOperator !== undefined &&
      (toTest as GrantedRequestRequest).selectedValueId !== undefined
    );
  }

  closeModal(): void {
    this.activeModal.close();
  }

  submitInterrogation(): void {
    if (this.request.selectedLayerId) {
      this.request.requestSended = true;
      this.interrogatingService
        .postGrantedInterrogationRequest(
          this.mapHrefPath,
          this.request.selectedLayer,
          this.request.selectedLayerId,
          this.request.limitNumberOfLines
            ? this.request.maxNumberOfLines
            : null,
          this.updateCompiledGrantedRequestWFS()
        )
        .toPromise()
        .then(
          () => {
            this.request.requestSended = false;
            this.activeModal.close();
          },
          (reason) => {
            // console.warn('submit interrogation error', reason);
            this.request.requestSended = false;
          }
        );
    }
  }

  reset(): void {
    this.possibleFields.length = 0;
    this.request.subRequest.length = 0;
    this.request.compiledGrantedRequest = "";
    this.request.selectedLayerId = null;
    this.request.limitNumberOfLines = false;
    this.request.maxNumberOfLines = 10;
  }

  grantedRequestIsNotValid(): boolean {
    return false;
  }

  protected getPossibleValues(request, term: string|null): Observable<Array<string>> {
    const fieldId = request.fieldId;

    let bodyPost = `<?xml version="1.0" encoding="UTF-8"?>
    <GetPropertyValue count="20" service="WFS" version="2.0.0"
    xmlns="http://www.opengis.net/wfs"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"
    valueReference="${fieldId}">
      <wfs:Query typeNames="${this.request.selectedLayerId}">
    `;

    if (term !== null) {
      bodyPost += `<Filter>
        <PropertyIsLike matchCase="false" wildcard="*" singleChar="." escape="!">
          <PropertyName>${fieldId}</PropertyName>
          <Literal>*${term}*</Literal>
        </PropertyIsLike>
      </Filter>`;
    }

    bodyPost += `</wfs:Query>
    </GetPropertyValue>`;

    const bodyPostOld =
      '<?xml version="1.0" encoding="UTF-8"?>' +
      '<GetPropertyValue count="20" service="WFS" version="2.0.0" ' +
      ' xmlns="http://www.opengis.net/wfs"' +
      ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
      ' xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"' +
      ' valueReference="' +
      fieldId +
      '" >' +
      '<wfs:Query typeNames="' +
      this.request.selectedLayerId +
      '">' +
      "<Filter>" +
      '<PropertyIsLike matchCase="false" wildcard="*" singleChar="." escape="!">' +
      "<PropertyName>" +
      fieldId +
      "</PropertyName>" +
      "<Literal>*" +
      term +
      "*</Literal>" +
      "</PropertyIsLike>" +
      "</Filter>" +
      "</wfs:Query>" +
      "</GetPropertyValue>";
    // request.selectedValueId = null;
    request.searchingValues = true;
    request.errorOnSearchingValues = false;

    const layerQuery = this.request.selectedLayer;
    let layerQueryUrl;
    try {
      layerQueryUrl = (layerQuery.getSource() as any).getUrl();
    } catch (exe) {
      console.error(layerQuery);
    }

    return this.http
      .post(layerQueryUrl, bodyPost, {
        responseType: "text",
      })
      .pipe(
        map((response) => {
          // En fonction du champs sélectionnée, retrouver la liste des valeurs possibles
          let possibleValues = [];
          const parser = new DOMParser();
          const xmlDoc = parser.parseFromString(response, "application/xml");
          const jsonDoc = this.xml2json(xmlDoc);
          const values = {};
          if (
            jsonDoc &&
            jsonDoc["wfs:ValueCollection"] &&
            jsonDoc["wfs:ValueCollection"] instanceof Array &&
            jsonDoc["wfs:ValueCollection"].length === 1 &&
            jsonDoc["wfs:ValueCollection"][0]["wfs:member"] &&
            jsonDoc["wfs:ValueCollection"][0]["wfs:member"] instanceof Array
          ) {
            jsonDoc["wfs:ValueCollection"][0]["wfs:member"].forEach(
              (memberValue) => {
                values[memberValue["ms:" + fieldId]] = true;
              }
            );
            possibleValues = Object.keys(values).sort((valueA, valueB) => {
              return valueA
                .toLocaleLowerCase()
                .localeCompare(valueB.toLocaleLowerCase());
            });
          }
          request.possibleValues = possibleValues;
          request.searchingValues = false;
          request.errorOnSearchingValues = false;
          return possibleValues as string[];
        }),
        catchError((error) => {
          console.error("post body error", error);
          request.searchingValues = false;
          request.errorOnSearchingValues = true;
          return [];
        })
      );
  }

  protected xml2json(srcDOM: Document | Element): {[key: string]: any } {
    const children = Array.from(srcDOM.children);
    // base case for recursion.
    if (!children.length) {
      if ((srcDOM as Element).hasAttributes()) {
        const attrs = (srcDOM as Element).attributes;
        const output: { [key: string]: any } = {};
        let i;
        for (i = attrs.length - 1; i >= 0; i--) {
          output[attrs[i].name] = attrs[i].value;
        }
        output.value = (srcDOM as Element).innerHTML;
        return output;
      } else {
        return (srcDOM as Element).innerHTML as any;
      }
    }

    // initializing object to be returned.
    const jsonResult = {};
    let child: Element;
    for (child of children) {
      // checking is child has siblings of same name.
      const childIsArray =
        children.filter((eachChild) => eachChild.nodeName === child.nodeName)
          .length > 0;
      // if child is array, save the values as array, else as strings.
      if (childIsArray) {
        if (jsonResult[child.nodeName] === undefined) {
          jsonResult[child.nodeName] = [this.xml2json(child)];
        } else {
          jsonResult[child.nodeName].push(this.xml2json(child));
        }
      } else {
        jsonResult[child.nodeName] = this.xml2json(child);
      }
    }

    return jsonResult;
  }
}
