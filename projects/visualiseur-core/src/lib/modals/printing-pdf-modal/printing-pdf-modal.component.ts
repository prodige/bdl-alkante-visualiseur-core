import { FeatureCollection } from "./../../models/context";
import {Subscription, take} from 'rxjs';
import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  NgbActiveModal,
  NgbModal,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import { MapService } from "../../map/map.service";
import { VisualiseurCoreService } from "../../visualiseur-core.service";
import { DrawingBoxRatioComponent } from "../../map/tools/annotating-tools/drawing-box-ratio/drawing-box-ratio.component";
import Collection from "ol/Collection";
import LayerGroup from "ol/layer/Group";
import BaseLayer from "ol/layer/Base";

import { Options as LayerGroupOptions } from "ol/layer/Group";

@Component({
  selector: "alk-printing-pdf-modal",
  templateUrl: "./printing-pdf-modal.component.html",
  styleUrls: ["./printing-pdf-modal.component.scss"],
})
export class PrintingPdfModalComponent implements OnInit, OnDestroy {
  private mapId: string;
  private refModalOpened: NgbModalRef;
  private contextSubscription: Subscription;
  private context: FeatureCollection;
  private newContext: FeatureCollection;
  private rootLayers: any;
  private layersRootSubscription: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    public mapService: MapService,
    private coreService: VisualiseurCoreService,
    private modalService: NgbModal
  ) {
    if (!this.mapService.selectedCarrouselOption) {
      this.mapService.selectedCarrouselOption = {
        indexCarrousel: null,
        indexOption: null,
      };
    }
  }

  ngOnInit() {
    this.mapService.mentionText = null;
    this.layersRootSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((root) => {
        this.rootLayers = root;
      });

    /** demande du context sans référence */
    this.coreService.getContextCopy$().pipe(take(1))
      .subscribe(contextCopy => {
        this.newContext = contextCopy;

        const rootLayerGroup: LayerGroup = this.rootLayerSearchRecursive(
          this.rootLayers.getLayers()
        );

        this.newContext.properties.extension.layers.layers =
          this.coreService.contextLayersAndLayerGroups(
            rootLayerGroup.getLayers()
          );
      });

    this.contextSubscription = this.coreService.getContext().subscribe(
      (context) => {
        if (!context) {
          return false;
        }
        this.context = context;

        this.mapService.mapTitlePrint = context.properties.title;
        this.mapService.mapSubtitlePrint = context.properties.subtitle;

        // On initialise le tableau des données pour les différents modèles de présentation.
        if (
          !this.mapService.carrouselItems ||
          (this.mapService.carrouselItems &&
            !this.mapService.carrouselItems.length)
        ) {
          const nbElemBySectionCarrousel = 4;
          let nbElemAddedBySectionCarrousel = 0;

          this.mapService.carrouselItems = [];
          this.context.properties.extension.Layout.PrintModels.forEach(
            (printModel) => {
              /* if (!printModel.exportOnly) { */
              if (nbElemAddedBySectionCarrousel === nbElemBySectionCarrousel) {
                nbElemAddedBySectionCarrousel = 0;
              }

              if (nbElemAddedBySectionCarrousel === 0) {
                this.mapService.carrouselItems.push([]);
              }

              this.mapService.carrouselItems[
                this.mapService.carrouselItems.length - 1
              ].push({
                img: printModel.image,
                text: printModel.title,
                selected: false,
                ratio: {
                  A3: 1.2,
                  A4: 1.8,
                },
                file: printModel.file,
              });

              nbElemAddedBySectionCarrousel++;
              /* } */
            }
          );
        }


      },
      (error) => console.error(error)
    );
  }

  rootLayerSearchRecursive(
    root: Collection<BaseLayer>,
    layerGroup: LayerGroup = null
  ): LayerGroup {
    const layersList = [];
    root.getArray().forEach((rootLayer) => {
      if (rootLayer.get("class") === "LayerGroup") {
        rootLayer = this.rootLayerSearchRecursive(
          (rootLayer as LayerGroup).getLayers(),
          rootLayer as LayerGroup
        );
        if ((rootLayer as LayerGroup).getLayers().getArray().length !== 0) {
          layersList.push(rootLayer);
        }
      } else {
        if (rootLayer.get("visible") || rootLayer.get("visibleChecked")) {
          layersList.push(rootLayer);
        }
      }
    });
    return new LayerGroup({
      name:
        layerGroup && layerGroup.get("name") ? layerGroup.get("name") : null,
      layers: layersList,
    } as LayerGroupOptions);
  }

  public selectModele(indexCarrousel, indexOption) {
    const nbCarousselSection = this.mapService.carrouselItems.length;

    if (indexCarrousel < nbCarousselSection) {
      const nbCarousselOption =
        this.mapService.carrouselItems[indexCarrousel].length;
      if (indexOption < nbCarousselOption) {
        this.mapService.carrouselItems[indexCarrousel][indexOption].selected =
          true;

        if (this.mapService.selectedCarrouselOption.indexCarrousel != null) {
          const itemCarrousel =
            this.mapService.carrouselItems[
              this.mapService.selectedCarrouselOption.indexCarrousel
            ];
          itemCarrousel[
            this.mapService.selectedCarrouselOption.indexOption
          ].selected = false;
        }

        this.mapService.selectedCarrouselOption.indexCarrousel = indexCarrousel;
        this.mapService.selectedCarrouselOption.indexOption = indexOption;
      }
    }
  }

  public selectImageFormat(format: string) {
    this.mapService.imageFormat = format;
  }

  public startSelectExportArea() {
    if (
      this.mapService.selectedCarrouselOption.indexCarrousel != null &&
      this.mapService.selectedCarrouselOption.indexOption != null &&
      !this.refModalOpened
    ) {
      const itemCarrousel =
        this.mapService.carrouselItems[
          this.mapService.selectedCarrouselOption.indexCarrousel
        ];
      const carrouselItem =
        itemCarrousel[this.mapService.selectedCarrouselOption.indexOption];
      /* this.mapService.shapeRatioPrint = (
        this.mapService.imageFormat &&
        carrouselItem.ratio[this.mapService.imageFormat]
        ? carrouselItem.ratio[this.mapService.imageFormat]
        : 1
      ); */
      this.coreService.setMapTool("drawShapeRatio");

      this.modalService.dismissAll();

      this.refModalOpened = this.modalService.open(DrawingBoxRatioComponent, {
        size: "sm",
        windowClass: "window-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });

      this.refModalOpened.componentInstance.setMapId(this.mapId || "main");
      this.refModalOpened.result.then(
        (close) => {
          this.refModalOpened = this.modalService.open(
            PrintingPdfModalComponent,
            {
              size: "lg",
              centered: true,
              windowClass: "window-xl",
              container: "#modals",
            }
          );
          if (this.refModalOpened.componentInstance.setMapId) {
            this.refModalOpened.componentInstance.setMapId(
              this.mapId || "main"
            );
          }
        },
        (dismiss) => {}
      );
    }
  }

  public setMapId(mapId: string): void {
    this.mapId = mapId;
  }

  public closeModal(removeAll: boolean) {
    if (removeAll) {
      this.mapService.mapCommentPrint = null;
      this.mapService.imageFormat = "A4";
      this.mapService.selectedCarrouselOption = {
        indexCarrousel: null,
        indexOption: null,
      };
      this.mapService.drawnFeaturePrint = null;
    }

    this.activeModal.close();
  }

  public prepareImpressionRequest() {
    // On met à jour les valeurs pour les layers
    this.mapService.setOrientation("landscape");
    this.mapService.startImpressionRequest("pdf", this.newContext);
  }

  ngOnDestroy(): void {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
      this.contextSubscription = null;
    }
    if (this.layersRootSubscription) {
      this.layersRootSubscription.unsubscribe();
      this.layersRootSubscription = null;
    }
  }
}
