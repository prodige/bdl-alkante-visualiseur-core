import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintingPdfModalComponent } from './printing-pdf-modal.component';

describe('PrintingPdfModalComponent', () => {
  let component: PrintingPdfModalComponent;
  let fixture: ComponentFixture<PrintingPdfModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintingPdfModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintingPdfModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
