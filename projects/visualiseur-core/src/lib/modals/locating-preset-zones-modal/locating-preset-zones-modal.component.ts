import { Component, OnInit, Optional, OnDestroy, Host } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { Subscription } from 'rxjs';
import { FeatureCollection } from '../../models/context';
import { HttpClient } from '@angular/common/http';

import Map from 'ol/Map';
import { MapService } from '../../map/map.service';
import { MapIdService } from '../../map/map-id.service';
import { PresetZonesSelectorService } from '../../services/preset-zones-selector.service';
import Feature from 'ol/Feature';
import { Polygon } from 'ol/geom';

@Component({
  selector: 'alk-locating-preset-zones-modal',
  templateUrl: './locating-preset-zones-modal.component.html',
  styleUrls: ['./locating-preset-zones-modal.component.scss']
})
export class LocatingPresetZonesModalComponent implements OnInit, OnDestroy {

  mapId: string;
  map: Map;
  screenMode: 'mobile' | 'desktop' = 'desktop';

  presetZoneProjectionSubscription: Subscription;
  selectedPresetZoneSubscription: Subscription;
  selectedPresetZone: FeatureCollection['Feature'];
  contextSubscription: Subscription;
  context: FeatureCollection;

  presetZones: Array<FeatureCollection['LocalisationConfig']>;
  presetZonesArray = [];
  presetZonesProjection = '';

  constructor(
    public activeModal: NgbActiveModal,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService,
    private presetZonesSelectorService: PresetZonesSelectorService,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.contextSubscription = this.coreService.getContext().subscribe(
      context => {
        if(!context){
          return false;
        }
        this.context = context;
        this.presetZones = context.properties.extension.Tools.LocalisationConfig;
        let array = [];
        this.presetZones.forEach((presetZone, index) => {
          if (index === 0) {
            array.push(presetZone);
          } else {
            if (presetZone.criteriaRelated === '') {
              this.presetZonesArray.push(array);
              array = [];
              array.push(presetZone);
            } else {
              array.push(presetZone);
            }
          }
        });
        this.presetZonesArray.push(array);
      },
      error => console.error(error)
    );
    this.presetZoneProjectionSubscription = this.presetZonesSelectorService.getPresetZoneProjection().subscribe(
      proj => {
        this.presetZonesProjection = proj;
      }
    );
    this.selectedPresetZoneSubscription = this.presetZonesSelectorService.getSelectedPresetZone().subscribe(zone => {
      this.selectedPresetZone = zone;
    });
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.selectedPresetZoneSubscription) {
      this.selectedPresetZoneSubscription.unsubscribe();
    }
    if (this.presetZoneProjectionSubscription) {
      this.presetZoneProjectionSubscription.unsubscribe();
    }
  }

  setMapId(id: string) {
    this.mapId = id;
    this.map = this.mapService.getMap(this.mapId || this.mapIdService.getId() || 'main');
  }

  setScreenMode(mode: 'mobile') {
    this.screenMode = mode;
  }

  locateOnMap() {
    if (this.selectedPresetZone && this.selectedPresetZone.geometry) {
      const oldProj = this.presetZonesProjection.split('::')[0].split(':')[4] + ':' + this.presetZonesProjection.split('::')[1];

      const feature = new Feature({
        geometry: new Polygon(this.selectedPresetZone.geometry.coordinates)
      });
      feature.getGeometry().transform(oldProj, this.map.getView().getProjection().getCode());

      this.map.getView().fit(feature.getGeometry().getExtent());
      this.mapService.setMapFitEvent(true);

      this.presetZonesSelectorService.setPresetZoneProjection('');

      this.activeModal.close();
    }
  }

}
