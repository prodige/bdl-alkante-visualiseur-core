import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocatingPresetZonesModalComponent } from './locating-preset-zones-modal.component';

describe('LocatingPresetZonesModalComponent', () => {
  let component: LocatingPresetZonesModalComponent;
  let fixture: ComponentFixture<LocatingPresetZonesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocatingPresetZonesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocatingPresetZonesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
