import { Component, OnInit, Input, ElementRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FeatureCollection, Feature } from '../../../../models/context';
import { HttpClient } from '@angular/common/http';
import { PresetZonesSelectorService } from '../../../../services/preset-zones-selector.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alk-preset-zone-first-selector',
  templateUrl: './preset-zone-first-selector.component.html',
  styleUrls: ['./preset-zone-first-selector.component.scss']
})
export class PresetZoneFirstSelectorComponent implements OnInit {

  lastSelectorSubscription: Subscription;
  lastSelector: [FeatureCollection, Feature];

  @Input() presetZone: FeatureCollection['LocalisationConfig'];
  @Output() firstSelection = new EventEmitter<Array<Feature>>();

  presetZonesList: FeatureCollection;
  presetZoneMessage = '';

  allSelections: Array<[FeatureCollection, Feature]> = [];
  selection: any;

  constructor(
    private http: HttpClient,
    private presetZonesSelectorService: PresetZonesSelectorService) { }

  ngOnInit() {
    this.selection = 'noPresetSelected';
    if (this.presetZone && !this.presetZone.criteriaRelated) {
      this.firstInit();
    }
  }

  firstInit() {
    const queries = this.presetZone.layerWFSUrl.split('?')[1].split('&');
    let body = '';
    body = `<?xml version="1.0" encoding="UTF-8"?>
    <GetFeature service="WFS" version="2.0.0" outputformat="geojson" maxFeatures="1000"
      xmlns="http://www.opengis.net/wfs/2.0"
      xmlns:cw="http://www.someserver.com/cw"
      xmlns:fes="http://www.opengis.net/ogc/1.1"
      xmlns:gml="http://www.opengis.net/gml/3.2">`;
    queries.forEach(query => {
      const name = query.split('=')[0];
      const value = query.split('=')[1];
      body += `
      <Query ${ name }="${ value }" ></Query>`;
    });
    body += `</GetFeature> `;
    this.http.post(this.presetZone.layerWFSUrl , body).subscribe(Response => {
      this.presetZonesList = Response as FeatureCollection;
    },
    error => {});
  }

  select() {
    this.presetZonesSelectorService.setPresetZoneProjection(this.presetZonesList.crs.properties.name);
    this.presetZonesSelectorService.setSelectedPresetZone(this.selection);

    this.firstSelection.emit([this.presetZone, this.selection]);
  }

}
