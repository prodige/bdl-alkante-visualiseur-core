import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresetZoneFirstSelectorComponent } from './preset-zone-first-selector.component';

describe('PresetZoneFirstSelectorComponent', () => {
  let component: PresetZoneFirstSelectorComponent;
  let fixture: ComponentFixture<PresetZoneFirstSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresetZoneFirstSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresetZoneFirstSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
