import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresetZonesContainerComponent } from './preset-zones-container.component';

describe('PresetZonesContainerComponent', () => {
  let component: PresetZonesContainerComponent;
  let fixture: ComponentFixture<PresetZonesContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresetZonesContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresetZonesContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
