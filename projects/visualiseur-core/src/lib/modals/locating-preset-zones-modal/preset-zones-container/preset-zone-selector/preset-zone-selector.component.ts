import { Component, OnInit, Input, ElementRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FeatureCollection, Feature } from '../../../../models/context';
import { HttpClient } from '@angular/common/http';
import { PresetZonesSelectorService } from '../../../../services/preset-zones-selector.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alk-preset-zone-selector',
  templateUrl: './preset-zone-selector.component.html',
  styleUrls: ['./preset-zone-selector.component.scss']
})
export class PresetZoneSelectorComponent implements OnInit {

  @Input() firstSelection: Array<Feature>;
  @Input() presetZones: FeatureCollection['LocalisationConfig'];

  detailedSelectors: Array<{presetZone: Feature, presetZonesList: FeatureCollection, selection: Feature|string}> = [];

  selection: any;

  constructor(
    private http: HttpClient,
    private presetZonesSelectorService: PresetZonesSelectorService) { }

  ngOnInit() {
    this.presetZones.forEach(zone => {
      this.detailedSelectors.push({
        presetZone: zone,
        presetZonesList: null,
        selection: 'noPresetSelected'
      })
    });
    this.detailedSelectors.forEach(selector => {
      if (selector.presetZone.criteriaRelated === 1) {
        this.selectorSearchList(selector, this.firstSelection);
      }
    });
  }

  selectorSearchList(selector: {presetZone: Feature, presetZonesList: FeatureCollection, selection: Feature|string}, selection: Array<Feature>) {
    let literal = selection[1].properties[selection[0].layerCodeField];
    if (literal !== '') {
      let body = `<?xml version="1.0" encoding="UTF-8"?>
      <GetFeature service="WFS" version="2.0.0" count="100" outputformat="geojson" maxFeatures="10000"
        xmlns="http://www.opengis.net/wfs/2.0"
        xmlns:cw="http://www.someserver.com/cw"
        xmlns:fes="http://www.opengis.net/ogc/1.1"
        xmlns:gml="http://www.opengis.net/gml/3.2">
        <Query typeNames="${ selector.presetZone.layerId }_locator"  styles="default"><Filter>
            <PropertyIsEqualTo><PropertyName>${ selector.presetZone.criteriaFieldRelated }</PropertyName>
        <Literal>${ literal }</Literal></PropertyIsEqualTo>`;
      body += `</Filter></Query></GetFeature> `;
      this.http.post(selector.presetZone.layerWFSUrl , body).subscribe(Response => {
        selector.presetZonesList =  Response as FeatureCollection;
        selector.selection = 'noPresetSelected';
      },
      error => {});
    }
  }

  select(detailedSelector: {presetZone: Feature, presetZonesList: FeatureCollection, selection: Feature|string}) {
    let selectorToSearch = null;
    this.detailedSelectors.forEach((selector, index) => {
      if (this.detailedSelectors[index + 1] && this.detailedSelectors[index + 1] > selector.presetZone.criteriaRelated) {
        this.detailedSelectors[index + 1].presetZonesList = null;
        this.detailedSelectors[index + 1].selection = 'noPresetSelected';
      }
      if ((selector.presetZone === detailedSelector.presetZone) && this.detailedSelectors[index + 1]) {
        selectorToSearch = this.detailedSelectors[index + 1];
      }
    });
    if (selectorToSearch) {
      this.selectorSearchList(selectorToSearch, [detailedSelector.presetZone, detailedSelector.selection as Feature]);
    }
    this.presetZonesSelectorService.setPresetZoneProjection(detailedSelector.presetZonesList.crs.properties.name);
    this.presetZonesSelectorService.setSelectedPresetZone(detailedSelector.selection as Feature);
  }

}
