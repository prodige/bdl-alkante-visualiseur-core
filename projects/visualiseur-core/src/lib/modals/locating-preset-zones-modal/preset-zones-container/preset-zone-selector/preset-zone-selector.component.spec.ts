import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresetZoneSelectorComponent } from './preset-zone-selector.component';

describe('PresetZoneSelectorComponent', () => {
  let component: PresetZoneSelectorComponent;
  let fixture: ComponentFixture<PresetZoneSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresetZoneSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresetZoneSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
