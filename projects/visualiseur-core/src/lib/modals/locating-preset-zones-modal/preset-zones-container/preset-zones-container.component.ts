import { Component, OnInit, Input } from '@angular/core';
import { FeatureCollection, Feature } from '../../../models/context';

@Component({
  selector: 'alk-preset-zones-container',
  templateUrl: './preset-zones-container.component.html',
  styleUrls: ['./preset-zones-container.component.scss']
})
export class PresetZonesContainerComponent implements OnInit {

  @Input() presetZones: Array<FeatureCollection['LocalisationConfig']>;
  firstSelector: Feature;
  firstSelection: Array<Feature> = null;
  dependentLists: Array<[Feature]> = [];

  constructor() { }

  ngOnInit() {
    let dependentListsUnsorted = []
    let dependentListsSorted = []
    this.presetZones.forEach(zone => {
      if (!zone.criteriaRelated) {
        this.firstSelector = zone;
      } else {
        dependentListsUnsorted.push(zone);
      }
    });
    if (dependentListsUnsorted.length !== 0) {
      let dependentListIntermediate = []
      dependentListsUnsorted.forEach((dependentZone: Feature) => {
        if (dependentZone.criteriaRelated === 1 && dependentListIntermediate.length !== 0) {
          dependentListsSorted.push(dependentListIntermediate);
          dependentListIntermediate = [];
        }
        dependentListIntermediate.push(dependentZone);
      });
      if (dependentListIntermediate.length !== 0) {
        dependentListsSorted.push(dependentListIntermediate);
        dependentListIntermediate = [];
      }
      this.dependentLists = dependentListsSorted;
    }
  }

  onSelection(firstSelection: Array<Feature>) {
    // On supprime le contenu de firstSelection pour réinitialiser les liste de sélecteurs dépendants
    this.firstSelection = null;
    setTimeout(() => {
      this.firstSelection = firstSelection;
    }, 500);
  }

}
