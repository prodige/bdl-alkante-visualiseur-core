import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'alk-sharing-modal',
  templateUrl: './sharing-modal.component.html',
  styleUrls: ['./sharing-modal.component.scss']
})
export class SharingModalComponent implements OnInit {

  constructor(
    public activeModal: NgbActiveModal) { }

  ngOnInit() {}

}
