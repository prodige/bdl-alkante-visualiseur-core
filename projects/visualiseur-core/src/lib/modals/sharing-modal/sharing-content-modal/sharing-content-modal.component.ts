import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alk-sharing-content-modal',
  templateUrl: './sharing-content-modal.component.html',
  styleUrls: ['./sharing-content-modal.component.scss']
})
export class SharingContentModalComponent implements OnInit {

  @Input() public screenMode: 'mobile' | 'desktop';
  showToast = false;
  currentUrl: Location;
  currentUrlEncoded: string;

  constructor() { }

  ngOnInit() {
    this.currentUrl = window.location;
    this.currentUrlEncoded = encodeURIComponent(window.location.toString());
  }

  copyLink(link) {
    link.select();
    document.execCommand('copy');
    link.setSelectionRange(0, 0);
    link.blur();

    this.showToast = true;
    setTimeout(() => {
      this.showToast = false;
    }, 3000);
  }

  facebookShare(event) {
    event.preventDefault();
    const facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' + this.currentUrl + '&t' /* + '&t=Venez visiter notre site !'*/,
      'facebook-popup',
      'height=350,width=600'
    );
    if (facebookWindow.focus) {
      facebookWindow.focus();
    }
  }

  twitterShare(event) {
    event.preventDefault();
    const twitterWindow = window.open(
      'https://twitter.com/intent/tweet?url=' + this.currentUrl/*  + '&text=' + this.currentUrl */,
      'twitter-popup',
      'height=350,width=600'
    );
    if (twitterWindow.focus) {
      twitterWindow.focus();
    }
  }

}
