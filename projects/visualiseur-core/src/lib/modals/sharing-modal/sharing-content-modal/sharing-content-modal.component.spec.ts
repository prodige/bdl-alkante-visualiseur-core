import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingContentModalComponent } from './sharing-content-modal.component';

describe('SharingContentModalComponent', () => {
  let component: SharingContentModalComponent;
  let fixture: ComponentFixture<SharingContentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharingContentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingContentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
