import { EnvCoreService } from "./../../env.core.service";
import {
  Component,
  Host,
  OnInit,
  Output,
  EventEmitter,
  ElementRef,
  OnDestroy,
  Optional,
  ViewChild,
  Injectable,
} from "@angular/core";
import { DragulaService } from "ng2-dragula";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from "rxjs";
import { MapService } from "../../map/map.service";
import { MapIdService } from "../../map/map-id.service";
import { LocalStorageService, SessionStorageService } from "ngx-webstorage";

import Map from "ol/Map";
import { FeatureCollection } from "../../models/context";
import { AlertConfirmPromptModalComponent } from "../alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import { HttpClient } from "@angular/common/http";
import { FavoriteZonesManageService } from "../../services/favorite-zones-manage.service";

@Injectable()
@Component({
  selector: "alk-locating-manage-modal",
  templateUrl: "./locating-manage-modal.component.html",
  styleUrls: ["./locating-manage-modal.component.scss"],
})
export class LocatingManageModalComponent implements OnInit, OnDestroy {
  mapId: string;
  map: Map;

  context: Promise<FeatureCollection> | null = null;
  contextSubscription: Subscription;
  isConnected = false;

  httpOptions = {
    withCredentials: true,
  };

  STORAGE_KEY = "favoriteZones";
  favoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> = [];
  localFavoriteZonesSubscription: Subscription;
  sessionFavoriteZonesSubscription: Subscription;
  editingFavoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> = [];
  editingLocalFavoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> = [];
  editingSessionFavoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> =
    [];
  edit = "";

  @ViewChild(AlertConfirmPromptModalComponent, {read: null, static: true})
  promptModal: AlertConfirmPromptModalComponent;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onZoneChanged = new EventEmitter<any>();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDismiss = new EventEmitter<any>();

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
    public activeModal: NgbActiveModal,
    private dragulaService: DragulaService,
    private elRef: ElementRef,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private http: HttpClient,
    private favoriteZonesManageService: FavoriteZonesManageService,
    private environment: EnvCoreService
  ) {}

  ngOnInit() {
    if (!this.dragulaService.find("ZONES")) {
      this.dragulaService.createGroup("ZONES", {
        moves: (el, source, handle) => {
          return (
            handle.parentElement.classList.contains("moving-item") ||
            handle.classList.contains("moving-item")
          );
        },
        direction: "vertical",
        mirrorContainer: this.elRef.nativeElement,
        revertOnSpill: true,
        removeOnSpill: false,
      });
    }

    this.sessionFavoriteZonesSubscription = this.favoriteZonesManageService
      .getSessionFavoriteZones()
      .subscribe((sessionFZ) => {
        if (sessionFZ) {
          this.editingSessionFavoriteZones = sessionFZ;
        }
      });

    this.localFavoriteZonesSubscription = this.favoriteZonesManageService
      .getLocalFavoriteZones()
      .subscribe((localFZ) => {
        if (localFZ) {
          localFZ.forEach((zone) => {
            this.editingLocalFavoriteZones = localFZ;
          });
        }
      });
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.sessionFavoriteZonesSubscription) {
      this.sessionFavoriteZonesSubscription.unsubscribe();
    }
    if (this.localFavoriteZonesSubscription) {
      this.localFavoriteZonesSubscription.unsubscribe();
    }
    this.dragulaService.destroy("ZONES");
  }

  setMapId(id: string) {
    this.mapId = id;
    this.map = this.mapService.getMap(
      this.mapIdService.getId() || this.mapId || "main"
    );
  }

  locateOnMap(bbox = null, id, user = null) {
    if (user) {
      this.http
        .get(
          `${this.environment.serverUrl}carto/FavoriteArea/get?id=${id}`,
          this.httpOptions
        )
        .subscribe((zone) => {
          this.map.getView().fit((zone as any).data.bbox);
          this.mapService.setMapFitEvent(true);
        });
    } else {
      this.map.getView().fit(bbox);
      this.mapService.setMapFitEvent(true);
    }
    this.activeModal.close();
  }

  dragEnd(event: any) {
    this.editingSessionFavoriteZones = event;
    this.favoriteZonesManageService.setSessionFavoriteZones(
      this.editingSessionFavoriteZones
    );

    const newLocalFavoriteZones = [];
    this.editingSessionFavoriteZones.forEach((sessionZone) => {
      if (
        this.editingLocalFavoriteZones.find(
          (localZone) => localZone.title === sessionZone.title && !localZone.id
        )
      ) {
        newLocalFavoriteZones.push(sessionZone);
      }
    });
    if (newLocalFavoriteZones.length) {
      this.favoriteZonesManageService.setLocalFavoriteZones(
        newLocalFavoriteZones
      );
    }
  }

  save(newName: string, id, user = null) {
    if (this.editingSessionFavoriteZones) {
      this.editingSessionFavoriteZones.forEach((zone) => {
        if (zone.id === this.edit) {
          zone.title = newName;
        }
      });
      this.favoriteZonesManageService.setSessionFavoriteZones(
        this.editingSessionFavoriteZones
      );
    }
    if (this.editingLocalFavoriteZones && this.editingSessionFavoriteZones) {
      const newLocalFavoriteZones = [];
      this.editingLocalFavoriteZones.forEach((localZone) => {
        this.editingSessionFavoriteZones.forEach((sessionZone) => {
          if (
            !localZone.id &&
            !localZone.bbox.filter((item) => sessionZone.bbox.indexOf(item) < 0)
              .length
          ) {
            newLocalFavoriteZones.push(sessionZone);
          }
        });
      });
      if (newLocalFavoriteZones.length) {
        this.favoriteZonesManageService.setLocalFavoriteZones(
          newLocalFavoriteZones
        );
      }
    }
    if (user) {
      this.http
        .get(
          `${this.environment.serverUrl}carto/FavoriteArea/rename?id=${id}&title=${newName}`,
          this.httpOptions
        )
        .subscribe((response) => {});
    }
    this.edit = null;
  }

  cancel(
    input: HTMLInputElement,
    zone: FeatureCollection["GeobookmarkConfig"]
  ) {
    input.value = zone.title;
    this.edit = null;
  }

  delete(selectedZone) {
    this.promptModal.openModal().then((result) => {
      if (result) {
        if (this.editingSessionFavoriteZones) {
          this.editingSessionFavoriteZones.forEach((zone, index) => {
            if (zone.title === selectedZone.title) {
              this.editingSessionFavoriteZones.splice(index, 1);
              if (this.editingSessionFavoriteZones.length === 0) {
                this.sessionStorage.clear(this.STORAGE_KEY);
              } else {
                this.favoriteZonesManageService.setSessionFavoriteZones(
                  this.editingSessionFavoriteZones
                );
              }
            }
          });
        }
        if (this.editingLocalFavoriteZones) {
          this.editingLocalFavoriteZones.forEach((zone, index) => {
            if (zone.title === selectedZone.title) {
              this.editingLocalFavoriteZones.splice(index, 1);
              if (this.editingLocalFavoriteZones.length === 0) {
                this.sessionStorage.clear(this.STORAGE_KEY);
                this.localStorage.clear(this.STORAGE_KEY);
              } else {
                this.favoriteZonesManageService.setSessionFavoriteZones(
                  this.editingLocalFavoriteZones
                );
              }
            }
          });
        }
        if (selectedZone.user) {
          this.http
            .get(
              `${this.environment.serverUrl}carto/FavoriteArea/delete?id=${selectedZone.id}`,
              this.httpOptions
            )
            .subscribe((response) => {});
        }
        if (
          this.editingLocalFavoriteZones.length === 0 &&
          this.editingSessionFavoriteZones.length === 0
        ) {
          this.activeModal.close();
        }
      }
    });
  }

  close() {
    this.onDismiss.emit(true);
  }
}
