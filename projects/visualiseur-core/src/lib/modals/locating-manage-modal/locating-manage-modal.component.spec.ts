import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocatingManageModalComponent } from './locating-manage-modal.component';

describe('LocatingManageModalComponent', () => {
  let component: LocatingManageModalComponent;
  let fixture: ComponentFixture<LocatingManageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocatingManageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocatingManageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
