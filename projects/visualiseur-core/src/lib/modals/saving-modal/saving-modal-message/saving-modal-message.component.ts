import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'alk-saving-modal-message',
  templateUrl: './saving-modal-message.component.html',
  styleUrls: ['./saving-modal-message.component.scss']
})
export class SavingModalMessageComponent implements OnInit {

  showToast = false;
  httpResponse: any;

  contextLifetime = 0;
  currentUrl: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.currentUrl = this.httpResponse.data.url;
  }

  setHttpResponse(response: any): void {
    this.httpResponse = response;
  }

  setLifeTime(lifetime: number): void {
    this.contextLifetime = lifetime;
  }

  copyLink(link: HTMLInputElement): void {
    link.select();
    document.execCommand('copy');
    link.setSelectionRange(0, 0);
    link.blur();

    this.showToast = true;
    setTimeout(() => {
      this.showToast = false;
    }, 3000);
  }

}
