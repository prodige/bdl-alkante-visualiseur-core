import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavingModalMessageComponent } from './saving-modal-message.component';

describe('SavingModalMessageComponent', () => {
  let component: SavingModalMessageComponent;
  let fixture: ComponentFixture<SavingModalMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavingModalMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavingModalMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
