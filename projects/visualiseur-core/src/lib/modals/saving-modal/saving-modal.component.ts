import { EnvCoreService } from './../../env.core.service';
import { Component, OnInit, Host, Optional, OnDestroy, Injectable, Inject } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FeatureCollection } from '../../models/context';
import { Subscription } from 'rxjs';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import Map from 'ol/Map';
import * as momentImported from 'moment';
const moment = momentImported;
import LayerGroup from 'ol/layer/Group';
import { MapService } from '../../map/map.service';
import { MapIdService } from './../../map/map-id.service';
import { saveAs } from 'file-saver';
import { HttpClient } from '@angular/common/http';
import { SavingModalMessageComponent } from './saving-modal-message/saving-modal-message.component';
import { UserConnectionService } from '../../services/user-connection.service';

@Injectable()
@Component({
  selector: 'alk-saving-modal',
  templateUrl: './saving-modal.component.html',
  styleUrls: ['./saving-modal.component.scss']
})
export class SavingModalComponent implements OnInit, OnDestroy {

  mapId: string;
  map: Map;
  context: FeatureCollection;
  contextSubscription: Subscription;
  rootLayers: LayerGroup;
  layersRootSubscription: Subscription;
  userConnectionSubscription: Subscription;
  isConnected = false;
  httpOptions = {
    withCredentials : true
  };

  option = 'computer';

  constructor(
    private http: HttpClient,
    private coreService: VisualiseurCoreService,
    private modalService: NgbModal,
    private mapService: MapService,
    private mapIdService: MapIdService,
    public activeModal: NgbActiveModal,
    private userConnectionService: UserConnectionService,
    private environment: EnvCoreService
  ) { }

  ngOnInit() {
    this.contextSubscription = this.coreService.getContext().subscribe(
      context => {
        if(!context){
          return false;
        }
        this.context = JSON.parse(JSON.stringify(context));
      }, error => console.error(error)
    );
    this.layersRootSubscription = this.coreService.getRootLayerGroup().subscribe(root => {
      this.rootLayers = root;
    });
    this.userConnectionSubscription = this.userConnectionService.getUserConfig().subscribe(config => {
      this.isConnected = config.userInternet !== null ? !config.userInternet : null;
    });
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.layersRootSubscription) {
      this.layersRootSubscription.unsubscribe();
    }
    if (this.userConnectionSubscription) {
      this.userConnectionSubscription.unsubscribe();
    }
  }

  setMapId(id: string) {
    this.mapId = id;
    this.map = this.mapService.getMap(this.mapIdService.getId() || this.mapId || 'main');
  }

  selectOption(option: string) {
    this.option = option;
  }

  saveContextAs(option) {
    this.context.properties.extension.layers.layers = this.coreService.contextLayersAndLayerGroups(
      this.rootLayers.getLayers()
    );

    const mapViewExtent = this.map.getView().calculateExtent();
    this.context.properties.bbox = mapViewExtent;

    const data = new Blob([JSON.stringify(this.context)], { type: 'application/json' });
    if (option === 'account') {
      const regex = /\ \(([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}\)$/g;
      if (this.context.properties.title.match(regex) && this.context.properties.title.match(regex).length) {
        const newContextTitle = this.context.properties.title.split(this.context.properties.title.match(regex)[0])[0];
        this.context.properties.title = newContextTitle + ' (' + moment().format('DD/MM/YYYY') + ')';
      } else {
        this.context.properties.title = this.context.properties.title + ' (' + moment().format('DD/MM/YYYY') + ')';
      }
      const userData = {
        context: this.context,
        lifetime: 0
      };
      this.http.post(
        `${this.environment.serverUrl}carto/Context/save`,
        userData,
        this.httpOptions
      )
      .toPromise()
      .then(response => {});
      this.activeModal.close();
    } else if (option === 'server') {
      const serverData = {
        context: this.context,
        lifetime: 0
      };

      this.http.post(
        `${this.environment.serverUrl}carto/context/saveContext?account=${
          ['fr', 'en'].includes(window.location.pathname.split('/')[1])
          ? window.location.pathname.split('/')[2]
          : window.location.pathname.split('/')[1]}`,
        serverData
      ).subscribe(response => {
        const modalRef = this.modalService.open(SavingModalMessageComponent, {
          size: 'lg',
          centered: true,
          windowClass: 'window-xl',
          container: '#modals'
        });
        if (modalRef.componentInstance.setHttpResponse) {
          modalRef.componentInstance.setHttpResponse(response);
          modalRef.componentInstance.setLifeTime(serverData.lifetime);
        }
      });
    } else if (option === 'computer') {
      saveAs(data, 'context.geojson');
    }
  }

}
