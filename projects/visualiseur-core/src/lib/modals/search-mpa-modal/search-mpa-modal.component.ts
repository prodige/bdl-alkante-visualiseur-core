import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { Subscription } from 'rxjs';
import LayerGroup from 'ol/layer/Group';
import BaseLayer from 'ol/layer/Base';
import { HttpClient } from '@angular/common/http';
import { EnvCoreService } from '../../env.core.service';
import { LayerQueryFieldType, InterrogatingService, InterrogationReturnType } from '../../map/services/interrogating.service';
import { Layer } from 'ol/layer';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';

@Component({
  selector: 'alk-search-mpa-modal',
  templateUrl: './search-mpa-modal.component.html',
  styleUrls: ['./search-mpa-modal.component.scss']
})
export class SearchMpaModalComponent implements OnInit {

  mpaSearchJson: any = null;

  countries = [];
  selectedCountries = [];
  designations = [];
  selectedDesignations = [];
  names = [];
  selectedNames = [];
  designationTypes = [];
  selectedDesignationTypes = ['All'];
  iucnCats = [];
  selectedIucnCats = ['All'];
  officialAims = [];
  selectedOfficialAims = ['All'];
  activities = [];
  selectedActivities = ['All'];
  marineSpecies = [];
  selectedMarineSpecies = ['All'];
  marineHabitats = [];
  selectedMarineHabitats = ['All'];

  objectKeys = Object.keys;
  managementPlans = {};
  selectedManagementPlan = 'All';
  numberOfEmployees = {};
  selectedNumberOfEmployees = 'All';

  statusYear: string;
  staffMember: -1|0|1;
  scubaDivingEquipmentAccess: -1|0|1;
  visitorCenterExistance: -1|0|1;

  rechercheEnCours = false;
  contextSubscription: Subscription;
  rootLayerSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  mpaSheetLayer: BaseLayer = null;

  constructor(
    public activeModal: NgbActiveModal,
    public coreService: VisualiseurCoreService,
    private http: HttpClient,
    private environment: EnvCoreService,
    private interrogatingService: InterrogatingService) { }

  ngOnInit() {

    this.rootLayerSubscription = this.coreService.getRootLayerGroup().subscribe(rootLayer => {
      this.rootLayerGroup = rootLayer;
      this.contextSubscription = this.coreService.getContext().subscribe(context => {
        if(!context){
          return false;
        }
        let mapName = null;
        this.rootLayerGroup.getLayers().getArray().forEach((layer: BaseLayer) => {
          if (layer.get('mapName')) {
            mapName = layer.get('mapName');
          }
        });
        this.http.get(
          `${this.environment.serverMpa}libconf/lib/getSearchParam.php?${
            ['fr', 'en'].includes(window.location.pathname.split('/')[1])
            ? 'lg=' + window.location.pathname.split('/')[1]
            : (['fr', 'en'].includes(window.location.pathname.split('/')[2])
              ? 'lg=' + window.location.pathname.split('/')[2]
              : 'lg=' + context.properties.lang )}&map=${mapName}.map`)
          .subscribe((response: any) => {
            this.mpaSearchJson = response;

            // Initialisation à partir du JSON
            this.countries = this.mpaSearchJson.country;
            this.designationTypes = this.mpaSearchJson.desig_type;
            this.iucnCats = this.mpaSearchJson.iucn_cat;
            /* this.officialAims = this.mpaSearchJson.officialAims;
            this.activities = this.mpaSearchJson.activities;
            this.marineSpecies = this.mpaSearchJson.marineSpecies;
            this.marineHabitats = this.mpaSearchJson.marineHabitats; */
            this.managementPlans = this.mpaSearchJson.managementPlans;
            this.numberOfEmployees = this.mpaSearchJson.numberOfEmployees;
            this.statusYear = '';
            this.staffMember = -1;
            /* this.scubaDivingEquipmentAccess = -1;
            this.visitorCenterExistance = -1; */

            this.selectedCountries = ['All'];
            this.selectCountries();
        });
      });
    });

  }

  search() {

    this.rechercheEnCours = true;

    this.searchForMpaSheet(this.rootLayerGroup);

    let bodyPost = `<?xml version="1.0" encoding="UTF-8"?>
    <GetFeature xmlns="http://www.opengis.net/wfs/2.0"
       xmlns:cw="http://www.someserver.com/cw"
       xmlns:fes="http://www.opengis.net/ogc/1.1"
       xmlns:gml="http://www.opengis.net/gml/3.2"
       service="WFS" version="2.0.0" outputformat="geojson">
      <Query typeNames="${this.mpaSheetLayer.get('layerName')}" styles="default">
        <Filter>
          <AND>`;

    const multipleSelecters = [
      ['country', this.selectedCountries, this.countries],
      ['desig', this.selectedDesignations, this.designations],
      ['name', this.selectedNames, this.names],
      ['desig_type', this.selectedDesignationTypes, this.designationTypes],
      ['iucn_cat', this.selectedIucnCats, this.iucnCats]/* ,
      ['off_aims', this.selectedOfficialAims, this.officialAims],
      ['activities', this.selectedActivities, this.activities],
      ['marine_species', this.selectedMarineSpecies, this.marineSpecies],
      ['marine_habitats', this.selectedMarineHabitats, this.marineHabitats] */
    ];

    multipleSelecters.forEach((selecter: [string, [any?], [any?]]) => {
      if (!selecter[1].includes('All') && selecter[1].length !== 0) {
        bodyPost += selecter[1].length > 1 ? '<Filter><OR>' : '';
        selecter[1].forEach(item => {
          bodyPost += `<PropertyIsEqualTo>
            <PropertyName>${selecter[0]}</PropertyName>
            <Literal>${item.id}</Literal>
          </PropertyIsEqualTo>`;
        });
        bodyPost += selecter[1].length > 1 ? '</OR></Filter>' : '';
      }
    });

    let areMultipleSelecters = multipleSelecters.filter( selecter => {
      const value = selecter[1] as string;
      return !value.includes('All') && value.length !== 0;
    }).length;

    if (this.statusYear !== '') {
      bodyPost += `<PropertyIsEqualTo>
            <PropertyName>status_yr</PropertyName>
            <Literal>${this.statusYear}</Literal>
          </PropertyIsEqualTo>`;
      areMultipleSelecters = areMultipleSelecters + 1;
    }

    if (this.selectedManagementPlan !== 'All') {
      bodyPost += `<PropertyIsEqualTo>
            <PropertyName>gnr_car_mang_auth_name</PropertyName>
            <Literal>${this.selectedManagementPlan}</Literal>
          </PropertyIsEqualTo>`;
      areMultipleSelecters = areMultipleSelecters + 1;
    }

    if (this.selectedNumberOfEmployees !== 'All') {
      let property = 'PropertyIsLessThan';
      let literal = `<Literal>5</Literal>`;
      switch (this.selectedNumberOfEmployees) {
        case 'between':
          property = 'PropertyIsBetween';
          literal = `<LowerBoundary>5</LowerBoundary>
          <UpperBoundary>10</UpperBoundary>`;
          break;
        case 'more':
          property = 'PropertyIsGreaterThan';
          literal = `<Literal>10</Literal>`;
          break;
        default:
          property = 'PropertyIsLessThan';
          literal = `<Literal>5</Literal>`;
          break;
      }
      bodyPost += `<${property}>
          <PropertyName>mrs_perm_staff_number</PropertyName>
          ${literal}
        </${property}>`;
      areMultipleSelecters = areMultipleSelecters + 1;
    }

    if (this.staffMember !== -1) {
      bodyPost += `<PropertyIsEqualTo>
            <PropertyName>mrs_staff_empowered_reg_number</PropertyName>
            <Literal>${this.staffMember > 0 ? 1 : this.staffMember = 0 ? 0 : null}</Literal>
          </PropertyIsEqualTo>`;
      areMultipleSelecters = areMultipleSelecters + 1;
    }

    /* if (this.scubaDivingEquipmentAccess !== -1) {
      bodyPost += `<PropertyIsEqualTo>
            <PropertyName>scubaDivingEquipmentAccess</PropertyName>
            <Literal>${this.scubaDivingEquipmentAccess === 1 ? true : false}</Literal>
          </PropertyIsEqualTo>`;
    }

    if (this.visitorCenterExistance !== -1) {
      bodyPost += `<PropertyIsEqualTo>
            <PropertyName>visitorCenterExistance</PropertyName>
            <Literal>${this.visitorCenterExistance === 1 ? true : false}</Literal>
          </PropertyIsEqualTo>`;
    } */

    bodyPost += `</AND>
        </Filter>
      </Query>
    </GetFeature>`;

    const test = bodyPost.split('PropertyIsEqualTo').length
    + bodyPost.split('PropertyIsLessThan').length
    + bodyPost.split('PropertyIsGreaterThan').length
    + bodyPost.split('PropertyIsBetween').length;

    if (areMultipleSelecters <= 1) {
    // if (test <= 6) {
      bodyPost = bodyPost.replace(/<AND>/g, '');
      bodyPost = bodyPost.replace(/<\/AND>/g, '');
    }
    const url = (this.mpaSheetLayer as Layer<VectorSource<Geometry>>).getSource().getUrl();

    this.http.post( url as string, bodyPost, { responseType: 'json' } ).subscribe(response => {

      this.rechercheEnCours = false;

      let totalResults = 0;
      let layerResults = 0;
      // Tableau dans lequel on stockera les données récupèrées par la requête
      const layerData = [];
      // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
      const layerQueryFields = this.mpaSheetLayer.get('LayerQuery').Fields as Array<LayerQueryFieldType>;
      // On extrait de la liste des colonnes précédentes les clés
      const layerQueryFieldsNames = layerQueryFields.map(layerQueryField => layerQueryField.FieldName);

      // On parcours enfin les réponse pour ne garder que les données qui nous intéresse
      (response as any).features.forEach(featureInResponse => {
        totalResults++;
        layerResults++;
        const datas = Object.entries(featureInResponse.properties as { [key: string]: any }).filter(
          ([key, value]) => layerQueryFieldsNames.includes(key)
        ).reduce((accum, [key, value]) => {
          accum[key] = value;
          return accum;
        }, {});

        layerData.push(Object.assign({}, datas, {
          feature: featureInResponse
        }));
      });

      const validResults = {
        layerName: this.mpaSheetLayer.get('name'),
        layer: this.mpaSheetLayer,
        layerQueryFields,
        data: layerData,
        nbResults: layerResults
      };

      const resultOfInterrogation = {
        featureOfRequest: null,
        layersResults: [validResults],
        totalResults
      } as InterrogationReturnType;

      this.interrogatingService.setResultOfInterrogation(resultOfInterrogation);

      this.interrogatingService.setResultSearching(false);
      this.interrogatingService.changeResultOfInterrogation();
      if (totalResults > 0) {
        this.interrogatingService.openResultCurrentBox();
        this.activeModal.close();
      }
    });

  }

  searchForMpaSheet(rootLayerGroup: LayerGroup) {
    rootLayerGroup.getLayers().getArray().forEach(layer => {
      if (layer.get('class') === 'LayerGroup') {
        this.searchForMpaSheet(layer as LayerGroup);
      } else {
        if (!this.mpaSheetLayer && (layer.get('layerIdentifier') && layer.get('layerIdentifier').includes('amp_sheet'))) {
          this.mpaSheetLayer = layer;
        }
      }
    });
  }

  selectCountries() {
    this.designations = [];
    this.selectedDesignations = ['All'];
    if (this.selectedCountries.includes('All')) {
      this.selectedCountries = ['All'];
      this.countries.forEach(country => {
        if (country.desig) {
          country.desig.forEach(desig => {
            this.designations.push(desig);
          });
        }
      });
    } else {
      this.selectedCountries.forEach(country => {
        if (country.desig) {
          country.desig.forEach(desig => {
            this.designations.push(desig);
          });
        }
      });
    }
    this.selectDesignations();
  }

  selectDesignations() {
    this.names = [];
    this.selectedNames = ['All'];
    if (this.selectedDesignations.includes('All')) {
      this.selectedDesignations = ['All'];
      this.designations.forEach(designation => {
        if (designation.name) {
          designation.name.forEach(name => {
            this.names.push(name);
          });
        }
      });
    } else {
      this.selectedDesignations.forEach(designation => {
        if (designation.name) {
          designation.name.forEach(name => {
            this.names.push(name);
          });
        }
      });
    }
  }

  selectItems(arrayName): any {
    switch (arrayName) {
      case 'selectedNames':
        if (this.selectedNames && this.selectedNames.length && this.selectedNames.includes('All')) {
          this.selectedNames = ['All'];
        }
        break;
      case 'selectedDesignationTypes':
          if (this.selectedDesignationTypes && this.selectedDesignationTypes.length && this.selectedDesignationTypes.includes('All')) {
            this.selectedDesignationTypes = ['All'];
          }
          break;
      case 'selectedIucnCats':
        if (this.selectedIucnCats && this.selectedIucnCats.length && this.selectedIucnCats.includes('All')) {
          this.selectedIucnCats = ['All'];
        }
        break;
      case 'selectedOfficialAims':
          if (this.selectedOfficialAims && this.selectedOfficialAims.length && this.selectedOfficialAims.includes('All')) {
            this.selectedOfficialAims = ['All'];
          }
          break;
      case 'selectedActivities':
          if (this.selectedActivities && this.selectedActivities.length && this.selectedActivities.includes('All')) {
            this.selectedActivities = ['All'];
          }
          break;
      case 'selectedMarineSpecies':
          if (this.selectedMarineSpecies && this.selectedMarineSpecies.length && this.selectedMarineSpecies.includes('All')) {
            this.selectedMarineSpecies = ['All'];
          }
          break;
      case 'selectedMarineHabitats':
          if (this.selectedMarineHabitats && this.selectedMarineHabitats.length && this.selectedMarineHabitats.includes('All')) {
            this.selectedMarineHabitats = ['All'];
          }
          break;
    }
  }
}
