import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMpaModalComponent } from './search-mpa-modal.component';

describe('SearchMpaModalComponent', () => {
  let component: SearchMpaModalComponent;
  let fixture: ComponentFixture<SearchMpaModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchMpaModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMpaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
