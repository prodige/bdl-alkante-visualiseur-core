import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { VisualiseurCoreService } from "../../../visualiseur-core.service";
import { FeatureCollection } from "../../../models/context";

import Map from "ol/Map";
import LayerGroup from "ol/layer/Group";
import { MapService } from "../../../map/map.service";
import { MapIdService } from "../../../map/map-id.service";
import { Subscription } from "rxjs";
import { DrawLayerService } from "../../../services/draw-layer.service";
import { HttpClient } from "@angular/common/http";
import { UserConnectionService } from "../../../services/user-connection.service";
import { EnvCoreService } from "../../../env.core.service";

@Component({
  selector: "alk-opening-content-modal",
  templateUrl: "./opening-content-modal.component.html",
  styleUrls: ["./opening-content-modal.component.scss"],
})
export class OpeningContentModalComponent implements OnInit, OnDestroy {
  @Input() screenMode: "desktop" | "mobile";
  @Input() mapId;
  map: Map;
  savedMaps = [];
  edit = "";

  rootLayers: LayerGroup;
  layersRootSubscription: Subscription;
  userConnectionSubscription: Subscription;
  isConnected = null;
  @ViewChild("fileUpload", {read: null, static: true}) fileUpload: ElementRef;
  httpOptions = {
    withCredentials: true,
  };

  constructor(
    private http: HttpClient,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService,
    private drawLayerService: DrawLayerService,
    private userConnectionService: UserConnectionService,
    private environment: EnvCoreService
  ) {}

  ngOnInit() {
    this.map = this.mapService.getMap(
      this.mapIdService.getId() || this.mapId || "main"
    );

    this.layersRootSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((root) => {
        this.rootLayers = root;
      });

    this.userConnectionSubscription = this.userConnectionService
      .getUserConfig()
      .subscribe((config) => {
        this.isConnected =
          config.userInternet !== null ? !config.userInternet : null;

        if (this.isConnected) {
          this.http
            .get(
              `${this.environment.serverUrl}carto/Context/list`,
              this.httpOptions
            )
            .subscribe((mapList) => {
              (mapList as any).data.forEach((map) => {
                this.savedMaps.push({
                  id: map.id,
                  title: map.title,
                });
              });
            });
        }
      });
  }

  ngOnDestroy() {
    if (this.layersRootSubscription) {
      this.layersRootSubscription.unsubscribe();
    }
    if (this.userConnectionSubscription) {
      this.userConnectionSubscription.unsubscribe();
    }
  }

  saveMap(newName: string, id) {
    this.savedMaps.forEach((map, index) => {
      if (map.id === this.edit) {
        this.savedMaps[index].title = newName;
      }
    });
    this.http
      .get(
        `${this.environment.serverUrl}carto/Context/rename?id=${id}&title=${newName}`,
        this.httpOptions
      )
      .subscribe((response) => {});
    this.edit = null;
  }

  cancelMap(input: HTMLInputElement, map: any) {
    input.value = map.title;
    this.edit = null;
  }

  deleteMap(selectedMap) {
    this.savedMaps.forEach((map, index) => {
      if (map.title === selectedMap.title) {
        this.savedMaps.splice(index, 1);
      }
    });
    this.http
      .get(
        `${this.environment.serverUrl}carto/Context/delete?id=${selectedMap.id}`,
        this.httpOptions
      )
      .subscribe((response) => {});
  }

  changeUserContext(id: number) {
    this.http
      .get(
        `${this.environment.serverUrl}carto/Context/get?id=${id}`,
        this.httpOptions
      )
      .subscribe((context) => {
        this.coreService.setContext((context as any).data as FeatureCollection);
        this.rootLayers.getLayers().forEach((layer) => {
          if (layer.get("isDrawLayer")) {
            this.drawLayerService.generateNewDrawLayer(this.mapId);
          }
          if (
            layer.get("extension") &&
            layer.get("extension").isBufferLayerGroup
          ) {
            this.drawLayerService.generateNewBufferLayerGroup(this.mapId);
          }
        });
        this.rootLayers.getLayers().clear();
        this.map.getLayers().clear();
      });
  }

  changeContext(file = null, id = null) {
    let geojsonContent = null;
    let fileReader = null;

    if (file) {
      fileReader = new FileReader();
      fileReader.onload = (e) => {
        geojsonContent = JSON.parse(fileReader.result as string);
        this.coreService.setContext(geojsonContent as FeatureCollection);
      };
    }
    if (id) {
      this.http
        .get(
          `${this.environment.serverUrl}carto/Context/get?id=${id}`,
          this.httpOptions
        )
        .subscribe((context) => {
          geojsonContent = (context as any).data as FeatureCollection;
          this.coreService.setContext(geojsonContent as FeatureCollection);
        });
    }

    this.rootLayers.getLayers().forEach((layer) => {
      if (layer.get("isDrawLayer")) {
        this.drawLayerService.generateNewDrawLayer(this.mapId);
      }
      if (layer.get("extension") && layer.get("extension").isBufferLayerGroup) {
        this.drawLayerService.generateNewBufferLayerGroup(this.mapId);
      }
    });

    this.rootLayers.getLayers().clear();
    this.map.getLayers().clear();

    if (file) {
      fileReader.readAsText(file);
    }
  }
}
