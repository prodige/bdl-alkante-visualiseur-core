import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeningContentModalComponent } from './opening-content-modal.component';

describe('OpeningContentModalComponent', () => {
  let component: OpeningContentModalComponent;
  let fixture: ComponentFixture<OpeningContentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpeningContentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeningContentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
