import { Component, OnInit, Injectable } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
@Component({
  selector: 'alk-opening-modal',
  templateUrl: './opening-modal.component.html',
  styleUrls: ['./opening-modal.component.scss']
})
export class OpeningModalComponent implements OnInit {

  mapId: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {}

  setMapId(id: string) {
    this.mapId = id;
  }

}
