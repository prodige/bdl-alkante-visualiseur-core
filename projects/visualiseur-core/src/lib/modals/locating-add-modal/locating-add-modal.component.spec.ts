import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocatingAddModalComponent } from './locating-add-modal.component';

describe('LocatingAddModalComponent', () => {
  let component: LocatingAddModalComponent;
  let fixture: ComponentFixture<LocatingAddModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocatingAddModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocatingAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
