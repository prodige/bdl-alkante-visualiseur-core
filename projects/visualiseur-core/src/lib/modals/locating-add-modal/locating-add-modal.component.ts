import { Component, OnInit, Optional, OnDestroy, Host, Injectable } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import Map from 'ol/Map';
import { MapService } from '../../map/map.service';
import { MapIdService } from '../../map/map-id.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserConnectionService } from '../../services/user-connection.service';
import { Subscription } from 'rxjs';
import { FavoriteZonesManageService } from '../../services/favorite-zones-manage.service';

@Injectable()
@Component({
  selector: 'alk-locating-add-modal',
  templateUrl: './locating-add-modal.component.html',
  styleUrls: ['./locating-add-modal.component.scss']
})
export class LocatingAddModalComponent implements OnInit, OnDestroy {

  mapId: string;
  map: Map;
  viewport: number[];

  userConnectionSubscription: Subscription;
  isConnected = false;

  minLongitude = 2.333333;
  minLatitude = 48.866667;
  maxLongitude = 2.333333;
  maxLatitude = 48.866667;
  public addFavoriteZone: FormGroup;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private favoriteZonesManageService: FavoriteZonesManageService,
    private userConnectionService: UserConnectionService
  ) { }

  ngOnInit() {
    this.addFavoriteZone = this.formBuilder.group({
      checkboxInput: new FormControl(
        '',
        [Validators.required])
    });

    this.userConnectionSubscription = this.userConnectionService.getUserConfig().subscribe(config => {
      this.isConnected = config.userInternet !== null ? !config.userInternet : null;
    });
  }

  ngOnDestroy() {
    if (this.userConnectionSubscription) {
      this.userConnectionSubscription.unsubscribe();
    }
  }

  setMapId(id: string) {
    this.mapId = id;

    this.map = this.mapService.getMap(this.mapIdService.getId() || this.mapId || 'main');

    this.viewport = this.map.getView().calculateExtent(this.map.getSize());
    this.minLongitude = this.viewport[0];
    this.minLatitude = this.viewport[1];
    this.maxLongitude = this.viewport[2];
    this.maxLatitude = this.viewport[3];
  }

  addZone(form: any) {
    this.favoriteZonesManageService.addZone(
      form.zoneName.value,
      this.addFavoriteZone.get('checkboxInput').value ? true : false,
      {
        minLongitude: this.minLongitude,
        minLatitude: this.minLatitude,
        maxLongitude: this.maxLongitude,
        maxLatitude: this.maxLatitude
      }
    );

    this.activeModal.close();
  }

}
