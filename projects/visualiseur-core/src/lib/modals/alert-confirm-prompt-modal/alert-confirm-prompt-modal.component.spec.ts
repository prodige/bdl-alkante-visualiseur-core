import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertConfirmPromptModalComponent } from './alert-confirm-prompt-modal.component';

describe('AlertConfirmPromptModalComponent', () => {
  let component: AlertConfirmPromptModalComponent;
  let fixture: ComponentFixture<AlertConfirmPromptModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertConfirmPromptModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertConfirmPromptModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
