import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import {
  NgbActiveModal,
  NgbModal,
  NgbModalRef,
  NgbModalConfig,
} from "@ng-bootstrap/ng-bootstrap";

@Component({
  templateUrl: "./alert-confirm-prompt-modal.component.html",
})
export class ModalConfirmComponent {
  type: "alert" | "confirm" | "prompt";

  canBeClosed: false | true = true;
  title: string;
  titleIsHtml: false | true = false;
  description: string;
  descriptionIsHtml: false | true = false;
  promptNeededValue: string;
  buttonValidationColor:
    | "primary"
    | "success"
    | "warning"
    | "info"
    | "danger"
    | "secondary"
    | "light"
    | "dark"
    | "default";
  buttonValidationText: string;

  @ViewChild("promptedValue", {read: null, static: true})
  promptedValue: ElementRef<HTMLInputElement>;

  modalReady = false;

  constructor(private config: NgbModalConfig, public modalRef: NgbActiveModal) {
    this.config.backdrop = "static";
    this.config.keyboard = false;
  }

  isPromptValid(): boolean {
    return (
      this.promptedValue &&
      this.promptNeededValue.toLocaleLowerCase() ===
        this.promptedValue.nativeElement.value.toLocaleLowerCase()
    );
  }

  tryClose(event: Event): void {
    if (
      this.promptedValue &&
      this.promptNeededValue === this.promptedValue.nativeElement.value
    ) {
      // this.activeModal.close(true);
      this.modalRef.close(true);
    }
  }
}

export interface AlertModalOptions {
  canBeClosed?: false | true;
  title?: string;
  titleIsHtml?: false | true;
  description?: string;
  descriptionIsHtml?: false | true;
  buttonValidationColor?:
    | "primary"
    | "success"
    | "warning"
    | "info"
    | "danger"
    | "secondary"
    | "light"
    | "dark"
    | "default";
  buttonValidationText?: string;
}
export type ConfirmModalOptions = AlertModalOptions;
export interface PromptModalOptions extends AlertModalOptions {
  promptNeededValue: string;
}

function isPromptModalOptions(object: any): object is PromptModalOptions {
  return "promptNeededValue" in object;
}

/**
 * Utiliser l'une des notations suivant pour utilier la modal depuis le component appelant
 * 1 - [
 *   <alk-alert-confirm-prompt-modal [type]="'alert'"></alk-alert-confirm-prompt-modal>
 *   @ViewChild(AlertConfirmPromptModalComponent) alertModal: AlertConfirmPromptModalComponent;
 * ]
 * 2 - [
 *   <alk-alert-confirm-prompt-modal [type]="'confirm'"></alk-alert-confirm-prompt-modal>
 *   @ViewChild(AlertConfirmPromptModalComponent) confirmModal: AlertConfirmPromptModalComponent;
 * ]
 * 3 - [
 *   <alk-alert-confirm-prompt-modal [type]="'prompt'" [promptNeededValue]="'SUPPRIMER'" ></alk-alert-confirm-prompt-modal>
 *   @ViewChild(AlertConfirmPromptModalComponent) promptModal: AlertConfirmPromptModalComponent;
 * ]
 */

@Component({
  selector: "alk-alert-confirm-prompt-modal[type]",
  template: `<!-- modal à ouvrir -->`,
  styleUrls: ["./alert-confirm-prompt-modal.component.scss"],
})
export class AlertConfirmPromptModalComponent implements OnInit {
  modalRef: NgbModalRef = null;

  @Input() type: "alert" | "confirm" | "prompt";
  @Input() canBeClosed: false | true = true;
  @Input() title: string = null;
  @Input() titleIsHtml: false | true = false;
  @Input() description = "";
  @Input() descriptionIsHtml: false | true = false;
  @Input() promptNeededValue: string;
  // tslint:disable-next-line: max-line-length
  @Input() buttonValidationColor:
    | "primary"
    | "success"
    | "warning"
    | "info"
    | "danger"
    | "secondary"
    | "light"
    | "dark"
    | "default" = "default";
  @Input() buttonValidationText: string = null;

  constructor(
    private config: NgbModalConfig,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal
  ) {
    this.config.backdrop = "static";
    this.config.keyboard = false;
  }

  openModal(
    options?: AlertModalOptions | ConfirmModalOptions | PromptModalOptions
  ): Promise<boolean> {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {
      centered: true,
      size: "lg",
      backdrop: "static",
    });

    const modalInstance: ModalConfirmComponent =
      this.modalRef.componentInstance;

    modalInstance.type = this.type;
    modalInstance.canBeClosed = options
      ? options.canBeClosed || this.canBeClosed
      : this.canBeClosed;
    modalInstance.title = options ? options.title || this.title : this.title;
    modalInstance.titleIsHtml = options
      ? options.titleIsHtml || this.titleIsHtml
      : this.titleIsHtml;
    modalInstance.description = options
      ? options.description || this.description
      : this.description;
    modalInstance.descriptionIsHtml = options
      ? options.descriptionIsHtml || this.descriptionIsHtml
      : this.descriptionIsHtml;
    modalInstance.promptNeededValue =
      (options && isPromptModalOptions(options)
        ? options.promptNeededValue
        : this.promptNeededValue) || this.promptNeededValue;
    modalInstance.buttonValidationColor = options
      ? options.buttonValidationColor || this.buttonValidationColor
      : this.buttonValidationColor;
    modalInstance.buttonValidationText = options
      ? options.buttonValidationText || this.buttonValidationText
      : this.buttonValidationText;

    modalInstance.modalReady = true;
    return this.modalRef.result.catch((result) => Promise.resolve(false));
  }

  ngOnInit() {
    if (this.type === null || this.type === undefined) {
      throw new TypeError(`The input ‘type’ is required`);
    }
    if (
      this.type === "prompt" &&
      (this.promptNeededValue === null || this.promptNeededValue === undefined)
    ) {
      throw new TypeError(
        `The input ‘promptNeededValue’ is required when type is prompt`
      );
    }
  }
}
