import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MapService } from '../../map/map.service';
import { Subscription } from 'rxjs';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import LayerGroup from 'ol/layer/Group';
import { EnvCoreService } from '../../env.core.service';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { DomSanitizer } from '@angular/platform-browser';
import { ConnexionConfig } from '../../models/connexion-config';
import { UserConnectionService } from '../../services/user-connection.service';

@Component({
  selector: 'alk-downloading-modal',
  templateUrl: './downloading-modal.component.html',
  styleUrls: ['./downloading-modal.component.scss']
})
export class DownloadingModalComponent implements OnInit {

  page = 1;
  coreSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  tablesCollapsed = [];
  selectedLayers = [];
  selectedLayersToEdit = [];

  formats = ['shp', 'tab', 'kml', 'mif/mid'];
  fileFormat: Array<any> = [];

  projections: Array<{key: string; label: string; proj: string; bounds: [number, number, number, number]; }> = [];
  mapProjection: Array<any> = [];

  emailAddress: string;
  termsOfUse = false;
  licenceUrl: string;
  licenceHTML: any;

  userConfigSubscription: Subscription;
  userConfig: ConnexionConfig;

  constructor(
    private sanitizer: DomSanitizer,
    private http: HttpClient,
    private environment: EnvCoreService,
    private coreService: VisualiseurCoreService,
    public activeModal: NgbActiveModal,
    public mapService: MapService,
    public userConnexionService: UserConnectionService
  ) { }

  ngOnInit() {

    this.projections = Object.values(this.environment.availableProjections);

    this.userConfigSubscription = this.userConnexionService.getUserConfig().subscribe(config => {
      this.userConfig = config;
    });

    this.coreSubscription = this.coreService.getContext().subscribe(context => {
      if(!context){
        return false;
      }
      this.licenceUrl = context.properties.extension.Licence;
      if (this.licenceUrl) {
        this.http.get(this.licenceUrl, {responseType : 'text'}).subscribe(licence => {
          this.licenceHTML = String(licence)
          .replace(/&quot;/g, '\"')
          .replace(/&#39;/g, '\'')
          .replace(/&lt;/g, '<')
          .replace(/&gt;/g, '>')
          .replace(/&amp;/g, '&');
          this.sanitizer.bypassSecurityTrustHtml(this.licenceHTML);
        });
      }

      const generatedLayersFromContext = this.coreService.generateLayersAndLayerGroups(context.properties.extension.layers);
      this.rootLayerGroup = generatedLayersFromContext as LayerGroup;

      this.rootLayerGroup = this.coreService.getDownloadableRootLayerGroup(new LayerGroup({ layers: this.rootLayerGroup.getLayers() }));
      

      this.toggleLayersSelection(this.rootLayerGroup, 'edit');
      this.selectedLayersToEdit.push({
        layer: this.rootLayerGroup,
        fileFormat: this.fileFormat[this.rootLayerGroup.get('id')],
        mapProjection: this.mapProjection[this.rootLayerGroup.get('id')]
      });
      this.fileFormat[this.rootLayerGroup.get('id')] = 'shp';
      this.mapProjection[this.rootLayerGroup.get('id')] = this.projections[0];
      this.tablesCollapsed[this.rootLayerGroup.get('id')] = true;
      this.fillTablesCollapsed(this.rootLayerGroup);
    });
  }

  counter(length: number): Array<number> {
    return new Array(length);
  }

  hasLayer(layerGroup: Array<any>): boolean {
    const layersTab = [];
    layerGroup.forEach(layer => {
      if (layer.get('class') !== 'LayerGroup') {
        layersTab.push(layer);
      }
    });
    if (layersTab.length) {
      return true;
    } else {
      return false;
    }
  }

  fillTablesCollapsed(layerGroup: LayerGroup) {
    layerGroup.getLayers().getArray().forEach(layer => {
      if (layer.get('class') === 'LayerGroup') {
        this.tablesCollapsed[layer.get('id')] = true;
        this.fileFormat[layer.get('id')] = 'shp';
        this.mapProjection[layer.get('id')] = this.projections[0];
        this.fillTablesCollapsed(layer as LayerGroup);
      } else {
        this.fileFormat[layer.get('layerIdx')] = 'shp';
        this.mapProjection[layer.get('layerIdx')] = this.projections[0];
      }
    });
  }

  toggleLayersSelection(selectedLayer: any, mode: 'edit'|'select') {
    if (mode === 'select') {
      if (this.selectedLayers.find(layer => layer.layer === selectedLayer)) {
        this.selectedLayers = this.selectedLayers.filter( selectedFeature => selectedFeature.layer !== selectedLayer );
      } else {
        this.selectedLayers.push({
          layer: selectedLayer,
          fileFormat: this.fileFormat[selectedLayer.get('layerIdx')],
          mapProjection: this.mapProjection[selectedLayer.get('layerIdx')]
        });
      }
    } else {
      if (this.selectedLayersToEdit.find(layer => layer.layer === selectedLayer)) {
        this.selectedLayersToEdit = this.selectedLayersToEdit.filter( selectedFeature => selectedFeature.layer !== selectedLayer );
      } else {
        this.selectedLayersToEdit.push({
          layer: selectedLayer,
          fileFormat: this.fileFormat[selectedLayer.get('layerIdx')],
          mapProjection: this.mapProjection[selectedLayer.get('layerIdx')]
        });
      }
    }
  }

  toggleAll(layerGroup: Array<any>, mode: 'edit'|'select') {
    layerGroup.forEach(layer => {
      if (layer.get('class') === 'LayerGroup') {
        if (mode === 'edit') {
          this.toggleLayersSelection(layer, mode);
        }
        this.toggleAll(layer.getLayers().getArray(), mode);
      } else {
        this.toggleLayersSelection(layer, mode);
      }
    });
  }

  allChecked(layerGroup: Array<any>, mode: 'edit'|'select'): boolean {
    let layersNumber = 0;
    const layersChecked = [];
    let subLayers = null;
    layerGroup.forEach(layer => {
      if (layer.get('class') !== 'LayerGroup') {
        layersNumber = layersNumber + 1;
        if (mode === 'select') {
          if (this.selectedLayers.find(selectedLayer => selectedLayer.layer === layer)) {
            layersChecked.push(layer);
          }
        } else {
          if (this.selectedLayersToEdit.find(selectedLayer => selectedLayer.layer === layer)) {
            layersChecked.push(layer);
          }
        }
      } else {
        subLayers = this.allChecked(layer.getLayers().getArray(), mode);
      }
    });

    if (subLayers !== null) {
      return subLayers;
    } else {
      if (layersChecked.length === layersNumber) {
        return true;
      } else {
        return false;
      }
    }
  }

  isLayerSelected(layer: any, mode: 'edit'|'select'): boolean {
    if (mode === 'select') {
      return this.selectedLayers.find(selectedLayer => selectedLayer.layer === layer);
    } else {
      return this.selectedLayersToEdit.find(selectedLayer => selectedLayer.layer === layer);
    }
  }

  changeOptionsSubLayers(layerGroup: LayerGroup) { // data: 'format'|'projection', event: any, layerGroup: LayerGroup
    layerGroup.getLayers().getArray().forEach(layer => {
      if (layer.get('class') === 'LayerGroup') {
        if (!this.selectedLayersToEdit.find(selectedLayer => selectedLayer.layer === layer)) {
          this.fileFormat[layer.get('id')] = this.fileFormat[layerGroup.get('id')];
          this.mapProjection[layer.get('id')] = this.mapProjection[layerGroup.get('id')];
        }
        this.changeOptionsSubLayers(layer as LayerGroup);
      } else {
        if (!this.selectedLayersToEdit.find(selectedLayer => selectedLayer.layer === layer)) {
          this.fileFormat[layer.get('layerIdx')] = this.fileFormat[layerGroup.get('id')];
          this.mapProjection[layer.get('layerIdx')] = this.mapProjection[layerGroup.get('id')];
          if (this.selectedLayers.find(selectedLayer => selectedLayer.layer === layer)) {
            const layerToEdit = this.selectedLayers.find(selectedLayer => selectedLayer.layer === layer);
            layerToEdit.fileFormat = this.fileFormat[layerGroup.get('id')];
            layerToEdit.mapProjection = this.mapProjection[layerGroup.get('id')];
          }
        }
      }
    });
  }

  validate() {
    const body = {
      mail: this.emailAddress,
      data: []
    };
    this.selectedLayers.forEach(layer => {
      body.data.push({
        title: layer.layer.get('name'),
        format: layer.fileFormat,
        crs: layer.layer.getSource().getProjection().getCode(),
        metadata_uuid: layer.layer.get('layerMetadataUuid'),
        metadata_file: layer.layer.get('metadataUrl'),
        layer_src_info: {
          mapfile: layer.layer.get('mapName') + '.map',
          layer_name: layer.layer.get('layerName'),
          service_idx: ['fr', 'en'].includes(window.location.pathname.split('/')[1])
          ? window.location.pathname.split('/')[2]
          : window.location.pathname.split('/')[1]
        }
      });
    });

    this.http.post(`${this.userConfig.URLS.telecarto}?bDirect=true&account=${
      ['fr', 'en'].includes(window.location.pathname.split('/')[1])
      ? window.location.pathname.split('/')[2]
      : window.location.pathname.split('/')[1]}`, body).subscribe((response: any) => {
        if (response && response.success) {
          const fileName = response.data.split('/').reverse()[0];
          this.http.get(response.data, { responseType: 'blob' }).subscribe(response => {
            // saveAs(response, fileName);
            this.page = 3;
          });
        } else {
          //TODO modal error
        }
      });
  }

}
