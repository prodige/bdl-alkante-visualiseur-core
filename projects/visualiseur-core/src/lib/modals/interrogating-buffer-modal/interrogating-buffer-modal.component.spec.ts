import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingBufferModalComponent } from './interrogating-buffer-modal.component';

describe('InterrogatingBufferModalComponent', () => {
  let component: InterrogatingBufferModalComponent;
  let fixture: ComponentFixture<InterrogatingBufferModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingBufferModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingBufferModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
