import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import GML3 from 'ol/format/GML3';
import GeoJSON from 'ol/format/GeoJSON';
import Feature from 'ol/Feature';
import LayerGroup from 'ol/layer/Group';
import { HttpClient } from '@angular/common/http';
import { LayerQueryFieldType, InterrogatingService } from '../../map/services/interrogating.service';
import Layer from 'ol/layer/Layer';

@Component({
  selector: 'alk-interrogating-buffer-modal',
  templateUrl: './interrogating-buffer-modal.component.html',
  styleUrls: ['./interrogating-buffer-modal.component.scss']
})
export class InterrogatingBufferModalComponent implements OnInit {

  mapProjection: string;
  selectedFeaturesOrLayer: any;
  layers: Layer<any>[];
  selectedLayer: any;
  selectedOption: string;
  distance: number = 0;
  mode: 'interrogation' | 'buffers';

  constructor(
    private http: HttpClient,
    public activeModal: NgbActiveModal,
    private interrogatingService: InterrogatingService) {
    this.selectedLayer = 'noPresetSelected';
    this.selectedOption = 'noPresetSelected';
  }

  ngOnInit() { }

  setMode(mode: 'interrogation' | 'buffers') {
    this.mode = mode;
  }

  setMapProjection(proj: string) {
    this.mapProjection = proj;
  }

  setSelectedFeaturesOrLayer(buffer: any) {
    this.selectedFeaturesOrLayer = buffer;
  }

  setLayers(layers: LayerGroup) {
    this.layers = layers.getLayersArray().filter(layer => {
      try {
        if (
          layer && layer.getVisible() && layer.get('layerType') && layer.get('visibleAtScale') && layer.get('visibleAtExtent') &&
          layer.get('LayerQuery') && layer.get('LayerQuery').Fields && layer.get('LayerQuery').Fields.length &&
          (layer.getSource() as any).getUrl()
        ) {
          return true;
        }
      } catch (exe) {
        console.error(exe, layer);
      }
      return false;
    });
  }

  interrogate() {
    const layerQueryUrl = this.selectedLayer.get('class') === 'ImageLayer'
    ? this.selectedLayer.getSource().getUrl()
    : this.selectedLayer.get('rootUrl');
    const multiGeometry = [];
    if (!this.selectedFeaturesOrLayer.length) {
      this.selectedFeaturesOrLayer.getSource().getFeatures().forEach(feature => {
        multiGeometry.push(new GML3({srsName: 'EPSG:2154'}).writeGeometryNode(feature.getGeometry(), {dataProjection: this.mapProjection}));
      });
    } else {
      this.selectedFeaturesOrLayer.forEach(geojsonFeature => {
        const feat = (new GeoJSON()).readFeature(geojsonFeature.feature) as unknown as Feature<any>;
        multiGeometry.push(new GML3({srsName: 'EPSG:2154'}).writeGeometryNode(feat.getGeometry(), {dataProjection: this.mapProjection}));
      });
    }
    let multiSurface = '';
    const serializer = new XMLSerializer();
    multiGeometry.forEach(geom => {
      multiSurface += serializer.serializeToString(geom.firstChild);
    });
    const body = `<?xml version="1.0" encoding="UTF-8"?>
      <GetFeature service="WFS" version="2.0.0" outputFormat="geojson"
        xmlns="http://www.opengis.net/wfs/2.0"
        xmlns:cw="http://www.someserver.com/cw"
        xmlns:fes="http://www.opengis.net/ogc/1.1"
        xmlns:gml="http://www.opengis.net/gml/3.2">
        <Query typeNames="${this.selectedLayer.get('layerName')}" styles="default">
          <Filter>
            <${this.selectedOption}>
              <PropertyName>msGeometry</PropertyName>
                <MultiSurface xmlns="http://www.opengis.net/gml" srsName="EPSG:2154">
                  <surfaceMember>
                    ${multiSurface}
                  </surfaceMember>
                </MultiSurface>
                ${this.selectedOption === 'Dwithin' ? `<Distance units='m'>${this.distance}</Distance>` : ''}
            </${this.selectedOption}>
          </Filter>
        </Query>
      </GetFeature>`;
    this.http.post(layerQueryUrl, body).subscribe(response => {
      let totalResults = 0;
      let layerResults = 0;
      // Tableau dans lequel on stockera les données récupèrées par la requête
      const layerData = [];
      // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
      const layerQueryFields = this.selectedLayer.get('LayerQuery').Fields as Array<LayerQueryFieldType>;
      // On extrait de la liste des colonnes précédentes les clés
      const layerQueryFieldsNames = layerQueryFields.map( layerQueryField => layerQueryField.FieldName);

      // On parcours enfin les réponse pour ne garder que les données qui nous intéresse
      (response as any).features.forEach( featureInResponse => {
        totalResults++;
        layerResults++;
        const datas = Object.entries( featureInResponse.properties as {[key: string]: any}).filter(
          ([key, value]) => layerQueryFieldsNames.includes(key)
        ).reduce((accum, [key, value]) => {
          accum[key] = value;
          return accum;
        }, {});

        layerData.push(Object.assign({}, datas, {
          feature: featureInResponse
        }));
      });

      const validResults = {
        layerName : this.selectedLayer.get('name'),
        layer: this.selectedLayer,
        layerQueryFields,
        data: layerData,
        nbResults: layerResults
      };

      const resultOfInterrogation = {
        featureOfRequest: null,
        layersResults: [validResults],
        totalResults
      };

      this.interrogatingService.setResultSearching(false);
      if (totalResults > 0) {
        this.interrogatingService.setResultOfInterrogation(resultOfInterrogation);
        this.interrogatingService.changeResultOfInterrogation();
        this.interrogatingService.openResultCurrentBox();
      }

      /* if (totalResults !== 0) { */
      this.activeModal.close();
      /* } */
    });
  }

}
