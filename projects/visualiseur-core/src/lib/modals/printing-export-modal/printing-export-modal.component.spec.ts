import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintingExportModalComponent } from './printing-export-modal.component';

describe('PrintingExportModalComponent', () => {
  let component: PrintingExportModalComponent;
  let fixture: ComponentFixture<PrintingExportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintingExportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintingExportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
