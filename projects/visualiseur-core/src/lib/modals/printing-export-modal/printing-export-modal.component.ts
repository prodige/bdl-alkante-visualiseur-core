import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import {
  NgbActiveModal,
  NgbModal,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import { ExportLegendModalComponent } from "./export-legend-modal/export-legend-modal.component";
import { MapService } from "../../map/map.service";
import { VisualiseurCoreService } from "../../visualiseur-core.service";
import { DrawingBoxRatioComponent } from "../../map/tools/annotating-tools/drawing-box-ratio/drawing-box-ratio.component";

import Map from "ol/Map";
import Collection from "ol/Collection";
import BaseLayer from "ol/layer/Base";
import LayerGroup from "ol/layer/Group";
import { MapIdService } from "../../map/map-id.service";
import { Subscription } from "rxjs";
import { FeatureCollection } from "../../models/context";

@Component({
  selector: "alk-printing-export-modal",
  templateUrl: "./printing-export-modal.component.html",
  styleUrls: ["./printing-export-modal.component.scss"],
})
export class PrintingExportModalComponent implements OnInit, OnDestroy {
  rootLayerGroupSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  selectedLayers = [];

  private contextSubscription: Subscription;
  private newContext: FeatureCollection;
  private context: FeatureCollection;

  map: Map;
  private mapId: string;
  private refModalOpened: NgbModalRef;

  // Récupération des élements nécessaires à la traduction
  @ViewChild("printingExportMentionText", { read: null, static: true })
  printingExportMentionText: ElementRef;

  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private mapIdService: MapIdService,
    public mapService: MapService,
    private coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {
    this.map = this.mapService.getMap(
      this.mapIdService.getId() || this.mapId || "main"
    );

    this.rootLayerGroupSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup: LayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
        this.rootLayerGroup.getLayersArray().forEach((layer) => {
          if (layer.get("visibleChecked")) {
            this.selectedLayers.push(layer);
          }
        });
      });

    this.contextSubscription = this.coreService.getContext().subscribe(
      (context) => {
        if (!context) {
          return false;
        }
        this.context = context;
        this.newContext = Object.assign({}, this.context);
        // this.newContext = JSON.parse(JSON.stringify(this.context));
        this.mapService.mapTitlePrint = context.properties.title;
        this.mapService.mapSubtitlePrint = context.properties.subtitle;

        const generatedLayersFromContext =
          this.coreService.generateLayersAndLayerGroups(
            this.context.properties.extension.layers
          );
        let rootLayerGroup: LayerGroup =
          generatedLayersFromContext as LayerGroup;

        let rootLayerSearch = this.rootLayerSearchRecursive(
          rootLayerGroup.getLayers()
        );
        this.newContext.properties.extension.layers.layers =
          this.coreService.contextLayersAndLayerGroups(rootLayerSearch);
      },
      (error) => console.error(error)
    );
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
      this.contextSubscription = null;
    }
    if (this.rootLayerGroupSubscription) {
      this.rootLayerGroupSubscription.unsubscribe();
      this.rootLayerGroupSubscription = null;
    }
  }

  public closeModal(removeAll: boolean) {
    if (removeAll) {
      this.mapService.imageFormat = "A4";
      this.mapService.mapCommentPrint = null;
      this.mapService.setOrientation("landscape");
      this.mapService.fileFormat = "pdf";
      this.mapService.pageNumber = 1;
      this.mapService.scalingCheckbox = false;
      this.mapService.keyMapCheckbox = true;
      this.mapService.mentionCheckbox = false;
      this.mapService.mentionText = (
        this.printingExportMentionText.nativeElement as HTMLInputElement
      ).value;
      this.mapService.legendCheckbox = true;
    }

    this.activeModal.close();
  }

  selectOrientation(orientation: "portrait" | "landscape") {
    this.mapService.setOrientation(orientation);
  }

  selectFileFormat(file: "pdf" | "jpg" | "png") {
    this.mapService.fileFormat = file;
  }

  selectImageFormat(image: "A3" | "A4") {
    this.mapService.imageFormat = image;
  }

  selectPageNumber(page: 1 | 2) {
    this.mapService.pageNumber = page;
  }

  openLegendModal() {
    const modalRef = this.modalService.open(ExportLegendModalComponent, {
      size: "lg",
      centered: true,
      windowClass: "window-xl",
      container: "#modals",
    });
    if (this.mapId && modalRef.componentInstance.setRootLayerGroup) {
      modalRef.componentInstance.setSelectedLayers(this.selectedLayers);
      modalRef.componentInstance.setRootLayerGroup(this.rootLayerGroup);
    }
    modalRef.result.then((close) => {
      if (close) {
        this.selectedLayers = close;

        const generatedLayersFromContext =
          this.coreService.generateLayersAndLayerGroups(
            this.context.properties.extension.layers
          );
        let rootLayerGroup = generatedLayersFromContext as LayerGroup;

        let layerGroupSearch = this.rootLayerSearchRecursive(
          rootLayerGroup.getLayers()
        );
        this.newContext.properties.extension.layers.layers =
          this.coreService.contextLayersAndLayerGroups(layerGroupSearch);
      }
    });
  }

  rootLayerSearchRecursive(
    rootLayers: Collection<BaseLayer>
  ): Collection<BaseLayer> {
    const layersToDelete = [];
    rootLayers.getArray().forEach((rootLayer: BaseLayer, index) => {
      if (rootLayer.get("class") === "LayerGroup") {
        if ((rootLayer as LayerGroup).getLayers().getArray().length === 0) {
          layersToDelete.push(index);
        }
        rootLayer = this.rootLayerSearchRecursive(
          (rootLayer as LayerGroup).getLayers()
        ) as unknown as LayerGroup;
      } else {
        let isFound = false;
        this.selectedLayers.forEach((layer) => {
          if (rootLayer.get("layerIdx") === layer.get("layerIdx")) {
            isFound = true;
          }
        });
        if (!isFound) {
          layersToDelete.push(index);
        }
      }
    });
    if (layersToDelete) {
      layersToDelete.reverse();
      layersToDelete.forEach((idx) => {
        rootLayers.getArray().splice(idx, 1);
      });
    }
    return rootLayers;
  }

  prepareExportRequest() {
    const modelImpress = {
      img: null,
      title: null,
      text: null,
      selected: true,
      ratio: {
        A3: 0.5,
        A4: 1,
      },
      file: "modele6.tpl",
    };
    this.newContext.properties.extension.Layout.PrintModels.push(modelImpress);
    this.mapService.startImpressionRequest(
      this.mapService.fileFormat,
      this.newContext,
      modelImpress
    );
  }

  public startSelectExportArea() {
    if (!this.refModalOpened) {
      this.mapService.shapeRatioPrint = 1;
      this.coreService.setMapTool("drawShapeRatio");

      this.modalService.dismissAll();

      this.refModalOpened = this.modalService.open(DrawingBoxRatioComponent, {
        size: "sm",
        windowClass: "window-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });

      this.refModalOpened.componentInstance.setMapId(this.mapId || "main");
      this.refModalOpened.result.then(
        (close) => {
          this.refModalOpened = this.modalService.open(
            PrintingExportModalComponent,
            {
              size: "lg",
              centered: true,
              windowClass: "window-xl",
              container: "#modals",
            }
          );
          if (this.refModalOpened.componentInstance.setMapId) {
            this.refModalOpened.componentInstance.setMapId(
              this.mapId || "main"
            );
          }
        },
        (dismiss) => {}
      );
    }
  }

  public setMapId(mapId: string): void {
    this.mapId = mapId;
  }
}
