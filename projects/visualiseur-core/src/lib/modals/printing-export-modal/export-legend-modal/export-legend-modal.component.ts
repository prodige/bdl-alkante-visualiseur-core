import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import LayerGroup from 'ol/layer/Group';
import { MapService } from '../../../map/map.service';

@Component({
  selector: 'alk-export-legend-modal',
  templateUrl: './export-legend-modal.component.html',
  styleUrls: ['./export-legend-modal.component.scss']
})
export class ExportLegendModalComponent implements OnInit {

  rootLayerGroup: LayerGroup;
  selectedLayers = [];

  constructor(
    public activeModal: NgbActiveModal,
    public mapService: MapService
  ) { }

  ngOnInit() {}

  public setRootLayerGroup(rootLayerGroup: LayerGroup) {
    this.rootLayerGroup = rootLayerGroup;
  }

  public setSelectedLayers(selectedLayers: []) {
    this.selectedLayers = selectedLayers;
  }

  toggleLayersSelection(layer: any) {
    if ( this.selectedLayers.includes(layer) ) {
      this.selectedLayers = this.selectedLayers.filter( selectedFeature => selectedFeature !== layer );
    } else {
      this.selectedLayers.push(layer);
    }
  }

}
