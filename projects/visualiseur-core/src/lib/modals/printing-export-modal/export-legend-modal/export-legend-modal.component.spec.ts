import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportLegendModalComponent } from './export-legend-modal.component';

describe('ExportLegendModalComponent', () => {
  let component: ExportLegendModalComponent;
  let fixture: ComponentFixture<ExportLegendModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportLegendModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportLegendModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
