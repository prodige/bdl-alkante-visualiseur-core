import { Component, OnInit, Injectable, OnDestroy, Input } from '@angular/core';
import { ActionButtonsComponent } from '../action-buttons/action-buttons.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../visualiseur-core.service';
import { ConnexionConfig } from '../models/connexion-config';
import { UserConnectionService } from '../services/user-connection.service';
import { Subscription } from 'rxjs';
import { MapService } from '../map/map.service';
import { MapIdService } from '../map/map-id.service';

@Injectable()
@Component({
  selector: 'alk-connexion-button',
  templateUrl: './connexion-button.component.html',
  styleUrls: ['./connexion-button.component.scss']
})
export class ConnexionButtonComponent extends ActionButtonsComponent implements OnInit, OnDestroy {

  @Input() redirectUrl: string;
  userConnectionSubscription: Subscription;
  config: ConnexionConfig;
  connexionUrl: string;
  isConnected = false;
  buttonId = 'connexionButton';
  // buttonLabel = 'Connexion';
  buttonIconClass = 'fa fars-profile';

  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    private userConnectionService: UserConnectionService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('');
  }

  ngOnInit() {
    // this.userConnectionService.reloadConfig();
    this.userConnectionSubscription = this.userConnectionService.getUserConfig().subscribe( (config: ConnexionConfig)  => {
      this.config = config;
      this.isConnected = config.userInternet !== null ? !config.userInternet : null;
      if (this.isConnected) {
        this.connexionUrl = this.config.URLS.caslogout + encodeURIComponent(window.location.href);
        this.buttonLabel = this.config.name + ' ' + this.config.firstName;
        this.buttonIconClass = 'fa fars-deconnecter';
      } else if (this.isConnected !== null) {
        this.connexionUrl = this.config.URLS.caslogin + encodeURIComponent(this.redirectUrl ? this.redirectUrl : window.location.href);
        this.buttonLabel = 'Connexion';
        this.buttonIconClass = 'fa fars-profile';
      }
      if (config.userInternet !== null) {
        this.userConnectionService.loadCatalogue(this.config);
      }
    });
  }

  ngOnDestroy() {
    if (this.userConnectionSubscription) {
      this.userConnectionSubscription.unsubscribe();
    }
  }

  buttonAction(event: Event) { }

}
