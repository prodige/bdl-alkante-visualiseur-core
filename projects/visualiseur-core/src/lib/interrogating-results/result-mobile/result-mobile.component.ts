import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MapIdService } from '../../map/map-id.service';
import { InterrogationReturnType, InterrogatingService, LayerDataResponseType, DataFeatureType } from '../../map/services/interrogating.service';
import { Subscription } from 'rxjs';
import { MapService } from '../../map/map.service';
import BaseLayer from 'ol/layer/Base';
import Feature from 'ol/Feature';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-result-mobile',
  templateUrl: './result-mobile.component.html',
  styleUrls: ['./result-mobile.component.scss']
})
export class ResultMobileComponent implements OnInit, OnDestroy {

  resultsOfInterrogation: InterrogationReturnType = null;
  resultsOfInterrogationSubscription: Subscription;
  resultMobileBoxClosedSubscription: Subscription;
  closed = true;
  selectedLayer: LayerDataResponseType;

  layerResultIndex: {[layerResultName: string]: {index: number, nbResults: number}} = {};

  public mapId: string;
  public mailingTool: any;

  constructor(
    public interrogatingService: InterrogatingService,
    public mapService: MapService
  ) {}

  ngOnInit() {

    this.resultsOfInterrogationSubscription = this.interrogatingService.getResultOfInterrogation().subscribe(
      ( results: InterrogationReturnType ) => {
        this.resultsOfInterrogation = results;
        this.layerResultIndex = {};
        if (this.resultsOfInterrogation && this.resultsOfInterrogation.layersResults) {
          this.resultsOfInterrogation.layersResults.forEach( layerResult => {
            this.layerResultIndex[layerResult.layerName] = {
              index: 0,
              nbResults: layerResult.nbResults
            };
          });
          this.selectedLayer = this.resultsOfInterrogation.layersResults[0];
        }
      }
    );

    this.resultMobileBoxClosedSubscription = this.interrogatingService.getResultMobileBox().subscribe(
      isClosed => {
        this.closed = isClosed;
      }
    );
  }

  ngOnDestroy() {
    if ( this.resultsOfInterrogationSubscription ) {
      this.resultsOfInterrogationSubscription.unsubscribe();
    }
    if ( this.resultMobileBoxClosedSubscription ) {
      this.resultMobileBoxClosedSubscription.unsubscribe();
    }
  }

  getLayerQueryField(layer: BaseLayer, feature: DataFeatureType, urlField: any): string {
    let finalUrl = (layer.get('LayerQuery').URLConfig ? layer.get('LayerQuery').URLConfig : '' )
                   + (urlField.FieldURLConfig ? urlField.FieldURLConfig : '');
    const fieldsToSearch = finalUrl.split(/[<>]/);

    if( finalUrl.match(/[<>]/) && fieldsToSearch.length>0){
      fieldsToSearch.forEach(fieldToSearch => {
        layer.get('LayerQuery').Fields.forEach(field => {
          if (field.FieldName && fieldToSearch) {
            for (const [key, value] of Object.entries(feature)) {
              if (key === field.FieldName) {
                finalUrl = finalUrl.replace(`<${field.FieldName}>`, value);
              }
            }
          }
        });
      });
    }else{
      finalUrl += feature[ urlField.FieldName ];
    }
    
    return finalUrl;
  }

  setMapId(id: string | MapIdService) {
    if (typeof id === 'string') {
      this.mapId = id;
    } else {
      this.mapId = id.getId();
    }
  }

  setMailingTool(tool: any) {
    this.mailingTool = tool;
  }

  closeModalBox(): void {
    this.interrogatingService.closeResultModalBox();
    this.interrogatingService.resetResultOfInterrogation();
  }

  prevFeature(layerResultName): void {
    if ( this.layerResultIndex[layerResultName].index === 0 ) {
      this.layerResultIndex[layerResultName].index = this.layerResultIndex[layerResultName].nbResults - 1;
    } else {
      this.layerResultIndex[layerResultName].index --;
    }
  }

  nextFeature(layerResultName): void {
    if ( this.layerResultIndex[layerResultName].index === (this.layerResultIndex[layerResultName].nbResults - 1) ) {
      this.layerResultIndex[layerResultName].index = 0;
    } else {
      this.layerResultIndex[layerResultName].index ++;
    }
  }

}
