import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultMobileComponent } from './result-mobile.component';

describe('ResultMobileComponent', () => {
  let component: ResultMobileComponent;
  let fixture: ComponentFixture<ResultMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
