import { EnvCoreService } from './../../../env.core.service';
import { Component, OnInit, Input, OnDestroy, Injectable, Inject, Output, EventEmitter } from '@angular/core';
import { DataFeatureType, LayerQueryFieldType, InterrogatingService, LayerDataResponseType } from '../../../map/services/interrogating.service';
import Map from 'ol/Map';
import Feature from 'ol/Feature';
import { MapService } from '../../../map/map.service';
import { MapIdService } from '../../../map/map-id.service';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InterrogatingMailingModalComponent } from '../../../modals/interrogating-mailing-modal/interrogating-mailing-modal.component';
import { createEmpty, extend } from 'ol/extent';
import { GeoJSONFeature } from 'ol/format/GeoJSON';
import GeoJSON from 'ol/format/GeoJSON';
import { Subscription, forkJoin } from 'rxjs';
import { BuffersModalComponent } from '../../../map/tools/annotating-tools/buffers/buffers-modal/buffers-modal.component';
import { DrawLayerService } from '../../../services/draw-layer.service';
import VectorLayer from 'ol/layer/Vector';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { ResultModalFicheComponent } from '../../result-modal-fiche/result-modal-fiche.component';
import { EditingToolService } from '../../../services/editing-tool.service';
import { ImageWMS } from 'ol/source';
import { WKT } from 'ol/format';
import { VisualiseurCoreService } from '../../../visualiseur-core.service';
import { InterrogatingBufferModalComponent } from '../../../modals/interrogating-buffer-modal/interrogating-buffer-modal.component';
import LayerGroup from 'ol/layer/Group';
import BaseLayer from 'ol/layer/Base';
import Collection from 'ol/Collection';
import { UserConnectionService } from '../../../services/user-connection.service';
import { Router } from '@angular/router';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';

@Injectable()
@Component({
  selector: 'alk-result-content',
  templateUrl: './result-content.component.html',
  styleUrls: ['./result-content.component.scss']
})
export class ResultContentComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  geojson = new GeoJSON();
  format = new WKT();

  coreSubscription: Subscription;
  spatialQuery: boolean;
  rootLayerSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  layerGroup: LayerGroup;

  forkJoin: Subscription;

  drawLayerSubscription: Subscription;
  drawLayerServiceSubscription: Subscription;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;

  currentLayerResultsSubscription: Subscription;
  currentLayerResults: LayerDataResponseType;

  interrogationDisplayLayerSubscription: Subscription;
  interrogationDisplayLayer: VectorLayer<VectorSource<Geometry>>;

  map: Map;
  mapReadySubscription: Subscription;
  modalRef: NgbModalRef = null;

  isOneLayer = false;
  isView = true;
  isZone = true;
  isMail = true;
  isExport = true;
  isEditable = false;
  isDeletable = false;
  isInfo = true;
  relations = [];

  @Output() editEvent = new EventEmitter<DataFeatureType>();
  @Output() deleteEvent = new EventEmitter<DataFeatureType>();

  @Input() mailingTool: any;
  // tslint:disable-next-line: no-input-rename
  @Input('mapId') mapId: string | MapIdService = null;

  constructor(
    public http: HttpClient,
    public coreService: VisualiseurCoreService,
    public mapService: MapService,
    public mapIdService: MapIdService,
    public modalService: NgbModal,
    public activeModal: NgbActiveModal,
    public editingToolService: EditingToolService,
    public drawLayerService: DrawLayerService,
    public interrogatingService: InterrogatingService,
    public environment: EnvCoreService,
    public userConnectionService: UserConnectionService,
    protected router: Router
  ) { }

  ngOnInit() {

    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }

    this.coreSubscription = this.coreService.getContext().subscribe(context => {
      if(!context){
        return false;
      }
      this.spatialQuery = context.properties.extension.Tools.InfoSpatialQuery;
    });

    this.rootLayerSubscription = this.coreService.getRootLayerGroup().subscribe(group => {
      this.rootLayerGroup = group;
    });

    this.layerGroup = new LayerGroup({
      layers: null
    });
    const collection: Collection<BaseLayer> = new Collection<BaseLayer>();
    if (this.interrogatingService.resultOfInterrogation && this.interrogatingService.resultOfInterrogation.layersResults) {
      this.interrogatingService.resultOfInterrogation.layersResults.forEach(layerResult => {
        /* if (layerResult.layer.getSource().get('class') !== 'ImageWMS') {
          collection.push(layerResult.layer);
        } */
        collection.push(layerResult.layer);
      });
      this.layerGroup.setLayers(collection);
    }

    this.mapReadySubscription = this.mapService.getMapReady(this.mapId || this.mapIdService.getId())
    .subscribe((map) => {
      this.map = map;

      this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
        ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
          if ( vectorLayer ) {
            this.vectorLayer = vectorLayer;
          }
        }
      );

      this.interrogationDisplayLayerSubscription = this.interrogatingService.getInterrogationDisplayLayer(this.map).subscribe(
        interrogationDisplayLayer => {
          this.interrogationDisplayLayer = interrogationDisplayLayer;
        }
      );
    });

    this.currentLayerResultsSubscription = this.interrogatingService.getCurrentLayerResults().subscribe(
      layerResults => {
        this.currentLayerResults = layerResults;
      }
    );
  }

  ngOnDestroy() {
    if (this.coreSubscription) {
      this.coreSubscription.unsubscribe();
    }
    if (this.rootLayerSubscription) {
      this.rootLayerSubscription.unsubscribe();
    }
    if (this.drawLayerSubscription) {
      this.drawLayerSubscription.unsubscribe();
    }
    if (this.drawLayerServiceSubscription) {
      this.drawLayerServiceSubscription.unsubscribe();
    }
    if (this.interrogationDisplayLayerSubscription) {
      this.interrogationDisplayLayerSubscription.unsubscribe();
    }
    if (this.mapReadySubscription) {
      this.mapReadySubscription.unsubscribe();
    }
  }

  createBuffer() {
    if (this.interrogatingService.selectedDataFeatures.length) {
      this.drawLayerService.generateNewBufferLayerGroup(this.mapId);
      if ( this.activeModal ) {
        this.activeModal.close('hide');
      }
      const modalRef = this.modalService.open(BuffersModalComponent, {
        size: 'lg',
        centered: true,
        container: '#modals',
        keyboard: false,
        windowClass: 'window-map-tool'
      });
      if (this.interrogatingService.selectedDataFeatures && modalRef.componentInstance.setSelectFeature) {
        modalRef.componentInstance.mapId = this.mapId;
        modalRef.componentInstance.setSelectedFeatures(this.interrogatingService.selectedDataFeatures);
        modalRef.componentInstance.setVectorLayer(this.vectorLayer);
      }
      modalRef.result.then(close => {

      });
    }
  }

  sendMail() {
    const modalRef = this.modalService.open(InterrogatingMailingModalComponent, {
      size: 'sm',
      centered: true,
      windowClass: 'window-xl',
      container: 'alk-visualiseur-core'
    });
    if (this.interrogatingService.currentLayerResults) {
      modalRef.componentInstance.setSelectedLayer(this.interrogatingService.currentLayerResults);
      modalRef.componentInstance.setMailingTool(this.mailingTool);
    }
  }

  linkRelation(relationId, gidData){
    this.router.navigateByUrl('/relations/' + relationId + '/' + gidData);
  }

  export(type: string) {
    const items = [];
    if (this.interrogatingService.currentLayerResults) {
      this.interrogatingService.currentLayerResults.data.forEach(result => {
        const item = {};
        this.interrogatingService.currentLayerResults.layerQueryFields.forEach(field => {
          item[field.FieldName] = result[field.FieldName];
        });
        items.push(item);
      });
    }

    let fileName = this.interrogatingService.currentLayerResults.layerName;
    const map: {[key: string]: RegExp } = {
      _: /[\s-]-?\s?/g,
      a: /á|à|ã|â|À|Á|Ã|Â/g,
      e: /é|è|ê|É|È|Ê/g,
      i: /í|ì|î|Í|Ì|Î/g,
      o: /ó|ò|ô|õ|Ó|Ò|Ô|Õ/g,
      u: /ú|ù|û|ü|Ú|Ù|Û|Ü/g,
      c: /ç|Ç/g,
      n: /ñ|Ñ/g
    };
    fileName = fileName.toLowerCase();
    let pattern: string;
    for ( pattern in map) {
      if ( typeof map[pattern] !== 'undefined' ) {
        fileName = fileName.replace(map[pattern], pattern);
      }
    }

    if (type === 'xls') {
      this.http.post(`${this.environment.serverUrl}core/export/jsonToExcel`, items, {responseType: 'blob'}).subscribe(response => {
        saveAs(response, fileName + '.xls');
      });
    } else if (type === 'csv') {
      this.http.post(`${this.environment.serverUrl}core/export/jsonToCsv`, items, {responseType: 'blob'}).subscribe(response => {
        saveAs(response, fileName + '.csv');
      });
    }
    else if(type === 'shp'){
      const url = this.environment.cartorExportUrlDev ? this.environment.cartorExportUrlDev : this.environment.serverUrl;
      const layerData = {
        type: "FeatureCollection",
        features:  [] 
      }
      


      this.interrogatingService.currentLayerResults.data.forEach(( featureType: DataFeatureType) => {
        layerData.features.push(featureType.feature);
      });

    
      if(layerData){
        this.http.post(`${url}core/export/jsonToShp`, layerData, {responseType: 'blob'}).subscribe(response => {
          saveAs(response, fileName + '.zip');
        })
      }
     
    }
  }

  selectLayerResults(layerResults): void {
    this.interrogatingService.selectLayerResults(layerResults);
  }

  centerOnSelectedFeatures(): void {
    if (this.interrogatingService.selectedDataFeatures && this.interrogatingService.selectedDataFeatures.length !== 0) {
      const extent = createEmpty();
      const featuresToDisplay: Array<Feature<any>> = [];
      this.interrogatingService.selectedDataFeatures.forEach(dataFeature => {
        const feature = this.geojson.readFeature(dataFeature.feature) as unknown as Feature<any>;
        if ( feature && feature.getGeometry() ) {
          extend(extent, feature.getGeometry().getExtent());
          featuresToDisplay.push(feature);
        }
      });
      if (extent) {
        this.map.getView().fit( extent, {size: this.map.getSize(), maxZoom: 16});
        this.mapService.setMapFitEvent(true);
      }
  
      if ( this.interrogationDisplayLayer ) {
        this.interrogationDisplayLayer.getSource().clear();
        this.interrogationDisplayLayer.getSource().addFeatures(featuresToDisplay);
      }
      if ( this.interrogatingService.getResultCurrentBox() === 'modal' ) {
        this.interrogatingService.closeResultCurrentBox();
        if ( this.activeModal ) {
          this.activeModal.close('hide');
        }
      }
    }
  }

  centerOnFeature(featureGeoJson: GeoJSONFeature): void {
    const feature = this.geojson.readFeature(featureGeoJson) as Feature<Geometry>;
    this.map.getView().fit( feature.getGeometry().getExtent(), {size: this.map.getSize(), maxZoom: 16});
    this.mapService.setMapFitEvent(true);

    if ( this.interrogationDisplayLayer ) {
      this.interrogationDisplayLayer.getSource().clear();
      this.interrogationDisplayLayer.getSource().addFeature(feature);
    }
    if ( this.interrogatingService.getResultCurrentBox() === 'modal' ) {
      this.interrogatingService.closeResultCurrentBox();
      if ( this.activeModal ) {
        this.activeModal.close('hide');
      }
    }
  }

  displayFeatureInformations(feature: DataFeatureType): void {

    const currentLayerResults = this.interrogatingService.currentLayerResults;

    const forkJoinObservables = [];

    this.userConnectionService.getUserConfig().subscribe(config => {
      if (config.userInternet !== null) {
        forkJoinObservables.push(this.editingToolService.getEditableFeaturesStructure(currentLayerResults.layer.get('layerMetadataUuid')));
      }

      const feat = this.geojson.readFeature(feature.feature) as unknown as Feature<any>;

      this.interrogatingService.closeResultCurrentBox();
      if ( this.activeModal ) {
        this.activeModal.close('hide');
      }

      if (forkJoinObservables.length !== 0) {
        this.forkJoin = forkJoin(forkJoinObservables).subscribe(response => {
          
          currentLayerResults.layerQueryFields.forEach(field => {
            const structure = (response[0] as any).filter(feat => feat.column_name === field.FieldName);
            if (structure.length > 0) {
              field['structure'] = structure[0];
            }
          });

          this.modalRef = this.modalService.open(ResultModalFicheComponent, {
            container: '#modals',
            centered: true,
            size: 'lg',
            backdrop: 'static'
          });

          const modalInstance: ResultModalFicheComponent = this.modalRef.componentInstance;
          modalInstance.currentLayerResults = this.interrogatingService.currentLayerResults;
          modalInstance.currentSelectedFeature = feature;
          modalInstance.modalReady = true;
          this.modalRef.result.then(
            result => {
              if ( modalInstance ) {
                if (result && result === 'save') {
                  for (const [key, value] of Object.entries(feature)) {
                    if (key !== 'feature' && key !== 'properties') {
                      feature.feature.properties[key] = value;
                    }
                  }
                  
              
                  for (let key in feature.feature.properties) {
                    //remove _fileName key included with image field and not necessary for API
                    if(key.includes('_fileName') && feature.feature.properties.hasOwnProperty(key.replace("_fileName", "")) ){
                      delete  feature.feature.properties[key];
                    }
                  }
                  const feat = this.geojson.readFeature(feature.feature) as unknown as Feature<any>;

                  this.editingToolService.modify(currentLayerResults.layer.get('layerMetadataUuid'), this.geojson.writeFeaturesObject([feat]))
                    .subscribe(modifed => {
                    (currentLayerResults.layer.getSource() as ImageWMS).updateParams({ date: new Date() });
                    currentLayerResults.layer.changed();
                  });
                }

                if (!result || result !== 'distanceTool') {
                  modalInstance.modalReady = false;
                  modalInstance.currentLayerResults = null;
                  modalInstance.editing = false;
                  modalInstance.currentSelectedFeature = null;
                }
              }
              if (!result || result !== 'distanceTool') {
                this.interrogatingService.openResultCurrentBox();
              }
            }
          );
        });
      } else {

        this.modalRef = this.modalService.open(ResultModalFicheComponent, {
          centered: true,
          size: 'lg',
          backdrop: 'static'
        });

        const modalInstance: ResultModalFicheComponent = this.modalRef.componentInstance;
        modalInstance.currentLayerResults = this.interrogatingService.currentLayerResults;
        modalInstance.currentSelectedFeature = feature;
        modalInstance.modalReady = true;
        this.modalRef.result.then(
          result => {
            if ( modalInstance ) {
              if (result && result === 'save') {
              
                for (const [key, value] of Object.entries(feature)) {
                  if (key !== 'feature' && key !== 'properties') {
                    feature.feature.properties[key] = value;
                  }
                }

                this.editingToolService.modify(currentLayerResults.layer.get('layerMetadataUuid'), this.geojson.writeFeaturesObject([feat]))
                  .subscribe(modifed => {
                  (currentLayerResults.layer.getSource() as ImageWMS).updateParams({ date: new Date() });
                  currentLayerResults.layer.changed();
                });
              }

              modalInstance.modalReady = false;
              modalInstance.currentLayerResults = null;
              modalInstance.editing = false;
              modalInstance.currentSelectedFeature = null;
            }
            this.interrogatingService.openResultCurrentBox();
          }
        );
      }
    });
  }

  openSelectingModal() {
    if (this.interrogatingService.selectedDataFeatures.length && (this.layerGroup && this.layerGroup.getLayersArray().length !== 0)) {
      this.interrogatingService.closeResultCurrentBox();
      if ( this.activeModal ) {
        this.activeModal.close('hide');
      }
      const modalRef = this.modalService.open(InterrogatingBufferModalComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'window-xl',
        container: '#modals',
        keyboard: false
      });
      this.map = this.mapService.getMap(this.mapId);
      modalRef.componentInstance.setMode('interrogation');
      modalRef.componentInstance.setMapProjection(this.map.getView().getProjection());
      modalRef.componentInstance.setSelectedFeaturesOrLayer(this.interrogatingService.selectedDataFeatures);
      modalRef.componentInstance.setLayers(this.layerGroup as LayerGroup);
    }
  }

  getLayerQueryField(layer: BaseLayer, feature: Feature<Geometry>, urlField: any): string {
    let finalUrl = (layer.get('LayerQuery').URLConfig ? layer.get('LayerQuery').URLConfig : '' )
                   + (urlField.FieldURLConfig ? urlField.FieldURLConfig : '');
    const fieldsToSearch = finalUrl.split(/[<>]/);
    
    if( finalUrl.match(/[<>]/) && fieldsToSearch.length>0){
      fieldsToSearch.forEach(fieldToSearch => {
        layer.get('LayerQuery').Fields.forEach(field => {
          if (field.FieldName && fieldToSearch) {
            for (const [key, value] of Object.entries(feature)) {
              if (key === field.FieldName) {
                finalUrl = finalUrl.replace(`<${field.FieldName}>`, value);
                
              }
            }
          }
        });
      });
    }else{
      finalUrl += feature[ urlField.FieldName ];
    }
    
    return finalUrl;
  }

  toggleAll(): void {
    this.interrogatingService.toggleAll();
  }

  toggleDataFeatureSelection(dataFeature: DataFeatureType): void {
    this.interrogatingService.toggleDataFeatureSelection(dataFeature);
  }

  searchText(searchTxt: string): void {
    this.interrogatingService.searchText(searchTxt);
  }

  changePageResult(numPage: string|number): void {
    this.interrogatingService.changePageResult(numPage.toString());
  }

  sortOn(sortOn: LayerQueryFieldType): void {
    this.interrogatingService.sortOn(sortOn);
  }

  editDataFeatureSelection(editDataFeature: DataFeatureType): void {
    this.editEvent.emit(editDataFeature);
  }

  deleteDataFeatureSelection(deleteDataFeature: DataFeatureType): void {
    this.deleteEvent.emit(deleteDataFeature);
  }

}
