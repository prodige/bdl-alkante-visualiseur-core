import { InterrogatingService, InterrogationReturnType } from './../../map/services/interrogating.service';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { MapService } from '../../map/map.service';
import { MapIdService } from '../../map/map-id.service';

@Component({
  selector: 'alk-result-modal',
  templateUrl: './result-modal.component.html',
  styleUrls: ['./result-modal.component.scss']
})
export class ResultModalComponent implements OnInit, OnDestroy {

  resultsOfInterrogation: InterrogationReturnType = null;
  resultsOfInterrogationSubscription: Subscription;
  resultModalBoxClosedSubscription: Subscription;
  closed = true;

  public mapId: string;
  public mailingTool: any;

  constructor(
    public activeModal: NgbActiveModal,
    public interrogatingService: InterrogatingService,
    public mapService: MapService
  ) {}

  ngOnInit() {

    this.resultsOfInterrogationSubscription = this.interrogatingService.getResultOfInterrogation().subscribe(
      ( results: InterrogationReturnType ) => {
        this.resultsOfInterrogation = results;
      }
    );

    this.resultModalBoxClosedSubscription = this.interrogatingService.getResultModalBox().subscribe(
      isClosed => {
        this.closed = isClosed;
      }
    );
  }

  ngOnDestroy() {
    if ( this.resultsOfInterrogationSubscription ) {
      this.resultsOfInterrogationSubscription.unsubscribe();
    }
    if ( this.resultModalBoxClosedSubscription ) {
      this.resultModalBoxClosedSubscription.unsubscribe();
    }
  }

  setMapId(id: string | MapIdService) {
    if (typeof id === 'string') {
      this.mapId = id;
    } else {
      this.mapId = id.getId();
    }
  }

  setMailingTool(tool: any) {
    this.mailingTool = tool;
  }

  openBottomBox(): void {
    this.activeModal.close(/*'bottom-box'*/);
    this.interrogatingService.closeResultModalBox();
    this.interrogatingService.openResultBottomBox();
    this.interrogatingService.setResultCurrentBox('bottom');
  }

  closeModalBox(): void {
    this.activeModal.close(/*'quit'*/);
    this.interrogatingService.closeResultModalBox();
    this.interrogatingService.resetResultOfInterrogation();
  }

  reduceModalBox(): void {
    this.activeModal.close(/*'hide'*/);
    this.interrogatingService.closeResultModalBox();
  }
}
