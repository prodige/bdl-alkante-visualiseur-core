import { MapService } from './../../map/map.service';
import { Component, OnInit, ElementRef, Renderer2, Input, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InterrogatingService, InterrogationReturnType } from '../../map/services/interrogating.service';
import { Subscription } from 'rxjs';
import { MapIdService } from '../../map/map-id.service';

@Component({
  selector: 'alk-result-bottom-box',
  templateUrl: './result-bottom-box.component.html',
  styleUrls: ['./result-bottom-box.component.scss']
})
export class ResultBottomBoxComponent implements OnInit, OnDestroy {

  /* modalReduceBottom = false; */
  resultsOfInterrogation: InterrogationReturnType = null;
  resultsOfInterrogationSubscription: Subscription;
  closed = true;
  resultBottomBoxClosedSubscription: Subscription;

  constructor(
    public modalService: NgbModal,
    public el: ElementRef,
    public renderer: Renderer2,
    public interrogatingService: InterrogatingService,
    public mapService: MapService
  ) { }

  ngOnInit() {

    this.resultsOfInterrogationSubscription = this.interrogatingService.getResultOfInterrogation().subscribe(
      ( results: InterrogationReturnType ) => {
        this.resultsOfInterrogation = results;
      }
    );

    this.resultBottomBoxClosedSubscription = this.interrogatingService.getResultBottomBox().subscribe(
      isClosed => {
        this.closed = isClosed;
        if ( !isClosed ) {
          (this.el.nativeElement as HTMLElement).classList.add('bottom-opened');
        } else {
          (this.el.nativeElement as HTMLElement).classList.remove('bottom-opened');
        }
      }
    );
  }

  ngOnDestroy(): void {
    if ( this.resultsOfInterrogationSubscription ) {
      this.resultsOfInterrogationSubscription.unsubscribe();
    }
    if ( this.resultBottomBoxClosedSubscription ) {
      this.resultBottomBoxClosedSubscription.unsubscribe();
    }
  }

  reduceBottomBox() {
    this.interrogatingService.closeResultBottomBox();
  }

  openModalBox() {
    this.interrogatingService.closeResultBottomBox();
    this.interrogatingService.openResultModalBox();
    this.interrogatingService.setResultCurrentBox('modal');
  }

  closeBottomBox() {
    this.interrogatingService.closeResultBottomBox();
    this.interrogatingService.resetResultOfInterrogation();
  }

}
