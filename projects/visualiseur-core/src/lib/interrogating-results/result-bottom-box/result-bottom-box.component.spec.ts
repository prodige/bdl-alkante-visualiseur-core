import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultBottomBoxComponent } from './result-bottom-box.component';

describe('ResultBottomBoxComponent', () => {
  let component: ResultBottomBoxComponent;
  let fixture: ComponentFixture<ResultBottomBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultBottomBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultBottomBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
