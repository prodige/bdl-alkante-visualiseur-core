import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";

import Map from "ol/Map";
import { map, catchError } from "rxjs/operators";
import {
  LayerDataResponseType,
  DataFeatureType,
  InterrogatingService,
} from "../../map/services/interrogating.service";
import {
  NgbModalConfig,
  NgbModal,
  NgbActiveModal,
} from "@ng-bootstrap/ng-bootstrap";
import { AlertConfirmPromptModalComponent } from "../../modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import { EditingToolService } from "../../services/editing-tool.service";
import { VisualiseurCoreService } from "../../visualiseur-core.service";
import Feature from "ol/Feature";
import BaseLayer from "ol/layer/Base";
import * as momentImported from "moment";
import { __core_private_testing_placeholder__ } from "@angular/core/testing";
import { Circle, Geometry } from "ol/geom";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import LayerGroup from "ol/layer/Group";
import { Subscription, forkJoin, of } from "rxjs";
import { getDistance } from "ol/sphere";
import { MapService } from "../../map/map.service";
import { MapIdService } from "../../map/map-id.service";
import { fromCircle } from "ol/geom/Polygon";
import { EnvCoreService } from "../../env.core.service";
import { getPointResolution } from "ol/proj";
import { HttpClient } from "@angular/common/http";
import { Polyline, GeoJSON } from "ol/format";
import Overlay from "ol/Overlay";
import { getCenter } from "ol/extent";

const moment = momentImported;

type Moment = momentImported.Moment;

@Component({
  selector: "alk-result-modal-fiche",
  templateUrl: "./result-modal-fiche.component.html",
  styleUrls: ["./result-modal-fiche.component.scss"],
})
export class ResultModalFicheComponent implements OnInit, OnDestroy {
  map: Map;
  mapId: string;

  currentLayerResults: LayerDataResponseType = null;
  currentSelectedFeature: DataFeatureType = null;
  modalReady = false;
  editing = false;

  rootLayerGroupSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  interrogationSubscription: Subscription;

  forkJoin: Subscription;

  @ViewChild("distanceTooltip", { read: null, static: true })
  distanceTooltipElementRef: ElementRef;
  distanceTooltip: Overlay;

  @ViewChild("copyToast", { read: null, static: true }) copyToast: ElementRef;
  isToastVisible = false;

  interrogationDisplayLayerSubscription: Subscription;
  interrogationDisplayLayer: VectorLayer<VectorSource<Geometry>>;

  enumsSubscription: Subscription;
  enum: Array<string>;

  editionConfigSubscription: Subscription;
  editionConfig: any;
  layerEditionConfig: any = null;

  isSearchingForShortPath: boolean = false;

  public dropdownSettings = {};

  public regex = {
    email:
      /^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/g,
    phone_number: /^(\+33|0)[1-9](\d\d){4}$/g,
  };

  @ViewChild(AlertConfirmPromptModalComponent, { read: null, static: true })
  confirmModal: AlertConfirmPromptModalComponent;

  constructor(
    public mapService: MapService,
    public mapIdService: MapIdService,
    private config: NgbModalConfig,
    public coreService: VisualiseurCoreService,
    public activeModal: NgbActiveModal,
    private editingToolService: EditingToolService,
    private interrogationService: InterrogatingService,
    private environment: EnvCoreService,
    private http: HttpClient,
    private elementRef: ElementRef
  ) {
    this.config.backdrop = "static";
    this.config.keyboard = false;
  }

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;

      if (this.map) {
        this.interrogationDisplayLayerSubscription = this.interrogationService
          .getInterrogationDisplayLayer(this.map)
          .subscribe((interrogationDisplayLayer) => {
            this.interrogationDisplayLayer = interrogationDisplayLayer;
          });
      }
    });

    this.rootLayerGroupSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      });

    this.editionConfigSubscription = this.coreService
      .getContext()
      .subscribe((context) => {
        if (!context) {
          return false;
        }
        this.editionConfig =
          context.properties.extension.Tools.Edition &&
          context.properties.extension.Tools.Edition.active
            ? context.properties.extension.Tools.Edition.config
            : null;
        if (this.editionConfig) {
          this.editionConfig.forEach((layerConfig) => {
            if (this.currentLayerResults.layer.get("layerMetadataUuid")) {
              if (
                layerConfig.uuid ===
                this.currentLayerResults.layer.get("layerMetadataUuid")
              ) {
                this.layerEditionConfig = layerConfig;
              }
            }
          });
        }
      });

    if (this.currentLayerResults.layer.get("layerMetadataUuid")) {
      this.enumsSubscription = this.editingToolService
        .getUserDefinedEnum(
          this.currentLayerResults.layer.get("layerMetadataUuid")
        )
        .subscribe((userDefinedEnum) => {
          this.enum = userDefinedEnum;
        });
      this.currentLayerResults.layerQueryFields.forEach((field) => {
        if (!this.currentSelectedFeature[field.FieldName] && field.structure) {
          if (
            field.structure.column_default &&
            field.structure.column_default.includes("::")
          ) {
            this.currentSelectedFeature[field.FieldName] =
              field.structure.column_default.split("::")[0].split("'")[1];
          } else {
            this.currentSelectedFeature[field.FieldName] =
              field.structure.column_default;
          }
        }
        if (
          field.structure &&
          field.structure.data_type === "boolean" &&
          !field.structure.is_nullable
        ) {
          this.currentSelectedFeature[field.FieldName] = false;
        }
      });
    }

    this.currentLayerResults.layerQueryFields.forEach((field) => {
      if (
        field.structure &&
        field.structure.data_type === "ARRAY" &&
        this.currentSelectedFeature[field.FieldName] &&
        this.currentSelectedFeature[field.FieldName].includes("{")
      ) {
        const list = this.currentSelectedFeature[field.FieldName]
          .replace("{", "")
          .replace("}", "")
          .split(",");
        this.currentSelectedFeature[field.FieldName] = list;
      }
    });

    /* this.interrogationSubscription = this.interrogationService.postInterrogation(
      interrogableLayers as VectorLayer<VectorSource<Geometry>>[], feature, this.map.getView().getProjection()
    ).subscribe(
      results => {},
      error => {
        console.error('this.interrogationService.postInterrogation error', error);
      }
    ); */
  }

  ngOnDestroy(): void {
    this.currentLayerResults = null;
    this.currentSelectedFeature = null;
    this.modalReady = false;
    this.editing = false;
    if (this.enumsSubscription) {
      this.enumsSubscription.unsubscribe();
    }
    if (this.editionConfigSubscription) {
      this.editionConfigSubscription.unsubscribe();
    }
    if (this.interrogationSubscription) {
      this.interrogationSubscription.unsubscribe();
    }
  }

  checkForShortPath(meters = 20000) {
    this.map.getOverlays().forEach((overlay, index) => {
      this.map.removeOverlay(overlay);
    });

    let format =
      (this.currentSelectedFeature.feature.geometry.type as any) ===
      "Linestring"
        ? new Polyline()
        : (this.currentSelectedFeature.feature.geometry.type as any) ===
          "Polygon"
        ? new GeoJSON()
        : null;

    this.isSearchingForShortPath = true;
    let center = format
      ? getCenter(
          format
            .readFeature(this.currentSelectedFeature.feature)
            .getGeometry()
            .getExtent()
        )
      : (this.currentSelectedFeature.feature.geometry as any).coordinates;
    let circle = new Feature(
      new Circle(
        center,
        meters /*  / getPointResolution(this.map.getView().getProjection(), 1, center, "m" ) */
      )
    );
    this.searchFromCircle(circle, meters, center);
  }

  searchFromCircle(feature: Feature<Circle>, meters, center) {
    this.interrogationService.setResultSearching(true);
    if (this.interrogationSubscription) {
      this.interrogationSubscription.unsubscribe();
    }

    if (feature && this.rootLayerGroup) {
      feature = new Feature<any>({
        name: "tooltip",
        geometry: fromCircle(feature.getGeometry()),
      });

      const interrogableLayers = this.rootLayerGroup
        .getLayersArray()
        .filter((layer) => {
          try {
            if (
              layer.get("layerMetadataUuid") ===
              this.environment.nearestPoint.uuid
            ) {
              return true;
            } else {
              return false;
            }
          } catch (exe) {
            console.error(exe, layer);
          }
          return false;
        });

      // this.interrogationService.setSpinner(true);

      this.interrogationSubscription = this.interrogationService
        .postInterrogation(
          interrogableLayers as VectorLayer<VectorSource<Geometry>>[],
          feature,
          this.map.getView().getProjection(),
          null,
          null,
          null,
          false
        )
        .subscribe({
          next: (results) => {
            let layerInResults = results.layersResults.filter(
              (result) =>
                result.layer.get("layerMetadataUuid") ===
                this.environment.nearestPoint.uuid
            );
            if (
              layerInResults.length !== 0 &&
              layerInResults[0].data.length > 3
            ) {
              let nearestFeatures = this.getNearestFeatures(
                results.layersResults[0].data,
                center
              );
              this.calculateRoutes(nearestFeatures, center);
            } else {
              if (meters < 100000) {
                this.checkForShortPath(meters + 20000);
              } else {
                setTimeout(() => {
                  this.copyToast.nativeElement.className = "toast show";
                  setTimeout(() => {
                    this.copyToast.nativeElement.className =
                      this.copyToast.nativeElement.className.replace(
                        "show",
                        ""
                      );
                  }, 8000);
                }, 1000);
                this.isSearchingForShortPath = false;
              }
            }
          },
          error: (err) => {
            console.error(
              "this.interrogationService.postInterrogation error",
              err
            );
          },
          complete: () => {},
        });
    }
    // A la fin du traitement, on retire le point
    // this.vectorLayer.getSource().clear();
  }

  getNearestFeatures(features: Array<any>, center) {
    // Calcul des distances entre le centre et chaque point
    features.forEach((feat) => {
      feat.distance = getDistance(center, feat.feature.geometry.coordinates);
    });
    // Tri croissant des distances
    features.sort((a, b) => a.distance - b.distance);
    // On ne renvoie pas features[0] car il correspond au point de référence lui-même
    return [features[1], features[2], features[3]];
  }

  calculateRoutes(nearestFeatures, center) {
    const forkJoinObservables = [];

    nearestFeatures.forEach((feat) => {
      forkJoinObservables.push(
        this.http
          .get(
            `https://wxs.ign.fr/calcul/geoportail/itineraire/rest/1.0.0/route?resource=bdtopo-pgr&profile=car&optimization=fastest&start=${
              center[0] + "," + center[1]
            }&end=${
              feat.feature.geometry.coordinates[0] +
              "," +
              feat.feature.geometry.coordinates[1]
            }&geometryFormat=polyline&getBbox=true&crs=${this.map
              .getView()
              .getProjection()
              .getCode()}`
          )
          .pipe(
            map((result) => {
              feat.itineraire = result;
              return result;
            }),
            catchError((error) => {
              return of(null);
            })
          )
      );
    });

    if (forkJoinObservables.length !== 0) {
      this.forkJoin = forkJoin(forkJoinObservables).subscribe((response) => {
        this.isSearchingForShortPath = false;
        let polylines = [];
        nearestFeatures.forEach((feat) => {
          let format = new Polyline();
          let polyline = format.readFeature(feat.itineraire.geometry, {
            dataProjection: feat.itineraire.crs,
            extent: feat.itineraire.bbox,
            featureProjection: this.map.getView().getProjection().getCode(),
          });
          polyline.set("distance", feat.itineraire.distance);
          polyline.set("distanceUnit", feat.itineraire.distanceUnit);
          polyline.set("popup", new Overlay({}));
          polylines.push(polyline);
        });

        // Tri de la distance l'itineraire le plus court
        polylines.sort((a, b) => a.distance - b.distance);

        // Création tooltip
        if (!this.distanceTooltip) {
          this.distanceTooltip = new Overlay({
            element: this.distanceTooltipElementRef.nativeElement,
            offset: [0, -15],
            positioning: "bottom-center",
          });
        }

        if (this.interrogationDisplayLayer) {
          this.interrogationDisplayLayer.getSource().clear();
          this.interrogationDisplayLayer
            .getSource()
            .addFeatures([polylines[0]]);
        }
        const newDistanceTooltipElement = document.createElement("div");
        newDistanceTooltipElement.className = "tooltip tooltip-static";
        /* newDistanceTooltipElement.innerText = parseInt(polylines[0].get('distance'))/1000 + ' km'; */
        let kmSpan = document.createElement("span");
        kmSpan.innerHTML =
          parseInt(polylines[0].get("distance")) / 1000 + " km";
        newDistanceTooltipElement.append(kmSpan);
        let closingButton = document.createElement("button");
        closingButton.className = "close-popup ml-2";
        closingButton.innerHTML = `<span aria-hidden="true">
          <i class="fa fars-close"></i>
        </span>`;
        closingButton.onclick = () => {
          this.map.removeOverlay(this.distanceTooltip);
        };
        newDistanceTooltipElement.append(closingButton);

        (this.elementRef.nativeElement as HTMLElement).prepend(
          newDistanceTooltipElement
        );
        (this.elementRef.nativeElement as HTMLElement).removeAttribute(
          "hidden"
        );
        (this.elementRef.nativeElement as HTMLElement).classList.remove(
          "hidden"
        );

        (this.distanceTooltip as Overlay).setElement(newDistanceTooltipElement);
        this.distanceTooltip.setPosition(
          getCenter(polylines[0].getGeometry().getExtent())
        );
        this.map.addOverlay(this.distanceTooltip);
        this.map.updateSize();

        this.activeModal.close("distanceTool");
      });
    }
  }

  getLayerQueryField(
    layer: BaseLayer,
    feature: DataFeatureType,
    urlField: any
  ): string {
    let finalUrl =
      (layer.get("LayerQuery").URLConfig
        ? layer.get("LayerQuery").URLConfig
        : "") + (urlField.FieldURLConfig ? urlField.FieldURLConfig : "");
    const fieldsToSearch = finalUrl.split(/[<>]/);

    if (finalUrl.match(/[<>]/) && fieldsToSearch.length > 0) {
      fieldsToSearch.forEach((fieldToSearch) => {
        layer.get("LayerQuery").Fields.forEach((field) => {
          if (field.FieldName && fieldToSearch) {
            for (const [key, value] of Object.entries(feature)) {
              if (key === field.FieldName) {
                finalUrl = finalUrl.replace(`<${field.FieldName}>`, value);
              }
            }
          }
        });
      });
    } else {
      finalUrl += feature[urlField.FieldName];
    }
    return finalUrl;
  }

  convertDate(date: Moment): string {
    return moment(date).format("YYYY-MM-DD").toString();
  }

  closeModal(): void {
    this.activeModal.close();
  }

  setImage(event: any, field: any, fieldName: any) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        field[fieldName] = reader.result.toString();
      };
    }
    field[fieldName + "_fileName"] = event.target.value
      ? event.target.value.split("/").pop().split("\\").pop()
      : null;
  }

  getEnum(enumKey: string): Array<string> {
    let ret = null;
    for (const [key, value] of Object.entries(this.enum)) {
      if (key === enumKey) {
        ret = value;
      }
    }
    return ret;
  }

  saveAndCloseModal(): void {
    this.confirmModal.openModal().then((result) => {
      if (result) {
        this.activeModal.close("save");
      }
    });
  }
}
