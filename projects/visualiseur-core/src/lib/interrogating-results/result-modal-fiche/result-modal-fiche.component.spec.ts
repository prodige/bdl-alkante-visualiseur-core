import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultModalFicheComponent } from './result-modal-fiche.component';

describe('ResultModalFicheComponent', () => {
  let component: ResultModalFicheComponent;
  let fixture: ComponentFixture<ResultModalFicheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultModalFicheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultModalFicheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
