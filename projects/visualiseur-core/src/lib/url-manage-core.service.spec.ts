import { TestBed } from '@angular/core/testing';

import { UrlManageCoreService } from './url-manage-core.service';

describe('UrlManageCoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UrlManageCoreService = TestBed.get(UrlManageCoreService);
    expect(service).toBeTruthy();
  });
});
