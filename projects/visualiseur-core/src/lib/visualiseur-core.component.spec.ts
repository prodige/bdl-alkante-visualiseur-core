import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualiseurCoreComponent } from './visualiseur-core.component';

describe('VisualiseurCoreComponent', () => {
  let component: VisualiseurCoreComponent;
  let fixture: ComponentFixture<VisualiseurCoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualiseurCoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualiseurCoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
