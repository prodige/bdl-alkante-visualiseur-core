import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapScaleDropdownComponent } from './map-scale-dropdown.component';

describe('MapScaleDropdownComponent', () => {
  let component: MapScaleDropdownComponent;
  let fixture: ComponentFixture<MapScaleDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapScaleDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapScaleDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
