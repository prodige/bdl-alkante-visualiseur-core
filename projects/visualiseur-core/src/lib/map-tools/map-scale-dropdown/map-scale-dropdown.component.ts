import { Component, OnInit, Host, Optional, OnDestroy, Input, ChangeDetectorRef } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { MapService } from '../../map/map.service';
import { MapIdService } from './../../map/map-id.service';
import { EnvCoreService } from '../../env.core.service';

import Map from 'ol/Map';
import { MapEvent } from 'ol/';
import { Subscription } from 'rxjs';

const INCHES_PER_UNIT = {
  m: 39.37,
  dd: 4374754
};
const DOTS_PER_INCH = 72;

@Component({
  selector: 'alk-map-scale-dropdown',
  templateUrl: './map-scale-dropdown.component.html',
  styleUrls: ['./map-scale-dropdown.component.scss']
})
export class MapScaleDropdownComponent implements OnInit, OnDestroy {

  printMode = true;

  isCollapsed = true;
  private map: Map;
  mapId: string;
  currentZoomLevel: number;

  private scales = [ 1, 2500, 5000, 10000, 25000, 50000, 100000, 250000, 500000, 1000000, 2000000, 4000000, 10000000 ];
  scaleOptions: Array<{label: string, resolution: number}> = [];
  selectedScale: {label: string, resolution: number} = { label: null, resolution: null};

  mapResolution: number;
  mapResolutions: Array<number>;

  mapResolutionSubscription: Subscription;
  mapResolutionsSubscription: Subscription;
  mapFitEventSubscription: Subscription;

  constructor(
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    public environment: EnvCoreService,
    config: NgbDropdownConfig,
    private cdref: ChangeDetectorRef
    ) {
    // customize default values of dropdowns used by this component tree
    config.placement = 'top-left';
    config.autoClose = true;
  }

  ngOnInit() {
    // getting scales
    if ((this.environment.scaleList) && (this.environment.scaleList.length>0)) {
      this.scales = this.environment.scaleList;
      this.cdref.detectChanges();
    }
    
    // Get the current map
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map as Map;

      if (this.map) {
        this.getPixelsPerProjectionUnits();
        this.currentZoomLevel = (this.map as Map).getView().getZoom();
        this.mapResolution = (this.map as Map).getView().getResolution();

        // On vide le tableau au cas où il contiendrait quelque chose
        this.scaleOptions.length = 0;
        this.scales.forEach((scale: number) => {
          this.scaleOptions.push({
            label: '1/' + scale,
            resolution: scale / this.getPixelsPerProjectionUnits()
          });
        });

        this.mapResolutions = this.scaleOptions.map((scaleOption) => scaleOption.resolution);        

        //*/
        this.mapService.setResolutions(this.mapIdService, this.mapResolutions);

        const nearestResolution = this.findNearestResolution(this.mapResolutions, this.mapResolution);
        const indexOfMapResolution = this.mapResolutions.indexOf(nearestResolution);
        if ( indexOfMapResolution !== -1 && indexOfMapResolution < ( this.mapResolutions.length - 1 ) ) {
          this.mapService.setMapResolution(this.mapIdService, this.mapResolutions[ indexOfMapResolution + 1 ]);
        }

        const findedInitScale = this.scaleOptions.find(scaleOption => scaleOption.resolution === this.map.getView().getResolution() );
        if ( findedInitScale ) {
          this.selectedScale = findedInitScale;
          this.cdref.detectChanges();
        }

        this.mapFitEventSubscription = this.mapService.getMapFitEvent().subscribe(event => {
          this.onZoomChange();
        });

        this.map.on('movestart', this.moveStart);
        this.map.on('moveend', this.moveEnd);

        this.mapResolutionSubscription = this.mapService.getMapResolution(this.mapIdService.getId() || 'main').subscribe(
          (resolution) => {
            /*this.selectScale(new MouseEvent('click'), resolution)*/ // uncomment to contrain map zoom to the scales list
            this.onZoomChange() // ajust zoom display to the nearest zoom value of the list without constraining map zoom
            this.cdref.detectChanges();
          });
      }
    });
    
  }

  ngOnDestroy(): void {
    if ( this.map ) {
      this.map.un('movestart', this.moveStart);
      this.map.un('moveend', this.moveEnd);
    }
    if ( this.mapResolutionSubscription ) {
      this.mapResolutionSubscription.unsubscribe();
    }
    if ( this.mapResolutionsSubscription ) {
      this.mapResolutionsSubscription.unsubscribe();
    }
  }

  private moveStart = () => {
    if (this.map) {
      this.currentZoomLevel = (this.map as Map).getView().getZoom();
    }
  }
  private moveEnd = (event: MapEvent) => {
    if (this.map) {
      if ( this.currentZoomLevel !== (this.map as Map).getView().getZoom()) {
        this.currentZoomLevel = (this.map as Map).getView().getZoom();
        this.onZoomChange();
      }
    }
  }

  onZoomChange(): void {
    const currentZoom = this.map.getView().getZoom();
    const currentResolution = this.map.getView().getResolutionForZoom(currentZoom);

    let findedScale = this.scaleOptions.find( scaleOption => scaleOption.resolution === currentResolution );
    if ( findedScale ) {
      this.selectedScale = findedScale;
    } else {
      const nearestResolution = this.findNearestResolution(this.mapResolutions, currentResolution);
      findedScale = this.scaleOptions.find( scaleOption => scaleOption.resolution === nearestResolution );
      if ( findedScale ) {
        const scale = this.map.getView().getResolution() * this.getPixelsPerProjectionUnits();
        if (( nearestResolution * this.getPixelsPerProjectionUnits())  < 2500) {
          const scale = this.map.getView().getResolution() * this.getPixelsPerProjectionUnits();
          this.selectedScale = { label: '1/' + Math.max(Math.ceil(Math.round(scale / 100) * 100),1), resolution: this.map.getView().getResolution()};
        } else {
          this.selectedScale = findedScale;
        }
      } else {
        this.selectedScale = { label: null, resolution: null};
      }
    }
  }

  selectScale(event: MouseEvent, resolution: number) {
    event.preventDefault();
    if ( resolution && this.selectedScale.resolution !== resolution ) {
      const findedScale = this.scaleOptions.find(scaleOption => scaleOption.resolution === resolution );
      if (this.map) {
        this.map.getView().setResolution(resolution);
        this.map.getView().setZoom( Math.round(this.map.getView().getZoomForResolution(resolution)) );
      }
      if ( findedScale ) {
        this.selectedScale = findedScale;
        this.mapService.setMapResolution(this.mapIdService, resolution);
      }
    }
  }

  private findNearestResolution(resolutions: Array<number>, resolution: number): number {
    return resolutions ? resolutions.reduce(
      (prev, curr) => Math.abs(curr - resolution) < Math.abs(prev - resolution) ? curr : prev
    ): null;
  }


  private getPixelsPerProjectionUnits() : number {
    // meter unit crs as default
    let res = INCHES_PER_UNIT.m * DOTS_PER_INCH;
    if (this.map) {
      const p = this.map.getView().getProjection();
      if (p.getUnits() == 'degrees') { 
        res = INCHES_PER_UNIT.dd * DOTS_PER_INCH;
      }
    }
    return res;
  }
}
