import { MapService } from "./../../map/map.service";
import {
  Component,
  OnInit,
  HostListener,
  ContentChild,
  ElementRef,
  Renderer2,
  Host,
  Optional,
  Input,
  OnDestroy,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { InterrogatingService } from "../../map/services/interrogating.service";
import { ResultModalComponent } from "../../interrogating-results/result-modal/result-modal.component";
import { Subscription } from "rxjs";
import { MapIdService } from "../../map/map-id.service";

@Component({
  selector: "alk-result-button",
  templateUrl: "./result-button.component.html",
  styleUrls: ["./result-button.component.scss"],
})
export class ResultButtonComponent implements OnInit, OnDestroy {
  @ContentChild("modal", {read: null, static: true}) modal: Component;

  public mapId: string;
  @Input() searching: boolean;
  @Input() mailingTool: any;
  public resultModalBoxSubscription: Subscription;

  constructor(
    public modalService: NgbModal,
    public interrogatingService: InterrogatingService,
    public mapService: MapService,
    public mapIdService: MapIdService
  ) {}

  ngOnInit() {
    this.resultModalBoxSubscription = this.interrogatingService
      .getResultModalBox()
      .subscribe((isClosed) => {
        if (!isClosed) {
          this.openModal();
        }
      });
  }

  @HostListener("click", ["$event"])
  click(event: MouseEvent): void {
    this.interrogatingService.openResultCurrentBox();
  }

  openModal = (event?: MouseEvent) => {
    if (this.modal && this.modal.constructor === ResultModalComponent) {
      const modalRef = this.modalService.open(ResultModalComponent, {
        size: "lg",
        centered: true,
        windowClass: "window-xxl",
        container: "alk-visualiseur-core",
      });

      if (
        modalRef &&
        modalRef.componentInstance instanceof ResultModalComponent &&
        modalRef.componentInstance.setMapId
      ) {
        modalRef.componentInstance.setMapId(
          this.mapIdService.getId() || "main"
        );
        modalRef.componentInstance.setMailingTool(this.mailingTool);
      }
    } else {
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "lg",
        centered: true,
        windowClass: "window-xl",
        container: "alk-visualiseur-core",
      });
    }
  };

  ngOnDestroy(): void {
    if (this.resultModalBoxSubscription) {
      this.resultModalBoxSubscription.unsubscribe();
    }
  }
}
