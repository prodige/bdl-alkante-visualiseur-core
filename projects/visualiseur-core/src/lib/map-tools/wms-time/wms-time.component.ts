import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { WmsTimeService } from '../../services/wms-time.service';
import ImageLayer from 'ol/layer/Image';
import * as momentImported from 'moment';
import ImageSource from 'ol/source/Image';
import { ImageWMS } from 'ol/source';
const moment = momentImported;

type Moment = momentImported.Moment;

@Component({
  selector: 'alk-wms-time',
  templateUrl: './wms-time.component.html',
  styleUrls: ['./wms-time.component.scss']
})
export class WmsTimeComponent implements OnInit {

  isCollapsed = false;
  isPlaying = false;
  isRepeating = false;

  layerSubscription: Subscription;
  selectedLayer: ImageLayer<ImageSource>;
  wmsLayers: Array<ImageLayer<ImageSource>> = [];

  playInterval: any;

  wmstimeMode: boolean;

  dateFormat = 'YYYY-MM-DD mm:ss';
  startDateToModify: string;
  startDate: string;
  wmstimeStartdateToModify: Moment;
  wmstimeStartdate: Moment;
  endDate: string;
  wmstimeEnddate: Moment;
  wmstimeCurrentdate: Moment;
  currentDate: string;

  dateRanges: Array<Moment> = [];
  minDateRange = 0;
  maxDateRange = 0;

  wmsTimeStep: number;
  wmstimeStepUnits: momentImported.unitOfTime.DurationConstructor;
  wmstimeSpeed: number;

  constructor(private wmsService: WmsTimeService) {

  }

  ngOnInit() {

    this.layerSubscription = this.wmsService.getWmsLayers().subscribe(layers => {

      this.wmsLayers = layers;
      this.selectedLayer = layers[0];

      this.changeSelectedLayer();

    });

  }

  changeSelectedLayer() {

    this.wmstimeMode = this.selectedLayer.get('wmstimeMode') ? this.selectedLayer.get('wmstimeMode') : null;

    this.wmsTimeStep = this.selectedLayer.get('wmstimeFrequency') ? this.selectedLayer.get('wmstimeFrequency') : null;
    this.wmstimeStepUnits = this.selectedLayer.get('wmstimeFrequencyUnits') ? this.selectedLayer.get('wmstimeFrequencyUnits') : null;

    this.wmstimeSpeed = this.selectedLayer.get('wmstimeSpeed') ?  this.selectedLayer.get('wmstimeSpeed') : null;

    if (['minutes', 'hours'].includes(this.wmstimeStepUnits)) {
      this.dateFormat = 'YYYY-MM-DD LT';
    } else {
      this.dateFormat = 'YYYY-MM-DD';
    }

    // tslint:disable-next-line: max-line-length
    this.wmstimeStartdateToModify = this.selectedLayer.get('wmstimeStartdate') ? moment(this.selectedLayer.get('wmstimeStartdate')) : null;
    const dateTick = moment(this.wmstimeStartdateToModify);
    this.startDateToModify = moment(this.wmstimeStartdateToModify).format(this.dateFormat);

    this.wmstimeStartdate = this.wmstimeStartdateToModify;
    this.startDate = moment(this.wmstimeStartdate).format(this.dateFormat);

    this.wmstimeEnddate = this.selectedLayer.get('wmstimeEnddate') ? moment(this.selectedLayer.get('wmstimeEnddate')) : null;
    this.endDate = moment(this.wmstimeEnddate).format(this.dateFormat);

    this.wmstimeCurrentdate = this.wmstimeStartdateToModify;
    this.currentDate = moment(this.wmstimeStartdateToModify).format(this.dateFormat);

    while (moment(this.wmstimeEnddate).isAfter(moment(dateTick))) {
      this.dateRanges.push(moment(dateTick));
      dateTick.add(this.wmsTimeStep, this.wmstimeStepUnits);
    }
    this.dateRanges.push(this.wmstimeEnddate);


  }

  next(dateType: 'start' | 'current') {

    if (dateType === 'start') {
      if (this.maxDateRange === this.minDateRange) {
        this.next('current');
      }
      this.minDateRange = Math.min(this.maxDateRange, this.minDateRange + 1);
      this.wmstimeStartdate = this.dateRanges[this.minDateRange];
      this.startDate = moment(this.wmstimeStartdate).format(this.dateFormat);
    } else {
      this.maxDateRange = Math.min(this.dateRanges.length - 1, this.maxDateRange + 1);
      this.wmstimeCurrentdate = this.dateRanges[this.maxDateRange];
      this.currentDate = moment(this.wmstimeCurrentdate).format(this.dateFormat);
    }

  }

  previous(dateType: 'start' | 'current') {

    if (dateType === 'start') {
      this.minDateRange = Math.max(0, this.minDateRange - 1);
      this.wmstimeStartdate = this.dateRanges[this.minDateRange];
      this.startDate = moment(this.wmstimeStartdate).format(this.dateFormat);
    } else {
      if (this.maxDateRange === this.minDateRange) {
        this.previous('start');
      }
      this.maxDateRange = Math.max(this.minDateRange, this.maxDateRange - 1);
      this.wmstimeCurrentdate = this.dateRanges[this.maxDateRange];
      this.currentDate = moment(this.wmstimeCurrentdate).format(this.dateFormat);
    }

  }

  play() {
    this.isPlaying = true;
    this.playInterval = setInterval(() => {
      this.next('current');
      
      (this.selectedLayer.getSource() as ImageWMS).updateParams({TIME: this.wmstimeCurrentdate.toISOString()});
      if (!this.isRepeating && (!this.isPlaying || (moment(this.wmstimeEnddate).isSame(moment(this.wmstimeCurrentdate))))) {
        clearInterval(this.playInterval);
      } else if (this.isRepeating && (moment(this.wmstimeEnddate).isSame(moment(this.wmstimeCurrentdate)))) {
        setTimeout(() => {
          this.maxDateRange = this.minDateRange;
        }, 1000 * this.wmstimeSpeed);
      }
    }, 1000 * this.wmstimeSpeed);
  }

  pause() {
    this.isPlaying = false;
    clearInterval(this.playInterval);
  }

  stop() {
    this.isRepeating = false;
    this.isPlaying = false;
    clearInterval(this.playInterval);
    this.minDateRange = 0;
    this.maxDateRange = 0;
    this.wmstimeCurrentdate = this.wmstimeStartdate;
    this.currentDate = moment(this.wmstimeCurrentdate).format(this.dateFormat);
    (this.selectedLayer.getSource()  as ImageWMS).updateParams({TIME: this.wmstimeCurrentdate.toISOString()});
  }


}
