import { Subscription } from 'rxjs';
import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { MapService } from '../../map/map.service';
import { MapIdService } from '../../map/map-id.service';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

import Map from 'ol/Map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import TileLayer from 'ol/layer/Tile';
import ImageLayer from 'ol/layer/Image';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';

@Component({
  selector: 'alk-base-map-dropdown',
  templateUrl: './base-map-dropdown.component.html',
  styleUrls: ['./base-map-dropdown.component.scss']
})
export class BaseMapDropdownComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  map: Map;
  mapId: string;
  baseLayerGroup: Array<BaseLayer> = [];
  visibleBaseLayer: BaseLayer;

  /* Abonnements */
  contextSubscription: Subscription;
  baseLayersSubscription: Subscription;

  constructor(
    public coreService: VisualiseurCoreService,
    public mapService: MapService,
    public mapIdService: MapIdService,
    public config: NgbDropdownConfig
  ) {}

  ngOnInit() {
    
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;

      if (this.map) {
        this.baseLayersSubscription = this.coreService.getBaseLayers().subscribe(
          (baseLayers: Array<BaseLayer>) => {
            if ( baseLayers ) {
              baseLayers.forEach( (baseLayer) => {
                if ( baseLayer.get('defaultbaseLayer') ) {
                  baseLayer.setVisible(true);
                  (baseLayer as VectorLayer<VectorSource<Geometry>> ).setMap(this.map);
                  baseLayer.setOpacity(
                    baseLayer.getOpacity()
                    ? baseLayer.getOpacity()
                    : (
                      baseLayer.get('opacity') !== undefined
                      ? baseLayer.get('opacity')
                      : 0
                    )
                  );
                  baseLayer.set('oldOpacity', baseLayer.getOpacity() ? baseLayer.getOpacity() : 0 );
                  if (this.map.getLayers().getArray().length !== 0) {
                    if (this.map.getLayers().getArray().filter(layer => {return layer === baseLayer}).length === 0) {
                      this.map.addLayer(baseLayer);
                    }
                  } else {
                    // this.map.addLayer(baseLayer);
                  }
                  this.chooseActiveBaseLayer(baseLayer);
                } else {
                  baseLayer.setVisible(false);
                }
                baseLayer.set('oldOpacity', baseLayer.getOpacity() ? baseLayer.getOpacity() : 0 );
                baseLayer.changed();
              });
            }
            this.baseLayerGroup = baseLayers;
          }
        );
      }
    });

  }

  ngOnDestroy() {
    if ( this.baseLayersSubscription ) {
      this.baseLayersSubscription.unsubscribe();
    }
    if ( this.contextSubscription ) {
      this.contextSubscription.unsubscribe();
    }
  }

  chooseActiveBaseLayer: (baseLayer: BaseLayer) => void = (baseLayer) => {
    if ( this.visibleBaseLayer !== baseLayer ) {
      if ( this.visibleBaseLayer ) {
        this.visibleBaseLayer.set('defaultbaseLayer', {read: null, static: true});
        this.visibleBaseLayer.set('oldOpacity', this.visibleBaseLayer.getOpacity() ? this.visibleBaseLayer.getOpacity() : 0 );
        this.visibleBaseLayer.setOpacity(0);
        this.visibleBaseLayer.setVisible(false);
        (this.visibleBaseLayer as VectorLayer<VectorSource<Geometry>> ).setMap(null);
        this.visibleBaseLayer.changed();
      }

      try {
        /* this.map.getView().fit(this.map.getView().getProjection().getExtent());
        this.mapService.setMapFitEvent(true); */

        this.visibleBaseLayer = baseLayer;
        if ( this.visibleBaseLayer ) {
          const oldOpactity = (
            this.visibleBaseLayer.get('oldOpacity') !== null && this.visibleBaseLayer.get('oldOpacity') !== undefined
            ? this.visibleBaseLayer.get('oldOpacity')
            : 0
          );
          this.visibleBaseLayer.setOpacity(oldOpactity);
          this.visibleBaseLayer.set('oldOpacity', null );
          this.visibleBaseLayer.set('defaultbaseLayer', true);
          this.visibleBaseLayer.setVisible(true);
          this.visibleBaseLayer.setZIndex(-1);
          this.visibleBaseLayer.changed();
          (this.visibleBaseLayer as VectorLayer<VectorSource<Geometry>>).setMap(this.map);
        }
      } catch ( err ) {
        console.error(err);
      }
    }
  }

  changeVisibilityPercent(change: {oldValue: number, newValue: number}): void {
    this.visibleBaseLayer.setOpacity(change.newValue);
    this.visibleBaseLayer.changed();
  }

}
