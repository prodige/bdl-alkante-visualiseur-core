import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseMapDropdownComponent } from './base-map-dropdown.component';

describe('BaseMapDropdownComponent', () => {
  let component: BaseMapDropdownComponent;
  let fixture: ComponentFixture<BaseMapDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseMapDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseMapDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
