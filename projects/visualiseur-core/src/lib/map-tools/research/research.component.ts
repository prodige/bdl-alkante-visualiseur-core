
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { Subscription, Observable, forkJoin, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MapService } from './../../map/map.service';
import { Component, OnInit, Optional, OnDestroy, Host, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { MapIdService } from '../../map/map-id.service';

import Map from 'ol/Map';
import { FeatureCollection } from '../../models/context';
import { HttpClient } from '@angular/common/http';
import { Polygon } from 'ol/geom';
import Feature from 'ol/Feature';
import { get, transform } from 'ol/proj';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';


@Component({
  selector: 'alk-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.scss']
})
export class ResearchComponent implements OnInit, OnDestroy {

  /** pour initialiser le compasant avec une recherche */
  @Input() initLocatePreset?: {polygon: any, location : any} = null;

  /** pour envoyer au parent les mots recherchés */
  @Output() onLocatePreset = new EventEmitter<{polygon: any, location : any}>();

  map: Map;

  sessionStorageSubscription: Subscription;
  FAVORITE_STORAGE_KEY = 'favoriteZones';

  searchAdressOpenLS: string;
  forkJoin: Subscription;
  favoriteZones: Array<FeatureCollection['GeobookmarkConfig']> = [];
  favoriteZonesSearchResult = [];
  addressSearch;
  addressSearchResult = [];
  presetZones: Array<FeatureCollection['LocalisationConfig']> = [];
  presetZonesSearchResult = [];
  contextSubscription: Subscription;
  tools: FeatureCollection['properties']['extension']['Tools'];

  isResultEmpty = true;
  inputFocused = true;
  isAddressClicked = false;

  public isCollapsed = false;

  private mapReadySubscription: Subscription;

  constructor(
    protected mapService: MapService,
    protected mapIdService: MapIdService,
    protected coreService: VisualiseurCoreService,
    protected http: HttpClient,
    protected localStorage: LocalStorageService,
    protected sessionStorage: SessionStorageService) { }

  ngOnInit() {
    // Get the current map
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');
    this.contextSubscription = this.coreService.getContext().subscribe(context => {
      if(!context){
        return false;
      }
      this.favoriteZones = context.properties.extension.Tools.GeobookmarkConfig;
      this.presetZones = context.properties.extension.Tools.LocalisationConfig;
      this.searchAdressOpenLS = context.properties.extension.Tools.SearchAdressOpenLS;
      this.tools = context.properties.extension.Tools;
    });

    let retrieved = [];
    if (this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      retrieved = this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY);
    }
    if (this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      retrieved = this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY);
    }
    if (retrieved && retrieved.length) {
      retrieved.forEach(zone => {
        this.favoriteZones.push(zone);
      });
    }

    this.sessionStorageSubscription = this.sessionStorage.observe(this.FAVORITE_STORAGE_KEY).subscribe(zones => {
      if (zones && zones.length) {
        this.favoriteZones = zones;
      } else if (zones) {
        this.favoriteZones = zones;
      } else {
        this.favoriteZones = [];
      }
    });
  }

  ngOnDestroy() {
    if(this.mapReadySubscription){
      this.mapReadySubscription.unsubscribe();
    }
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.sessionStorageSubscription) {
      this.sessionStorageSubscription.unsubscribe();
    }
    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }
  }
  
  /** */
  ngOnChanges(changes: SimpleChanges): void {
    if( changes.initLocatePreset ){
      const initLocatePreset = changes.initLocatePreset.currentValue;
      if(initLocatePreset && initLocatePreset.polygon){
        this.mapReadySubscription = this.mapService.getMapReady(this.mapIdService.getId() || 'main').subscribe((map) => {
          if(!this.map){
            this.map = map;
          }

          this.locatePresetZoneOnMap(initLocatePreset.polygon, initLocatePreset.location);
        })
        
      }
    }
  }

  isLocating() {
    setTimeout(() => {
      if (!this.isAddressClicked) {
        this.inputFocused = false;
      }
    }, 500);
  }

  search(event: any) {

    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }

    if (event.target && event.target.value) {

      const searchingWord = event.target.value;

      this.favoriteZonesSearchResult = [];
      this.addressSearchResult = [];
      this.presetZonesSearchResult = [];

      const forkJoinObservables = [];
      forkJoinObservables.push(this.getAddressResult(searchingWord));
      if (this.presetZones) {
        this.presetZones.forEach(zone => {
          if (zone.criteriaInSearch) {
            forkJoinObservables.push(this.getPresetZoneResult(zone, searchingWord));
          }
        });
      }

      this.forkJoin = forkJoin(forkJoinObservables).subscribe(responses => {
        // Favorite zones searching part
        if (this.favoriteZones && this.tools.Geobookmark && this.tools.GeobookmarkConfig) {
          this.favoriteZones.forEach(zone => {
            if (zone.title.toLocaleLowerCase().includes(searchingWord.toLowerCase())) {
              const position = zone.title.toLowerCase().indexOf(searchingWord.toLowerCase());
              const cutWord = zone.title.substring(position);

              const startVal = zone.title.substring(0, position);
              const strongVal = cutWord.substring(0, searchingWord.length);
              const endVal = cutWord.substring(0 + searchingWord.length);

              this.favoriteZonesSearchResult.push({
                start: startVal,
                strong: strongVal,
                end: endVal,
                bbox: zone.bbox
              });
            }
          });
        }
        responses.forEach((response: any, index) => {
          // Address searching part
          if (index === 0 && response) {
            const kinds = [];
            response.results.forEach(address => {
              if (!kinds.includes(address.kind)) {
                kinds.push(address.kind);
              }
            });
            kinds.forEach(kind => {
              const addressSearchByKind = [];
              response.results.forEach(address => {
                if (address.kind === kind) {
                  const position = address.fulltext.toLowerCase().indexOf(searchingWord.toLowerCase());
                  const cutWord = address.fulltext.substring(position);

                  const startVal = address.fulltext.substring(0, position);
                  const strongVal = cutWord.substring(0, searchingWord.length);
                  const endVal = cutWord.substring(0 + searchingWord.length);

                  addressSearchByKind.push({
                    start: startVal,
                    strong: strongVal,
                    end: endVal,
                    coordinates: [address.x, address.y]
                  });
                }
              });
              if (addressSearchByKind.length) {
                this.addressSearchResult.push({
                  header: kind,
                  links: addressSearchByKind
                });
              }
            });
          // Preset zones searching part
          } else if (index !== 0 && response && this.tools.Localisation) {
            const presetZonesFound = response as FeatureCollection;
            const presetZonesResult = [];
            let presetZoneHeader = null;
            this.presetZones.forEach(zone => {
              presetZonesFound.features.forEach(presetZones => {
                if (presetZones.properties[zone.layerTextField]) {
                  presetZoneHeader = zone;
                  if (presetZones.properties[zone.layerTextField].toLocaleLowerCase().includes(searchingWord.toLowerCase())) {
                    const position = presetZones.properties[zone.layerTextField].toLowerCase().indexOf(searchingWord.toLowerCase());
                    const cutWord = presetZones.properties[zone.layerTextField].substring(position);

                    const startVal = presetZones.properties[zone.layerTextField].substring(0, position);
                    const strongVal = cutWord.substring(0, searchingWord.length);
                    const endVal = cutWord.substring(0 + searchingWord.length);

                    presetZonesResult.push({
                      start: startVal,
                      strong: strongVal,
                      end: endVal,
                      polygon: {
                        projection: presetZonesFound.crs.properties.name,
                        coordinates: presetZones.geometry.coordinates
                      }
                    });
                  }
                }
              });
            });
            if (presetZonesResult.length) {
              this.presetZonesSearchResult.push({
                header: presetZoneHeader.title,
                links: presetZonesResult
              });
            }
          }
        });
      });

      if (
        (searchingWord !== '') ||
        (this.addressSearchResult.length !== 0) ||
        (this.favoriteZonesSearchResult.length !== 0) ||
        (this.presetZonesSearchResult.length !== 0)
      ) {
        this.isResultEmpty = false;
      } else {
        this.isResultEmpty = true;
      }
    } else {
      this.isResultEmpty = true;
    }
  }

  fitToZone(bbox: []) {
    this.isAddressClicked = true;
    this.map.getView().fit(bbox);
    this.mapService.setMapFitEvent(true);
    this.isAddressClicked = false;
  }

  locatePresetZoneOnMap(polygon: any, location = null) {
    this.isAddressClicked = true;
    
    let feature;

    this.onLocatePreset.emit({polygon,location});

    if (polygon && polygon.projection) {
      const oldProj = polygon.projection.split('::')[0].split(':')[4] + ':' + polygon.projection.split('::')[1];
      feature = new Feature({
        geometry: new Polygon(polygon.coordinates)
      });

      feature.getGeometry().transform(oldProj, this.map.getView().getProjection().getCode());

      this.map.getView().fit(feature.getGeometry().getExtent());
      this.mapService.setMapFitEvent(true);
    } else {
      this.map.getView().setCenter(transform(polygon, 'EPSG:4326', this.map.getView().getProjection().getCode()));
      if (location === 'Région') {
        this.map.getView().setZoom(3);
      } else if (location === 'Département') {
        this.map.getView().setZoom(8);
      } else if (['Capitale d\'état', 'Commune', 'Préfecture de région', 'Préfecture'].includes(location)) {
        this.map.getView().setZoom(10);
      } else if (['Adresses', 'Lieu-dit habité'].includes(location)) {
        this.map.getView().setZoom(15);
      } else {
        this.map.getView().setZoom(8);
      }
    }

    this.inputFocused = false;
    this.isAddressClicked = false;
  }

  getAddressResult(searchingWord: string): Observable<[]> {
    const parameters = `?gp-access-lib=2.1.2&maximumResponses=5&terr=&text=${ searchingWord }&type=PositionOfInterest,StreetAddress`;
    // tslint:disable-next-line:max-line-length
    const url = this.searchAdressOpenLS + '/apis/completion' + parameters;
    return this.http.get<[]>(url).pipe(
      map(result => {
        return result;
      }), catchError (error => {
        return of(null);
      })
    );
  }

  getPresetZoneResult(zone: FeatureCollection, searchingWord): Observable<FeatureCollection> {
    const queries = zone.layerWFSUrl.split('?')[1].split('&');
    let body = `<?xml version="1.0" encoding="UTF-8"?>
      <GetFeature service="WFS" version="2.0.0" outputformat="geojson"
        xmlns="http://www.opengis.net/wfs/2.0"
        xmlns:cw="http://www.someserver.com/cw"
        xmlns:fes="http://www.opengis.net/ogc/1.1"
        xmlns:gml="http://www.opengis.net/gml/3.2"
        maxFeatures="5">`;
    queries.forEach(query => {
      const name = query.split('=')[0];
      const value = query.split('=')[1];
      /* if (zone.criteriaFieldRelated) { */
        body += `
      <Query ${ name }="${ value }" styles="default">
        <Filter>
        <PropertyIsLike matchCase="false" wildcard="*" singleChar="." escape="!">
            <PropertyName>${ zone.layerTextField }</PropertyName>
            <Literal>*${ searchingWord.toUpperCase() }*</Literal>
          </PropertyIsLike>
        </Filter>
      </Query>`;
      /* } else {
        body += `<Query ${ name }="${ value }" ></Query>`;
      } */
    });
    body += `</GetFeature> `;
    return this.http.post<FeatureCollection>(zone.layerWFSUrl , body).pipe(
      map(result => {
        return result;
      }), catchError (error => {
        return of(null);
      })
    );
  }
}
