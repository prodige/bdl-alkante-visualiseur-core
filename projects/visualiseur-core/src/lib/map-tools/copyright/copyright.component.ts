import { Component, OnInit, OnDestroy } from '@angular/core';
import { FeatureCollection } from '../../models/context';
import { Subscription } from 'rxjs';
import { VisualiseurCoreService } from '../../visualiseur-core.service';

@Component({
  selector: 'alk-copyright',
  templateUrl: './copyright.component.html',
  styleUrls: ['./copyright.component.scss']
})
export class CopyrightComponent implements OnInit, OnDestroy {

  context: FeatureCollection;
  contextSubscription: Subscription;

  copyrightContent: string;

  constructor(private coreService: VisualiseurCoreService) { }

  ngOnInit() {
    this.contextSubscription = this.coreService.getContext().subscribe(
      context => {
        if(!context){
          return false;
        }
        this.context = context;

        const copyrightContext = context.properties.extension.Layout.Copyright;
        this.copyrightContent = copyrightContext.Text;

      }, error => console.error(error)
    );
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
  }

}
