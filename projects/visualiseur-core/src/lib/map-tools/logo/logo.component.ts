import { Component, OnInit, OnDestroy } from '@angular/core';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { Subscription } from 'rxjs';
import { FeatureCollection } from '../../models/context';

@Component({
  selector: 'alk-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit, OnDestroy {

  context: FeatureCollection;
  contextSubscription: Subscription;

  logoUrl: string;

  constructor(private coreService: VisualiseurCoreService) { }

  ngOnInit() {
    this.contextSubscription = this.coreService.getContext().subscribe(
      context => {
        if(!context){
          return false;
        }
        this.context = context;

        const logoContext = context.properties.extension.Layout.LogoURL.properties.links.previews[0];
        this.logoUrl = logoContext.href;

      }, error => console.error(error)
    );
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
  }
}
