import { FeatureCollection } from "./../../models/context";
import { VisualiseurCoreService } from "./../../visualiseur-core.service";
import {
  Component,
  OnInit,
  Host,
  Optional,
  ViewChild,
  ElementRef,
  OnDestroy,
} from "@angular/core";

import { MapService } from "../../map/map.service";
import { MapIdService } from "../../map/map-id.service";

import Map from "ol/Map";
import View from "ol/View";
import OverviewMap from "ol/control/OverviewMap";
import ImageLayer from "ol/layer/Image";
import Static from "ol/source/ImageStatic";
import { Subscription } from "rxjs";
import { get as getProjection } from "ol/proj";
import { getCenter } from "ol/extent";
import ImageSource from "ol/source/Image";

@Component({
  selector: "alk-map-visualisation",
  templateUrl: "./map-visualisation.component.html",
  styleUrls: ["./map-visualisation.component.scss"],
})
export class MapVisualisationComponent implements OnInit, OnDestroy {
  public isCollapsed = this.coreService.isApplicationInLiteMode ? false : true;

  @ViewChild("mapOverview", {read: null, static: true}) mapOverview: ElementRef;

  map: Map;
  mapId: string;
  overviewLayer: ImageLayer<ImageSource> = null;
  overviewSource: Static = null;

  coreServiceSubscription: Subscription = null;

  constructor(
    public coreService: VisualiseurCoreService,
    private mapService: MapService,
    private mapIdService: MapIdService
  ) {}

  ngOnInit() {
    // Get the current map
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;

      if (this.map) {
        this.coreServiceSubscription = this.coreService
        .getContext()
        .subscribe((context: FeatureCollection) => {
          if (context) {
            const regexProj = new RegExp(/^urn:ogc:def:crs:(.*)$/i);
            const contextMapProjection = regexProj.exec(
              context.crs.properties.name
            );
            const mapProjection = getProjection(contextMapProjection[1]);

            const sourceUrl =
              context.properties.extension.Layout.ReferenceMapConfig.properties
                .links.previews[0].href;
            const width = context.properties.extension.Layout.ReferenceMapConfig
              .properties.links.previews[0].width as unknown as number;
            const height = context.properties.extension.Layout.ReferenceMapConfig
              .properties.links.previews[0].height as unknown as number;

            const imageExtent =
              context.properties.extension.Layout.ReferenceMapConfig.properties
                .bbox;
            this.overviewSource = new Static({
              url: sourceUrl,
              crossOrigin: "Anonymous",
              projection: mapProjection,
              imageExtent,
              imageSize: [width, height],
            });

            // En fonction si le layer existe déjà ou non, on le créer ou on le met à jour
            if (this.overviewLayer !== null && this.overviewSource !== null) {
              this.overviewLayer.setSource(this.overviewSource);
            } else {
              this.overviewLayer = new ImageLayer({
                source: this.overviewSource,
              });

              const overview = new View({
                projection: mapProjection,
                center: getCenter(imageExtent || [0, 0, 0, 0]),
                zoom: 0,
                extent: imageExtent || undefined,
              });

              const overviewMapControl = new OverviewMap({
                view: overview,
                target: this.mapOverview.nativeElement,
                // see in overviewmap-custom.html to see the custom CSS used
                className: "ol-overviewmap ol-custom-overviewmap",
                layers: [this.overviewLayer],
                collapsed: false,
                collapsible: false,
              });

              this.map.addControl(overviewMapControl);
            }
          }
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.coreServiceSubscription) {
      this.coreServiceSubscription.unsubscribe();
    }
    this.overviewLayer = null;
    this.overviewSource = null;
  }
}
