import { MapService } from "./../../map/map.service";
import {
  Component,
  OnInit,
  HostListener,
  Optional,
  Host,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { MapIdService } from "../../map/map-id.service";

import Map from "ol/Map";

import { fromLonLat } from "ol/proj";

@Component({
  // tslint:disable-next-line: component-selector
  selector: "button[geolocationButton],a[geolocationButton]",
  templateUrl: "./geolocation-button.component.html",
  styleUrls: ["./geolocation-button.component.scss"],
})
export class GeolocationButtonComponent implements OnInit {
  map: Map;
  @ViewChild("copyToastGeoloc", {read: null, static: true}) copyToastGeoloc: ElementRef;
  toastContent: string = "";

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService
  ) {}

  ngOnInit() {
    // Get the current map
    this.map = this.mapService.getMap(this.mapIdService.getId() || "main");
  }

  @HostListener("click", ["$event"])
  getLocation(event: MouseEvent): void {
    event.preventDefault();

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position: any) => {
          const lonLat = [position.coords.longitude, position.coords.latitude];

          this.map
            .getView()
            .setCenter(fromLonLat(lonLat, this.map.getView().getProjection()));
          this.map.getView().setZoom(10);
        },
        (positionError: any) => {
          if (positionError.code === 1) {
            this.toastContent =
              "Vous avez interdit la géolocalisation. Veuillez modifier les autorisations de géolocalisation puis ré-essayer";
            this.copyToastGeoloc.nativeElement.removeAttribute("hidden");
            this.copyToastGeoloc.nativeElement.className = "toast show";
            setTimeout(() => {
              this.copyToastGeoloc.nativeElement.className =
                this.copyToastGeoloc.nativeElement.className.replace(
                  "show",
                  ""
                );
              this.copyToastGeoloc.nativeElement.setAttribute("hidden", true);
            }, 2000);
          }
          if (positionError.code === 3) {
            this.toastContent =
              "La géolocalisation a échoué. Veuillez ré-essayer dans quelques instants";
            this.copyToastGeoloc.nativeElement.removeAttribute("hidden");
            this.copyToastGeoloc.nativeElement.className = "toast show";
            setTimeout(() => {
              this.copyToastGeoloc.nativeElement.className =
                this.copyToastGeoloc.nativeElement.className.replace(
                  "show",
                  ""
                );
              this.copyToastGeoloc.nativeElement.setAttribute("hidden", true);
            }, 2000);
          }
        },
        {
          enableHighAccuracy: true,
          maximumAge: 0,
          timeout: 500,
        }
      );
    } else {
      console.error(`La géolocalisation n'est pas supporté par le navigateur`);
    }
  }
}
