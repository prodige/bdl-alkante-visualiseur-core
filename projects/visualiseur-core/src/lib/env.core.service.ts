export class EnvCoreService {
  // The values that are defined here are the default values that can
  // be overridden by env.js

  public serverUrl = "https://frontcarto-prodige41.alkante.al:19020/";

  public serverProj = "https://admincarto-prodige41.alkante.al:19020/";

  public serverMpa = "https://france-amp.alkante.al:19003/";

  public prodigeUrl = "https://prodige41.alkante.al/";

  public redirectUrl = "";

  public headerToken = "";

  public isPrintMode = false;

  public interrogationPointPrecision = 5;

  public interrogationTooltipPrecision = 5;

  public defaultMousePositionProjection = "EPSG:4326";

  public defaultSetOfColors = [];

  public scaleList: number[] = [
    1, 2500, 5000, 10000, 25000, 50000, 100000, 250000, 500000, 1000000,
    2000000, 4000000, 10000000,
  ];

  public namedStyles: {
    [key: string]: { (styleFn?: (feature: any, resolution: number) => any) };
  } = {};

  public enabledExportShp = false;

  public cartorExportUrlDev = null; //TODO: pour les dev

  /** Variables spécifiques Calculatrice foncière */

  public account = "";

  public mapName = "";

  public layersConnection = [
    {
      accessibilite: "",
    },
    {
      zoneCaptage: "",
    },
    {
      aleaNaturel: "",
    },
    {
      declivite: "",
    },
    {
      raccordementReseau: '',
    }
  ]

  public nearestPoint = {
    active: false,
    uuid: ''
  }


  public statsConfigs = [];

  public availableProjections: {
    [key: string]: {
      key: string;
      label: string;
      proj: string;
      bounds: [number, number, number, number];
    };
  } = {
    4258: {
      key: "EPSG:4258",
      label: "ETRS89 (INSPIRE)",
      proj: "+proj=longlat +ellps=GRS80 +no_defs",
      bounds: [-10.67, 34.5, 31.55, 71.05],
    },
    4171: {
      key: "EPSG:4171",
      label: "RGF93 non projete",
      proj: "+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs",
      bounds: [-9.62, 41.18, 10.3, 51.54],
    },
    2154: {
      key: "EPSG:2154",
      label: "RGF93 / Lambert 93",
      proj: "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [-357823.2365, 6037008.6939, 1313632.3628, 7230727.3772],
    },
    3942: {
      key: "EPSG:3942",
      label: "RGF93 / CC42",
      proj: "+proj=lcc +lat_1=41.25 +lat_2=42.75 +lat_0=42 +lon_0=3 +x_0=1700000 +y_0=1200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1296335.9393, 1100257.6521, 2254779.1544, 1331811.4895],
    },
    3943: {
      key: "EPSG:3943",
      label: "RGF93 / CC43",
      proj: "+proj=lcc +lat_1=42.25 +lat_2=43.75 +lat_0=43 +lon_0=3 +x_0=1700000 +y_0=2200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1302506.4384, 2100044.3184, 2093357.4402, 2322094.8666],
    },
    3944: {
      key: "EPSG:3944",
      label: "RGF93 / CC44",
      proj: "+proj=lcc +lat_1=43.25 +lat_2=44.75 +lat_0=44 +lon_0=3 +x_0=1700000 +y_0=3200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1308798.423, 3100052.0968, 2087131.0906, 3322128.5732],
    },
    3945: {
      key: "EPSG:3945",
      label: "RGF93 / CC45",
      proj: "+proj=lcc +lat_1=44.25 +lat_2=45.75 +lat_0=45 +lon_0=3 +x_0=1700000 +y_0=4200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1351247.6083, 4098245.7319, 2080786.3812, 4321920.5102],
    },
    3946: {
      key: "EPSG:3946",
      label: "RGF93 / CC46",
      proj: "+proj=lcc +lat_1=45.25 +lat_2=46.75 +lat_0=46 +lon_0=3 +x_0=1700000 +y_0=5200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1270591.648, 5097385.5589, 2027087.9253, 5325334.0729],
    },
    3947: {
      key: "EPSG:3947",
      label: "RGF93 / CC47",
      proj: "+proj=lcc +lat_1=46.25 +lat_2=47.75 +lat_0=47 +lon_0=3 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1112163.1498, 6100231.0759, 2071615.7622, 6338667.6413],
    },
    3948: {
      key: "EPSG:3948",
      label: "RGF93 / CC48",
      proj: "+proj=lcc +lat_1=47.25 +lat_2=48.75 +lat_0=48 +lon_0=3 +x_0=1700000 +y_0=7200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1092603.2215, 7102517.8254, 2100529.4893, 7341541.8099],
    },
    3949: {
      key: "EPSG:3949",
      label: "RGF93 / CC49",
      proj: "+proj=lcc +lat_1=48.25 +lat_2=49.75 +lat_0=49 +lon_0=3 +x_0=1700000 +y_0=8200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1104062.1794, 8102449.7137, 2092985.7591, 8341410.9019],
    },
    3950: {
      key: "EPSG:3950",
      label: "RGF93 / CC50",
      proj: "+proj=lcc +lat_1=49.25 +lat_2=50.75 +lat_0=50 +lon_0=3 +x_0=1700000 +y_0=9200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [1338040.0982, 9100766.7009, 2085321.7705, 9324263.6899],
    },
    32620: {
      key: "EPSG:32620",
      label: "WGS84 / UTM 20 Nord",
      proj: "+proj=utm +zone=20 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
      bounds: [166021.4431, 0.0, 833978.5569, 9329005.1825],
    },
    32621: {
      key: "EPSG:32621",
      label: "WGS84 / UTM zone 21 Nord",
      proj: "+proj=utm +zone=21 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
      bounds: [166021.4431, 0.0, 833978.5569, 9329005.1825],
    },
    2972: {
      key: "EPSG:2972",
      label: "RGFG95 / UTM 22 Nord",
      proj: "+proj=utm +zone=22 +ellps=GRS80 +towgs84=2,2,-2,0,0,0,0 +units=m +no_defs",
      bounds: [166254.8015, 238817.6898, 653463.1459, 1002848.2203],
    },
    2975: {
      key: "EPSG:2975",
      label: "RGR92 / UTM 40 sud",
      proj: "+proj=utm +zone=40 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [-1663497.9817, 6983564.613, 638934.0546, 8827964.8996],
    },
    4326: {
      key: "EPSG:4326",
      label: "WGS84",
      proj: "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
      bounds: [-180.0, -90.0, 180.0, 90.0],
    },
    27571: {
      key: "EPSG:27571",
      label: "NTF(Paris) / Lambert zone 1",
      proj: "+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=1200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [40063.0848, 1067022.9279, 1038149.3679, 1404377.9733],
    },
    27572: {
      key: "EPSG:27572",
      label: "NTF(Paris) / Lambert zone 2",
      proj: "+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [-22841.3905, 1712212.6192, 1087335.9605, 2703971.1254],
    },
    27573: {
      key: "EPSG:27573",
      label: "NTF(Paris) / Lambert zone 3",
      proj: "+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=3200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [262373.4731, 3011755.1415, 1047663.1958, 3364038.4062],
    },
    27574: {
      key: "EPSG:27574",
      label: "NTF(Paris) / Lambert zone 4",
      proj: "+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=4185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [501997.4013, 4111734.6001, 607585.0937, 4309328.6374],
    },
    27561: {
      key: "EPSG:27561",
      label: "NTF(Paris) / Lambert Nord France",
      proj: "+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [40063.0848, 67022.9279, 1038149.3679, 404377.9733],
    },
    27562: {
      key: "EPSG:27562",
      label: "NTF(Paris) / Lambert centre France",
      proj: "+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [50198.0586, 64100.6841, 1016966.9382, 373525.1009],
    },
    27563: {
      key: "EPSG:27563",
      label: "NTF(Paris) / Lambert Sud France",
      proj: "+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [262373.4731, 11755.1415, 1047663.1958, 364038.4062],
    },
    27564: {
      key: "EPSG:27564",
      label: "NTF(Paris) / Lambert Corse",
      proj: "+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [501997.4013, 111734.6001, 607585.0937, 309328.6374],
    },
    2971: {
      key: "EPSG:2971",
      label: "CSG 67 / UTM 22 Nord",
      proj: "+proj=utm +zone=22 +ellps=intl +towgs84=-186,230,110,0,0,0,0 +units=m +no_defs",
      bounds: [166259.5247, 238704.1046, 653464.7958, 1002730.5245],
    },
    3312: {
      key: "EPSG:3312",
      label: "CSG 67 / UTM 21 Nord",
      proj: "+proj=utm +zone=21 +ellps=intl +towgs84=-186,230,110,0,0,0,0 +units=m +no_defs",
      bounds: [760423.4997, 233305.3381, 833781.878, 603120.5165],
    },
    2973: {
      key: "EPSG:2973",
      label: "Fort Desaix / UTM 20",
      proj: "+proj=utm +zone=20 +ellps=intl +units=m +no_defs",
      bounds: [688258.3769, 1590487.6272, 738323.9861, 1648464.5603],
    },
    4625: {
      key: "EPSG:4625",
      label: "Fort Desaix",
      proj: "+proj=longlat +ellps=intl +no_defs",
      bounds: [-61.25, 14.38, -60.79, 14.9],
    },
    4559: {
      key: "EPSG:4559",
      label: "RRAF 91 / UTM 20",
      proj: "+proj=utm +zone=20 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [428749.41, 1556673.78, 1079045.02, 2058754.66],
    },
    3727: {
      key: "EPSG:3727",
      label: "Piton des neiges / Gauss Laborde Réunion",
      proj: "+proj=tmerc +lat_0=-21.11666666666667 +lon_0=55.53333333333333 +k=1 +x_0=160000 +y_0=50000 +ellps=intl +units=m +no_defs",
      bounds: [125295.6683, 18592.0157, 187763.4229, 85037.85],
    },
    2987: {
      key: "EPSG:2987",
      label: "IGN1950 / UTM 21 Nord",
      proj: "+proj=utm +zone=21 +ellps=clrk66 +towgs84=30,430,368,0,0,0,0 +units=m +no_defs",
      bounds: [417320.6529, 4819696.5057, 651653.322, 5249734.3074],
    },
    32738: {
      key: "EPSG:32738",
      label: "WGS84 / UTM 38 Sud",
      proj: "+proj=utm +zone=38 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
      bounds: [166021.4431, 1116915.044, 833978.5569, 10000000.0],
    },
    32301: {
      key: "EPSG:32301",
      label: "WGS72 / UTM 1 Sud",
      proj: "+proj=utm +zone=1 +south +ellps=WGS72 +units=m +no_defs",
      bounds: [166021.5478, 1116917.6395, 833978.4522, 10000000.0],
    },
    3296: {
      key: "EPSG:3296",
      label: "RGPF / UTM 5 Sud",
      proj: "+proj=utm +zone=5 +south +ellps=GRS80 +units=m +no_defs",
      bounds: [280129.2701, 7342521.2906, 829886.3124, 9004547.3278],
    },
    3297: {
      key: "EPSG:3297",
      label: "RGPF / UTM 6 Sud",
      proj: "+proj=utm +zone=6 +south +ellps=GRS80 +units=m +no_defs",
      bounds: [174541.3305, 6788302.6267, 825458.6695, 8560946.2959],
    },
    2980: {
      key: "EPSG:2980",
      label: "Combani 1950 / UTM 38 sud",
      proj: "+proj=utm +zone=38 +south +ellps=intl +towgs84=-382,-59,-262,0,0,0,0 +units=m +no_defs",
      bounds: [496513.3239, 8563111.2089, 534527.8266, 8607366.641],
    },
    3163: {
      key: "EPSG:3163",
      label: "RGNC91-93 / Lambert New Caledoni",
      proj: "+proj=lcc +lat_1=-20.66666666666667 +lat_2=-22.33333333333333 +lat_0=-21.5 +lon_0=166 +x_0=400000 +y_0=300000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [142766.6147, 159616.5213, 630986.912, 515387.2131],
    },
    3169: {
      key: "EPSG:3169",
      label: "RGNC91-93 / UTM 57",
      proj: "+proj=utm +zone=57 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [296944.6351, 7402360.5269, 819007.0498, 8090677.6218],
    },
    3170: {
      key: "EPSG:3170",
      label: "RGNC91-93 / UTM 58",
      proj: "+proj=utm +zone=58 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [180992.9502, 7402360.5269, 819007.0498, 8089202.4923],
    },
    3171: {
      key: "EPSG:3171",
      label: "RGNC91-93 / UTM 59",
      proj: "+proj=utm +zone=59 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [180992.9502, 7402360.5269, 807301.735, 8089381.1677],
    },
    32630: {
      key: "EPSG:32630",
      label: "WGS84 / UTM 30 Nord",
      proj: "+proj=utm +zone=30 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
      bounds: [166021.4431, 0.0, 833978.5569, 9329005.1825],
    },
    32631: {
      key: "EPSG:32631",
      label: "WGS84 / UTM 31 Nord",
      proj: "+proj=utm +zone=31 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
      bounds: [166021.4431, 0.0, 833978.5569, 9329005.1825],
    },
    32632: {
      key: "EPSG:32632",
      label: "WGS84 / UTM 32 Nord",
      proj: "+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
      bounds: [166021.4431, 0.0, 833978.5569, 9329005.1825],
    },
    27581: {
      key: "EPSG:27581",
      label: "NTF(Paris) / Lambert France 1",
      proj: "+proj=lcc +lat_1=49.50000000000001 +lat_0=49.50000000000001 +lon_0=0 +k_0=0.999877341 +x_0=600000 +y_0=1200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [40063.0848, 1067022.9279, 1038149.3679, 1404377.9733],
    },
    27582: {
      key: "EPSG:27582",
      label: "NTF(Paris) / Lambert France 2",
      proj: "+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [-22841.3905, 1712212.6192, 1087335.9605, 2703971.1254],
    },
    27583: {
      key: "EPSG:27583",
      label: "NTF(Paris) / Lambert France 3",
      proj: "+proj=lcc +lat_1=44.10000000000001 +lat_0=44.10000000000001 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=3200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [262373.4731, 3011755.1415, 1047663.1958, 3364038.4062],
    },
    27584: {
      key: "EPSG:27584",
      label: "NTF(Paris) / Lambert France 4",
      proj: "+proj=lcc +lat_1=42.16500000000001 +lat_0=42.16500000000001 +lon_0=0 +k_0=0.99994471 +x_0=234.358 +y_0=4185861.369 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs",
      bounds: [501997.4013, 4111734.6001, 607585.0937, 4309328.6374],
    },
    4471: {
      key: "EPSG:4471",
      label: "RGM04 / UTM zone 38S",
      proj: "+proj=utm +zone=38 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [357748.31, 8397670.97, 685530.19, 8746991.06],
    },
    3857: {
      key: "EPSG:3857",
      label: "WGS 84 / Pseudo-Mercator",
      proj: "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs",
      bounds: [
        -20037508.342789244, -20037508.342789244, 20037508.342789244,
        20037508.342789244,
      ],
    },
    4467: {
      key: "EPSG:4467",
      label: "RGSPM06 / UTM 21",
      proj: "+proj=utm +zone=21 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
      bounds: [491903.57, 4806351.09, 583048.65, 5246868.78],
    },
  };

  constructor() {}
}
