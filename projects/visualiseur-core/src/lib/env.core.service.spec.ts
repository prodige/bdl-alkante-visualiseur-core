import { TestBed, inject } from '@angular/core/testing';

import { EnvCoreService } from './env.core.service';

describe('EnvCoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnvCoreService]
    });
  });

  it('should be created', inject([EnvCoreService], (service: EnvCoreService) => {
    expect(service).toBeTruthy();
  }));
});
