import { MapService } from './../../map/map.service';
import { Component, OnInit } from '@angular/core';
import { ActionButtonsComponent } from '../action-buttons.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { MapIdService } from '../../map/map-id.service';

@Component({
  selector: 'alk-drawing-button',
  templateUrl: '../action-buttons.component.html',
  styleUrls: [
    '../action-buttons.component.scss',
    './drawing-button.component.scss'
  ]
})
export class DrawingButtonComponent extends ActionButtonsComponent implements OnInit {

  public isCollapsed: [] = [];
  buttonId = 'editButton';
  // buttonLabel = 'editer';
  buttonIconClass = 'fa fars-editer';

  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('');
    this.isCollapsed[this.buttonId] = true;
  }

  ngOnInit() {
  }

}
