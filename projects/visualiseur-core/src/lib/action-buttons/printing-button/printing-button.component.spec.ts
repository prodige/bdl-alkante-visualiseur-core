import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintingButtonComponent } from './printing-button.component';

describe('PrintingButtonComponent', () => {
  let component: PrintingButtonComponent;
  let fixture: ComponentFixture<PrintingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintingButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
