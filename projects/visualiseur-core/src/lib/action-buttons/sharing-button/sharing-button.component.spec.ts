import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingButtonComponent } from './sharing-button.component';

describe('SharingButtonComponent', () => {
  let component: SharingButtonComponent;
  let fixture: ComponentFixture<SharingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharingButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
