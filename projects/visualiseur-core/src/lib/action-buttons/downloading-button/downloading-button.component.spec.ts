import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadingButtonComponent } from './downloading-button.component';

describe('DownloadingButtonComponent', () => {
  let component: DownloadingButtonComponent;
  let fixture: ComponentFixture<DownloadingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadingButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
