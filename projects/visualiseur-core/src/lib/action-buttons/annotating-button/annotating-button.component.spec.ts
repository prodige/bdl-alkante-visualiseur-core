import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnotatingButtonComponent } from './annotating-button.component';

describe('AnnotatingButtonComponent', () => {
  let component: AnnotatingButtonComponent;
  let fixture: ComponentFixture<AnnotatingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnotatingButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnotatingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
