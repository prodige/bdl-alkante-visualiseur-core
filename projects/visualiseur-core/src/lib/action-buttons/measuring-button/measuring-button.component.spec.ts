import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuringButtonComponent } from './measuring-button.component';

describe('MeasuringButtonComponent', () => {
  let component: MeasuringButtonComponent;
  let fixture: ComponentFixture<MeasuringButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasuringButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuringButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
