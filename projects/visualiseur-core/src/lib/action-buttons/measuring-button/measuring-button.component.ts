import { MapService } from './../../map/map.service';
import { Component, OnInit } from '@angular/core';
import { ActionButtonsComponent } from '../action-buttons.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { MapIdService } from '../../map/map-id.service';

@Component({
  selector: 'alk-measuring-button',
  templateUrl: '../action-buttons.component.html',
  styleUrls: [
    '../action-buttons.component.scss',
    './measuring-button.component.scss'
  ]
})
export class MeasuringButtonComponent extends ActionButtonsComponent implements OnInit {

  public isCollapsed: [] = [];
  buttonId = 'measureButton';
  // buttonLabel = 'mesurer';
  buttonIconClass = 'fa fars-mesurer';

  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('');
    this.isCollapsed[this.buttonId] = true;
  }

  ngOnInit() { }

}
