import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocatingButtonComponent } from './locating-button.component';

describe('LocateButtonComponent', () => {
  let component: LocatingButtonComponent;
  let fixture: ComponentFixture<LocatingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocatingButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocatingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
