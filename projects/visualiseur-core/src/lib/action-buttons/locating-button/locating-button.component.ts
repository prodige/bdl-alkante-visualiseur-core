import { MapService } from './../../map/map.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActionButtonsComponent } from '../action-buttons.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { MapIdService } from '../../map/map-id.service';

@Component({
  selector: 'alk-locating-button',
  templateUrl: '../action-buttons.component.html',
  styleUrls: [
    '../action-buttons.component.scss',
    './locating-button.component.scss'
  ]
})
export class LocatingButtonComponent extends ActionButtonsComponent implements OnInit, OnChanges {

  public isCollapsed: [] = [];
  buttonId = 'locateDropdown';
  dropdown = true;
  @Input() favoriteZones;
  // buttonLabel = 'localiser';
  buttonIconClass = 'fa fars-localiser';

  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('');
    this.isCollapsed[this.buttonId] = true;
  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) { }

}
