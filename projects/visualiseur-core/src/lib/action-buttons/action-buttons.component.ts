import { MapService } from "./../map/map.service";
import {
  Component,
  OnInit,
  ContentChild,
  HostListener,
  OnDestroy,
  Input,
} from "@angular/core";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { VisualiseurCoreService } from "../visualiseur-core.service";
import { MapIdService } from "../map/map-id.service";

@Component({
  selector: "alk-action-buttons",
  templateUrl: "./action-buttons.component.html",
  styleUrls: ["./action-buttons.component.scss"],
})
export class ActionButtonsComponent implements OnInit, OnDestroy {
  @Input() dropdownPosition = "bottom-left";
  public isCollapsed: [] = [];
  @Input() mapSection: string;
  @Input() buttonLabel: string;
  public buttonModal = "";

  @ContentChild("modal", {read: null, static: true}) modal: Component;
  dropdown = false;
  connexionUrl: string;
  paramsModal: NgbModalOptions;
  buttonIconClass: string;
  buttonId: string;

  constructor(
    public modalService: NgbModal,
    public coreService: VisualiseurCoreService,
    public mapService: MapService,
    public mapIdService: MapIdService
  ) {}

  ngOnInit() {}

  ngOnDestroy() {}

  @HostListener("click", ["$event"])
  click(event: MouseEvent): void {
    this.coreService.setMapTool("");
    if (
      this.modal &&
      !this.dropdown &&
      !["measureButton"].includes(this.buttonId)
    ) {
      this.mapService.removeInformativeDisplayLayer();
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "lg",
        centered: true,
        windowClass: "window-xl",
        container: "#modals",
      });
      if (modalRef.componentInstance.setMapId) {
        modalRef.componentInstance.setMapId(
          this.mapIdService.getId() || "visualiseur"
        );
      }
    } else if (["measureButton"].includes(this.buttonId)) {
      this.mapService.removeInformativeDisplayLayer();
      this.coreService.setMapTool(this.buttonLabel);
      this.coreService.setAnnotatingSnapping(false);
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "sm",
        windowClass: "window-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });
      if (modalRef.componentInstance.setType) {
        modalRef.componentInstance.setType('length');
      }
      modalRef.result.then(
        (close) => {
          this.coreService.setMapTool("");
        },
        (dismiss) => {}
      );
    }
  }

  isDropdown(): boolean {
    return this.dropdown;
  }

  setButtonModal(buttonModal: string) {
    this.buttonModal = buttonModal;
  }
}
