import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingButtonComponent } from './interrogating-button.component';

describe('InterrogatingButtonComponent', () => {
  let component: InterrogatingButtonComponent;
  let fixture: ComponentFixture<InterrogatingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
