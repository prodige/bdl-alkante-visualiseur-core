import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeningButtonComponent } from './opening-button.component';

describe('OpeningButtonComponent', () => {
  let component: OpeningButtonComponent;
  let fixture: ComponentFixture<OpeningButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpeningButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeningButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
