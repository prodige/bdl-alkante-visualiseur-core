import { MapService } from './../../map/map.service';
import { Component, OnInit } from '@angular/core';
import { ActionButtonsComponent } from '../action-buttons.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { MapIdService } from '../../map/map-id.service';

@Component({
  selector: 'alk-opening-button',
  templateUrl: '../action-buttons.component.html',
  styleUrls: [
    '../action-buttons.component.scss',
    './opening-button.component.scss'
  ]
})
export class OpeningButtonComponent extends ActionButtonsComponent implements OnInit {

  public isCollapsed: [] = [];
  buttonId = 'openButton';
  // buttonLabel = 'ouvrir';
  buttonIconClass = 'fa fars-ouvrir';

  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('#opening-modal');
    this.isCollapsed[this.buttonId] = true;
  }

  ngOnInit() { }

}
