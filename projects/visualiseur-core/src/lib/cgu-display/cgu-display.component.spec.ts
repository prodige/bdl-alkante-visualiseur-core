import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CguDisplayComponent } from './cgu-display.component';

describe('CguDisplayComponent', () => {
  let component: CguDisplayComponent;
  let fixture: ComponentFixture<CguDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CguDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CguDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
