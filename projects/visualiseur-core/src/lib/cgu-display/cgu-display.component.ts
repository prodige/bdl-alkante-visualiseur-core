import { Component, OnInit, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { VisualiseurCoreService } from "../visualiseur-core.service";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { LocalStorageService } from "ngx-webstorage";

import { Location } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { EnvCoreService } from "../env.core.service";

const CGU_STORAGE_KEY = "cgu_storage_key";

@Component({
  selector: "alk-cgu-display",
  templateUrl: "./cgu-display.component.html",
  styleUrls: ["./cgu-display.component.scss"],
})
export class CguDisplayComponent implements OnInit {
  /** */
  public hideHelpContent: FormGroup;

  @ViewChild("checkboxInput", { read: null, static: true }) checkboxInput;

  /** subscriptions */
  contextSubscription: Subscription;
  logSubscription: Subscription;

  /** */
  public cguContent: SafeHtml;

  constructor(
    private coreService: VisualiseurCoreService,
    private formBuilder: FormBuilder,
    private localStorage: LocalStorageService,
    private sanitized: DomSanitizer,
    private location: Location,
    private http: HttpClient,
    private environment: EnvCoreService
  ) {}

  ngOnInit() {
    this.contextSubscription = this.coreService
      .getContext()
      .subscribe((context) => {
        if (!context) {
          return false;
        }

        this.cguContent = context.properties.extension.MapCGU;
        this.cguContent = String(this.cguContent)
          .replace(/&quot;/g, '"')
          .replace(/&#39;/g, "'")
          .replace(/&lt;/g, "<")
          .replace(/&gt;/g, ">")
          .replace(/&amp;/g, "&");
        this.cguContent = this.sanitized.bypassSecurityTrustHtml(
          String(this.cguContent)
        );
      });

    this.hideHelpContent = this.formBuilder.group({
      checkboxInput: new FormControl(
        this.localStorage.retrieve(CGU_STORAGE_KEY) ? true : false,
        [Validators.required]
      ),
    });

    this.localStorage.observe(CGU_STORAGE_KEY).subscribe((isChecked) => {
      this.hideHelpContent
        .get("checkboxInput")
        .setValue(isChecked ? true : false);
    });
  }

  validate() {
    this.localStorage.store(
      CGU_STORAGE_KEY,
      this.hideHelpContent.get("checkboxInput").value
    );
    this.coreService.setCguRead(true);

    const httpOptions = {
      withCredentials: true,
    };

    this.logSubscription = this.http
      .get(
        `${this.environment.serverUrl}/prodige/add_log?logFile=cgu&objectName=${this.environment.mapName}`,
        httpOptions
      )
      .subscribe();

    this.location.back();
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }

    if (this.logSubscription) {
      this.logSubscription.unsubscribe();
    }
  }
}
