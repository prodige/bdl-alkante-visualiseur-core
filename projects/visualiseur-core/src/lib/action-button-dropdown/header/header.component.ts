import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alk-action-header-dropdown',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() icon = null;

  constructor() { }

  ngOnInit() {
  }

}
