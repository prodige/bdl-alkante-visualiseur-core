import {
  Component,
  OnInit,
  Input,
  HostListener,
  ContentChild,
  Host,
  Optional,
  ElementRef,
  OnDestroy,
  Injectable,
  Inject,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MapService } from "../../map/map.service";
import { MapIdService } from "../../map/map-id.service";
import Map from "ol/Map";
import { AnnotatingModalComponent } from "../../map/tools/annotating-tools/annotating-modal/annotating-modal.component";
import { VisualiseurCoreService } from "../../visualiseur-core.service";
import { Subscription } from "rxjs";
import { InterrogatingModalComponent } from "../../map/tools/interrogating-tools/interrogating-modal/interrogating-modal.component";
import { EnvCoreService } from "../../env.core.service";
import { HttpClient } from "@angular/common/http";

@Injectable()
@Component({
  selector: "alk-action-item-dropdown",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"],
})
export class ItemComponent implements OnInit, OnDestroy {
  @Input() disabled;

  map: Map;
  @Input() zone: any;
  @Input() icon = null;
  @ContentChild("modal", {read: null, static: true}) modal: Component;

  mapToolSubscription: Subscription;
  mapTool: string;

  httpOptions = {
    withCredentials: true,
  };

  constructor(
    private elementRef: ElementRef,
    private modalService: NgbModal,
    private mapService: MapService,
    private http: HttpClient,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService,
    private environment: EnvCoreService
  ) {}

  ngOnInit() {
    this.mapToolSubscription = this.coreService
      .getMapTool()
      .subscribe((tool) => {
        if (tool) {
          this.mapTool = tool;
        }
      });
  }

  ngOnDestroy() {
    if (this.mapToolSubscription) {
      this.mapToolSubscription.unsubscribe();
    }
  }

  @HostListener("click", ["$event"])
  click(event: MouseEvent): void {
    this.coreService.setMapTool("");
    this.mapService.removeInformativeDisplayLayer();
    if (
      this.modal &&
      this.modal.constructor !== AnnotatingModalComponent &&
      this.modal.constructor !== InterrogatingModalComponent
    ) {
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "lg",
        centered: true,
        windowClass: "window-xl",
        container: "#modals",
      });
      if (modalRef.componentInstance.setMapId) {
        modalRef.componentInstance.setMapId(
          this.mapIdService.getId() || "visualiseur"
        );
      }
    } else if (
      this.modal &&
      (this.modal.constructor === AnnotatingModalComponent ||
        this.modal.constructor === InterrogatingModalComponent)
    ) {
      this.coreService.setAnnotatingSnapping(false);
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(
        (this.modal as Component).constructor,
        {
          size: "sm",
          windowClass: "window-tool",
          container: "#map-container",
          backdropClass: "backdrop-tool",
          keyboard: false,
        }
      );
      modalRef.result.then(
        (close) => {
          this.coreService.setMapTool("");
        },
        (dismiss) => {}
      );
      if (this.elementRef) {
        this.coreService.setInterrogatingOption("desktop");
        const toolName =
          this.elementRef.nativeElement.firstElementChild.firstElementChild
            .textContent;
        modalRef.componentInstance.setToolName(toolName);
        if (modalRef.componentInstance.setMapId) {
          modalRef.componentInstance.setMapId(
            this.mapIdService.getId() || "main"
          );
        }
      }
    } else if (this.zone && this.zone.bbox) {
      this.map = this.mapService.getMap(this.mapIdService.getId() || "main");
      this.map.getView().fit(this.zone.bbox);
      this.mapService.setMapFitEvent(true);
    } else if (this.zone && this.zone.user) {
      this.http
        .get(
          `${this.environment.serverUrl}carto/FavoriteArea/get?id=${this.zone.id}`,
          this.httpOptions
        )
        .subscribe((zone) => {
          this.map = this.mapService.getMap(
            this.mapIdService.getId() || "main"
          );
          this.map.getView().fit((zone as any).data.bbox);
          this.mapService.setMapFitEvent(true);
        });
    }
  }
}
