import { TestBed } from '@angular/core/testing';

import { VisualiseurCoreService } from './visualiseur-core.service';

describe('VisualiseurCoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VisualiseurCoreService = TestBed.get(VisualiseurCoreService);
    expect(service).toBeTruthy();
  });
});
