import { EnvCoreService } from './../env.core.service';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlProxyService {

  proxyUrl = '';

  constructor(
    private environment: EnvCoreService
  ) {
    this.proxyUrl = this.environment.serverUrl + 'core/proxy';
  }

  getFinalUrl(url: string, options: string[][], bool: boolean = false): string {
    let newUrl = url.substr(url.length - 1) === '?' ? url.substring(0, url.length - 1) : url;
    options.forEach((opt, index) => {
      if (!this.isOptionExist(url, opt[0])) {
        newUrl += (newUrl.includes('?') ? '&' : '?') + opt[0] + '=' + opt[1];
      }
    });
    let finalUrl = '';
    if (bool) {
      finalUrl = newUrl;
    } else {
      finalUrl = this.proxyUrl + '?proxyUrl=' + encodeURIComponent(newUrl);
    }
    return finalUrl;
  }

  isOptionExist(url: string, opt: string): boolean {
    let ret = false;
    if (url.toUpperCase().includes(opt.toUpperCase())) {
      ret = true;
    }
    return ret;
  }

  getProxyUrl(url: string): string {
    return this.proxyUrl + '?proxyUrl=' + encodeURIComponent(url);
  }
}
