import { TestBed } from '@angular/core/testing';

import { DrawLayerService } from './draw-layer.service';

describe('DrawLayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DrawLayerService = TestBed.get(DrawLayerService);
    expect(service).toBeTruthy();
  });
});
