import { TestBed } from '@angular/core/testing';

import { FavoriteZonesManageService } from './favorite-zones-manage.service';

describe('FavoriteZonesManageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FavoriteZonesManageService = TestBed.get(FavoriteZonesManageService);
    expect(service).toBeTruthy();
  });
});
