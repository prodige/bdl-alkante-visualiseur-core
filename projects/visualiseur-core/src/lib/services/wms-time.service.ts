import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import ImageLayer from 'ol/layer/Image';
import ImageSource from 'ol/source/Image';

@Injectable({
  providedIn: 'root'
})
export class WmsTimeService {

  wmsLayers: ReplaySubject<Array<ImageLayer<ImageSource>>> = new ReplaySubject<Array<ImageLayer<ImageSource>>>(1);

  constructor() { }

  setWmsLayers(layer: Array<ImageLayer<ImageSource>>): void {
    this.wmsLayers.next(layer);
  }

  getWmsLayers(): Observable<Array<ImageLayer<ImageSource>>> {
    return this.wmsLayers;
  }

}
