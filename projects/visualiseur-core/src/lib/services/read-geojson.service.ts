import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FeatureCollection } from '../models/context';

@Injectable({
  providedIn: 'root'
})
export class ReadGeojsonService {

  constructor(private http: HttpClient) { }

  async getGeojson(link: string): Promise<FeatureCollection> {
    return (this.http.get(link).toPromise() as Promise<unknown> as Promise<FeatureCollection>);
  }
}
