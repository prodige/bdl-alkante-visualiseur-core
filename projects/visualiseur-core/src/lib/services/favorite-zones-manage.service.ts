import { EnvCoreService } from './../env.core.service';
import { Injectable, Inject } from '@angular/core';
import { ReplaySubject, Observable, Subscription, forkJoin } from 'rxjs';
import { FeatureCollection } from '../models/context';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { HttpClient } from '@angular/common/http';
import { UserConnectionService } from './user-connection.service';

@Injectable({
  providedIn: 'root'
})
export class FavoriteZonesManageService {

  FAVORITE_STORAGE_KEY = 'favoriteZones';
  httpOptions = {
    withCredentials : true
  };
  isConnected = false;
  forkJoin: Subscription;
  private sessionFavoriteZones = new ReplaySubject<Array<FeatureCollection['GeobookmarkConfig']>>(1);
  private localFavoriteZones = new ReplaySubject<Array<FeatureCollection['GeobookmarkConfig']>>(1);
  private userFavoriteZones = new ReplaySubject<Array<FeatureCollection['GeobookmarkConfig']>>(1);

  constructor(
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private http: HttpClient,
    private userConnectionService: UserConnectionService,
    private environment: EnvCoreService
  ) {

    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }

    let localFZ = [];
    if (this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      localFZ = this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY);
      this.setLocalFavoriteZones(this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY));
    }

    const userFZ = [];
    this.userConnectionService.getUserConfig().subscribe(config => {
      this.isConnected = config.userInternet !== null ? !config.userInternet : null;
      if (this.isConnected) {
        const forkJoinObservables = [];
        forkJoinObservables.push(this.http.get(`${this.environment.serverUrl}carto/FavoriteArea/list`, this.httpOptions));

        this.forkJoin = forkJoin(forkJoinObservables).subscribe(zoneList => {
          (zoneList[0] as any).data.forEach(zone => {
            userFZ.push({
              id: zone.id,
              title: zone.title,
              user: true
            });
          });
          if (userFZ.length) {
            this.setUserFavoriteZones(userFZ);
          }
          if (this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
            const sessionFZ = this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY);
            if (userFZ.length) {
              const isUserFZ = userFZ.some(user => sessionFZ.find(session => {
                if (!session.user) {
                  return false;
                } else {
                  return session.id = user.id;
                }
              }));
              if (isUserFZ) {
                this.setSessionFavoriteZones(sessionFZ);
              } else {
                if (userFZ.length) {
                  userFZ.forEach(zone => {
                    sessionFZ.push(zone);
                  });
                }
                if (sessionFZ) {
                  this.setSessionFavoriteZones(sessionFZ);
                }
              }
            }
          } else {
            const listToSet = [];
            if (localFZ.length) {
              localFZ.forEach(zone => {
                listToSet.push(zone);
              });
            }
            if (userFZ.length) {
              userFZ.forEach(zone => {
                listToSet.push(zone);
              });
            }
            if (listToSet) {
              this.setSessionFavoriteZones(listToSet);
            }
          }
        });
      } else if (this.isConnected !== null && !this.isConnected) {
        this.setUserFavoriteZones([]);
        if (this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY) && this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY).length) {
          const sessionFZ = [];
          this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY).forEach(zone => {
            if (!zone.user) {
              sessionFZ.push(zone);
            }
          });
          if (sessionFZ) {
            this.setSessionFavoriteZones(sessionFZ);
          }
        } else {
          if (localFZ.length) {
            this.setSessionFavoriteZones(localFZ);
          }
        }
      }
    });

  }

  getSessionFavoriteZones(): Observable<Array<FeatureCollection['GeobookmarkConfig']>> {
    return this.sessionFavoriteZones;
  }

  setSessionFavoriteZones(zones: Array<FeatureCollection['GeobookmarkConfig']>): void {
    this.sessionFavoriteZones.next(zones);
    this.sessionStorage.store(this.FAVORITE_STORAGE_KEY, zones);
  }

  getLocalFavoriteZones(): Observable<Array<FeatureCollection['GeobookmarkConfig']>> {
    return this.localFavoriteZones;
  }

  setLocalFavoriteZones(zones: Array<FeatureCollection['GeobookmarkConfig']>): void {
    this.localFavoriteZones.next(zones);
    this.localStorage.store(this.FAVORITE_STORAGE_KEY, zones);
  }

  getUserFavoriteZones(): Observable<Array<FeatureCollection['GeobookmarkConfig']>> {
    return this.userFavoriteZones;
  }

  setUserFavoriteZones(zones: Array<FeatureCollection['GeobookmarkConfig']>): void {
    this.userFavoriteZones.next(zones);
  }

  // tslint:disable-next-line: max-line-length
  addZone(name: string, saveFavoriteZone: boolean, bbox: {minLongitude: number, minLatitude: number, maxLongitude: number, maxLatitude: number}) {
    const newZone = {
      id: Date.now(),
      title: name,
      bbox: [bbox.minLongitude, bbox.minLatitude, bbox.maxLongitude, bbox.maxLatitude]
    };

    // On vérifie si localStorage et sessionStorage sont vides ou non.
    // Lorsqu'ils sont vides : On y crée une première zone dans un Array
    // Lorsqu'ils ne sont pas vides :  On prend les valeurs dans des variables locales
    let localRetrieve = [];
    if (this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      localRetrieve = this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY);
    } else {
      if (saveFavoriteZone && !this.isConnected) {
        this.setLocalFavoriteZones([]);
      }
    }
    let sessionRetrieve = [];
    if (this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      sessionRetrieve = this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY);
    } else {
      this.setSessionFavoriteZones([]);
    }


    // On vérifie que la nouvelle zone n'existe dans aucun des deux storage
    let canBeInsertedLocal = true;
    let canBeInsertedSession = true;
    if (localRetrieve.length) {
      localRetrieve.forEach(zone => {
        if (zone.bbox) {
          if (zone.bbox.toString() === newZone.bbox.toString()) {
            canBeInsertedLocal = false;
          }
        }
      });
    }
    if (sessionRetrieve.length) {
      sessionRetrieve.forEach(zone => {
        if (zone.bbox) {
          if (zone.bbox.toString() === newZone.bbox.toString()) {
            canBeInsertedSession = false;
          }
        }
      });
    }

    // Si la nouvelle zone n'existe dans aucun des deux storage, on l'ajoute aux existants (s'il y en a)
    if (canBeInsertedLocal) {
      if (saveFavoriteZone && !this.isConnected) {
        // Enregistrement en local storage car utilisateur déconnecté
        localRetrieve.push(newZone);
        this.setLocalFavoriteZones(localRetrieve);
        sessionRetrieve.push(newZone);
        this.setSessionFavoriteZones(sessionRetrieve);
      } else if (saveFavoriteZone && this.isConnected) {
        // Enregistrement en base
        this.http.post(`${this.environment.serverUrl}carto/FavoriteArea/save`, newZone, this.httpOptions).subscribe(response => {
          if (canBeInsertedSession) {
            sessionRetrieve.push({
              id: (response as any).data.id,
              title: (response as any).data.title,
              user: true
            });
            this.setSessionFavoriteZones(sessionRetrieve);
          }
        });
      }
    }
    if (canBeInsertedSession && !saveFavoriteZone) {
      sessionRetrieve.push(newZone);
      this.setSessionFavoriteZones(sessionRetrieve);
    }
  }

}
