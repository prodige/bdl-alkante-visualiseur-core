import { Injectable } from '@angular/core';
import { Style, Fill, Stroke, Icon, Text, Circle } from 'ol/style';
import CircleStyle from 'ol/style/Circle';
import Cluster from 'ol/source/Cluster';
import { MapService } from '../map/map.service';
import { MapIdService } from '../map/map-id.service';
import Map from 'ol/Map';
import { Select } from 'ol/interaction';
import { doubleClick } from 'ol/events/condition';
import {toContext} from 'ol/render';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import AnimatedCluster from 'ol-ext/layer/AnimatedCluster';
import { Geometry } from 'ol/geom';
export interface ClusterStyleOption {
  distance: number;
  singleColor: boolean;
  color: string;
  fixedSize: boolean;
  size: number;
  transitionEffect: boolean;
  zoomOnClick: boolean; // not implemented
  withText: boolean;
  url: string;
}

@Injectable({
  providedIn: 'root'
})
export class ClusterService {
  /** */
  styleCache = {};

  /** */
  clusterSource: Cluster = null;

  /** */
  map: Map;

  /** */
  selectFeature: Select;

  /** */
  layer: VectorLayer;

  /** */
  options: ClusterStyleOption;

  constructor(
    private mapIdService: MapIdService
  ) {
  }

  /** */
  setOptions(option: ClusterStyleOption = null) {
    this.options = option;
  }

  clearCache() {
    this.styleCache = [];
  }

  /** */
  generateLegendToLayer(layer: AnimatedCluster): void {
    const canvas: HTMLCanvasElement = document.createElement('canvas');
    const vectorContext = toContext(canvas.getContext('2d'), {size: [30, 30]});

    // Choix de la couleur
    const size = 1;
    let color = size > 25 ? '248, 128, 0' : size > 8 ? '248, 192, 0' : '128, 192, 64';

    if (this.options.singleColor && this.options.color) {
      color = '#' + this.options.color;
    }


      // choix de la taille
    let radius = Math.max(8, Math.min(size * 0.75, 20));
    if (this.options.fixedSize && this.options.size) {
      radius =  this.options.size;
    }

    const style = new Style({
      image: new Circle({
        radius: 10,
        fill: new Fill({
          color: !this.options.singleColor ? 'rgba(' + color + ',0.6)' : color
        })
      }),
      /** Choix du text */
      text: (!this.options.withText ? null :  new Text({
        text: size.toString(),
        fill: new Fill({
          color: '#000'
        })
      }))
    });

    const feature = new Feature({
      geometry: new Point([15, 15]),
      labelPoint: new Point([15, 15]),
      name: 'My Polygon'
    });

    vectorContext.drawFeature(feature, style);
    const canvasContents = canvas.toDataURL('image/png', 1);
    layer.set('imageDataUrl', canvasContents);
    layer.set('isPrefixLegendImg', true );
    layer.set('oneClassOnly', false );
    canvas.remove();
  }

  /** */
  applyStyle(feature: Feature<Geometry>): Style[] {
    const size = feature.get('features').length;
    let style = this.styleCache[size];
    if (!style) {

      // Choix de la couleur
      let color = size > 25 ? '248, 128, 0' : size > 8 ? '248, 192, 0' : '128, 192, 64';

      if (this.options.singleColor && this.options.color) {
        color = '#' + this.options.color;
      }


      // choix de la taille
      let radius = Math.max(8, Math.min(size * 0.75, 20));
      if (this.options.fixedSize && this.options.size) {
        radius =  this.options.size;
      }



      style = [
        new Style({
          image: new Circle({
            radius: this.options.fixedSize ? radius : radius + 2,
            stroke: new Stroke({
              color:  !this.options.singleColor ? 'rgba(' + color + ',0.3)' : color,
              width: 4
            })
          })
        }),
        new Style({
          image: new Circle({
            radius,
            fill: new Fill({
              color: !this.options.singleColor ? 'rgba(' + color + ',0.6)' : color
            })
          }),
          /** Choix du text */
          text: (!this.options.withText ? null :  new Text({
            text: size.toString(),
            fill: new Fill({
              color: '#000'
            })
          }))
        })
      ];

      this.styleCache[size] = style;
    }
    return style;
  }


}
