import { Injectable } from '@angular/core';
import { TreeviewItem } from 'ngx-treeview';
import { ReplaySubject, Observable } from 'rxjs';
import { optionsFromCapabilities } from 'ol/source/WMTS';
import { boundingExtent } from 'ol/extent';

interface BaseLayerConfig {
  id: string;
  class: string; // 'ImageLayer' | 'TileLayer';;
  options: {
    opacity: number,
    visible: boolean,
    source: {
      [param: string]: any;
    };
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}
interface LayerGroupConfig {
  class: 'LayerGroup';
  layers: Array<BaseLayerConfig|LayerGroupConfig>;
  options: {
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}

@Injectable({
  providedIn: 'root'
})
export class AddLayerServersService {

  private fullItem: ReplaySubject<TreeviewItem> = new ReplaySubject<TreeviewItem>(1);
  allowedFormats = ['image/png', 'image/gif', 'image/jpeg'];

  constructor() { }

  /**
   * Constructs the layers tree
   * @param type Capabilities type (wms | wfs | wmts)
   * @param tree The tree to fill and complete
   * @param data The capabilities got from request
   * @param mapProj The map projection
   * @param capabilityLvl null: searching for crs matching  | 0: searching for crs matching on first lvl | 1: searching for crs matching on other lvls
   */
  constructTree(type: string, tree: Array<TreeviewItem>, data: any, mapProj: string = null, capabilityLvl: number = 0): Array<TreeviewItem> {
    let dataLayer = null;
    if (data && type === 'wfs') {
      dataLayer = data.FeatureType;
    } else if (data && type === 'wms') {
      if (data.Layer && [0, 1].includes(capabilityLvl)) {
        if (data.Layer.length) {
          dataLayer = data.Layer.filter(layerWms => {
            return layerWms.CRS ? layerWms.CRS.includes(mapProj) : layerWms;
          });
        } else {
          dataLayer = data.Layer.CRS ? (data.Layer.CRS.includes(mapProj) ? data.Layer : null) : data.Layer;
        }

        if (capabilityLvl === 0 && dataLayer) {
          capabilityLvl = null;
        } else if (capabilityLvl === 0 && !dataLayer) {
          capabilityLvl = 1;
        }
      } else {
        dataLayer = data.Layer;
      }
    } else if ( data && type === 'wmts' ) {
      if ( data.TileMatrixSet ) {
        const tileMatrixSet = data.TileMatrixSet.filter(matrix => {
          const regexProj = new RegExp(/(urn:ogc:def:crs:)(EPSG):([0-9].*)?:([0-9]*)/).exec(matrix.SupportedCRS);
          return matrix.SupportedCRS === mapProj ? matrix.SupportedCRS === mapProj
          : (regexProj && regexProj.length >= 4) ? ((regexProj[2] + ':' + regexProj[4]) === mapProj) : null;
        });
        if ( tileMatrixSet.length ) {
          dataLayer = data.Layer.filter(layerWmts => {
            let bFound = false;
            // filter on comatible matrixset according to map SRS
            layerWmts.TileMatrixSetLink.forEach( TileMatrixSetLink => {
              if ( TileMatrixSetLink.TileMatrixSet === tileMatrixSet[0].Identifier ) {
                bFound = true;
                return;
              }
            });
            if ( bFound ) {
              // introduce selected matrixset according to compatible SRS to use it later
              layerWmts.choosenTileMatrixSet = tileMatrixSet[0].Identifier;
            }
            return bFound;
          });
        }
      } else {
        dataLayer = data.Layer;
      }
    }

    if (data && dataLayer) {
      if (dataLayer.length) {
        dataLayer.forEach(layer => {
          tree.push (new TreeviewItem({
            // text: (type === 'wfs' ? layer.title[0].value : layer.Title),
            text: layer.Title,
            value: {
              value: type === 'wmts' ? layer.Identifier : layer.Name,
              metadata: type === 'wms' ? (layer.MetadataURL ? layer.MetadataURL[0].OnlineResource : '') : '',
              format: type === 'wmts' ? (layer.Format ? layer.Format[0] : '') : ''
            },
            checked: false,
            children: this.constructTree(type, [], layer, mapProj, capabilityLvl)
          }));
        });
      } else if ( !(dataLayer instanceof Array) ) {
        tree.push (new TreeviewItem({
          // text: (type === 'wfs' ? dataLayer.title[0].value : dataLayer.Title),
          text: dataLayer.Title,
          value: {
            value: (type === 'wfs' ? '' : (type === 'wms' ? dataLayer.Name : dataLayer.Identifier)),
            metadata: type === 'wms' ? (dataLayer.MetadataURL ? dataLayer.MetadataURL[0].OnlineResource : '') : '',
            format: type === 'wmts' ? (dataLayer.Format ? dataLayer.Format[0] : '') : ''
          },
          checked: false,
          children: this.constructTree(type, [], dataLayer, mapProj, capabilityLvl)
        }));
      }
    }
    return tree;
  }

  getFullItem(): Observable<TreeviewItem> {
    return this.fullItem;
  }

  searchItem(type: string, name: string, searchIn: any) {
    const searchInLayer = (type === 'wfs' ? searchIn.FeatureType : searchIn.Layer);
    if (searchIn && searchInLayer) {
      if (searchInLayer.length) {
        searchInLayer.forEach(layer => {
          if ((type === 'wmts' ? layer.Identifier : layer.Name ) === name) {
            this.fullItem.next(layer);
          } else {
            this.searchItem(type, name, layer);
          }
        });
      } else {
        if ((type === 'wmts' ? searchIn.Identifier : searchIn.Name ) === name) {
          this.fullItem.next(searchIn);
        } else {
          this.searchItem(type, name, searchInLayer);
        }
      }
    }
  }

  addLayer(
    type: string,
    layer: any,
    index: number,
    jsonResponse: any,
    layerIndex: number,
    selectedFormat = '',
    mapProj?: string,
    legendLayerUrl?: string
    ): BaseLayerConfig | LayerGroupConfig | Array<BaseLayerConfig | LayerGroupConfig> {
    let layerConfig;
    const layerGroupId = (new Date()).getTime();
    if (type === 'wms' && layer) {
      layerConfig = {
        id: (index === 0) ? layerGroupId : layerGroupId + index,
        class: 'ImageLayer',
        options: {
          opacity: 0.5,
          visible: true,
          source: {
            class: 'ImageWMS',
            options: {
              url: (
                jsonResponse.Capability.Request.GetMap.DCPType[0].HTTP.Get.OnlineResource ?
                jsonResponse.Capability.Request.GetMap.DCPType[0].HTTP.Get.OnlineResource : ''),
              params: {
                LAYERS: layer.Name,
                FORMAT : selectedFormat,
                VERSION : jsonResponse.version
              }
            }
          }
        },
        extension: {
          imageDataUrl: legendLayerUrl,
          legendUrl: legendLayerUrl,
          layerIdx: layerIndex,
          name: layer.Title,
          layerName: layer.Name,
          rootUrl: (
            jsonResponse.Capability.Request.GetMap.DCPType[0].HTTP.Get.OnlineResource ?
            jsonResponse.Capability.Request.GetMap.DCPType[0].HTTP.Get.OnlineResource : ''),
          displayClass: true,
          externalLayer: true,
          depth: 1,
          forceOpacityControl: true,
          LayerDownload: false,
          LayerLegendScaleDisplay: false,
          baseLayer: false,
          defaultbaseLayer: false
        }
      };
    } else if (type === 'wfs' && layer) {
      let gmlFormatSelection;
      let optionsUrl = '';
      const operations = jsonResponse['wfs:WFS_Capabilities']['ows:OperationsMetadata']['ows:Operation'];
      operations.forEach(op => {
        if (op.name === 'GetFeature' || op['$'].name === 'GetFeature') {
          const parameter = op['ows:Parameter'];
          parameter.forEach(param => {
            if (param['$'].name === 'outputFormat') {
              let list = [];
              /* param.allowedValues.valueOrRange.forEach(vor => {
                list.push(vor.value);
              }); */
              list = Array.isArray(param['ows:Value']) ? param['ows:Value'] : [param['ows:Value']];
              if (list && list.length > 1) {
                list = list.filter(e => {
                  return e.indexOf('gml') !== -1;
                // tslint:disable-next-line:only-arrow-functions
                }).sort(function(e1, e2) {
                  const pattern = /^[^0-9]*([0-9\.]+)$/;
                  const res1 = e1.match(pattern);
                  const ve1 = res1 != null && res1.length >= 2 ? res1[1] : '';
                  const res2 = e2.match(pattern);
                  const ve2 = res2 != null && res2.length >= 2 ? res2[1] : '';
                  return Number(ve1 < ve2);
                });
              }
              gmlFormatSelection = list ? list[0] : '';
            }
          });
        }
        if(op['ows:DCP']['ows:HTTP']['ows:Get'] && op['ows:DCP']['ows:HTTP']['ows:Get']['$']['xlink:href']) {
          optionsUrl = op['ows:DCP']['ows:HTTP']['ows:Get']['$']['xlink:href'];
        }
      });
      layerConfig = {
        id: (index === 0) ? layerGroupId : layerGroupId + index,
        class: 'VectorLayer',
        options: {
          opacity: 0.5,
          visible: true,
          extent: boundingExtent([layer['ows:WGS84BoundingBox']['ows:LowerCorner'], layer['ows:WGS84BoundingBox']['ows:UpperCorner']]),
          source: {
            class: 'VectorSource',
            options: {
                format: 'WFS',
                version: jsonResponse['wfs:WFS_Capabilities']['$'].version,
                gmlFormat: gmlFormatSelection,
                typename: {
                    name: layer.Name,
                    prefix: '', //layer.name.prefix,
                    ns: '', //layer.name.namespaceURI
                  },
                url: optionsUrl
            }
          }
        },
        extension: {
          layerIdx: layerIndex,
          // name: layer.title[0].value,
          name: layer.Title,
          layerName: layer.Name,
          displayClass: true,
          externalLayer: true,
          rootUrl: optionsUrl,
          depth: 1,
          forceOpacityControl: true,
          LayerDownload: true,
          LayerLegendScaleDisplay: false,
          defaultbaseLayer: false,
          LayerQuery: {
            URLConfig: '',
            Fields: [
            ]
          }
        }
      };
    } else if (type === 'wmts' && layer) {
      const tileMatrixSet = jsonResponse.Contents.TileMatrixSet.filter(matrix => {
        return matrix.SupportedCRS === mapProj;
      });
      const layerConfigOptions = optionsFromCapabilities(jsonResponse, {
        layer: layer.Identifier,
        matrixSet: layer.choosenTileMatrixSet
      });

      /* if (tileMatrixSet.length && (tileMatrixSet[0].Identifier === layerConfigOptions.matrixSet)) { */
      layerConfig = {
        class: 'TileLayer',
        options: {
          opacity: 1,
          visible: true,
          source: {
            class: 'WMTS',
            options: {
              url: layerConfigOptions.urls[0],
              layer: layerConfigOptions.layer,
              matrixSet: layerConfigOptions.matrixSet,
              format: layerConfigOptions.format,
              style: layer.Style[0].Identifier,
              tileGrid: {
                class: 'WMTSTileGrid',
                options: {
                  origins: layerConfigOptions.tileGrid.getOrigin(5),
                  resolutions: layerConfigOptions.tileGrid.getResolutions(),
                  matrixIds: layerConfigOptions.tileGrid.getMatrixIds()
                }
              }
            }
          }
        },
        extension: {
          layerIdx: layerIndex,
          name: layer.Title,
          displayClass: true,
          depth: 1,
          externalLayer: true,
          forceOpacityControl: true,
          LayerDownload: true,
          LayerLegendScaleDisplay: false,
          defaultbaseLayer: false,
          LayerQuery: {
            URLConfig: '',
            Fields: [
            ]
          }
        }
      };
    }
    return layerConfig;
  }

}
