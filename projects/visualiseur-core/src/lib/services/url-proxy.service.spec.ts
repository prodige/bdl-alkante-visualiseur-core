import { TestBed } from '@angular/core/testing';

import { UrlProxyService } from './url-proxy.service';

describe('UrlProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UrlProxyService = TestBed.get(UrlProxyService);
    expect(service).toBeTruthy();
  });
});
