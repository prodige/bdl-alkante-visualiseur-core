import { TestBed } from '@angular/core/testing';

import { NavToolsService } from './nav-tools.service';

describe('NavToolsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavToolsService = TestBed.get(NavToolsService);
    expect(service).toBeTruthy();
  });
});
