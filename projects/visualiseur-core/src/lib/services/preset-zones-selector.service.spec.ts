import { TestBed } from '@angular/core/testing';

import { PresetZonesSelectorService } from './preset-zones-selector.service';

describe('PresetZonesSelectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PresetZonesSelectorService = TestBed.get(PresetZonesSelectorService);
    expect(service).toBeTruthy();
  });
});
