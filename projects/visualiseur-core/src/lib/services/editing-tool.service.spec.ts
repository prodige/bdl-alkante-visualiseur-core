import { TestBed } from '@angular/core/testing';

import { EditingToolService } from './editing-tool.service';

describe('EditingToolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditingToolService = TestBed.get(EditingToolService);
    expect(service).toBeTruthy();
  });
});
