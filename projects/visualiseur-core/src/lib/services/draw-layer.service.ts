import { VisualiseurCoreService } from '../visualiseur-core.service';
import { Injectable } from '@angular/core';

import Map from 'ol/Map';

import FontSymbol from 'ol-ext/style/FontSymbol';
import LayerGroup from 'ol/layer/Group';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { ReplaySubject, Observable } from 'rxjs';
import { Feature } from 'ol/';
import FillPattern from 'ol-ext/style/FillPattern';
import { Style, Fill, Stroke, Text, Circle, Icon  } from 'ol/style';
import { FeatureCollection } from '../models/context';
import GeoJSON from 'ol/format/GeoJSON';
import WKTReader from 'jsts/org/locationtech/jts/io/WKTReader';
import GeometryFactory from 'jsts/org/locationtech/jts/geom/GeometryFactory';
import WKT from 'ol/format/WKT';
import { BufferOp } from 'jsts/org/locationtech/jts/operation/buffer';
import WKTParser from 'jsts/org/locationtech/jts/io/WKTParser';
import { MapService } from '../map/map.service';
import { MapIdService } from '../map/map-id.service';
import { HttpClient } from '@angular/common/http';
import { EnvCoreService } from '../env.core.service';
import { FormGroup } from '@angular/forms';

import StrokePattern from 'ol-ext/style/StrokePattern';
import MultiPoint from 'ol/geom/MultiPoint';
import { Geometry, Point } from 'ol/geom';

import { Options as SourceOptions } from 'ol/source/Source';
import { Layer } from 'ol/layer';
import saveAs from 'file-saver';

interface FeaturesInfo {
  id: string;
  geometry: string;
}

interface EditionSession {
  mapExtent: Array<number>;
  idEditableLayer: string;
  idSnappingLayer: string;
  tolerance: number;
  addedFeatures: Array<FeaturesInfo>;
  modifiedFeatures: Array<FeaturesInfo>;
  deletedFeatures: Array<FeaturesInfo>;
}

interface BaseLayerConfig {
  id: string;
  class: string; // 'ImageLayer' | 'TileLayer';;
  options: {
    opacity: number,
    visible: boolean,
    source: {
      [param: string]: any;
    };
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}
interface LayerGroupConfig {
  class: 'LayerGroup';
  layers: Array<BaseLayerConfig|LayerGroupConfig>;
  options: {
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}

@Injectable({
  providedIn: 'root'
})
export class DrawLayerService {

  rootLayerGroup: LayerGroup;
  drawnFeatures: Array<Feature<any>> = [];
  map: Map;

  private hasFeature: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  private bufferGeneralNameIndex: ReplaySubject<number> = new ReplaySubject<number>(1);

  private defaultDrawStyle: Style = null;
  private defaultSelectStyle: Style = null;

  private pointStyleOptions: ReplaySubject<object> = new ReplaySubject<object>(1);
  private pointStyle: ReplaySubject<Style> = new ReplaySubject<Style>(1);

  private lineStringStyleOptions: ReplaySubject<object> = new ReplaySubject<object>(1);
  private lineStringStyle: ReplaySubject<Style> = new ReplaySubject<Style>(1);

  private polygonStyleOptions: ReplaySubject<object> = new ReplaySubject<object>(1);
  private polygonStyle: ReplaySubject<Style> = new ReplaySubject<Style>(1);

  private vectorDrawLayer: VectorLayer<VectorSource<Geometry>>;
  private vectorDrawSource: VectorSource<any>;
  private vectorDrawLayer$: ReplaySubject<VectorLayer<VectorSource<Geometry>>> = new ReplaySubject<VectorLayer<VectorSource<Geometry>>>(1);

  private bufferLayerGroup: LayerGroup;
  private bufferLayerGroup$: ReplaySubject<LayerGroup> = new ReplaySubject<LayerGroup>(1);
  private vectorBufferLayer: VectorLayer<VectorSource<Geometry>>;
  private vectorBufferSource: VectorSource<any>;
  private vectorBufferLayer$: ReplaySubject<VectorLayer<VectorSource<Geometry>>> = new ReplaySubject<VectorLayer<VectorSource<Geometry>>>(1);

  private vectorEditLayer: VectorLayer<VectorSource<Geometry>>;
  private vectorEditSource: VectorSource<any>;
  private vectorEditLayer$: ReplaySubject<VectorLayer<VectorSource<Geometry>>> = new ReplaySubject<VectorLayer<VectorSource<Geometry>>>(1);

  private vectorSnappingLayer: VectorLayer<VectorSource<Geometry>>;
  private vectorSnappingSource: VectorSource<any>;
  private vectorSnappingLayer$: ReplaySubject<VectorLayer<VectorSource<Geometry>>> = new ReplaySubject<VectorLayer<VectorSource<Geometry>>>(1);

  private hasBeenInitialized = false;

  constructor(
    private http: HttpClient,
    public mapService: MapService,
    private mapIdService: MapIdService,

    private coreService: VisualiseurCoreService,
    private environment: EnvCoreService
  ) {
    this.bufferGeneralNameIndex.next(1);
    this.coreService.getRootLayerGroup().subscribe(
      (rootLayerGroup: LayerGroup) => {
        if (!this.hasBeenInitialized && this.vectorDrawLayer) {
          this.vectorDrawLayer.setMap(null);
          this.vectorDrawLayer = null;
          this.vectorDrawSource = null;
          this.vectorDrawLayer$.next(null);
        }
        if (!this.hasBeenInitialized && this.vectorBufferLayer) {
          this.vectorBufferLayer.setMap(null);
          this.vectorBufferLayer = null;
          this.vectorBufferSource = null;
          this.vectorBufferLayer$.next(null);
        }
        this.rootLayerGroup = rootLayerGroup;
        this.hasBeenInitialized = true;
      }
    );
  }

  generateNewDrawLayer(mapId: any): void {

    if ( !this.vectorDrawLayer ) {

      let isLayerInContext = false;
      this.rootLayerGroup.getLayers().forEach(layer => {
        if (layer instanceof VectorLayer && layer.get('isDrawLayer')) {
          isLayerInContext = true;
          layer.getSource().getFeatures().forEach((feature: Feature<any>) => {
            if (feature.get('color') || feature.get('font') || feature.get('size') || feature.get('text')) {
              const newStyle: Style = new Style({
                fill: new Fill({
                  color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new Stroke({
                  color: '#ffcc33',
                  width: 2
                }),
                /* image: new Circle({
                  radius: 7,
                  fill: new Fill({
                    color: '#ffcc33'
                  })
                }), */
                text: new Text({
                  text: feature.get('text') ? feature.get('text') : '',
                  textAlign: 'center',
                  textBaseline: 'middle',
                  font: (feature.get('size') ? feature.get('size') + 'px ' : '') + feature.get('font') ? feature.get('font') : '',
                  fill: new Fill({
                    color: feature.get('color') ? feature.get('color') : ''
                  })
                })
              });
              feature.setStyle(newStyle);
            }
          });
          this.vectorDrawSource = layer.getSource();
          this.vectorDrawLayer = layer;
        }
      });

      if (!isLayerInContext) {
        this.vectorDrawSource = new VectorSource({
          wrapX: false,
          features: this.drawnFeatures,
          useSpatialIndex: false
        });
        this.vectorDrawSource.set('class', 'VectorSource');

        // Initialisation du vectorDrawLayer
        this.vectorDrawLayer = new VectorLayer({
          source: this.vectorDrawSource,
          zIndex: Infinity
        });

        this.vectorDrawLayer.set('name', 'Couche dessins');
        this.vectorDrawLayer.set('isDrawLayer', true);
        this.vectorDrawLayer.set('visibleChecked', true);
        this.vectorDrawLayer.set('class', 'VectorLayer');

        const rootLayers = this.rootLayerGroup.getLayers();
        rootLayers.insertAt(0, this.vectorDrawLayer);
        this.rootLayerGroup.setLayers(rootLayers);

        this.mapService.verifyLayerGroupVisibility(this.rootLayerGroup, mapId);

      }

      this.vectorDrawLayer$.next(this.vectorDrawLayer);

      this.vectorDrawLayer.setMap = (map: Map) => {
        if ( this.vectorDrawLayer ) {
          VectorLayer.prototype.setMap.call(this.vectorDrawLayer, map);
          if ( map === null ) {
            this.vectorDrawLayer.getSource().clear();
            this.vectorDrawLayer = null;
            this.vectorDrawLayer$.next(null);
          }
        }
      };

    } else {
      if (this.vectorDrawLayer.getSource().getFeatures().length) {
        this.setHasFeature(true);
      } else {
        this.setHasFeature(false);
      }
    }

  }

  generateEditLayer(features: FeatureCollection, session: EditionSession, mapId): void {
    const styles = [
      /* We are using two different styles for the polygons:
       *  - The first style is for the polygons themselves.
       *  - The second style is to draw the vertices of the polygons.
       *    In a custom `geometry` function the vertices of a polygon are
       *    returned as `MultiPoint` geometry, which will be used to render
       *    the style.
       */
      new Style({
          stroke: new Stroke({
          color: 'blue',
          width: 3
        }),
        fill: new Fill({
          color: 'rgba(0, 0, 255, 0.1)'
        })
      }),
      new Style({
        image: new Circle({
          radius: 5,
          fill: new Fill({
            color: 'orange'
          })
        }),
        geometry(feature) {
          // on renvoie un multipoint constitué des vertex

          // Fonction d'applatissement du tableau de coordonnées
          // pour arriver à un tableau [[x,y],[x,y]...]
          function flatDeep(arr) {
            return arr.reduce(function(acc, val) {
              if (((Array.isArray(val) && (val.length == 2) && (!Array.isArray(val[0])))
              || (!Array.isArray(val)))) {
                return acc.concat([val]);
              } else {
                return acc.concat(flatDeep(val));
              }
            }
            , []);
          }
          // return the coordinates of the geometry
          const coordinates = ((feature.getGeometry() as Point).clone().getCoordinates());
          const flatCoord = flatDeep(coordinates);
          const geom =  new MultiPoint(flatCoord);
          return geom;
        }
      })]
    ;
    if ( !this.vectorEditLayer ) {

      const format = new WKT();

      this.vectorEditSource = new VectorSource({
        wrapX: false,
        features: features.length === 0 ? features : (new GeoJSON()).readFeatures(features)
      } as SourceOptions);

      if (session && session.addedFeatures.length) {
        session.addedFeatures.forEach(added => {
          if (this.vectorEditSource.getFeatures().filter(feature => feature.get('gid') === added.id).length === 0) {
            this.vectorEditSource.addFeature(format.readFeature(added.geometry));
          }
        });
      }

      if (session && session.modifiedFeatures.length) {
        session.modifiedFeatures.forEach(modified => {
          this.vectorEditSource.getFeatures().forEach(feature => {
            if (feature.get('gid') && feature.get('gid') === modified.id) {
              const newFeat = format.readFeature(modified.geometry);
              newFeat.setProperties({gid: modified.id});
              this.vectorEditSource.removeFeature(feature);
              this.vectorEditSource.addFeature(newFeat);
            }
          });
        });
      }

      if (session && session.deletedFeatures.length) {
        session.deletedFeatures.forEach(deleted => {
          this.vectorEditSource.getFeatures().forEach(feature => {
            if (feature.get('gid') && feature.get('gid') === deleted.id) {
              this.vectorEditSource.removeFeature(feature);
            }
          });
        });
      }

      this.vectorEditSource.set('clavectorSnappingLayerss', 'VectorSource');

      // Initialisation du vectorDrawLayer
      this.vectorEditLayer = new VectorLayer({
        source: this.vectorEditSource,
        zIndex: Infinity
      });

      this.vectorEditLayer.set('name', 'Couche d\'édition');
      this.vectorEditLayer.set('isEditLayer', true);
      this.vectorEditLayer.set('class', 'VectorLayer');

      /* const rootLayers = this.rootLayerGroup.getLayers();
      rootLayers.insertAt(0, this.vectorEditLayer);
      this.rootLayerGroup.setLayers(rootLayers); */
      const rootLayers = this.rootLayerGroup.getLayers();
      rootLayers.insertAt(0, this.vectorEditLayer);
      this.rootLayerGroup.setLayers(rootLayers);

      this.mapService.verifyLayerGroupVisibility(this.rootLayerGroup, mapId);

      this.vectorEditLayer$.next(this.vectorEditLayer);

      this.vectorEditLayer.setMap = (map: Map) => {
        if ( this.vectorEditLayer ) {
          VectorLayer.prototype.setMap.call(this.vectorEditLayer, map);
          if ( map === null ) {
            this.vectorEditLayer.getSource().clear();
            this.vectorEditLayer = null;
            this.vectorEditLayer$.next(null);
          }
        }
      };

    } else {
      if (this.vectorEditLayer.getSource().getFeatures().length) {
        this.setHasFeature(true);
      } else {
        this.setHasFeature(false);
      }
    }

    this.vectorEditLayer.setStyle(styles);
  }
  deleteSnappingLayer(): void {
    this.vectorSnappingLayer = null;
    this.vectorSnappingLayer$.next(null);
  }

  generateSnappingLayer(features: FeatureCollection, layerType: string, annotation: boolean = false): void {

    if ( !this.vectorSnappingLayer ) {

      const clonedFeatures = [];

      if (!features.type) {
        features.getArray().forEach(
          (feature: Feature<any>) => {
          clonedFeatures.push(feature.clone());
        });
      }

      this.vectorSnappingSource = new VectorSource({
        wrapX: false,
        features: ((features.length === 0)) ? features : (!features.type ? clonedFeatures : (new GeoJSON()).readFeatures(features))
      } as SourceOptions);

      const stylePoints = new Style({
        image: new Circle({
          radius: 5,
          fill: new Fill({
            color: 'orange'
          })
        }),
        geometry(featureEdit) {
          // on renvoie un multipoint constitué des vertex

          // Fonction d'applatissement du tableau de coordonnées
          // pour arriver à un tableau [[x,y],[x,y]...]
          function flatDeep(arr) {
            return arr.reduce((acc, val) => {
              if (((Array.isArray(val) && (val.length === 2) && (!Array.isArray(val[0])))
              || (!Array.isArray(val)))) {
                return acc.concat([val]);
              } else {
                return acc.concat(flatDeep(val));
              }
            }
            , []);
          }

          // return the coordinates of the geometry
          const coordinates = ((featureEdit.getGeometry() as Point).clone().getCoordinates());
          const flatCoord = flatDeep(coordinates);
          const geom =  new MultiPoint(flatCoord);

          return geom;
        }
      });

      this.vectorSnappingSource.getFeatures().forEach(feature => {
        // Changement du style des features (pour que la couche d'accroche soit transparente)
        if (['Point', 'MultiPoint'].includes(layerType)) {
          feature.setStyle([
            new Style({
              stroke: new Stroke({
                color: `rgba(190, 190, 190, ${annotation ? '0' : '0.5'}`,
                // color: 'red',
                width: 1
              })
            }),
            stylePoints
          ]);
        } else {
          feature.setStyle([
            new Style({
              image: new FontSymbol({
                glyph: 'fa-circle',
                fontSize: 0.3,
                fontStyle: 'normal',
                color: `rgba(190, 190, 190, ${annotation ? '0' : '0.85'}`,
                fill: new Fill({
                  color: `rgba(190, 190, 190, ${annotation ? '0' : '0.85'}`
                }),
                stroke: new Stroke({
                  color: `rgba(190, 190, 190, ${annotation ? '0' : '0.85'}`,
                  width: 2
                }),
                radius: 20
              })
            }),
            stylePoints
          ]);
        }
      });

      this.vectorSnappingSource.set('class', 'VectorSource');

      // Initialisation du vectorDrawLayer
      this.vectorSnappingLayer = new VectorLayer({
        source: this.vectorSnappingSource,
        zIndex: Infinity
      });

      this.vectorSnappingLayer.set('name', 'Couche d\'accroche');
      this.vectorSnappingLayer.set('isSnappingLayer', true);
      this.vectorSnappingLayer.set('class', 'VectorLayer');

      this.vectorSnappingLayer$.next(this.vectorSnappingLayer);

      this.vectorSnappingLayer.setMap = (map: Map) => {
        if ( this.vectorSnappingLayer ) {
          VectorLayer.prototype.setMap.call(this.vectorSnappingLayer, map);
          if ( map === null ) {
            this.vectorSnappingLayer.getSource().clear();
            this.vectorSnappingLayer = null;
            this.vectorSnappingLayer$.next(null);
          }
        }
      };

    } else {
      if (this.vectorSnappingLayer.getSource().getFeatures().length) {
        this.setHasFeature(true);
      } else {
        this.setHasFeature(false);
      }
    }

  }

  generateNewBufferLayerGroup(mapId: any): void {
    let hasBufferLayerGroup = false;
    if (!hasBufferLayerGroup)  {
      const bufferLayerGroupConfig = {
        class: 'LayerGroup',
        options: {
          opacity: 1,
          visible: true
        },
        extension: {
          id: Date.now(),
          name: 'Zones tampons',
          open: '1'
        },
        layers: []
      };

      this.bufferLayerGroup = this.coreService.generateLayersAndLayerGroups(bufferLayerGroupConfig as LayerGroupConfig) as LayerGroup;

      this.bufferLayerGroup.set('isBufferLayerGroup', true);
      this.bufferLayerGroup.set('visibleChecked', true);

      const rootLayers = this.rootLayerGroup.getLayers();
      rootLayers.insertAt(0, this.bufferLayerGroup);
      this.bufferLayerGroup$.next(this.bufferLayerGroup);
      this.rootLayerGroup.setLayers(rootLayers);

      this.mapService.verifyLayerGroupVisibility(this.bufferLayerGroup, mapId);
    }
  }

  addBufferLayer(newFeature: Array<Feature<any>>, name: string, map: Map): VectorLayer<VectorSource<Geometry>> | LayerGroup {

    let returnVector = true;

    this.vectorBufferSource = new VectorSource({
      wrapX: false,
      features: newFeature
    });
    this.vectorBufferSource.set('class', 'VectorSource');

    // Initialisation du vectorDrawLayer
    this.vectorBufferLayer = new VectorLayer({
      source: this.vectorBufferSource,
      zIndex: Infinity
    });

    this.vectorBufferLayer.set('name', name);
    this.vectorBufferLayer.set('isBuffer', true);
    this.vectorBufferLayer.set('class', 'VectorLayer');

    const rootLayers = this.rootLayerGroup.getLayers();
    rootLayers.forEach(layer => {
      if (layer.get('isBufferLayerGroup')) {
        layer.get('layers').insertAt(0, this.vectorBufferLayer);
        returnVector = false;
      }
    });
    this.rootLayerGroup.setLayers(rootLayers);

    this.vectorBufferLayer$.next(this.vectorBufferLayer);

    this.bufferLayerGroup$.next(this.bufferLayerGroup);

    return this.vectorBufferLayer;
  }

  createBuffer(selectedFeature: Feature<Geometry>, polygonCanvas: any, bufferForm: FormGroup, bufferFeatures: Array<Feature<any>>, map: Map) {
    const newBufferFeature = new Feature(selectedFeature);

    const format = new WKT();
    const wktGeom = format.writeFeature(selectedFeature);

    const wktReader = new WKTReader(new GeometryFactory());
    const jstsGeom = wktReader.read(wktGeom);

    const buffered = BufferOp.bufferOp(jstsGeom, Number(bufferForm.get('radius').value));
    const writer = new WKTParser(new GeometryFactory());
    newBufferFeature.setGeometry(format.readGeometry(writer.write(buffered)));

    const newStyle: Style = new Style({
      fill: new FillPattern({
        pattern: polygonCanvas.settedStyle.pattern.name,
        ratio: 2,
        icon: new Icon ({
          src: 'data/target.png'
        }),
        color: polygonCanvas.settedStyle.color,
        offset: 0,
        scale: polygonCanvas.settedStyle.size,
        fill: new Fill ({
          color: polygonCanvas.settedStyle.background
        }),
        size: 1,
        spacing: 10,
        angle: 0
      }),
      stroke: new Stroke({
        color: polygonCanvas.settedStyle.color,
        width: 2
      })
    });

    newBufferFeature.setStyle(newStyle);
    newBufferFeature.setProperties(polygonCanvas);

    bufferFeatures.push(newBufferFeature);
  }

  saveBuffer(map: Map, bufferForm: FormGroup, bufferFeatures: Array<Feature<any>>) {
    const vectorOrLayerGroup = this.addBufferLayer(bufferFeatures, bufferForm.get('name').value, map);
    const rootLayers = this.rootLayerGroup.getLayers();
    rootLayers.forEach(layer => {
      if (layer.get('isBufferLayerGroup')) {
        map.removeLayer(layer);
        (vectorOrLayerGroup as VectorLayer<VectorSource<Geometry>>).setMap(map);
        // map.addLayer(vectorOrLayerGroup);
      }
    });
    this.coreService.setRootLayerGroup(this.rootLayerGroup);
    this.coreService.updateRootLayerGroup();
  }

  changeColInUri(data: any[], color: string) {

    data.forEach(pattern => {
      if (pattern) {
        // create fake image to calculate height / width
        const img = document.createElement('img');
        img.src = pattern.pattern;
        img.style.visibility = 'hidden';

        img.onload = () => {
          document.body.appendChild(img);

          const canvas = document.createElement('canvas');
          canvas.width = (img.offsetWidth !== 0) ? img.offsetWidth : 1;
          canvas.height = (img.offsetHeight !== 0) ? img.offsetWidth : 1;

          const ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0);

          // remove image
          img.parentNode.removeChild(img);

          // do actual color replacement
          const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
          const dataColor = imageData.data;

          let colorToSet = null;
          if (color.includes('rgb') || color.includes('rgba')) {
            const colors = /rgba?\((\d+),\s*(\d+),\s*(\d+),?\s*([\d\.]+)?\)/.exec(color);
            colorToSet = {
              r: parseInt(colors[1]),
              g: parseInt(colors[2]),
              b: parseInt(colors[3])
            };
          } else {
            colorToSet.r = parseInt(color.substr(1, 2), 16);
            colorToSet.g = parseInt(color.substr(3, 2), 16);
            colorToSet.b = parseInt(color.substr(5, 2), 16);
          }

          for (let x = 0; x < dataColor.length; x += 4) {
            dataColor[x] = colorToSet.r;
            dataColor[x + 1] = colorToSet.g;
            dataColor[x + 2] = colorToSet.b;
          }

          ctx.putImageData(imageData, 0, 0);

          pattern.pattern = canvas.toDataURL();
        };
      }
    });

  }

  exportAnnotations(format: string) {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');
    const body = {
      type: '',
      geojson: (new GeoJSON({
        dataProjection: this.map.getView().getProjection().getCode(),
        featureProjection: this.map.getView().getProjection().getCode()
      })).writeFeaturesObject(this.vectorDrawLayer.getSource().getFeatures(),  {
        dataProjection: this.map.getView().getProjection().getCode(),
        featureProjection: this.map.getView().getProjection().getCode()
      }
      ),
      crs: { type: 'name', properties: { name: 'urn:ogc:def:crs:' + this.map.getView().getProjection().getCode() } }
    };
    // TODO why OL does not take in account crs ?
    // body.geojson.crs = { type: 'name', properties: { name: 'urn:ogc:def:crs:' + this.map.getView().getProjection().getCode() } };
    if (format === 'exportDrawsShp') {
      body.type = 'shp';
    } else if (format === 'exportDrawsKml') {
      body.type = 'kml';
    }
    this.http.post(`${this.environment.serverUrl}core/Exportannotation/`, body, {responseType: 'blob'}).subscribe(response => {
      saveAs(response, 'exported_annotations' + (format === 'exportDrawsShp' ? '.zip' : '.kml'));
    });
  }

  checkIfIndexExists(index: number, layerGroup: LayerGroup) {
    layerGroup.getLayers().forEach(layer => {
      if (layer.get('isBufferLayerGroup')) {
        (layer as LayerGroup).getLayers().forEach(sublayer => {
          if (sublayer.get('name').split('zone tampon globale ')[1] === index.toString()) {
            this.incrementBufferGeneralNameIndex(index);
          }
        });
      }
    });
  }

  incrementBufferGeneralNameIndex(oldIndex: number): void {
    this.bufferGeneralNameIndex.next(oldIndex + 1);
  }

  getBufferGeneralNameIndex(): Observable<number> {
    return this.bufferGeneralNameIndex;
  }

  setDefaultDrawStyle(style: Style): void {
    this.defaultDrawStyle = style;
  }

  getDefaultDrawStyle(): Style {
    this.defaultDrawStyle = (this.defaultDrawStyle == null) ? null : this.defaultDrawStyle;
    return this.defaultDrawStyle;
  }

  setDefaultSelectStyle(style: Style): void {
    this.defaultSelectStyle = style;
  }

  getDefaultSelectStyle(): Style {
    this.defaultSelectStyle = (this.defaultDrawStyle == null) ? null : this.defaultDrawStyle;
    return this.defaultSelectStyle;
  }

  getPointStyleOptions(): Observable<object> {
    return this.pointStyleOptions;
  }

  setPointStyleOptions(styleOptions: object): void {
    this.pointStyleOptions.next(styleOptions);
  }

  getPointStyle(): Observable<Style> {
    return this.pointStyle;
  }

  setPointStyle(style: Style): void {
    this.pointStyle.next(style);
  }

  getLineStringStyleOptions(): Observable<object> {
    return this.lineStringStyleOptions;
  }

  setLineStringStyleOptions(styleOptions: object): void {
    this.lineStringStyleOptions.next(styleOptions);
  }

  getLineStringStyle(): Observable<Style> {
    return this.lineStringStyle;
  }

  setLineStringStyle(style: Style): void {
    this.lineStringStyle.next(style);
  }

  getPolygonStyleOptions(): Observable<object> {
    return this.polygonStyleOptions;
  }

  setPolygonStyleOptions(styleOptions: object): void {
    this.polygonStyleOptions.next(styleOptions);
  }

  getPolygonStyle(): Observable<Style> {
    return this.polygonStyle;
  }

  setPolygonStyle(style: Style): void {
    this.polygonStyle.next(style);
  }

  getHasFeature(): Observable<boolean> {
    return this.hasFeature;
  }

  setHasFeature(bool: boolean): void {
    this.hasFeature.next(bool);
  }

  getDrawLayer(): Observable<VectorLayer<VectorSource<Geometry>>> {
    return this.vectorDrawLayer$;
  }

  getBufferLayerGroup(): Observable<LayerGroup> {
    return this.bufferLayerGroup$;
  }

  getEditLayer(): Observable<VectorLayer<VectorSource<Geometry>>> {
    return this.vectorEditLayer$;
  }

  getSnappingLayer(): Observable<VectorLayer<VectorSource<Geometry>>> {
    return this.vectorSnappingLayer$;
  }

}
