import { TestBed } from '@angular/core/testing';

import { AddLayerServersService } from './add-layer-servers.service';

describe('AddLayerServersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddLayerServersService = TestBed.get(AddLayerServersService);
    expect(service).toBeTruthy();
  });
});
