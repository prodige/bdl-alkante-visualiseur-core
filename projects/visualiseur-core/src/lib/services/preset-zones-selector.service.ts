import { Injectable } from '@angular/core';
import { Feature, FeatureCollection } from '../models/context';
import { ReplaySubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PresetZonesSelectorService {

  private presetZoneProjection: ReplaySubject<string> = new ReplaySubject<string>(1);
  private selectedPresetZone: ReplaySubject<Feature> = new ReplaySubject<Feature>(1);
  private allSelectorData: ReplaySubject<Array<[FeatureCollection, Feature]>> = new ReplaySubject<Array<[FeatureCollection, Feature]>>(1);

  constructor() { }

  getSelectedPresetZone(): Observable<Feature> {
    return this.selectedPresetZone;
  }

  setSelectedPresetZone(data: Feature) {
    this.selectedPresetZone.next(data);
  }

  getAllSelectorData(): Observable<Array<[FeatureCollection, Feature]>> {
    return this.allSelectorData;
  }

  setAllSelectorData(data: Array<[FeatureCollection, Feature]>) {
    this.allSelectorData.next(data);
  }

  getPresetZoneProjection(): Observable<string> {
    return this.presetZoneProjection;
  }

  setPresetZoneProjection(projection: string) {
    this.presetZoneProjection.next(projection);
  }

}
