import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject, Observable, Subscription } from 'rxjs';
import { LayerDataResponseType } from '../map/services/interrogating.service';
import { FormGroup } from '@angular/forms';
import { UserConnectionService } from './user-connection.service';

@Injectable({
  providedIn: 'root'
})
export class InterrogatingMailingService {

  coreServiceSubscription: Subscription;
  catalogueUrl: string;
  private modelList$: ReplaySubject<any> = new ReplaySubject<any>(1);
  httpOptions = {
    withCredentials: true
  };

  constructor(private http: HttpClient, private userConnectionService: UserConnectionService) {
    this.userConnectionService.getUserConfig().subscribe(config => {
      if (config.userInternet !== null) {
        this.catalogueUrl = config.URLS.catalogue;
      }
    });
  }

  getModelList(currentLayerResults: LayerDataResponseType): Observable<any> {
    return this.http.get(`${this.catalogueUrl}/prodige/mailing/model?mapName=${currentLayerResults.layer.get('mapName')}&layerName=${currentLayerResults.layer.get('layerName')}`, this.httpOptions);
  }

  setModelList(list: any) {
    this.modelList$.next(list);
  }

  updateModel(mailingForm: FormGroup, currentLayerResults: LayerDataResponseType, currentModelList: any[], selectedModel: any): Observable<any> {
    const modelMaj = {
      objectMessage : mailingForm.get('mailingObject').value,
      bodyMessage : mailingForm.get('mailingMessage').value,
      layerName : currentLayerResults.layer.get('layerName'),
      mapName : currentLayerResults.layer.get('mapName'),
      modelName : mailingForm.get('modelName').value
    }
    if (mailingForm.get('savedModel').value !== 'noPresetSelected') {
      modelMaj['modeleId'] = selectedModel.pk_modele_id;
      return this.http.patch(`${this.catalogueUrl}/prodige/mailing/model`, JSON.stringify(modelMaj), this.httpOptions);
    } else {
      return this.http.post(`${this.catalogueUrl}/prodige/mailing/model`, JSON.stringify(modelMaj), this.httpOptions);
    }
  }

  deleteModel(selectedModel: any): Observable<any> {
    return this.http.delete(`${this.catalogueUrl}/prodige/mailing/model/${selectedModel.pk_modele_id}`, this.httpOptions);
  }

  mailing(mode: string, mailingForm: FormGroup, currentLayerResults: LayerDataResponseType): Observable<any> {
    const datas = []
    currentLayerResults.data.forEach(data => {
      const item = {
        name: mailingForm.get('fromField').value,
        email: mailingForm.get('fromMail').value
      };
      for (const [key, value] of Object.entries(data)) {
        if (key !== 'feature') {
          item[key] = value;
        }
      }
      datas.push(item);
    });
    const mailToSend = {
      data : datas,
      destEmailField: mailingForm.get('toMail').value,
      destNameField : mailingForm.get('toField').value,
      senderName : mailingForm.get('fromField').value,
      senderEmail : mailingForm.get('fromMail').value,
      objectMessage : mailingForm.get('mailingObject').value,
      bodyMessage : mailingForm.get('mailingMessage').value,
      layerName : currentLayerResults.layer.get('layerName'),
      mapName : currentLayerResults.layer.get('mapName'),
      modelName : mailingForm.get('modelName').value
    }
    if (mode === 'test') {
      return this.http.post(`${this.catalogueUrl}/prodige/mailing/test`, JSON.stringify(mailToSend), this.httpOptions);
    } else if (mode === 'send') {
      return this.http.post(`${this.catalogueUrl}/prodige/mailing/send`, JSON.stringify(mailToSend), this.httpOptions);
    }
  }
}
