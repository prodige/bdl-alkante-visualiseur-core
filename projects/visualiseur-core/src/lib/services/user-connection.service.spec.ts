import { TestBed } from '@angular/core/testing';

import { UserConnectionService } from './user-connection.service';

describe('UserConnectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserConnectionService = TestBed.get(UserConnectionService);
    expect(service).toBeTruthy();
  });
});
