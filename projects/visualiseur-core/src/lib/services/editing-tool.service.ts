import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, ReplaySubject } from "rxjs";
import { UserConnectionService } from "./user-connection.service";
import { LocalStorageService } from "ngx-webstorage";
import { Feature } from "ol/";

export interface FeaturesInfo {
  id: string;
  geometry: string;
  properties: Array<Object>;
}

export interface EditionSession {
  mapExtent: Array<number>;
  idEditableLayer: string;
  idSnappingLayer: string;
  tolerance: number;
  addedFeatures: Array<FeaturesInfo>;
  modifiedFeatures: Array<FeaturesInfo>;
  deletedFeatures: Array<FeaturesInfo>;
}

@Injectable({
  providedIn: "root",
})
export class EditingToolService {
  catalogueUrl: string;
  httpOptions = {
    withCredentials: true,
  };

  EDITING_STORAGE_KEY = "editingSession";
  private editingSession$: ReplaySubject<EditionSession> =
    new ReplaySubject<EditionSession>(1);
  private localEditingSession$: ReplaySubject<EditionSession> =
    new ReplaySubject<EditionSession>(1);
  private globalFeature$: ReplaySubject<FeaturesInfo> =
    new ReplaySubject<FeaturesInfo>(1);

  constructor(
    private localStorage: LocalStorageService,
    private http: HttpClient,
    private userConnectionService: UserConnectionService
  ) {
    this.userConnectionService.getUserConfig().subscribe((config) => {
      if (config.userInternet !== null) {
        this.catalogueUrl = config.URLS.catalogue;
      }
    });

    this.setEditingSession(
      this.localStorage.retrieve(this.EDITING_STORAGE_KEY)
    );
  }

  getGlobalFeature(): Observable<FeaturesInfo> {
    return this.globalFeature$;
  }

  setGlobalFeature(feat: FeaturesInfo): void {
    this.globalFeature$.next(feat);
  }

  getEditingSession(): Observable<EditionSession> {
    return this.editingSession$;
  }

  setEditingSession(editingSession: EditionSession): void {
    this.editingSession$.next(editingSession);
    this.setLocalEditingSession(editingSession);
  }

  getLocalEditingSession(): Observable<EditionSession> {
    return this.localEditingSession$;
  }

  setLocalEditingSession(editingSession: EditionSession): void {
    this.localEditingSession$.next(editingSession);
    this.localStorage.store(this.EDITING_STORAGE_KEY, editingSession);
  }

  getSnappingFeatures(uuid: string, bbox: number[]): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/data/${uuid}?bbox=[${bbox}]`,
      this.httpOptions
    );
  }

  getEditableFeatures(uuid: string, bbox: number[]): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/data/${uuid}?nature=edit&bbox=[${bbox}]`,
      this.httpOptions
    );
  }

  getEditableFeaturesStructure(uuid: string): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/structure/${uuid}`,
      this.httpOptions
    );
  }

  get(uuid: string, gid: any, nature: string = "edit"): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/data/${uuid}?gid=${gid}&nature=${nature}`,
      this.httpOptions
    );
  }

  getDraft(uuid: string, gid: any, nature: string = "edit"): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/data/draft/${uuid}?gid=${gid}&nature=${nature}`,
      this.httpOptions
    );
  }

  getAllDrafts(uuid: string): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/data/draft/${uuid}`,
      this.httpOptions
    );
  }

  add(uuid: string, data: any): Observable<any> {
    return this.http.post(
      `${this.catalogueUrl}/api/data/${uuid}`,
      data,
      this.httpOptions
    );
  }

  addDraft(uuid: string, data: any): Observable<any> {
    return this.http.post(
      `${this.catalogueUrl}/api/data/draft/${uuid}`,
      data,
      this.httpOptions
    );
  }

  modify(uuid: string, data: any): Observable<any> {
    return this.http.patch(
      `${this.catalogueUrl}/api/data/${uuid}`,
      data,
      this.httpOptions
    );
  }

  delete(uuid: string, gids: string): Observable<any> {
    return this.http.delete(
      `${this.catalogueUrl}/api/data/${uuid}/${gids}`,
      this.httpOptions
    );
  }

  deleteDraft(uuid: string, gids: string): Observable<any> {
    return this.http.delete(
      `${this.catalogueUrl}/api/data/draft/${uuid}/${gids}`,
      this.httpOptions
    );
  }

  getUserDefinedEnum(uuid: string): Observable<any> {
    return this.http.get(
      `${this.catalogueUrl}/api/enum/${uuid}`,
      this.httpOptions
    );
  }
}
