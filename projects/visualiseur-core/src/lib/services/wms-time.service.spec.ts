import { TestBed } from '@angular/core/testing';

import { WmsTimeService } from './wms-time.service';

describe('WmsTimeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WmsTimeService = TestBed.get(WmsTimeService);
    expect(service).toBeTruthy();
  });
});
