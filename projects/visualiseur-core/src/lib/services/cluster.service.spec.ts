import { TestBed } from '@angular/core/testing';

import { ClusterStyleService } from './cluster-style.service';

describe('ClusterStyleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClusterStyleService = TestBed.get(ClusterStyleService);
    expect(service).toBeTruthy();
  });
});
