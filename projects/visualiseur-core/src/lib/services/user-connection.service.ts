import { EnvCoreService } from './../env.core.service';
import { Injectable, Inject } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import { ConnexionConfig } from '../models/connexion-config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserConnectionService {

  private userConfig$: ReplaySubject<ConnexionConfig> = new ReplaySubject<ConnexionConfig>(1);
  httpOptions = {
    withCredentials: true
  };

  constructor(
    private http: HttpClient,
    private environment: EnvCoreService
  ) {
    this.http.jsonp<ConnexionConfig>(`${environment.serverUrl}carto/config`, 'callback').subscribe(config => {
      this.setUserConfig(config);
    });
  }

  getUserConfig(): Observable<ConnexionConfig> {
    return this.userConfig$;
  }
  setUserConfig(newConfig) {
    this.userConfig$.next(newConfig);
  }

  reloadConfig() {
    return new Promise<ConnexionConfig>((resolve, reject) => {
      this.http.jsonp<ConnexionConfig>(`${this.environment.serverUrl}carto/config`, 'callback').subscribe(config => {
        this.setUserConfig(config);
        resolve(config);
      });
    });
  }

  loadCatalogue(config: ConnexionConfig) {
    return this.http.jsonp(`${config.URLS.catalogue}/prodige/connect`, 'callback').toPromise();
  }
}
