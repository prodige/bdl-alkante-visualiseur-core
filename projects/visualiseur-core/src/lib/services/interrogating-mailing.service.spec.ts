import { TestBed } from '@angular/core/testing';

import { InterrogatingMailingService } from './interrogating-mailing.service';

describe('InterrogatingMailingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterrogatingMailingService = TestBed.get(InterrogatingMailingService);
    expect(service).toBeTruthy();
  });
});
