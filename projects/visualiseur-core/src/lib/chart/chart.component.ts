import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
  ViewEncapsulation
} from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import Chart, {FontSpec} from 'chart.js/auto';
import ChartDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'alk-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChartComponent implements OnInit, AfterViewInit, OnDestroy {


  @ViewChildren('appChart') queryAppChart: QueryList<ElementRef>;
  @ViewChild('appChart') appChart: ElementRef;

  @ViewChildren('containerID') queryContainerID: QueryList<ElementRef>;
  @ViewChild('containerID') containerID: ElementRef;

  protected chartIsInit = false;

  protected querySubscription: Subscription;

  protected colorDefault = ['#165f10', '#58b311', '#d6df0b', '#80290f', '#0f2c80'];

  protected datas = {};

  protected labelOnChartParam = {
    color: '#FFF',
    anchor: 'center',
    align: 'end',
    offset: 4,
    clamp: true,
    formatter: (value) => this.formatDataLabel(value),
    font: {
      weight: 'bold',
    },

  };

  public height = 250;
  public width = 800;

  public legendId = null;

  @Input() chartData: {[key: string]: number};
  @Input() type: 'pie' | 'bar';
  @Input() labels: {[key: string]: string};
  @Input() columns: number|string;
  @Input() colors: string[];
  @Input() withAnimation = true;
  @Input() legendClass: string = null;
  @Input() withValueOnChart = false;
  @Input() font: FontSpec = {family: 'Roboto, sans-serif', size: 16};
  @Input() locale: string = null;
  @Input() angleValueOnChart = 0;

  constructor() {
    this.legendId = Math.ceil(Math.random() * (9999 - 1000) + 1000);
  }

  protected htmlLegendPlugin = {
    id: 'htmlLegend',
    afterUpdate:  (chart) => {
      const ul = this.getOrCreateLegendList();

      // Remove old legend items
      while (ul.firstChild) {
        ul.firstChild.remove();
      }

      // Reuse the built-in legendItems generator
      const items = chart.options.plugins.legend.labels.generateLabels(chart);

      items.forEach(item => {
        const li = document.createElement('li');

        if (this.legendClass){
          li.classList.add(this.legendClass);
        }
        else{
          li.style.alignItems = 'center';
          li.style.cursor = 'pointer';
          li.style.display = 'flex';
          li.style.flexDirection = 'row';
          li.style.marginLeft = '10px';
          li.style.marginTop = '15px';
        }


        li.onclick = () => {
          const {type} = chart.config;
          if (type === 'pie' || type === 'doughnut') {
            // Pie and doughnut charts only have a single dataset and visibility is per item
            chart.toggleDataVisibility(item.index);
          } else {
            chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
          }
          chart.update();
        };

        // Color box
        const boxSpan = document.createElement('span');

        boxSpan.style.background = item.fillStyle;
        boxSpan.style.borderColor = item.strokeStyle;
        boxSpan.style.borderWidth = item.lineWidth + 'px';

        if (!this.legendClass){
          boxSpan.style.display = 'inline-block';
          boxSpan.style.height = '20px';
          boxSpan.style.marginRight = '10px';
          boxSpan.style.minWidth = '20px';
        }


        // Text
        const textContainer = document.createElement('p');
        textContainer.style.color = item.color;
        textContainer.style.textDecoration = item.hidden ? 'line-through' : '';

        if (!this.legendClass){
          textContainer.style.margin = '0';
          textContainer.style.padding = '0';
        }

        const text = document.createTextNode(this.capitalizeFirstLetter(item.text));
        textContainer.appendChild(text);

        li.appendChild(boxSpan);
        li.appendChild(textContainer);
        ul.appendChild(li);
      });
    }
  };

  ngOnInit(): void {

    // TODO: utilisation d'un service pour construire les différent type de graphs
    Chart.defaults.font = Object.assign(Chart.defaults.font, this.font);
    Chart.defaults.set('plugins.datalabels', {
      font: Chart.defaults.font
    });


    if ( this.locale) {
      Chart.defaults.locale = this.locale;
    }


    if (!this.colors || !(this.colors instanceof Array) || this.colors.length === 0) {
      this.colors = this.colorDefault;
    }

    // Remplacement des index par des labels
    if (this.labels) {
      Object.entries(this.chartData).forEach(([key, value] ) => {
        this.datas[this.labels[key] ? this.labels[key] : key] = value;
      });
    } else {
      this.datas = this.chartData;
    }
  }

  ngAfterViewInit() {
    if (this.columns && typeof this.columns === 'string') {
      this.columns = parseInt(this.columns, 10);
    }

    if (this.appChart.nativeElement && this.containerID.nativeElement) {
      this.buildChart(this.appChart.nativeElement);
      this.chartIsInit = true;
    }

    this.querySubscription = forkJoin([this.queryAppChart.changes, this.queryContainerID.changes]).subscribe({
      next: () => {
        this.buildChart(this.appChart.nativeElement);
        this.chartIsInit = true;
      }
    });

  }

  buildChart(ctx: HTMLCanvasElement) {
    switch (this.type) {
      case 'bar':
        this.barChart(ctx);
        break;
      case 'pie':
        this.pieChart(ctx);
        break;
      default:
        console.error(this.type, ' not implemented !!!');
    }
  }

  protected buildDataSet(datas) {
    const retour = [];
    const newColor = [];

    Object.entries(datas).forEach(([key, value], idx) => {
      if (value) {
        retour.push( {
          label: this.capitalizeFirstLetter(key),
          data: [value],
          backgroundColor: this.colors[idx % this.colors.length]
        });

        if (this.colors[idx]) {
          newColor.push(this.colors[idx]);
        }
      }
    });

    this.colors = newColor;
    return retour;
  }

  protected filterData(datas) {
    const retour = {};
    const newColor = [];
    Object.entries(datas).forEach(([key, value], idx) => {
      if (value) {
        retour[this.capitalizeFirstLetter(key)] = value;

        if (this.colors[idx]) {
          newColor.push(this.colors[idx]);
        }
      }
    });
    this.colors = newColor;
    return retour;
  }

  protected barChart(ctx: HTMLCanvasElement) {
    const ctx2d = ctx.getContext('2d');

    const dataSets = this.buildDataSet(this.datas);

    this.labelOnChartParam.align = 'end';
    this.labelOnChartParam.anchor = 'end';
    this.labelOnChartParam.offset = -6;
    this.labelOnChartParam.color = 'black';
    this.labelOnChartParam.font.weight = 'normal';
    let maxY = this.calculAxeX();

    if (this.angleValueOnChart){
      maxY = this.calculAxeX(0.25);
      this.labelOnChartParam = Object.assign({rotation: this.angleValueOnChart}, this.labelOnChartParam);
    }




    const chartRef = new Chart(ctx2d, {
      plugins: [ChartDataLabels, this.htmlLegendPlugin],
      type: 'bar',
      data: {
        labels: [''],
        datasets: dataSets
      },
      options: {
        scales: {
          y: {
            max: maxY,
            ticks: {
              // forces step size to be 50 units
              stepSize: Math.ceil(maxY / 3)
            }
          },

        },
        animation: this.withAnimation,
        indexAxis: 'x',
        maintainAspectRatio: false,
        plugins: {
          datalabels: this.withValueOnChart ? this.labelOnChartParam : null,
          htmlLegend: {
            // ID of the container to put the legend in
            containerID: 'legend-container',
          },
          legend: {
            display: false,
            position: 'left'
          },
        },

        responsive: true,

      },

    }, );
  }

  protected pieChart(ctx: HTMLCanvasElement) {
    const ctx2d = ctx.getContext('2d');

    const dataFilter = this.filterData(this.datas);

    const labels = Object.keys(dataFilter);
    const values = Object.values(dataFilter);

    const chatRef = new Chart(ctx2d, {
      plugins: [ChartDataLabels],
      type: 'pie',
      data: {
        labels,
        datasets: [{
          label: '',
          data: values,
          backgroundColor: this.colors
        }]
      },
      options: {
        animation: this.withAnimation,
        indexAxis: 'x',
        maintainAspectRatio: false,
        plugins: {
          datalabels: this.withValueOnChart ? this.labelOnChartParam : null,
          htmlLegend: {
            // ID of the container to put the legend in
            containerID: 'legend-container-' + this.legendId
          },
          legend: {
            display: true,
            position: 'right',

          },

        },
        responsive: true,
      },
    });
  }

  protected getOrCreateLegendList() {
    const legendContainer = this.containerID.nativeElement;
    let listContainer = legendContainer.querySelector('ul');

    if (!listContainer) {
      listContainer = document.createElement('ul');

      listContainer.style.display = 'grid';
      listContainer.style.gridTemplateColumns = Array(this.columns ? this.columns : 2).fill('auto').join(' ');

      legendContainer.appendChild(listContainer);
    }

    return listContainer;
  }

  protected capitalizeFirstLetter(text: string): string {
    return text.charAt(0).toUpperCase() + text.toLowerCase().slice(1);
  }

  /**
   *
   * Calcul d'une marge pour l'axe X supérieur de 10% par rapport à la valeur max de chartData
   * @protected
   */
  protected calculAxeX(marge: number = 0.1): number{
    const max = Math.max(...Object.values(this.chartData));

    return ( Math.ceil( ( max * marge) ) + max);
  }

  protected formatDataLabel(value: number){
    return value.toLocaleString(Chart.defaults.locale);
  }

  ngOnDestroy(): void {
    if (this.querySubscription) {
      this.querySubscription.unsubscribe();
    }
  }
}
