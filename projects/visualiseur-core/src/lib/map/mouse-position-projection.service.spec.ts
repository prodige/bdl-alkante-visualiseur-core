import { TestBed } from '@angular/core/testing';

import { MousePositionProjectionService } from './mouse-position-projection.service';

describe('MousePositionProjectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MousePositionProjectionService = TestBed.get(MousePositionProjectionService);
    expect(service).toBeTruthy();
  });
});
