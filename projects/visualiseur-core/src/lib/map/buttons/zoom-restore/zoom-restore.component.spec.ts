import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomRestoreComponent } from './zoom-restore.component';

describe('ZoomRestoreComponent', () => {
  let component: ZoomRestoreComponent;
  let fixture: ComponentFixture<ZoomRestoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomRestoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomRestoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
