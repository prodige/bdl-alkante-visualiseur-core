import { Component, OnInit, HostListener, Host } from '@angular/core';
import { MapService } from '../../map.service';
import { MapIdService } from '../../map-id.service';

import Map from 'ol/Map';
import '../../extensions/ol.extensions';

@Component({
  selector: 'alk-buttons-zoom-restore',
  templateUrl: './zoom-restore.component.html',
  styleUrls: ['./zoom-restore.component.scss']
})
export class ZoomRestoreComponent implements OnInit {

  map: Map;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService
  ) { }

  ngOnInit() {
    // Get the current map
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');
  }

  @HostListener('click', ['$event'])
  click(event: MouseEvent): void {
    this.map.getView().fit( this.map.getView().getProjection().getExtent() );
    this.mapService.setMapFitEvent(true);
  }
}
