import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../../visualiseur-core.service';
import { MapService } from '../../map.service';
import { InterrogatingTooltipComponent } from '../../tools/interrogating-tools/interrogating-tooltip/interrogating-tooltip.component';

@Component({
  selector: 'alk-buttons-interrogating-tooltip',
  templateUrl: './interrogating-tooltip.component.html',
  styleUrls: ['./interrogating-tooltip.component.scss']
})
export class InterrogatingTooltipButtonComponent implements OnInit {


  isTooltipInterrogationEnabled = false;
  interrogatingTooltipTool: NgbModalRef;

  constructor(
    private modalService: NgbModal,
    private mapService: MapService,
    private coreService: VisualiseurCoreService,
    ) { }

  ngOnInit() {
    this.coreService.getMapTool().subscribe(tool => {
      if (tool === 'toggleTooltip') {
        this.isTooltipInterrogationEnabled = true;
      } else {
        this.isTooltipInterrogationEnabled =  false;
        if (this.interrogatingTooltipTool) {
          this.interrogatingTooltipTool.close();
        }
      }
    });
  }

  interrogationTooltip() {
    this.isTooltipInterrogationEnabled = !this.isTooltipInterrogationEnabled;
    this.modalService.dismissAll();

    if (this.isTooltipInterrogationEnabled === true) {
      this.coreService.setMapTool('toggleTooltip');
    } else {
      this.coreService.setMapTool('');
    }
  }

}
