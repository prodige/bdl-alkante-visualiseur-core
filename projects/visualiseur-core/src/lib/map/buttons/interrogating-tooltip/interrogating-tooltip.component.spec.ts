import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingTooltipButtonComponent } from './interrogating-tooltip.component';

describe('InterrogatingTooltipButtonComponent', () => {
  let component: InterrogatingTooltipButtonComponent;
  let fixture: ComponentFixture<InterrogatingTooltipButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingTooltipButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingTooltipButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
