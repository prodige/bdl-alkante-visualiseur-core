import { Component, OnInit, HostListener, Host } from '@angular/core';
import { MapService } from '../../map.service';
import { MapIdService } from '../../map-id.service';
import Map from 'ol/Map';

@Component({
  selector: 'alk-buttons-full-screen',
  templateUrl: './full-screen.component.html',
  styleUrls: ['./full-screen.component.scss']
})
export class ButtonFullScreenComponent implements OnInit {

  map: Map
  mapElem: Element;
  isfullscreen: Boolean;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService
  ) { }

  ngOnInit() {
    // Get the current map
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');
    this.mapElem = this.map.getTargetElement();
    this.isfullscreen = false;
  }

  @HostListener('click', ['$event'])
  click(event: MouseEvent): void {
    
    if (this.isfullscreen) {
      const docWithBrowsersExitFunctions = document as Document & {
        mozCancelFullScreen(): Promise<void>;
        webkitExitFullscreen(): Promise<void>;
        msExitFullscreen(): Promise<void>;
      };
      if (docWithBrowsersExitFunctions.exitFullscreen) {
        docWithBrowsersExitFunctions.exitFullscreen();
      } else if (docWithBrowsersExitFunctions.mozCancelFullScreen) { /* Firefox */
        docWithBrowsersExitFunctions.mozCancelFullScreen();
      } else if (docWithBrowsersExitFunctions.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        docWithBrowsersExitFunctions.webkitExitFullscreen();
      } else if (docWithBrowsersExitFunctions.msExitFullscreen) { /* IE/Edge */
        docWithBrowsersExitFunctions.msExitFullscreen();
      }
      this.isfullscreen = false;

    } else {
     
      // Trigger fullscreen
      const docElmWithBrowsersFullScreenFunctions = this.mapElem as HTMLElement & {
        mozRequestFullScreen(): Promise<void>;
        webkitRequestFullscreen(): Promise<void>;
        msRequestFullscreen(): Promise<void>;
      };
    
      if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
        docElmWithBrowsersFullScreenFunctions.requestFullscreen();
      } else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) { /* Firefox */
        docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen();
      } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen();
      } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) { /* IE/Edge */
        docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();
      }
      this.isfullscreen = true;
    }
  }

}
