import {
  Component,
  OnInit,
  ElementRef,
  Input,
  ViewChild,
  TemplateRef,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { VisualiseurCoreService } from "../../../visualiseur-core.service";
import { MapService } from "../../map.service";
import { InterrogatingModalComponent } from "../../tools/interrogating-tools/interrogating-modal/interrogating-modal.component";
import { MapIdService } from "../../map-id.service";

@Component({
  selector: "alk-buttons-mobile-interrogation",
  templateUrl: "./mobile-interrogation.component.html",
  styleUrls: ["./mobile-interrogation.component.scss"],
})
export class MobileInterrogationComponent implements OnInit {
  @ViewChild("interrogationModalToolName", {read: null, static: true})
  interrogationModalToolName: ElementRef;

  constructor(
    private elementRef: ElementRef,
    private modalService: NgbModal,
    private coreService: VisualiseurCoreService,
    private mapIdService: MapIdService
  ) {}

  ngOnInit() {}

  openInterrogation() {
    this.coreService.setMapTool("infoPoint");
    this.modalService.dismissAll();
    const modalRef = this.modalService.open(InterrogatingModalComponent, {
      size: "sm",
      windowClass: "window-tool",
      container: "#map-container",
      backdropClass: "backdrop-tool",
      keyboard: false,
    });
    modalRef.result.then(
      (close) => {
        this.coreService.setMapTool("");
      },
      (dismiss) => {}
    );
    if (this.elementRef) {
      this.coreService.setInterrogatingOption("mobile");
      // modalRef.componentInstance.setToolName('Information ponctuelle'); // A changer
      modalRef.componentInstance.setToolName(
        this.interrogationModalToolName.nativeElement.textContent
      ); // A changer
      if (modalRef.componentInstance.setMapId) {
        modalRef.componentInstance.setMapId(
          this.mapIdService.getId() || "main"
        );
      }
    }
  }
}
