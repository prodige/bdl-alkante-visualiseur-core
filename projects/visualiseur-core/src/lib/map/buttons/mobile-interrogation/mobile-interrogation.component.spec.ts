import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileInterrogationComponent } from './mobile-interrogation.component';

describe('MobileInterrogationComponent', () => {
  let component: MobileInterrogationComponent;
  let fixture: ComponentFixture<MobileInterrogationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileInterrogationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileInterrogationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
