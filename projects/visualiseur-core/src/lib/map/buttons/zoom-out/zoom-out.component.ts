import { Component, OnInit, HostListener, Host, OnDestroy } from '@angular/core';
import { MapService } from '../../map.service';
import { MapIdService } from '../../map-id.service';

import Map from 'ol/Map';
import '../../extensions/ol.extensions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alk-buttons-zoom-out',
  templateUrl: './zoom-out.component.html',
  styleUrls: ['./zoom-out.component.scss']
})
export class ZoomOutComponent implements OnInit, OnDestroy {

  disabled = false;

  private map: Map;

  private mapResolution: number;
  private mapResolutions: Array<number> = [];

  private mapResolutionSubscription: Subscription;
  private mapResolutionsSubscription: Subscription;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.mapResolutionsSubscription = this.mapService.getResolutions(this.mapIdService.getId() || 'main').subscribe(
      (resolutions) => this.mapResolutions = resolutions
    );
    this.mapResolutionSubscription = this.mapService.getMapResolution(this.mapIdService.getId() || 'main').subscribe(
      (resolution) => this.mapResolution = resolution
    );
  }

  ngOnDestroy() {
    if ( this.mapResolutionsSubscription ) {
      this.mapResolutionsSubscription.unsubscribe();
    }
    if ( this.mapResolutionSubscription ) {
      this.mapResolutionSubscription.unsubscribe();
    }
  }

  @HostListener('click', ['$event'])
  click(event: MouseEvent): void {
    const mapView = this.map.getView();
    if ( this.mapResolutions && this.mapResolutions.length ) {
      const currentResolution = mapView.getResolution();

      const nearestResolution = this.findNearestResolution(this.mapResolutions, currentResolution);
      const indexOfMapResolution = this.mapResolutions.indexOf(nearestResolution);
      if ( indexOfMapResolution !== -1 && indexOfMapResolution < ( this.mapResolutions.length - 1 ) ) {
        mapView.setResolution( this.mapResolutions[ indexOfMapResolution + 1 ] );
        mapView.setZoom( mapView.getZoomForResolution(this.mapResolutions[ indexOfMapResolution + 1 ]) );
        this.mapService.setMapResolution(this.mapIdService, this.mapResolutions[ indexOfMapResolution + 1 ]);
      }
    } else {
      mapView.animate({
        zoom: mapView.getZoom() - 1,
        duration: 300
      });
    }
  }

  private findNearestResolution(resolutions: Array<number>, resolution: number): number {
    return resolutions.reduce(
      (prev, curr) => Math.abs(curr - resolution) < Math.abs(prev - resolution) ? curr : prev
    );
  }

}
