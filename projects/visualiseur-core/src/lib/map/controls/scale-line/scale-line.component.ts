import {
  Component,
  OnInit,
  Host,
  Optional,
  Input,
  OnDestroy,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { MapService } from "../../map.service";
import { MapIdService } from "../../map-id.service";

import Map from "ol/Map";
import { ScaleLine, Control } from "ol/control";
// import { getScaleForResolution } from 'ol/control/ScaleLine';
import MapEvent from "ol/MapEvent";

@Component({
  selector: "alk-controls-scale-line",
  templateUrl: "./scale-line.component.html",
  styleUrls: ["./scale-line.component.scss"],
})
export class ScaleLineComponent implements OnInit, OnDestroy {
  @ViewChild("scaleLine", {read: null, static: true}) scaleLine: ElementRef;

  map: Map;
  mapId: string;

  @Input() type: "default" | "nauticalMile" = "default";
  scaleLineControl: ScaleLine;
  scaleBarSteps = 4;
  scaleBarText = true;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService
  ) {}

  ngOnInit() {
    // Récupération de la carte
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;

      if (this.map) {
        this.map.removeControl(this.scaleLineControl);

        if (this.type === "default") {
          this.scaleLineControl = new ScaleLine({
            className: "scale-line",
            target: this.scaleLine.nativeElement,
            units: "metric",
          });
        } else {
          this.scaleLineControl = new ScaleLine({
            className: "scale-line",
            target: this.scaleLine.nativeElement,
            units: "nautical",
            bar: false,
            steps: this.scaleBarSteps,
            text: this.scaleBarText,
          });
        }

        this.scaleLineControl.setMap(this.map);
        this.map.addControl(this.scaleLineControl);
      }
    });
  }

  ngOnDestroy() {
    if (this.scaleLineControl) {
      this.map.removeControl(this.scaleLineControl);
    }
  }
}
