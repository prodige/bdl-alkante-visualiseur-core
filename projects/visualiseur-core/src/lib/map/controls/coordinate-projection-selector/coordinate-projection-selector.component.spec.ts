import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordinateProjectionSelectorComponent } from './coordinate-projection-selector.component';

describe('CoordinateProjectionSelectorComponent', () => {
  let component: CoordinateProjectionSelectorComponent;
  let fixture: ComponentFixture<CoordinateProjectionSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoordinateProjectionSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordinateProjectionSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
