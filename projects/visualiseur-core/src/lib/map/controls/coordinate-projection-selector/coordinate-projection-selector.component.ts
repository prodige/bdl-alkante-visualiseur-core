import { EnvCoreService } from './../../../env.core.service';
import { Subscription } from 'rxjs';
import { MousePositionProjectionService } from './../../mouse-position-projection.service';
import { Component, Input, OnInit, Host, Optional, OnDestroy, Injectable, Inject } from '@angular/core';

import Projection from 'ol/proj/Projection';
import '../../extensions/ol.extensions';

@Injectable()
@Component({
  selector: 'alk-controls-coordinate-projection-selector',
  templateUrl: './coordinate-projection-selector.component.html',
  styleUrls: ['./coordinate-projection-selector.component.scss']
})
export class CoordinateProjectionSelectorComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  /** Map id */
  @Input() mapId: string;
  /** Map projection */
  @Input() selectedProjectionKey: Projection | string = 'EPSG:4326';
  selectedProjectionLabel: string|null = '';
  mouseProjectionSubscription: Subscription;

  projectionsList: Array<{key: string; label: string; proj: string; bounds: [number, number, number, number]; }> = [];

  constructor(
    private mousePositionProjectionService: MousePositionProjectionService,
    private environment: EnvCoreService
  ) { }

  ngOnInit() {
    // this.selectedProjectionKey = environment.defaultMousePositionProjection;
    this.projectionsList = Object.values(this.environment.availableProjections);
    // this.selectedProjectionLabel = this.projectionsList.find( projection =>
    //   projection.key === this.selectedProjectionKey
    // ).label;

    this.mouseProjectionSubscription = this.mousePositionProjectionService.get().subscribe(
      projection => {
        if ( typeof projection === 'string' ) {
          if ( projection !== this.selectedProjectionKey ) {
            this.selectedProjectionKey = projection;
            this.selectedProjectionLabel = this.projectionsList.find( projectionElem =>
              projectionElem.key === projection
            ).label;
          }
        } else {
          if ( projection.getCode() !== this.selectedProjectionKey ) {
            this.selectedProjectionKey = projection.getCode();
            this.selectedProjectionLabel = this.projectionsList.find( projectionElem =>
              projectionElem.key === projection.getCode()
            ).label;
          }
        }
      }
    );
  }

  ngOnDestroy() {
    if ( this.mouseProjectionSubscription  ) {
      this.mouseProjectionSubscription.unsubscribe();
    }
  }

  selectProjection(event: MouseEvent, projectionKey: string) {
    this.selectedProjectionKey = projectionKey;
    this.selectedProjectionLabel = this.projectionsList.find( projection =>
      projection.key === this.selectedProjectionKey
    ).label;

    this.mousePositionProjectionService.set(this.selectedProjectionKey);
  }
}
