import { Component, Input, OnInit, Host, Optional } from '@angular/core';

import { MapService } from '../../map.service';
import { MapIdService } from '../../map-id.service';
import { MousePositionProjectionService } from '../../mouse-position-projection.service';

import Map from 'ol/Map';
import {transform, ProjectionLike} from 'ol/proj';

import '../../extensions/ol.extensions';

/**
 * Add a control to the map
 * The control can be set inside the map (using parent id) or outside (using a mapId attribute)
 * @example
 * <!-- Display a control inside a map -->
 * <alk-ol-map>
 *   <alk-controls-mouse-position></alk-controls-mouse-position>
 * </alk-ol-map>
 * <!-- Display a control outside a map -->
 * <alk-controls-mouse-position mapId="map"></alk-controls-mouse-position>
 */
@Component({
  selector: 'alk-controls-mouse-position',
  templateUrl: './mouse-position.component.html',
  styleUrls: ['./mouse-position.component.scss']
})
export class MousePositionComponent implements OnInit {

  /** Map id */
  @Input() longLat = true;

  private mapProjectionCoordinates: number[];
  private mapProjection: ProjectionLike;
  visibleCoordinates: number[];
  private visualisationProjection: ProjectionLike;

  /**
   */
  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
    private mousePositionProjectionService: MousePositionProjectionService
  ) { }

  ngOnInit() {
    // Get the current map or get map by id
    const map: Map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.mapProjection = map.getView().getProjection();
    this.visualisationProjection = map.getView().getProjection();
    this.mapProjectionCoordinates = map.getView().getCenter();

    map.on('pointermove', (evt: any) => {
      this.mapProjectionCoordinates = evt.coordinate;
      if ( this.mapProjection && this.visualisationProjection ) {
        const bProjectionIs = ['EPSG:4326', 'EPSG:4171'].indexOf(this.visualisationProjection as string) !== -1 ? true : false;
        const coordinatesToFormat = transform(this.mapProjectionCoordinates, this.mapProjection, this.visualisationProjection);
        if (this.longLat === true) {
          this.visibleCoordinates = [
            parseFloat(coordinatesToFormat[0].toFixed( bProjectionIs ? 5 : 2)),
            parseFloat(coordinatesToFormat[1].toFixed( bProjectionIs ? 5 : 2))
          ];
        } else {
          this.visibleCoordinates = [
            parseFloat(coordinatesToFormat[1].toFixed( bProjectionIs ? 5 : 2)),
            parseFloat(coordinatesToFormat[0].toFixed( bProjectionIs ? 5 : 2))
          ];
        }
      }
    });

    this.mousePositionProjectionService.get().subscribe(
      (projectionKey: ProjectionLike) => {
        this.visualisationProjection = projectionKey;
        if ( this.mapProjectionCoordinates && this.mapProjection ) {
          const bProjectionIs = ['EPSG:4326', 'EPSG:4171'].indexOf(this.visualisationProjection as string) !== -1 ? true : false;
          const coordinatesToFormat = transform(this.mapProjectionCoordinates, this.mapProjection, this.visualisationProjection);
          if (this.longLat === true) {
            this.visibleCoordinates = [
              parseFloat(coordinatesToFormat[0].toFixed( bProjectionIs ? 5 : 2)),
              parseFloat(coordinatesToFormat[1].toFixed( bProjectionIs ? 5 : 2))
            ];
          } else {
            this.visibleCoordinates = [
              parseFloat(coordinatesToFormat[1].toFixed( bProjectionIs ? 5 : 2)),
              parseFloat(coordinatesToFormat[0].toFixed( bProjectionIs ? 5 : 2))
            ];
          }
        }
      }
    );
  }
}
