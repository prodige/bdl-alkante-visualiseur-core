import {MapIdService} from './map-id.service';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClientJsonpModule} from '@angular/common/http';

import {OlMapComponent} from './ol-map/ol-map.component';
import {ScaleLineComponent} from './controls/scale-line/scale-line.component';
import {MousePositionComponent} from './controls/mouse-position/mouse-position.component';
import {ButtonFullScreenComponent as ControlsFullScreenComponent} from './buttons/full-screen/full-screen.component' ;
import {MapService} from './map.service';
import {ZoomInComponent} from './buttons/zoom-in/zoom-in.component';
import {ZoomOutComponent} from './buttons/zoom-out/zoom-out.component';
import {ZoomRestoreComponent} from './buttons/zoom-restore/zoom-restore.component';
import {ButtonFullScreenComponent} from './buttons/full-screen/full-screen.component';
import {CoordinateProjectionSelectorComponent} from './controls/coordinate-projection-selector/coordinate-projection-selector.component';
import {MeasuringToolsComponent} from './tools/measuring-tools/measuring-tools.component';
import {DrawingPolygonComponent} from './tools/annotating-tools/drawing-polygon/drawing-polygon.component';
import {DrawingLinestringComponent} from './tools/annotating-tools/drawing-linestring/drawing-linestring.component';
import {DrawingCircleComponent} from './tools/annotating-tools/drawing-circle/drawing-circle.component';
import {DrawingSymbolComponent} from './tools/annotating-tools/drawing-symbol/drawing-symbol.component';
import {InsertTextComponent} from './tools/annotating-tools/insert-text/insert-text.component';
import {BuffersComponent} from './tools/annotating-tools/buffers/buffers.component';
import {EditDrawsComponent} from './tools/annotating-tools/edit-draws/edit-draws.component';
import {ModifyDrawComponent} from './tools/annotating-tools/edit-draws/modify-draw/modify-draw.component';
import {MoveDrawComponent} from './tools/annotating-tools/edit-draws/move-draw/move-draw.component';
import {RemoveDrawComponent} from './tools/annotating-tools/edit-draws/remove-draw/remove-draw.component';
import {IndeterminateDirective} from './directives/indeterminate.directive';
import {InterrogatingPointComponent} from './tools/interrogating-tools/interrogating-point/interrogating-point.component';
import {InterrogatingCircleComponent} from './tools/interrogating-tools/interrogating-circle/interrogating-circle.component';
import {InterrogatingRectangleComponent} from './tools/interrogating-tools/interrogating-rectangle/interrogating-rectangle.component';
import {InterrogatingPolygonComponent} from './tools/interrogating-tools/interrogating-polygon/interrogating-polygon.component';
import {InterrogatingTooltipComponent} from './tools/interrogating-tools/interrogating-tooltip/interrogating-tooltip.component';
import {
  InterrogatingGrantedRequestComponent
} from './tools/interrogating-tools/interrogating-granted-request/interrogating-granted-request.component';
import {StyleDrawsComponent} from './tools/annotating-tools/style-draws/style-draws.component';
import {MobileInterrogationComponent} from './buttons/mobile-interrogation/mobile-interrogation.component';
import {InterrogatingTooltipButtonComponent} from './buttons/interrogating-tooltip/interrogating-tooltip.component';
import {EnvCoreServiceProvider} from '../env.core.service.provider';
import {ExportDrawsComponent} from './tools/annotating-tools/export-draws/export-draws.component';
import {AttributesDrawComponent} from './tools/annotating-tools/edit-draws/attributes-draw/attributes-draw.component';
import {AlertConfirmPromptModalComponent} from '../modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component';
import {LayerLegendLiteComponent} from '../layers-legend/layer-legend-lite/layer-legend-lite.component';
import {LegendFilterPipe} from '../pipes/legend-filter.pipe';

@NgModule({
  declarations: [
    OlMapComponent,
    ScaleLineComponent,
    MousePositionComponent,
    ControlsFullScreenComponent,
    ZoomInComponent,
    ZoomOutComponent,
    ZoomRestoreComponent,
    ButtonFullScreenComponent,
    CoordinateProjectionSelectorComponent,
    MeasuringToolsComponent,
    DrawingPolygonComponent,
    DrawingLinestringComponent,
    DrawingCircleComponent,
    DrawingSymbolComponent,
    InsertTextComponent,
    BuffersComponent,
    IndeterminateDirective,
    EditDrawsComponent,
    ModifyDrawComponent,
    MoveDrawComponent,
    RemoveDrawComponent,
    InterrogatingPointComponent,
    InterrogatingCircleComponent,
    InterrogatingRectangleComponent,
    InterrogatingPolygonComponent,
    InterrogatingTooltipComponent,
    InterrogatingGrantedRequestComponent,
    StyleDrawsComponent,
    MobileInterrogationComponent,
    ExportDrawsComponent,
    AttributesDrawComponent,
    AlertConfirmPromptModalComponent,
    InterrogatingTooltipButtonComponent,
    LayerLegendLiteComponent,
    LegendFilterPipe
  ],
  providers: [
    MapService,
    MapIdService,
    EnvCoreServiceProvider,
    LegendFilterPipe
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
  ],
  exports: [
    OlMapComponent,
    ScaleLineComponent,
    MousePositionComponent,
    ControlsFullScreenComponent,
    ZoomInComponent,
    ZoomOutComponent,
    ZoomRestoreComponent,
    ButtonFullScreenComponent,
    CoordinateProjectionSelectorComponent,
    MeasuringToolsComponent,
    DrawingPolygonComponent,
    DrawingLinestringComponent,
    DrawingCircleComponent,
    DrawingSymbolComponent,
    InsertTextComponent,
    BuffersComponent,
    IndeterminateDirective,
    EditDrawsComponent,
    ModifyDrawComponent,
    MoveDrawComponent,
    RemoveDrawComponent,
    InterrogatingPointComponent,
    InterrogatingCircleComponent,
    InterrogatingRectangleComponent,
    InterrogatingPolygonComponent,
    InterrogatingTooltipComponent,
    InterrogatingGrantedRequestComponent,
    StyleDrawsComponent,
    MobileInterrogationComponent,
    ExportDrawsComponent,
    AttributesDrawComponent,
    AlertConfirmPromptModalComponent,
    InterrogatingTooltipButtonComponent,
    LayerLegendLiteComponent,
    LegendFilterPipe
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class MapModule {
}

