import { EnvCoreService } from './../../env.core.service';
import { DrawLayerService } from './../../services/draw-layer.service';
import {
  ViewEncapsulation,
  OnDestroy,
  Injectable,
  Inject,
} from '@angular/core';

// import { fromLonLat } from 'ol/proj';
import { MapService } from '../map.service';
import { MapIdService } from '../map-id.service';
import { MousePositionProjectionService } from '../mouse-position-projection.service';

import Map from 'ol/Map';
import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { get as getProjection, fromLonLat } from 'ol/proj';
import '../extensions/ol.extensions';
import { VisualiseurCoreService } from '../../visualiseur-core.service';
import { Subscription, timer } from 'rxjs';
import Projection from 'ol/proj/Projection';

import LayerGroup from 'ol/layer/Group';
import VectorLayer from 'ol/layer/Vector';
import ImageLayer from 'ol/layer/Image';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { Geometry } from 'ol/geom';

import VectorSource from 'ol/source/Vector';

const INCHES_PER_UNIT = {
  m: 39.37,
  dd: 4374754,
};
const DOTS_PER_INCH = 72;
const MAP_SCALES = [
  1, 2500, 5000, 10000, 25000, 50000, 100000, 250000, 500000, 1000000, 2000000,
  4000000, 10000000,
];

const CGU_STORAGE_KEY = 'cgu_storage_key';
/**
 * Map Component: load and display a map
 * @example
 * <alk-ol-map id="map"></alk-ol-map>
 */
@Injectable()
@Component({
  selector: 'alk-ol-map',
  templateUrl: './ol-map.component.html',
  // Include ol style as global
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    '../../../../../../node_modules/ol/ol.css',
    '../../../../../../node_modules/ol-ext/dist/ol-ext.css',
    './ol-map.component.scss',
  ],
  providers: [MapIdService, MousePositionProjectionService],
})
export class OlMapComponent implements OnInit, OnDestroy {
  contextSubscription: Subscription;

  /** Map id */
  public id: string | null = 'visualiseur';

  /**
   * The initial center for the view. The coordinate system for the center is specified with the projection option.
   * Layer sources will not be fetched if this is not set, but the center can be set later with #setCenter.
   */
  public center: [number, number];

  /**
   * Rotation constraint. false means no constraint. true means no constraint, but snap to zero near zero.
   * A number constrains the rotation to that number of values. For example, 4 will constrain the rotation to 0, 90, 180, and 270 degrees.
   */
  @Input() constrainRotation: boolean | number = true;

  /**
   * Enable rotation. If false, a rotation constraint that always sets the rotation to zero is used.
   * The constrainRotation option has no effect if enableRotation is false
   */
  @Input() enableRotation: boolean | null = true;

  /**
   * The extent that constrains the center, in other words, center cannot be set outside this extent.
   * [minx, miny, maxx, maxy].
   */
  @Input() extent: [number, number, number, number];

  /**
   * The maximum resolution used to determine the resolution constraint.
   * It is used together with minResolution (or maxZoom) and zoomFactor.
   * If unspecified it is calculated in such a way that the projection's validity extent fits in a 256x256 px tile.
   * If the projection is Spherical Mercator (the default) then maxResolution defaults to
   * 40075016.68557849 / 256 = 156543.03392804097.
   */
  @Input() maxResolution: number;

  /**
   * The minimum resolution used to determine the resolution constraint.
   * It is used together with maxResolution (or minZoom) and zoomFactor.
   * If unspecified it is calculated assuming 29 zoom levels (with a factor of 2).
   * If the projection is Spherical Mercator (the default) then minResolution defaults to
   * 40075016.68557849 / 256 / Math.pow(2, 28) = 0.0005831682455839253.
   */
  @Input() minResolution: number;

  /**
   * The maximum zoom level used to determine the resolution constraint.
   * It is used together with minZoom (or maxResolution) and zoomFactor.
   * Note that if minResolution is also provided, it is given precedence over maxZoom.
   */
  @Input() maxZoom: number | null = 28;

  /**
   * The minimum zoom level used to determine the resolution constraint.
   * It is used together with maxZoom (or minResolution) and zoomFactor.
   * Note that if maxResolution is also provided, it is given precedence over minZoom.
   */
  @Input() minZoom: number | null = 0;

  /**
   * The projection. The default is Spherical Mercator.
   */
  @Input() projection: Projection | string = 'EPSG:3857';

  /**
   * The initial resolution for the view. The units are projection units per pixel (e.g. meters per pixel).
   * An alternative to setting this is to set zoom.
   * Layer sources will not be fetched if neither this nor zoom are defined, but they can be set later with #setZoom or #setResolution.
   */
  @Input() resolution: number;

  /**
   * Resolutions to determine the resolution constraint.
   * If set the maxResolution, minResolution, minZoom, maxZoom, and zoomFactor options are ignored.
   */
  @Input() resolutions: Array<number>;

  /**
   * The initial rotation for the view in radians (positive rotation clockwise, 0 means North)
   */
  @Input() rotation: number | null = 0;

  /**
   * Only used if resolution is not defined. Zoom level used to calculate the initial resolution for the view.
   * The initial resolution is determined using the #constrainResolution method.
   */
  public zoom: number | null;

  /**
   * The zoom factor used to determine the resolution constraint.
   */
  @Input() zoomFactor: number | null = 2;

  map: Map;

  // Mise en base standalone
  public resolutionChange = false;

  private baseLayersSubscription: Subscription;
  private updatedRootLayerGroupSubscription: Subscription;
  private drawLayerSubscription: Subscription;
  private bufferLayerGroupSubscription: Subscription;
  private rootLayerGroup: LayerGroup;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
    private mousePositionProjectionService: MousePositionProjectionService,
    private elementRef: ElementRef<HTMLElement>,
    private coreService: VisualiseurCoreService,
    private drawLayerService: DrawLayerService,
    private environment: EnvCoreService,
    private router: Router,
    private localStorage: LocalStorageService
  ) {}

  /**
   * Create map on Init
   */
  ngOnInit() {
    // On récupère les données de base (zoom, centre, ...)
    this.zoom = this.mapService.mapZoom;
    this.center = this.mapService.mapCenter;

    this.baseLayersSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup: LayerGroup) => {
        this.mapService.rootLayerGroup = rootLayerGroup;
        timer(0)
          .toPromise()
          .then((_) => {
            this.mapService.rootLayerGroup
              .getLayersArray()
              .forEach(
                (layer: VectorLayer<VectorSource<Geometry>>, idxLayer) => {
                  if (layer.get('baseLayer') !== true) {
                    if (!layer.get('imageDataUrl')) {
                      this.mapService.generateLegendForLayer(
                        layer,
                        this.mapService.getLayerPath(
                          this.mapService.rootLayerGroup,
                          0,
                          layer
                        ),
                        this.id
                      );
                    }
                    if (
                      this.map &&
                      this.map
                        .getLayers()
                        .getArray()
                        .filter((layer) => {
                          return layer === layer;
                        }).length === 0
                    ) {
                      // this.map.addLayer(layer);
                      layer.setMap(this.map);
                    } else if (this.map) {
                      this.map.removeLayer(layer);
                      // this.map.addLayer(layer);
                      layer.setMap(this.map);
                    }
                  }
                }
              );
            this.mapService.verifyLayerGroupVisibility(
              this.mapService.rootLayerGroup,
              this.id
            );
          });
      });

    this.updatedRootLayerGroupSubscription = this.coreService
      .getUpdatedRootLayerGroup()
      .subscribe((updated) => {
        this.mapService.verifyLayerGroupVisibility(
          this.mapService.rootLayerGroup,
          this.id
        );
      });

    this.drawLayerSubscription = this.drawLayerService
      .getDrawLayer()
      .subscribe((drawLayer: VectorLayer<VectorSource<Geometry>>) => {
        if (drawLayer) {
          this.mapService.verifyLayerGroupVisibility(
            this.rootLayerGroup,
            this.id
          );
          this.mapService.generateLegendForLayer(
            drawLayer,
            this.mapService.getLayerPath(this.rootLayerGroup, 0, drawLayer),
            this.id
          );

          drawLayer
            .getSource()
            .on(
              ['addfeature', 'changefeature', 'removefeature', 'clear'],
              (...args) => {
                this.mapService.generateLegendForLayer(
                  drawLayer,
                  this.mapService.getLayerPath(
                    this.rootLayerGroup,
                    0,
                    drawLayer
                  ),
                  this.id
                );
              }
            );
        }
      });

    this.bufferLayerGroupSubscription = this.drawLayerService
      .getBufferLayerGroup()
      .subscribe((bufferLayerGroup) => {
        this.mapService.verifyLayerGroupVisibility(
          this.mapService.rootLayerGroup,
          this.id
        );

        bufferLayerGroup.getLayersArray().forEach((layer: any) => {
          this.mapService.generateLegendForLayer(
            layer,
            this.mapService.getLayerPath(
              this.mapService.rootLayerGroup,
              0,
              layer
            ),
            this.id
          );
        });
      });
    if (this.elementRef.nativeElement.id) {
      this.id = this.elementRef.nativeElement.id;
    } else {
      this.elementRef.nativeElement.id = this.id;
    }
    // Initialisation de la projection à utiliser en fonction du fichier environnement
    this.mousePositionProjectionService.set(
      this.environment.defaultMousePositionProjection
    );

    // Enregistrement de l'id de la carte
    this.mapIdService.setId(this.id);

    let mapResolutions = [];

    this.contextSubscription = this.coreService
      .getContext()
      .subscribe((context) => {
        if (!context) {
          return false;
        }

        const isChecked = this.localStorage.retrieve(CGU_STORAGE_KEY);

        if (
          !this.coreService.getCguRead() &&
          context.properties.extension.MapCGUDisplay &&
          context.properties.extension.MapCGU &&
          !isChecked
        ) {
          this.router.navigate([`/cgu-display`]);
        }

        const regexProj = new RegExp(/^urn:ogc:def:crs:(.*)$/i);
        const contextMapProjection = regexProj.exec(
          context.crs.properties.name
        );
        const proj = getProjection(contextMapProjection[1]);
        proj.setExtent(context.properties.bbox);
        this.coreService.setMapProjection(proj);

        this.mousePositionProjectionService.set(contextMapProjection[1]);

        // ajustement de la constrainte d'emprise à partir du ratio du viewport de la carte
        const initialViewPortSize = [
          this.elementRef.nativeElement.clientWidth,
          this.elementRef.nativeElement.clientHeight,
        ];
        const extentConstraint = context.properties.extension.maxBbox
          ? this.mapService.adjustExtentToSize(
              context.properties.extension.maxBbox,
              initialViewPortSize,
              0.05
            )
          : context.properties.bbox
          ? this.mapService.adjustExtentToSize(
              context.properties.bbox,
              initialViewPortSize,
              0.05
            )
          : getProjection(contextMapProjection[1]).getExtent();

        mapResolutions = [];
        MAP_SCALES.forEach((scale: number) => {
          mapResolutions.push(
            scale /
              this.getPixelsPerProjectionUnits(
                getProjection(contextMapProjection[1])
              )
          );
        });

        if (context.properties.extension.MapMinscaleDenominator) {
          const scaleMin = context.properties.extension.MapMinscaleDenominator;
          this.minResolution =
            scaleMin /
            this.getPixelsPerProjectionUnits(
              getProjection(contextMapProjection[1])
            );
        }

        if (context.properties.extension.MapMaxscaleDenominator) {
          const scaleMax = context.properties.extension.MapMaxscaleDenominator;
          this.maxResolution =
            scaleMax /
            this.getPixelsPerProjectionUnits(
              getProjection(contextMapProjection[1])
            );
        }

        // Creation de la carte
        this.map = this.mapService.getMap(
          this.id ? this.id : this.mapIdService.getId(),
          true,
          {
            center: this.center
              ? fromLonLat(this.center, contextMapProjection[1])
              : [0, 0],
            constrainRotation: this.constrainRotation,
            enableRotation: this.enableRotation,
            extent: extentConstraint,
            // constrainOnlyCenter: true,
            smoothExtentConstraint: true,
            maxResolution: this.maxResolution,
            maxZoom: this.maxZoom,
            minResolution: this.minResolution,
            minZoom: this.minZoom,
            projection: contextMapProjection[1],
            resolution: this.resolution,
            // resolutions: this.resolutions || mapResolutions,
            rotation: this.rotation,
            zoom: this.zoom,
            zoomFactor: this.zoomFactor,
          }
        );

        // Ajout de comportements
        this.map.getView().on('change:resolution', (...args) => {
          this.resolutionChange = true;
        });

        this.map.on('moveend', (...args) => {
          if (this.resolutionChange === true) {
            this.resolutionChange = false;
          }
          if (this.mapService.rootLayerGroup) {
            this.mapService.verifyLayerGroupVisibility(
              this.mapService.rootLayerGroup,
              this.id
            );
          }
        });

        const size = this.map.getSize();

        if (context.properties.bbox && size && size.length >= 2) {
          const adjustedExtent = this.mapService.adjustExtentToSize(
            context.properties.bbox,
            this.map.getSize(),
            0.05
          );
          this.map.getView().fit(adjustedExtent, { size: this.map.getSize() });
          this.mapService.setMapFitEvent(true);
        }
        // If id is not defined place the map in the component element itself
        if (!this.id) {
          this.id = 'visualiseur';
        }

        const mainMapContainer = this.elementRef.nativeElement.children['main-map-container'];
        if (mainMapContainer){
          this.map.setTarget( mainMapContainer);
        }

        this.mapService.emitMapComplete(this.id);
      });
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.baseLayersSubscription) {
      this.baseLayersSubscription.unsubscribe();
    }
    if (this.updatedRootLayerGroupSubscription) {
      this.updatedRootLayerGroupSubscription.unsubscribe();
    }
    if (this.drawLayerSubscription) {
      this.drawLayerSubscription.unsubscribe();
    }
    if (this.bufferLayerGroupSubscription) {
      this.bufferLayerGroupSubscription.unsubscribe();
    }

    this.mapService.deleteMap(this.id);

    // pour éviter de charger 2 fois le context quand on change de fenêtre (exemple sur geo-dae)
    // this.coreService.setContext(null);
  }

  private getPixelsPerProjectionUnits(p: Projection): number {
    // meter unit crs as default
    let res = INCHES_PER_UNIT.m * DOTS_PER_INCH;
    if (p.getUnits() == 'degrees') {
      res = INCHES_PER_UNIT.dd * DOTS_PER_INCH;
    }
    return res;
  }
}
