import { EnvCoreService } from "./../../env.core.service";
import { HttpClient } from "@angular/common/http";
import { Injectable, Inject } from "@angular/core";
import Feature from "ol/Feature";
import GML32 from "ol/format/GML32";
import { ProjectionLike } from "ol/proj";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";
import Projection from "ol/proj/Projection";
import { Observable, of, throwError, ReplaySubject, forkJoin } from "rxjs";
import {
  map,
  catchError,
  timeout,
  tap,
  debounceTime,
  distinctUntilChanged,
  finalize,
} from "rxjs/operators";
import { GeoJSONFeature } from "ol/format/GeoJSON";
import VectorSource from "ol/source/Vector";
import Map from "ol/Map";
import * as momentImported from "moment";
import { Style, Stroke, Fill } from "ol/style";
import CircleStyle from "ol/style/Circle";
import { ImageWMS, TileWMS, TileImage } from "ol/source";
import { UrlProxyService } from "../../services/url-proxy.service";
import { WMSGetFeatureInfo, GeoJSON } from "ol/format";

import booleanWithin from "@turf/boolean-within";
import booleanContains from "@turf/boolean-contains";
import flatten from "turf-flatten";
import { FilterLayer } from "../../models/filter-layer";

import AnimatedCluster from "ol-ext/layer/AnimatedCluster";
import Cluster from "ol/source/Cluster";
import { Geometry } from "ol/geom";
import ImageSource from "ol/source/Image";

import TileSource from "ol/source/Tile";

export interface LayerQueryFieldType {
  FieldAlias: string;
  FieldDateFormat: string;
  FieldName: string;
  FieldTooltip?: string;
  FieldQuery?: string;
  FieldSorting?: string;
  FieldType: string;
  FieldURLConfig: string;
  structure?: any;
}

export interface DataFeatureType {
  feature: GeoJSONFeature;
  [key: string]: any;
}

export interface LayerDataResponseType {
  layerName: string;
  layer: ImageLayer<any> | TileLayer<any> | VectorLayer<any>;
  layerQueryFields: Array<LayerQueryFieldType>;
  data: Array<DataFeatureType>;
  nbResults: number;
  isExternalLayer?: boolean;
  hasFeature?: boolean;
}

export interface InterrogationReturnType {
  featureOfRequest: Feature<any>;
  layersResults: Array<LayerDataResponseType>;
  totalResults: number;
}

@Injectable({
  providedIn: "root",
})
export class InterrogatingService {
  resultSearching$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  tooltipResultSearching$: ReplaySubject<boolean> = new ReplaySubject<boolean>(
    1
  );
  resultOfInterrogation: InterrogationReturnType;
  resultOfInterrogation$: ReplaySubject<InterrogationReturnType> =
    new ReplaySubject<InterrogationReturnType>(1);

  resultOfInterrogationTooltip: InterrogationReturnType;
  resultOfInterrogationTooltip$: ReplaySubject<InterrogationReturnType> =
    new ReplaySubject<InterrogationReturnType>(1);

  /* Variables pour le résultat d'interrogation */
  currentLayerResults: LayerDataResponseType;
  currentLayerResults$: ReplaySubject<LayerDataResponseType> =
    new ReplaySubject<LayerDataResponseType>(1);
  selectedDataFeatures: Array<DataFeatureType> = [];
  resultPage = 1;
  resultSort: { [columnName: string]: "ASC" | "DESC" } = {};
  maxPage: number;
  nbElemsByPage = 10;
  paginationRange: Array<number> = [];
  completePageRange: Array<number> = [];
  eventOffset: ReplaySubject<Array<number>> = new ReplaySubject<Array<number>>(
    1
  );

  sortedOrFilteredData: Array<DataFeatureType> = [];

  searchTxt = "";
  searchTxt$: ReplaySubject<string> = new ReplaySubject<string>(null);
  searching = false;

  interrogationDisplayLayer: VectorLayer<VectorSource<Geometry>> = null;
  interrogationDisplayLayer$: ReplaySubject<
    VectorLayer<VectorSource<Geometry>>
  > = new ReplaySubject<VectorLayer<VectorSource<Geometry>>>(1);

  resultBottomBoxClosed = true;
  resultBottomBoxClosed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(
    1
  );
  resultModalBoxClosed = true;
  resultModalBoxClosed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  resultMobileBoxClosed = true;
  resultMobileBoxClosed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(
    1
  );
  resultCurrentBox: "modal" | "bottom" = "modal";

  public moment = momentImported;
  spinner$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  pageNum$: ReplaySubject<number> = new ReplaySubject<number>(1);

  constructor(
    public http: HttpClient,
    public urlProxy: UrlProxyService,
    public environment: EnvCoreService
  ) {
    // Recherche textuelle
    this.searchTxt$
      .pipe(
        tap((searchText) => {
          if (searchText) {
            this.searching = true;
          }
        }),
        debounceTime(300),
        distinctUntilChanged(),
        finalize(() => {
          this.searching = false;
        })
      )
      .subscribe((searchText) => {
        this.searchTxt = searchText;
        if (searchText) {
          let tmpFullLayerResultData = this.currentLayerResults.data.slice();

          // On applique la recherche textuelle sur le tableau
          tmpFullLayerResultData = tmpFullLayerResultData.filter((data) => {
            return Object.entries(data).some(([key, val]) => {
              // Si la donnée a au moins l'une de ses valeurs qui contient ce qui est recherché
              if (
                this.currentLayerResults.layerQueryFields.some(
                  (layerQueryField) => {
                    return (
                      layerQueryField.FieldName === key &&
                      layerQueryField.FieldType === "TXT"
                    );
                  }
                ) &&
                val !== undefined &&
                val !== null &&
                ((typeof val === "string" &&
                  val
                    .toLocaleLowerCase()
                    .includes(searchText.toLocaleLowerCase())) ||
                  (typeof val === "number" &&
                    ("" + val).includes(
                      searchText.replace(",", ".").toLocaleLowerCase()
                    )))
              ) {
                return true;
              } else {
                return false;
              }
            });
          });
          this.sortedOrFilteredData = tmpFullLayerResultData;

          // On trie le résultat de la recherche
          this.sortData();

          this.resultPage = 1;
          this.maxPage = Math.ceil(
            this.sortedOrFilteredData.length / this.nbElemsByPage
          );
          this.paginationRange = this.range();
          this.recalculateCompletePageRange();
        } else {
          // on rétabli la liste complète des résultats
          this.sortedOrFilteredData = this.currentLayerResults.data.slice();
          this.resultPage = 1;
          this.maxPage = Math.ceil(
            this.sortedOrFilteredData.length / this.nbElemsByPage
          );
          this.paginationRange = this.range();
          this.recalculateCompletePageRange();
        }
      });
  }

  selectLayerResults(layerResults): void {
    this.selectedDataFeatures = [];
    this.currentLayerResults = layerResults;
    this.setCurrentLayerResults(layerResults);

    this.sortedOrFilteredData = this.currentLayerResults.data.slice();
    this.resultPage = 1;
    this.searchTxt = "";
    this.maxPage = Math.ceil(
      this.sortedOrFilteredData.length / this.nbElemsByPage
    );
    this.paginationRange = this.range();
    this.recalculateCompletePageRange();
  }

  toggleAll(): void {
    if (this.selectedDataFeatures.length === this.sortedOrFilteredData.length) {
      this.selectedDataFeatures.length = 0;
    } else {
      this.selectedDataFeatures = this.sortedOrFilteredData.slice();
    }
  }

  toggleDataFeatureSelection(dataFeature: DataFeatureType): void {
    if (this.selectedDataFeatures.includes(dataFeature)) {
      this.selectedDataFeatures = this.selectedDataFeatures.filter(
        (selectedFeature) => selectedFeature !== dataFeature
      );
    } else {
      this.selectedDataFeatures.push(dataFeature);
    }
  }

  searchText(searchTxt: string): void {
    this.searchTxt$.next(searchTxt);
  }

  changePageResult(numPage: string): void {
    this.resultPage = parseInt(numPage, 10);
    this.paginationRange = this.range();
    this.pageNum$.next(this.resultPage);
  }

  getPageNum$(): ReplaySubject<number> {
    return this.pageNum$;
  }

  sortOn(sortOn: LayerQueryFieldType): void {
    this.resultSort =
      this.resultSort[sortOn.FieldName] === "ASC"
        ? { [sortOn.FieldName]: "DESC" }
        : { [sortOn.FieldName]: "ASC" };
    this.sortData();
  }

  range(): Array<number> {
    const tabRange = [];
    const start = Math.max(Math.min(this.maxPage - 6, this.resultPage - 2), 1);
    tabRange.length =
      start === this.maxPage - 6
        ? Math.min(this.maxPage, 7)
        : this.resultPage < 4
        ? Math.min(this.maxPage, 7)
        : this.resultPage < 5
        ? Math.min(this.maxPage, 6)
        : Math.min(this.maxPage, 5);
    return tabRange
      .fill(null)
      .map((_, idx) => start + idx)
      .filter((val) => val <= this.maxPage && val >= 1);
  }

  recalculateCompletePageRange(): void {
    const pageRange = [];
    pageRange.length = this.maxPage;
    this.completePageRange = pageRange.fill(null).map((_, idx) => 1 + idx);
  }

  sortData(): void {
    if (!Object.keys(this.resultSort).length) {
      return;
    }
    const isInAsc = Object.values(this.resultSort)[0] === "ASC" ? 1 : -1;
    const sortedColumnName = Object.keys(this.resultSort)[0];
    const queryField = this.currentLayerResults.layerQueryFields.find(
      (layerQueryField) => {
        return layerQueryField.FieldName === sortedColumnName;
      }
    );
    if (!queryField) {
      return;
    }

    // Si on est sur du type texte

    this.sortedOrFilteredData = this.sortedOrFilteredData.sort(
      (dataA, dataB) => {
        const valueDataA = dataA[sortedColumnName] as string;
        const valueDataB = dataB[sortedColumnName] as string;
        if (
          queryField.FieldType === "TXT" &&
          // On test si les 2 valeurs sont numérique
          Number.isFinite(Number(valueDataA)) &&
          Number.isFinite(Number(valueDataB)) &&
          !Number.isNaN(Number(valueDataA)) &&
          !Number.isNaN(Number(valueDataB))
        ) {
          const numValueDataA = Number(valueDataA);
          const numValueDataB = Number(valueDataB);
          return (
            isInAsc *
            (numValueDataA === numValueDataB
              ? 0
              : numValueDataA < numValueDataB
              ? -1
              : 1)
          );
        }

        // Si l'une des 2 valeurs est null
        if (!valueDataA || !valueDataB) {
          return (
            isInAsc *
            (!valueDataA && !valueDataB
              ? 0
              : valueDataA && !valueDataB
              ? -1
              : 1)
          );
        }

        if (queryField.FieldType === "Date") {
          if (!queryField.FieldDateFormat) {
            return 0;
          }

          let dateDataA =
            queryField.FieldType === "Date" &&
            this.moment(valueDataA, queryField.FieldDateFormat)
              ? this.moment(valueDataA, queryField.FieldDateFormat)
              : null;

          let dateDataB =
            queryField.FieldType === "Date" &&
            this.moment(valueDataB, queryField.FieldDateFormat)
              ? this.moment(valueDataB, queryField.FieldDateFormat)
              : null;
          dateDataA = this.moment.isMoment(dateDataA) ? dateDataA : null;
          dateDataB = this.moment.isMoment(dateDataB) ? dateDataB : null;

          // Si l'une des 2 dates est null
          if (!dateDataA || !dateDataB) {
            return (
              isInAsc *
              (!dateDataA && !dateDataB ? 0 : dateDataA && !dateDataB ? -1 : 1)
            );
          } else {
            return dateDataA.isAfter(dateDataB)
              ? 1
              : dateDataA.isBefore(dateDataB)
              ? -1
              : 0;
          }
        }

        return (
          isInAsc *
          (queryField.FieldType === "TXT"
            ? // Comparaison de 2 chaines
              valueDataA.localeCompare(valueDataB)
            : 0)
        );
      }
    );
  }

  getInterrogationDisplayLayer(
    usedMap: Map
  ): Observable<VectorLayer<VectorSource<Geometry>>> {
    if (this.interrogationDisplayLayer === null) {
      const interrogationDisplaySource = new VectorSource({
        wrapX: false,
        features: [],
      });
      this.interrogationDisplayLayer = new VectorLayer({
        source: interrogationDisplaySource,
        zIndex: Infinity,
        map: usedMap,
        style: new Style({
          stroke: new Stroke({
            color: "orange",
            width: 3,
          }),
          fill: new Fill({
            color: "rgba(0, 0, 255, 0.3)",
          }),
          image: new CircleStyle({
            radius: 5,
            fill: new Fill({
              color: "orange",
            }),
          }),
        }),
      });
      this.interrogationDisplayLayer$.next(this.interrogationDisplayLayer);
    }
    return this.interrogationDisplayLayer$;
  }

  setEventOffset(offset: Array<number>): void {
    this.eventOffset.next(offset);
  }

  getEventOffset(): Observable<Array<number>> {
    return this.eventOffset;
  }

  setCurrentLayerResults(data: LayerDataResponseType) {
    this.currentLayerResults$.next(data);
  }

  getCurrentLayerResults(): Observable<LayerDataResponseType> {
    return this.currentLayerResults$;
  }

  setResultSearching(bool: boolean) {
    this.resultSearching$.next(bool);
  }

  getResultSearching(): Observable<boolean> {
    return this.resultSearching$;
  }

  setTooltipResultSearching(bool: boolean) {
    this.tooltipResultSearching$.next(bool);
  }

  getTooltipResultSearching(): Observable<boolean> {
    return this.tooltipResultSearching$;
  }

  setSpinner(bool: boolean) {
    this.spinner$.next(bool);
  }

  getSpinner(): Observable<boolean> {
    return this.spinner$;
  }

  getResultBottomBox(): Observable<boolean> {
    return this.resultBottomBoxClosed$;
  }

  closeResultBottomBox(): void {
    this.resultBottomBoxClosed = true;
    this.resultBottomBoxClosed$.next(true);
  }

  openResultBottomBox(): void {
    this.resultBottomBoxClosed = false;
    this.resultBottomBoxClosed$.next(false);
  }

  getResultModalBox(): Observable<boolean> {
    return this.resultModalBoxClosed$;
  }

  closeResultModalBox(): void {
    this.resultModalBoxClosed = true;
    this.resultModalBoxClosed$.next(true);
  }

  openResultModalBox(): void {
    this.resultModalBoxClosed = false;
    this.resultModalBoxClosed$.next(false);
  }

  setResultCurrentBox(currentBox: "modal" | "bottom"): void {
    this.resultCurrentBox = currentBox;
  }

  getResultCurrentBox(): "modal" | "bottom" {
    return this.resultCurrentBox;
  }

  closeResultCurrentBox(): void {
    if (this.resultCurrentBox === "modal") {
      this.closeResultModalBox();
    } else {
      this.closeResultBottomBox();
    }
  }

  openResultCurrentBox(): void {
    if (this.resultCurrentBox === "modal") {
      this.openResultModalBox();
    } else {
      this.openResultBottomBox();
    }
  }

  getResultMobileBox(): Observable<boolean> {
    return this.resultMobileBoxClosed$;
  }

  closeResultMobileBox(): void {
    this.resultMobileBoxClosed = true;
    this.resultMobileBoxClosed$.next(true);
  }

  openResultMobileCurrentBox(): void {
    this.resultMobileBoxClosed = false;
    this.resultMobileBoxClosed$.next(false);
  }

  resetResultOfInterrogationTooltip(): void {
    return this.resultOfInterrogationTooltip$.next(null);
  }

  setResultOfInterrogationTooltip(
    resultOfInterrogation: InterrogationReturnType
  ): void {
    this.resultOfInterrogationTooltip = resultOfInterrogation;
    this.resultOfInterrogationTooltip$.next(resultOfInterrogation);
  }

  getResultOfInterrogationTooltip(): Observable<InterrogationReturnType> {
    return this.resultOfInterrogationTooltip$;
  }

  resetResultOfInterrogation(): void {
    if (this.interrogationDisplayLayer) {
      this.interrogationDisplayLayer.getSource().clear();
      this.interrogationDisplayLayer.setMap(null);
      this.interrogationDisplayLayer = null;
      this.interrogationDisplayLayer$.next(null);
    }
    this.changeResultOfInterrogation();
    return this.resultOfInterrogation$.next(null);
  }

  public changeResultOfInterrogation(): void {
    this.selectedDataFeatures = [];
    if (this.resultOfInterrogation) {
      if (
        this.resultOfInterrogation.layersResults &&
        this.resultOfInterrogation.layersResults.length
      ) {
        this.currentLayerResults = this.resultOfInterrogation.layersResults[0];
        this.sortedOrFilteredData = this.currentLayerResults.data.slice();
        this.resultPage = 1;
        this.searchTxt = "";
        this.maxPage = Math.ceil(
          this.sortedOrFilteredData.length / this.nbElemsByPage
        );
        this.paginationRange = this.range();
        this.recalculateCompletePageRange();
        this.resultSort = {};
      } else {
        this.currentLayerResults = null;
      }
    } else {
      this.currentLayerResults = null;
    }
  }

  setResultOfInterrogation(
    resultOfInterrogation: InterrogationReturnType
  ): void {
    this.resultOfInterrogation = resultOfInterrogation;
    this.resultOfInterrogation$.next(resultOfInterrogation);
  }

  getResultOfInterrogation(): Observable<InterrogationReturnType> {
    return this.resultOfInterrogation$;
  }

  postGrantedInterrogationRequest(
    mapHrefPath: string,
    layer:
      | TileLayer<TileSource>
      | VectorLayer<VectorSource<Geometry>>
      | ImageLayer<any>,
    layerId: string,
    maxAnswers: number,
    request: string,
    startindex: number = null,
    sortRequest: string = ""
  ): Observable<InterrogationReturnType> {
    const bodyPost =
      '<?xml version="1.0" encoding="UTF-8"?>' +
      '<GetFeature service="WFS" version="2.0.0" outputformat="geojson"' +
      (startindex !== null && isFinite(startindex)
        ? ' startindex="' + startindex + '"'
        : "") +
      (maxAnswers !== null ? ' count="' + maxAnswers + '"' : "") +
      ' xmlns="http://www.opengis.net/wfs/2.0"' +
      ' xmlns:cw="http://www.someserver.com/cw"' +
      ' xmlns:fes="http://www.opengis.net/ogc/1.1"' +
      ' xmlns:gml="http://www.opengis.net/gml/3.2" >' +
      `<Query typeNames="${layerId}" styles="default">` +
      `${sortRequest ? sortRequest : ""}` +
      `<Filter>${request}</Filter>` +
      "</Query>" +
      "</GetFeature>";

    const layerQuery = layer;
    let layerQueryUrl;
    try {
      layerQueryUrl = (layerQuery.getSource() as any).getUrl();
    } catch (exe) {
      console.error(layerQuery);
    }
    return this.http.post(layerQueryUrl, bodyPost).pipe(
      map((layerResponseData: any) => {
        layerResponseData.layer = layer;
        // Traitement de la données ( pour chaque couche, on match les données avec celles du contexte)
        let totalResults = 0;
        const validResults = [];
        let layerResults = 0;
        // Tableau dans lequel on stockera les données récupèrées par la requête
        const layerData = [];
        // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
        const layerQueryFields = layerResponseData.layer.get("LayerQuery")
          .Fields as Array<LayerQueryFieldType>;
        // On extrait de la liste des colonnes précédentes les clés
        const layerQueryFieldsNames = layerQueryFields.map(
          (layerQueryField) => layerQueryField.FieldName
        );

        // On parcours enfin les réponse pour ne garder que les données qui nous intéresse
        layerResponseData.features.forEach((featureInResponse) => {
          totalResults++;
          layerResults++;
          const datas = Object.entries(
            featureInResponse.properties as { [key: string]: any }
          )
            .filter(([key, value]) => layerQueryFieldsNames.includes(key))
            .reduce((accum, [key, value]) => {
              accum[key] = value;
              return accum;
            }, {});

          layerData.push(
            Object.assign({}, datas, {
              feature: featureInResponse,
            })
          );
        });
        // Pour chaque couche, on assemble les données nécessaire à l'affichage de tableau de résultats
        validResults.push({
          layerName: layerResponseData.layer.get("name"),
          layer: layerResponseData.layer,
          layerQueryFields,
          data: layerData,
          nbResults: layerResults,
        });

        this.resultOfInterrogation = {
          featureOfRequest: null,
          layersResults: validResults,
          totalResults,
        };
        this.setResultSearching(false);
        this.changeResultOfInterrogation();
        if (totalResults > 0) {
          this.openResultCurrentBox();
        }
        this.resultOfInterrogation$.next(this.resultOfInterrogation);
        return this.resultOfInterrogation;
      }),
      catchError((err) => throwError(err))
    );
  }

  /**
   * 02/08/2022
   * TODO: Fonction à revoir, les return throwError mettent fin la fontion du foreach et non de postInterrogationTooltip
   * @param layers
   * @param feature
   * @param projection
   * @returns
   */
  postInterrogationTooltip(
    layers: Array<VectorLayer<VectorSource<Geometry>>>,
    feature: Feature<any>,
    projection: Projection
  ): Observable<InterrogationReturnType> {
    if (layers && layers.length > 0) {
      const tasks$ = [];
      layers.forEach((layer) => {
        // Si la couche layer n'est pas sélectionnable
        const layerQuery = layer.get("LayerQuery");
        if (
          !(
            layer &&
            layer.getVisible() &&
            layer.get("visibleAtScale") &&
            layer.get("visibleAtExtent")
          )
        ) {
          return throwError(
            `La couche ${layer.get("name")} n'est actuellement pas visible`
          );
        }
        if (!layerQuery || !layerQuery.Fields || layerQuery.Fields.length < 1) {
          return throwError(
            `La couche ${layer.get("name")} n'est pas interrogeable`
          );
        }
        let layerQueryUrl;
        try {
          layerQueryUrl = (layer.getSource() as any).getUrl();
        } catch (exe) {
          console.error(layer);
          return throwError(
            `Nous n'avons pas pu récupérer l'url de la source de la couche ${layer.get(
              "name"
            )}`
          );
        }
        if (!layerQueryUrl) {
          // console.warn('layer info %s pas d URL', layer.get('name'));
          // console.warn('layer query url', layerQueryUrl );
          return throwError(`Url d'appel du layer est vide`);
        }
        const featureToGML = new GML32({
          featureNS: "ms",
          featureType: layer.get("layerName"),
          srsName: projection.getCode(),
          surface: false,
          curve: false,
          multiCurve: false,
          multiSurface: false,
        });
        const featureInGmlFormat = featureToGML.writeFeatures([feature]);
        const extractionRegex = /<Polygon.+Polygon>/;
        const matchArray = featureInGmlFormat.match(extractionRegex);

        if (!matchArray || !matchArray.length) {
          console.error(
            "GML invalide",
            featureInGmlFormat,
            extractionRegex,
            matchArray
          );
          return throwError(`Invalid GML`);
        }
        const xmlPayload = `<?xml version="1.0" encoding="UTF-8"?>
          <GetFeature service="WFS" version="2.0.0" outputformat="geojson" xmlns="http://www.opengis.net/wfs/2.0"
            xmlns:cw="http://www.someserver.com/cw" xmlns:fes="http://www.opengis.net/ogc/1.1" xmlns:gml="http://www.opengis.net/gml/3.2"
          >
            <Query typeNames="${layer.get("layerName")}" styles="default">
              <Filter>
                <Intersects>
                  <PropertyName>msGeometry</PropertyName>
                  ${matchArray[0]}
                </Intersects>
              </Filter>
            </Query>
          </GetFeature>`;

        tasks$.push(
          this.http.post(layerQueryUrl, xmlPayload).pipe(
            timeout(2000),
            map((response: any) => {
              response.layer = layer;
              return response;
            }),
            catchError((err) => of(null))
          )
        );
      });

      return forkJoin(tasks$).pipe(
        map((results) => {
          // Traitement de la données ( pour chaque couche, on match les données avec celles du contexte)
          let totalResults = 0;
          const validResults = (results as any)
            .filter(
              (result) =>
                result !== null &&
                result.layer &&
                result.features &&
                result.features.length
            )
            .map((layerResponseData: any) => {
              let layerResults = 0;
              // Tableau dans lequel on stockera les données récupèrées par la requête
              const layerData = [];
              // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
              const layerQueryFields = (
                layerResponseData.layer.get("LayerQuery")
                  .Fields as Array<LayerQueryFieldType>
              ).filter((field) => {
                return field.FieldTooltip;
              });
              // On extrait de la liste des colonnes précédentes les clés
              const layerQueryFieldsNames = layerQueryFields.map(
                (layerQueryField) => layerQueryField.FieldName
              );

              // On parcours enfin les réponse pour ne garder que les données qui nous intéresse
              layerResponseData.features.forEach((featureInResponse) => {
                totalResults++;
                layerResults++;
                const datas = Object.entries(
                  featureInResponse.properties as { [key: string]: any }
                )
                  .filter(([key, value]) => layerQueryFieldsNames.includes(key))
                  .reduce((accum, [key, value]) => {
                    accum[key] = value;
                    return accum;
                  }, {});

                layerData.push(
                  Object.assign({}, datas, {
                    feature: featureInResponse,
                  })
                );
              });

              // Pour chaque couche, on assemble les données nécessaire à l'affichage de tableau de résultats
              return {
                layerName: layerResponseData.layer.get("name"),
                layer: layerResponseData.layer,
                layerQueryFields,
                data: layerData,
                nbResults: layerResults,
              };
            });

          // On trie les resultats par nombre de réponses
          validResults.sort((validResultA, validResultB) => {
            return validResultA.nbResults > validResultB.nbResults
              ? -1
              : validResultA.nbResults < validResultB.nbResults
              ? 1
              : 0;
          });

          this.resultOfInterrogationTooltip = {
            featureOfRequest: feature,
            layersResults: validResults,
            totalResults,
          };
          this.resultOfInterrogationTooltip$.next(
            this.resultOfInterrogationTooltip
          );
          return this.resultOfInterrogationTooltip;
        }),
        catchError((err) => throwError(err))
      );
    } else {
      return throwError("Pas de layers");
    }
  }

  /** */
  generateFilterXml(
    filterLayer: FilterLayer[],
    geometry: string,
    layerId: string
  ): string {
    let retour = "";

    // multi filtre
    const multiFilterStart = " <AND> ";
    const multiFilterEnd = " </AND> ";

    // champs de recherche
    const property =
      " <PropertyIsEqualTo> " +
      " <PropertyName>%FIELD%</PropertyName> " +
      " <Literal>%VALUE%</Literal> " +
      " </PropertyIsEqualTo> ";
    // on filtre les filtre correspondant aux layerId
    const filterToLayer = filterLayer.filter(
      (filter) => filter.layerId === layerId
    );

    // construction du xml
    let xmlRequest = "";

    let champsCount = 0;

    filterToLayer.forEach((filter) => {
      if (filter.selecteds.length) {
        champsCount++;
      }
      let filterCount = 0;

      let xmlFilter = "";
      filter.selecteds.forEach((value) => {
        filterCount++;
        if (filterCount === 2) {
          xmlFilter = "<OR>" + xmlFilter;
        }

        let xmlProperty = property.replace("%VALUE%", value.name.toString());
        xmlProperty = xmlProperty.replace("%FIELD%", filter.field);
        xmlFilter += xmlProperty;
      });

      if (filterCount > 1) {
        xmlFilter = xmlFilter + "</OR>";
      }

      xmlRequest += xmlFilter;
    });

    const xmlIntersect = `<Intersects>
                            <PropertyName>msGeometry</PropertyName>
                              ${geometry}
                            </Intersects>`;

    // xml avec juste la partie filtre
    retour += champsCount >= 1 ? multiFilterStart : "";
    retour += xmlIntersect + xmlRequest;
    retour += champsCount >= 1 ? multiFilterEnd : "";

    return retour;
  }

  /**
   * 02/08/2022
   * TODO: Fonction à revoir, les return throwError mettent fin à la fontion du foreach et non de postInterrogation
   * @param layers
   * @param feature
   * @param projection
   * @returns
   */
  postInterrogation(
    layers: Array<VectorLayer<VectorSource<Geometry>>>,
    feature: Feature<any>,
    projection: Projection,
    coordinates?: Array<number>,
    mapFrom?: Map,
    screenMode?: "mobile" | "desktop",
    openResult: boolean = true,
    filterLayers: FilterLayer[] = []
  ): Observable<InterrogationReturnType> {
    if (layers && layers.length > 0) {
      const tasks$ = [];
      const errors = layers.map((layer) => {
        // Si la couche layer n'est pas sélectionnable
        let layerQueryUrl;
        const layerQuery = layer.get("LayerQuery");
        if (layer.get("externalLayer") !== true) {
          if (
            !(
              layer &&
              layer.getVisible() &&
              layer.get("visibleAtScale") &&
              layer.get("visibleAtExtent")
            )
          ) {
            return `La couche ${layer.get(
              "name"
            )} n'est actuellement pas visible`;
          }
          if (
            !layerQuery ||
            !layerQuery.Fields ||
            layerQuery.Fields.length < 1
          ) {
            return `La couche ${layer.get("name")} n'est pas interrogeable`;
          }

          try {
            layerQueryUrl = (layer.getSource() as any).getUrl();
          } catch (exe) {
            console.error(layer);
            return `Nous n'avons pas pu récupérer l'url de la source de la couche ${layer.get(
              "name"
            )}`;
          }
          if (!layerQueryUrl) {
            // console.warn('layer info %s pas d URL', layer.get('name'));
            // console.warn('layer query url', layerQueryUrl );
            return `Url d'appel du layer est vide`;
          }
          const featureToGML = new GML32({
            featureNS: "ms",
            featureType: layer.get("layerName"),
            srsName: projection.getCode(),
            surface: false,
            curve: false,
            multiCurve: false,
            multiSurface: false,
          });
          const featureInGmlFormat = featureToGML.writeFeatures([feature]);

          const extractionRegex = /<Polygon.+Polygon>/;
          const matchArray = featureInGmlFormat.match(extractionRegex);

          if (!matchArray || !matchArray.length) {
            console.error(
              "GML invalide",
              featureInGmlFormat,
              extractionRegex,
              matchArray
            );
            return "Invalid GML";
          }
          const xmlPayload = `<?xml version="1.0" encoding="UTF-8"?>
            <GetFeature service="WFS" version="2.0.0" outputformat="geojson" xmlns="http://www.opengis.net/wfs/2.0"
              xmlns:cw="http://www.someserver.com/cw" xmlns:fes="http://www.opengis.net/ogc/1.1" xmlns:gml="http://www.opengis.net/gml/3.2"
            >
              <Query typeNames="${layer.get("layerName")}" styles="default">
                <Filter>
                 ${this.generateFilterXml(
                   filterLayers,
                   matchArray[0],
                   layer.get("layerName")
                 )}
                </Filter>
              </Query>
            </GetFeature>`;

          tasks$.push(
            this.http.post(layerQueryUrl, xmlPayload).pipe(
              // timeout(2000),
              map((response: any) => {
                response.layer = layer;
                return response;
              }),
              catchError((err) => of(null))
            )
          );
        } else if (
          coordinates &&
          layer.get("externalLayer") === true &&
          ((layer instanceof ImageLayer &&
            layer.getSource() instanceof ImageWMS) ||
            (layer instanceof TileLayer &&
              layer.getSource() instanceof TileWMS))
        ) {
          layerQueryUrl = (
            layer.getSource() as unknown as TileWMS | ImageWMS
          ).getFeatureInfoUrl(
            coordinates,
            mapFrom.getView().getResolution(),
            mapFrom.getView().getProjection().getCode(),
            {
              INFO_FORMAT: "application/vnd.ogc.gml",
            }
          );

          tasks$.push(
            this.http
              .get(this.urlProxy.getProxyUrl(layerQueryUrl), {
                responseType: "text",
              })
              .pipe(
                map((response: any) => {
                  if (response) {
                    const features = new WMSGetFeatureInfo().readFeatures(
                      response
                    );
                    const fields = {};
                    const featuresGeoJSON = features.map((wmsFeature) => {
                      Object.keys(wmsFeature.getProperties()).forEach((key) => {
                        if (key !== "boundedBy") {
                          fields[key] = {
                            FieldAlias: key,
                            FieldName: key,
                            FieldQuery: true,
                            FieldSorting: true,
                            FieldTooltip: false,
                            FieldType: "TXT",
                            FieldURLConfig: "",
                          };
                        }
                      });
                      return new GeoJSON().writeFeatureObject(wmsFeature);
                    });

                    // On parcours les feature récupérées pour en extraire les propriétés et les insérer dans le layerQueryField
                    layer.set("LayerQuery", {
                      Fields: Object.values(fields),
                      URLConfig: "",
                      hasTooltip: false,
                    });

                    const structuredResponse = {
                      layer,
                      features: featuresGeoJSON,
                      isExternalLayer: true,
                    };
                    return structuredResponse;
                  } else {
                    return null;
                  }
                }),
                catchError((err) => {
                  console.error("err on external proxyUrl call", err);
                  return of(null);
                })
              )
          );
          // */
        } else if (
          layer &&
          layer.get("externalLayer") === true &&
          layer instanceof VectorLayer &&
          layer.getSource() instanceof VectorSource
        ) {
          // Il faut retrouver les features qui intersect la sélection
          const features = layer.getSource().getFeatures();
          let i = 0;
          const selectedFeatures = [];
          const geojsonF1 = new GeoJSON().writeFeatureObject(feature);
          for (i; i < features.length; i++) {
            const geojsonF2 = new GeoJSON().writeFeatureObject(features[i]);

            let f2Wf1 = true;
            let f1Cf2 = true;

            let flattenedGeojson = flatten(geojsonF2);

            flattenedGeojson.features.forEach((currentFeature) => {
              f2Wf1 = f2Wf1 && booleanWithin(currentFeature, geojsonF1);
              f1Cf2 = f1Cf2 && booleanContains(geojsonF1, currentFeature);
            });

            /* f2Wf1 = booleanWithin(geojsonF2, geojsonF1);
            f1Cf2 = booleanContains(geojsonF1, geojsonF2); */
            if (f2Wf1 || f1Cf2) {
              selectedFeatures.push(features[i]);
            }
          }
          const fields = {};
          const featuresGeoJSON = selectedFeatures.map((wfsFeature) => {
            Object.keys(wfsFeature.getProperties()).forEach((key) => {
              const wfsFeatureValue = wfsFeature.get(key);
              if (
                wfsFeatureValue === null ||
                typeof wfsFeatureValue !== "object" ||
                wfsFeatureValue instanceof Array ||
                wfsFeatureValue instanceof Date
              ) {
                if (key !== "boundedBy") {
                  fields[key] = {
                    FieldAlias: key,
                    FieldName: key,
                    FieldQuery: true,
                    FieldSorting: true,
                    FieldTooltip: false,
                    FieldType: "TXT",
                    FieldURLConfig: "",
                  };
                }
              }
            });
            return new GeoJSON().writeFeatureObject(wfsFeature);
          });

          // On parcours les feature récupérées pour en extraire les propriétés et les insérer dans le layerQueryField
          layer.set("LayerQuery", {
            Fields: Object.values(fields),
            URLConfig: "",
            hasTooltip: false,
          });

          const structuredResponse = {
            layer,
            features: featuresGeoJSON,
            isExternalLayer: true,
            hasFeature: featuresGeoJSON.some((featureGeoJSON) => {
              return featureGeoJSON.geometry !== null;
            }),
          };
          // return structuredResponse;
          // */

          tasks$.push(of(structuredResponse));
        } else if (
          layer &&
          layer.get("externalLayer") === true &&
          layer instanceof AnimatedCluster &&
          layer.getSource() instanceof Cluster
        ) {
          // Il faut retrouver les cluster puis les features qui intersect la sélection
          const features = layer.getSource().getFeatures();
          let i = 0;
          let selectedFeatures = [];

          const selectedCluster = [];
          const geojsonF1 = new GeoJSON().writeFeatureObject(feature);
          for (i; i < features.length; i++) {
            const geojsonF2 = new GeoJSON().writeFeatureObject(features[i]);
            const f2Wf1 = booleanWithin(geojsonF2, geojsonF1);
            const f1Cn2 = booleanContains(geojsonF1, geojsonF2);
            if (f2Wf1 || f1Cn2) {
              selectedCluster.push(features[i]);
            }
          }

          selectedCluster.forEach((clusterFeature) => {
            selectedFeatures = selectedFeatures.concat(
              clusterFeature.get("features")
            );
          });
          const fields = {};
          const featuresGeoJSON = selectedFeatures.map((wfsFeature) => {
            Object.keys(wfsFeature.getProperties()).forEach((key) => {
              const wfsFeatureValue = wfsFeature.get(key);
              if (
                wfsFeatureValue === null ||
                typeof wfsFeatureValue !== "object" ||
                wfsFeatureValue instanceof Array ||
                wfsFeatureValue instanceof Date
              ) {
                if (key !== "boundedBy") {
                  fields[key] = {
                    FieldAlias: key,
                    FieldName: key,
                    FieldQuery: true,
                    FieldSorting: true,
                    FieldTooltip: false,
                    FieldType: "TXT",
                    FieldURLConfig: "",
                  };
                }
              }
            });
            return new GeoJSON().writeFeatureObject(wfsFeature);
          });

          // On parcours les feature récupérées pour en extraire les propriétés et les insérer dans le layerQueryField
          layer.set("LayerQuery", {
            Fields: Object.values(fields),
            URLConfig: "",
            hasTooltip: false,
          });

          const structuredResponse = {
            layer,
            features: featuresGeoJSON,
            isExternalLayer: true,
            hasFeature: featuresGeoJSON.some((featureGeoJSON) => {
              return featureGeoJSON.geometry !== null;
            }),
          };
          // return structuredResponse;
          // */

          tasks$.push(of(structuredResponse));
        }

        return null;
      });

      const error = errors.find((error) => error !== null);

      if (error) {
        console.error(`error in postInterrogation`, error);
        return throwError(error);
      }

      if (tasks$.length === 0) {
        console.error(`error in postInterrogation : tasks$ emtpy`);
        return throwError(`error in postInterrogation : tasks$ emtpy`);
      }

      return forkJoin(tasks$).pipe(
        map((results) => {
          // Traitement de la données ( pour chaque couche, on match les données avec celles du contexte)
          let totalResults = 0;
          const validResults = (results as any)
            .filter((result) => result !== null && result.layer)
            .map((layerResponseData: any) => {
              let layerResults = 0;
              // Tableau dans lequel on stockera les données récupèrées par la requête
              const layerData = [];
              // Liste des colonnes ( clé, label, type, format, etc...) autorisées à être affichées
              const layerQueryFields = layerResponseData.layer.get("LayerQuery")
                .Fields as Array<LayerQueryFieldType>;
              // On extrait de la liste des colonnes précédentes les clés
              const layerQueryFieldsNames = layerQueryFields.map(
                (layerQueryField) => layerQueryField.FieldName
              );

              // On parcours enfin les réponse pour ne garder que les données qui nous intéresse
              layerResponseData.features.forEach((featureInResponse) => {
                totalResults++;
                layerResults++;
                let datas = {};
                if (featureInResponse.properties) {
                  datas = Object.entries(
                    featureInResponse.properties as { [key: string]: any }
                  )
                    .filter(([key, value]) =>
                      layerQueryFieldsNames.includes(key)
                    )
                    .reduce((accum, [key, value]) => {
                      accum[key] = value;
                      return accum;
                    }, {});
                }
                layerData.push(
                  Object.assign({}, datas, {
                    feature: featureInResponse,
                  })
                );
              });

              // Pour chaque couche, on assemble les données nécessaire à l'affichage de tableau de résultats
              return {
                isExternalLayer: layerResponseData.isExternalLayer
                  ? true
                  : false,
                hasFeature: layerResponseData.isExternalLayer
                  ? layerResponseData.hasFeature
                    ? true
                    : false
                  : true,
                layerName: layerResponseData.layer.get("name"),
                layer: layerResponseData.layer,
                layerQueryFields,
                data: layerData,
                nbResults: layerResults,
              };
            });

          // On trie les resultats par nombre de réponses
          validResults.sort((validResultA, validResultB) => {
            return validResultA.nbResults > validResultB.nbResults
              ? -1
              : validResultA.nbResults < validResultB.nbResults
              ? 1
              : 0;
          });

          let resultOfInterrogation = {
            featureOfRequest: feature,
            layersResults: validResults,
            totalResults,
          };
          this.setResultSearching(false);
          if (openResult) {
            this.resultOfInterrogation = resultOfInterrogation;
            this.changeResultOfInterrogation();
            if (totalResults > 0 && (!screenMode || screenMode === "desktop")) {
              this.openResultCurrentBox();
            } else if (
              totalResults > 0 &&
              screenMode &&
              screenMode === "mobile"
            ) {
              this.openResultMobileCurrentBox();
            }
            this.resultOfInterrogation$.next(this.resultOfInterrogation);
          } else {
            setTimeout(() => {
              this.resultOfInterrogation$.next(this.resultOfInterrogation);
            }, 1500);
          }
          return resultOfInterrogation;
        }),
        catchError((err) => throwError(() => new Error(err)))
      );
    } else {
      return throwError(() => new Error("Pas de layer"));
    }
  }
}
