import { TestBed } from '@angular/core/testing';

import { InterrogatingService } from './interrogating.service';

describe('InterrogatingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterrogatingService = TestBed.get(InterrogatingService);
    expect(service).toBeTruthy();
  });
});
