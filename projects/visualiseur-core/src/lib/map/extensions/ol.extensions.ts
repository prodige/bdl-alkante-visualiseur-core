import { Source } from 'ol/source';
import TileSource from 'ol/source/Tile';
import UrlTile from 'ol/source/UrlTile';
import { Coordinate } from 'ol/coordinate';
import {containsCoordinate as ol_extent_containsCoordinate} from 'ol/extent';

import BaseLayer from 'ol/layer/Base';
import LayerGroup from 'ol/layer/Group';
// TileLayers & Cie
import TileLayer from 'ol/layer/Tile';
import BingMaps from 'ol/source/BingMaps';
import TileArcGISRest from 'ol/source/TileArcGISRest';
import TileDebug from 'ol/source/TileDebug';
import TileJSON from 'ol/source/TileJSON';
import TileWMS from 'ol/source/TileWMS';
import UTFGrid from 'ol/source/UTFGrid';
import VectorTile from 'ol/source/VectorTile';
import WMTS from 'ol/source/WMTS';
import XYZ from 'ol/source/XYZ';
import CartoDB from 'ol/source/CartoDB';
import OSM from 'ol/source/OSM';
import Stamen from 'ol/source/Stamen';
import Zoomify from 'ol/source/Zoomify';

// ImageLayers & Cie
import ImageLayer from 'ol/layer/Image';
import ImageArcGISRest from 'ol/source/ImageArcGISRest';
import ImageCanvasSource from 'ol/source/ImageCanvas';
import ImageMapGuide from 'ol/source/ImageMapGuide';
import Static from 'ol/source/ImageStatic';
import ImageWMS from 'ol/source/ImageWMS';
import RasterSource from 'ol/source/Raster';

// VectorLayers & Cie
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Cluster from 'ol/source/Cluster';
import VectorTileLayer from 'ol/layer/VectorTile';

import Projection from 'ol/proj/Projection';

import GeoJSON from 'ol/format/GeoJSON';
import WFS from 'ol/format/WFS';

import AnimatedCluster from 'ol-ext/layer/AnimatedCluster';

import MapboxVectorLayer  from 'ol/layer/MapboxVector';

declare global {

  // interface Style {
  //   toJSON(): any;
  // }
  // Style as any ).prototype.toJSON = () => {
  //   return {
  //     fill: this.getFill(),
  //     geometry: this.getGeometry(),
  //     image: this.getImage(),
  //     renderer: this.getRenderer(),
  //     stroke: this.getStroke(),
  //     text: this.getText()
  //   };
  // }
  //type ProjectionLike = string | Projection;


  interface Source {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface TileSource {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }


  interface LayerGroup {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }

  interface MapboxVectorLayer {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }

  interface ImageLayer {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface TileLayer {
    getPreview(lonlatVectorLayer: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface VectorLayer {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface AnimatedCluster {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface BingMaps {
    getProdigeType(): string;
  }
  interface TileArcGISRest {
    getProdigeType(): string;
  }
  interface TileDebug {
    getProdigeType(): string;
  }
  interface TileJSON {
    getProdigeType(): string;
  }
  interface TileWMS {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface UTFGrid {
    getProdigeType(): string;
  }
  interface VectorTile {
    getProdigeType(): string;
  }
  interface WMTS {
    getProdigeType(): string;
  }
  interface XYZ {
    getProdigeType(): string;
  }
  interface CartoDB {
    getProdigeType(): string;
  }
  interface OSM {
    getProdigeType(): string;
  }
  interface Stamen {
    getProdigeType(): string;
  }
  interface Zoomify {
    getProdigeType(): string;
  }
  interface ImageArcGISRest {
    getProdigeType(): string;
  }
  interface ImageCanvasSource {
    getProdigeType(): string;
  }
  interface ImageMapGuide {
    getProdigeType(): string;
  }
  interface Static {
    getProdigeType(): string;
  }
  interface ImageWMS {
    getProdigeType(): string;
  }
  interface RasterSource {
    getProdigeType(): string;
  }
  interface VectorSource {
    getProdigeType(): string;
  }
  interface WFS {
    getProdigeType(): string;
  }
  interface GeoJSON {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
  interface Cluster {
    getProdigeType(): string;
  }


}

(Source as any ).prototype.getPreview = function(): string {
  // tslint:disable-next-line: max-line-length
  return `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAk6QAAJOkBUCTn+AAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAANeSURBVHic7ZpPiE1RHMc/780MBhkik79JSUlIUbOxI+wkI2yRhYSUlJLNpJF/xcpiJBmZGBZsNM1CkmhKITGkGbH0/BuPmXnP4rxbb/TOn3fvOffeec6nfqvb/b7f93fveeec37ng8Xg8Ho/nf6Uu4d+fDswFssCvhHOJhaXAMeApMAQUyyIPPAdOAiuTStAVy4EHjDWsix5gdRLJ2mY34ulWYz6IEeA4kIk9awtkgTOEM/5vdAKT4k0/Ou3YMR/ELcbRm9AKFLBbgCJwNE4TYZkJfMG++SIwDCyLz0o4bI17WdyJz0r1TAZ+oDcxCBwAFgIzEIuhvcBbg3sLwOK4DFXLFvQGniCGSSUagS4DjUPOHESkA3XiOWCORqMR6Nfo9DjI3QqPUSd+ylBnv0Zn0GrWFvmIOvGNhjqrNDp/EAutyFgRKUM2tgO+Gur81FxvAKYZaimxXYBvmuuLDHWWaK4X0RfJCNsF6NdcbzXU2a65PohYFKWOc+jn8PUajbWIXaBKp9NB7lZYh34OzwFbFfd/NtDYYSth27urLGIm0M31AL3APWAAmIooymaDnPIl/Vz4NN1yHrd7gcvxWQnHAuA3bsyPop8hUsE13BSgK04TUViBeFo2zedJ8S6wElexW4D2eNOPTjNi6WvD/DtEr8E6tk6GGoAmxFY2iFHE9NZiQf8gogiB9gTEH23izAZuE77vHyU+ANucO1QwD3hD/MbLowAcdm20EmkwXx4n3NodS9rMB2HabYpEWs0HcRqHp0fNwAvJD+eBTZr7p6BvmQVxUaEzEbiruNfJekH15L8jtrEm7JJolEcOmKXRqQOuKDQuY7HZY8s8iNfzkSLxIuI43FTrkkLnOlBfRW4VsWk+oAX5weknxFAxJQNckGgVgZuIRVoomoGXEmGTMa+iQ6K7M4SW7k24QYgiuDQPYinbhugiF4H3RGtzZYCzyIvQXfpNI1ybLyeLpf5+iTbkRbiP2EcocTHm4+YI8iI8RFHwWjAfsA95Q+YZFU6wasl8wB7kReijtNbIILa0vcg/PRlGfPQwHmlCviDqAzaA+OREtzqr1ejOIDorxlNEjTGUBV4nnUWCvAJxGDlA8q9j3DEArAn2zvXAfOwfl6eVAmJrPpJ0Ih6Px+PxeJLjLwPul3vj5d0eAAAAAElFTkSuQmCC`;
};

(TileSource as any ).prototype.getPreview = function(
  this: TileDebug | UrlTile,
  lonlat: Coordinate = [0, 0],
  resolution: number = 150
): string {
  const tileGrid = this.getTileGrid();
  const coord = tileGrid ? tileGrid.getTileCoordForCoordAndResolution(lonlat, resolution) : [0, 0];
  const fn = this.getTileUrlFunction();
  return fn.call(this, coord, this.getProjection());
};

(TileWMS as any ).prototype.getPreview = function(
  lonlat: Coordinate = [0, 0],
  resolution: number = 150
): string {
/*	No way to acces tileUrlFunction...
	var fn = this.getTileUrlFunction();
	return fn.call(this, lonlat, this.getProjection());
*/
  // Use getfeature info instead
  let url = this.getFeatureInfoUrl(lonlat, resolution, this.getProjection() || 'EPSG:3857', {});
  url = url.replace(/getfeatureinfo/i, 'GetMap');
  return url;
};

declare module 'ol/layer/Base'{
  interface BaseLayer {
    getPreview(lonlat: Coordinate, resolution: number): string;
    getProdigeType(): string;
  }
}


BaseLayer.prototype['getPreview'] = function(
  lonlat: Coordinate = [0, 0],
  resolution: number = 150
): Array<string> {
  if ( this.get('preview') ) {
    return [ this.get('preview') ];
  }

  // Get middle resolution
  if ( resolution < this.getMinResolution() || resolution > this.getMaxResolution() ) {
    let rmin = this.getMinResolution();
    let rmax = this.getMaxResolution();
    if ( rmax > 100000 ) {
      rmax = 156543;	// min zoom : world
    }
    if ( rmin < 0.15 ) {
      rmin = 0.15;	// max zoom
    }
    resolution = rmax;
    while ( rmax > rmin ) {
      rmin *= 2;
      rmax /= 2;
      resolution = rmin;
    }
  }
  const e = this.getExtent();

  if ( e && !ol_extent_containsCoordinate(e, lonlat) ) {
    lonlat = [ (e[0] + e[2]) / 2, (e[1] + e[3]) / 2 ];
  }

  if ( this.getSource ) {
    return [ this.getSource().getPreview(lonlat, resolution) ];
  }
  return [];
};

(VectorLayer as any ).prototype.getPreview = (BaseLayer as any).prototype.getPreview;
(AnimatedCluster as any ).prototype.getPreview = (BaseLayer as any ).prototype.getPreview;
(TileLayer as any ).prototype.getPreview = (BaseLayer as any ).prototype.getPreview;
(ImageLayer as any ).prototype.getPreview = (BaseLayer as any ).prototype.getPreview;

(LayerGroup as any ).prototype.getPreview = function(
  lonlat: Coordinate = [0, 0],
  resolution: number = 150
): Array<string> {
  if ( this.get('preview') ) {
    return [ this.get('preview') ];
  }
  let t = [];
  if ( this.getLayers ) {
    const layers = this.getLayers().getArray();
    let i: number;
    for ( i = 0 ; i < layers.length ; i++ ) {
      t = t.concat(layers[i].getPreview(lonlat, resolution));
    }
  }
  return t;
};

(Source as any).prototype.getProdigeType = () => 'Source';
(TileSource as any ).prototype.getProdigeType = () => 'TileSource';
declare module 'ol/layer/Base'{

}


BaseLayer.prototype['getProdigeType'] = () => 'BaseLayer';

(LayerGroup as any ).prototype.getProdigeType = () => 'LayerGroup';
(ImageLayer as any ).prototype.getProdigeType = () => 'ImageLayer';
(TileLayer as any ).prototype.getProdigeType = () => 'TileLayer';
(VectorLayer as any ).prototype.getProdigeType = () => 'VectorLayer';
(AnimatedCluster as any ).prototype.getProdigeType = () => 'VectorLayer';
(BingMaps as any ).prototype.getProdigeType = () => 'BingMaps';
(TileArcGISRest as any ).prototype.getProdigeType = () => 'TileArcGISRest';
(TileDebug as any ).prototype.getProdigeType = () => 'TileDebug';
(TileJSON as any ).prototype.getProdigeType = () => 'TileJSON';
(TileWMS as any ).prototype.getProdigeType = () => 'TileWMS';
(UTFGrid as any ).prototype.getProdigeType = () => 'UTFGrid';
(VectorTile as any ).prototype.getProdigeType = () => 'VectorTile';
(WMTS as any ).prototype.getProdigeType = () => 'WMTS';
(XYZ as any ).prototype.getProdigeType = () => 'XYZ';
(CartoDB as any ).prototype.getProdigeType = () => 'CartoDB';
(OSM as any ).prototype.getProdigeType = () => 'OSM';
(Stamen as any ).prototype.getProdigeType = () => 'Stamen';
(Zoomify as any ).prototype.getProdigeType = () => 'Zoomify';
(ImageArcGISRest as any ).prototype.getProdigeType = () => 'ImageArcGISRest';
(ImageCanvasSource as any ).prototype.getProdigeType = () => 'ImageCanvasSource';
(ImageMapGuide as any ).prototype.getProdigeType = () => 'ImageMapGuide';
(Static as any ).prototype.getProdigeType = () => 'Static';
(ImageWMS as any ).prototype.getProdigeType = () => 'ImageWMS';
(RasterSource as any ).prototype.getProdigeType = () => 'RasterSource';
(VectorSource as any ).prototype.getProdigeType = () => 'VectorSource';
(WFS as any ).prototype.getProdigeType = () => 'WFS';
(GeoJSON as any ).prototype.getProdigeType = () => 'GeoJSON';
(Cluster as any ).prototype.getProdigeType = () => 'Cluster';
(VectorTileLayer as any ).prototype.getProdigeType = () => 'VectorTileLayer';
(MapboxVectorLayer as any ).prototype.getProdigeType = () => 'VectorTileLayer';
