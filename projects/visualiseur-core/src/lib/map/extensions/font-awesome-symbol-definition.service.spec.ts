import { TestBed } from '@angular/core/testing';

import { FontAwesomeSymbolDefinitionService } from './font-awesome-symbol-definition.service';

describe('FontAwesomeSymbolDefinitionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FontAwesomeSymbolDefinitionService = TestBed.get(FontAwesomeSymbolDefinitionService);
    expect(service).toBeTruthy();
  });
});
