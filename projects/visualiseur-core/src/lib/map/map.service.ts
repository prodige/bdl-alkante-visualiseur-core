import {EnvCoreService} from './../env.core.service';
import {VisualiseurCoreService} from './../visualiseur-core.service';
import {HttpClient} from '@angular/common/http';
import {FeatureCollection} from './../models/context';
import Feature from 'ol/Feature';
import {Injectable, Inject} from '@angular/core';

import {MapIdService} from './map-id.service';
import {get, transform, transformExtent} from 'ol/proj';
import Map from 'ol/Map';
import View from 'ol/View';

import './extensions/ol.extensions';
import {ReplaySubject, Observable, Subject} from 'rxjs';

import LayerGroup from 'ol/layer/Group';
import BaseLayer from 'ol/layer/Base';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import ImageLayer from 'ol/layer/Image';
import {
  Polygon,
  LineString,
  Point,
  MultiPoint,
  MultiLineString,
  Geometry,
} from 'ol/geom';
import {
  intersects,
  containsExtent,
  Extent,
  getWidth,
  getHeight,
  buffer,
} from 'ol/extent';
import {Size} from 'ol/size';
import {Style, Stroke, Fill} from 'ol/style';
import {createDefaultStyle} from 'ol/style/Style';
import {Icon} from 'ol/style';
import {toContext} from 'ol/render';
import {UrlProxyService} from '../services/url-proxy.service';

import Projection from 'ol/proj/Projection';
import CircleStyle from 'ol/style/Circle';
import VectorSource from 'ol/source/Vector';

import AnimatedCluster from 'ol-ext/layer/AnimatedCluster';

import Cluster from 'ol/source/Cluster';

import VectorTileLayer from 'ol/layer/VectorTile';
import {Layer} from 'ol/layer';
import {OptionImpression} from '../models/option-impression';

const INCHES_PER_UNIT = {
  m: 39.37,
  dd: 4374754,
};
const DOTS_PER_INCH = 72;

@Injectable({
  providedIn: 'root',
})
export class MapService {
  private fitEvent$ = new ReplaySubject<boolean>(1);

  private map: { [mapId: string]: Map } = {};
  private readyMaps: { [mapId: string]: ReplaySubject<Map> } = {};
  private initMaps: { [mapId: string]: Subject<Map> } = {};
  private mapResolution: { [mapId: string]: ReplaySubject<number> } = {};
  private resolutions: { [mapId: string]: ReplaySubject<Array<number>> } = {};
  private sideBarMode:
    | 'Couches'
    | 'Légende'
    | 'Information'
    | 'Tools'
    | 'Open'
    | 'Share' = 'Couches';
  private sideBarLegendMode = new ReplaySubject<'Couches' | 'Légende' | 'Information' | 'Tools' | 'Open' | 'Share'>(1);

  public rootLayerGroup: LayerGroup;
  public shapeRatioPrint = 0;
  public mapZoom = 28;
  public mapCenter: [number, number] = [0, 0];
  private mapIds: { [key: string]: string } = {};

  private featureTools$ = new ReplaySubject<FeatureCollection['properties']['extension']['Tools']>(1);

  // Attributs pour impression et export
  public impressionEnCours = false;
  public mapTitlePrint: string;
  public mapSubtitlePrint: string;
  public mapCommentPrint: string;
  public $orientation = new ReplaySubject<'portrait' | 'landscape'>(1);
  public orientation: 'portrait' | 'landscape' = 'landscape';
  public imageFormat = 'A4';
  public fileFormat: 'pdf' | 'jpg' | 'png' = 'pdf';
  public pageNumber = 1;
  public carrouselItems = [];
  public selectedCarrouselOption: {
    indexCarrousel: number;
    indexOption: number;
  };
  public drawnFeaturePrint: Feature<any>;
  public scalingCheckbox = false;
  public keyMapCheckbox = true;
  public mentionCheckbox = false;
  public mentionText = 'Document de Travail';
  public legendCheckbox = true;

  informativeDisplayLayer: VectorLayer<VectorSource<Geometry>> = null;
  informativeDisplayLayer$: ReplaySubject<VectorLayer<VectorSource<Geometry>>> =
    new ReplaySubject<VectorLayer<VectorSource<Geometry>>>(1);

  constructor(
    private http: HttpClient,
    private urlProxyService: UrlProxyService,
    private coreService: VisualiseurCoreService,
    private environment: EnvCoreService,
    private mapIdService: MapIdService
  ) {
    this.setSideBarLegendMode(
      this.coreService.isApplicationInLiteMode ? 'Légende' : 'Couches'
    );
  }

  getInformativeDisplayLayer(
    usedMap: Map
  ): Observable<VectorLayer<VectorSource<Geometry>>> {
    if (this.informativeDisplayLayer === null) {
      const interrogationDisplaySource = new VectorSource({
        wrapX: false,
        features: [],
      });
      this.informativeDisplayLayer = new VectorLayer({
        source: interrogationDisplaySource,
        zIndex: Infinity,
        map: usedMap,
        style: new Style({
          stroke: new Stroke({
            color: 'orange',
            width: 3,
          }),
          fill: new Fill({
            color: 'rgba(0, 0, 255, 0.3)',
          }),
          image: new CircleStyle({
            radius: 5,
            fill: new Fill({
              color: 'orange',
            }),
          }),
        }),
      });
      this.informativeDisplayLayer$.next(this.informativeDisplayLayer);
    }
    return this.informativeDisplayLayer$;
  }

  removeInformativeDisplayLayer() {
    if (this.informativeDisplayLayer) {
      this.informativeDisplayLayer.getSource().clear();
      this.informativeDisplayLayer.setMap(null);
      this.informativeDisplayLayer = null;
      this.informativeDisplayLayer$.next(null);
    }
  }

  setMapFitEvent(event: boolean) {
    this.fitEvent$.next(event);
  }

  getMapFitEvent(): Observable<boolean> {
    return this.fitEvent$;
  }

  private createMap(id: string, options?): Map {
    return new Map({
      target: id,
      controls: [],
      view: new View(
        options
          ? options
          : {
            center: [0, 0],
            zoom: 1,
            projection: 'EPSG:2154',
          }
      ),
    });
  }

  getMap(id: string | MapIdService = 'main', rewrite = false, options?): Map {
    id = typeof id !== 'string' ? id.getId() : id;
    if (!this.map[id] || rewrite) {
      this.map[id] = this.createMap(id, options);
      this.mapResolution[id] = new ReplaySubject<number>(1);
      this.mapResolution[id].next(this.map[id].getView().getResolution());
      this.resolutions[id] = new ReplaySubject<Array<number>>(1);
      this.resolutions[id].next(this.map[id].getView().getResolutions());

      if (!this.readyMaps[id]) {
        this.readyMaps[id] = new ReplaySubject<Map>(1);
      }
    }
    this.readyMaps[id].next(this.map[id]);
    return this.map[id];
  }

  getMapReady(id: string | MapIdService = 'main'): Observable<Map> {
    id = typeof id !== 'string' ? id.getId() : id;
    if (!this.readyMaps[id]) {
      this.readyMaps[id] = new ReplaySubject<Map>(1);
    }
    return this.readyMaps[id];
  }

  /**
   * utiliser getMapAfterComplete
   * @deprecated
   */
  getInitMap(id: string | MapIdService = 'main'): Observable<Map> {
    return this.getMapAfterComplete(id);
  }

  /**
   * Pour savoir quand la map est entièrement prête
   * @param id de la carte
   * @return un observable de la carte à partir d'un subject(rxjs)
   */
  getMapAfterComplete(id: string | MapIdService = 'main'): Observable<Map> {
    id = typeof id !== 'string' ? id.getId() : id;
    if (!this.initMaps[id]) {
      this.initMaps[id] = new Subject<Map>();
    }

    return this.initMaps[id];
  }

  /**
   * Pour prévenir que la map est entièrement prête
   * @param id de la carte
   */
  emitMapComplete(id: string | MapIdService = 'main'): void {
    id = typeof id !== 'string' ? id.getId() : id;
    if (!this.initMaps[id]) {
      this.initMaps[id] = new Subject<Map>();
    }

    this.initMaps[id].next(this.map[id]);
  }

  deleteMap(id: string | MapIdService = 'main'): void {
    id = typeof id !== 'string' ? id.getId() : id;

    if (this.map[id]) {
      this.map[id].setTarget(null);
      /* delete this.map[id]; */
    }
    if (this.mapResolution[id]) {
      this.mapResolution[id].complete();
      /* delete this.mapResolution[id]; */
    }
    if (this.resolutions[id]) {
      this.resolutions[id].complete();
      /* delete this.resolutions[id]; */
    }
  }

  getMaps(): { [mapId: string]: Map } {
    return this.map;
  }

  getArrayMaps(): Array<Map> {
    return Object.values(this.map);
  }

  getMapResolution(mapId: string | MapIdService = 'main'): Observable<number> {
    mapId = typeof mapId !== 'string' ? mapId.getId() : mapId;
    return this.mapResolution[mapId];
  }

  setMapResolution(
    mapId: string | MapIdService = 'main',
    resolution: number
  ): void {
    mapId = typeof mapId !== 'string' ? mapId.getId() : mapId;
    try {
      this.mapResolution[mapId].next(resolution);
    } catch (err) {
      console.error(err);
    }
  }

  getSideBarLegendMode(): Observable<'Couches' | 'Légende' | 'Information' | 'Tools' | 'Open' | 'Share'> {
    return this.sideBarLegendMode;
  }

  setSideBarLegendMode(
    mode: 'Couches' | 'Légende' | 'Information' | 'Tools' | 'Open' | 'Share'
  ): void {
    this.sideBarMode = mode;
    this.sideBarLegendMode.next(mode);
  }

  getResolutions(
    mapId: string | MapIdService = 'main'
  ): Observable<Array<number>> {
    mapId = typeof mapId !== 'string' ? mapId.getId() : mapId;
    return this.resolutions[mapId];
  }

  setResolutions(
    mapId: string | MapIdService = 'main',
    resolutions: Array<number>
  ): void {
    mapId = typeof mapId !== 'string' ? mapId.getId() : mapId;
    try {
      this.resolutions[mapId].next(resolutions);
    } catch (err) {
      console.error(err);
    }
  }

  getLayerPath(
    layer: BaseLayer,
    layerIdx: number,
    layerSearched: BaseLayer
  ): string {
    let path = '';
    if (layer === layerSearched) {
      path += layerIdx;
    } else if (layer instanceof LayerGroup) {
      layer
        .getLayers()
        .getArray()
        .forEach((baseLayer: BaseLayer, idx, tab) => {
          const tmpSubPath = this.getLayerPath(baseLayer, idx, layerSearched);
          if (tmpSubPath) {
            path += layerIdx + tmpSubPath + '-';
          }
        });
    }
    return path ? '-' + path : null;
  }

  getOrientation(): Observable<'portrait' | 'landscape'> {
    return this.$orientation;
  }

  setOrientation(orientation: 'portrait' | 'landscape'): void {
    this.$orientation.next(orientation);
    this.orientation = orientation;
  }

  generateLegendForLayer(
    layer: Layer<VectorSource<Geometry>>,
    path: string,
    mapId: string = 'main',
    forceRedraw = false
  ): void {
    if (layer && (forceRedraw || !layer.get('imageDataUrl'))) {
      if (layer.get('legendUrl')) {
        const newImg = new Image();
        // newImg.crossOrigin = '';
        newImg.onerror = (...args) => {
          // console.warn(' layer ', layer.get('name'), 'IMAGE ERROR', args);
          if (layer.getVisible() && layer.getOpacity() > 0) {
            const pv = (layer as any).getPreview(
              this.map[mapId].getView().getCenter()
            );
            if (pv && pv.length) {
              const newImgSecondTry = new Image();
              // newImgSecondTry.crossOrigin = '';
              newImgSecondTry.onerror = (...args) => {
                // console.warn(' layer ', layer.get('name'), 'IMAGE ERROR 2', args);
              };
              newImgSecondTry.onload = () => {
                // On utilise des canvas pour redimensionner l'image dans une taille acceptable
                const canvas = document.createElement('canvas');
                const ctx = canvas.getContext('2d');
                const oc = document.createElement('canvas');
                const octx = oc.getContext('2d');

                canvas.width =
                  newImgSecondTry.naturalWidth *
                  (30 / newImgSecondTry.naturalHeight);
                canvas.height = 30;

                let cur = {
                  width: Math.floor(newImgSecondTry.width * 0.5),
                  height: Math.floor(newImgSecondTry.height * 0.5),
                };

                oc.width = cur.width;
                oc.height = cur.height;

                octx.drawImage(newImgSecondTry, 0, 0, cur.width, cur.height);

                while (cur.width * 0.5 > 30) {
                  cur = {
                    width: Math.floor(cur.width * 0.5),
                    height: Math.floor(cur.height * 0.5),
                  };
                  octx.drawImage(
                    oc,
                    0,
                    0,
                    cur.width * 2,
                    cur.height * 2,
                    0,
                    0,
                    cur.width,
                    cur.height
                  );
                }
                ctx.drawImage(
                  oc,
                  0,
                  0,
                  cur.width,
                  cur.height,
                  0,
                  0,
                  canvas.width,
                  canvas.height
                );

                try {
                  const canvasContents = canvas.toDataURL('image/png', 1);
                  // On affecte les variables au layer
                  layer.set('imageDataUrl', canvasContents);
                  layer.set('oneClassOnly', false);
                  layer.set('isPrefixLegendImg', true);
                } catch (exception) {
                  console.error(
                    'Erreur onerror image legendUrl',
                    exception,
                    pv[0]
                  );
                }
                newImgSecondTry.remove();
                canvas.remove();
                oc.remove();
              };
              newImgSecondTry.src = pv[0];
            }
          }
        };
        newImg.onload = () => {
          newImg.crossOrigin = 'anonymous';
          const canvas: HTMLCanvasElement = document.createElement('canvas');
          canvas.width = newImg.naturalWidth; // or 'width' if you want a special/scaled size
          canvas.height = newImg.naturalHeight; // or 'height' if you want a special/scaled size

          layer.set('oneClassOnly', !!(newImg.naturalHeight < 40));
          layer.set('isPrefixLegendImg', false);

          canvas.getContext('2d').drawImage(newImg, 0, 0);
          try {
            const canvasContents = canvas.toDataURL('image/png', 1);
            layer.set('imageDataUrl', canvasContents);
          } catch (exception) {
            console.error(
              'Erreur onload image legendUrl',
              exception,
              this.urlProxyService.getProxyUrl(layer.get('legendUrl'))
            );
          }
          newImg.remove();
          canvas.remove();
        };
        newImg.crossOrigin = 'Anonymous';
        newImg.src = this.urlProxyService.getProxyUrl(layer.get('legendUrl'));
        // layer.set('imageDataUrl', this.urlProxyService.getProxyUrl(layer.get('legendUrl')));
      } else if (
        layer instanceof VectorLayer ||
        layer instanceof AnimatedCluster
      ) {
        // generate canvas
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        const legendLayer: { [key: string]: any } = {};
        let style = (layer as VectorLayer<VectorSource<Geometry>>).getStyle();
        // On essaie de récupérer l'image du style appliqué aux features de la couche
        let image;
        if (style) {
          if (style instanceof Array) {
            image = style.find((s) => !!s.getImage())
              ? style.find((s) => !!s.getImage()).getImage()
              : null;
          } else if (typeof style !== 'function') {
            image = style.getImage();
          } else {
            let feature: Feature<Geometry> = null;
            if (layer.getSource() instanceof Cluster) {
              this.coreService.getClusterService().generateLegendToLayer(layer);
            } else {
              feature = layer
                .getSource()
                .getFeatures()
                .find((featureElem) => !!featureElem.getGeometry());
            }

            if (feature) {
              const featureStyle = feature.getStyle();
              if (featureStyle && typeof featureStyle !== 'function') {
                style = featureStyle;
                if (featureStyle instanceof Array) {
                  image = featureStyle.find((s) => !!s.getImage())
                    ? featureStyle.find((s) => !!s.getImage()).getImage()
                    : null;
                } else if (typeof featureStyle !== 'function') {
                  image = featureStyle.getImage();
                }
              } else {
                const styleResolution = layer.getMinResolution()
                  ? layer.getMinResolution()
                  : layer.getMaxResolution()
                    ? layer.getMaxResolution()
                    : layer.getExtent()
                      ? this.map[mapId]
                        .getView()
                        .getResolutionForExtent(layer.getExtent())
                      : this.map[mapId]
                        .getView()
                        .getResolutionForZoom(
                          this.map[mapId].getView().getZoom()
                        );
                const tempStyle = style(feature, styleResolution);
                if (tempStyle instanceof Array) {
                  image = tempStyle.find((s) => !!s.getImage())
                    ? tempStyle.find((s) => !!s.getImage()).getImage()
                    : null;
                } else if (typeof tempStyle !== 'function') {
                  image = (tempStyle as Style).getImage();
                }
              }
            }
          }
        } else {
          image = null;
        }

        if (image) {
          if (image instanceof Icon) {
            // raster icon from url
            const icon2 = new Icon({
              src: image.getSrc(),
            });
            const iconStyle2 = new Style({
              image: icon2,
            });
            legendLayer.style = iconStyle2;
            legendLayer.title = layer.get('title');
          } else {
            // ol.style.RegularShape?
            legendLayer.style = style;
            legendLayer.title = layer.get('title');
          }
        } else {
          legendLayer.style = style;
          legendLayer.title = layer.get('title');
        }

        // geometry type
        const features = layer.getSource().getFeatures();
        if (features && features.length) {
          const feature = features.find(
            (featureElem) => !!featureElem.getGeometry()
          );
          if (feature) {
            if (
              feature.getGeometry() instanceof Point ||
              feature.getGeometry() instanceof MultiPoint
            ) {
              legendLayer.geomType = `point`;
            } else if (
              feature.getGeometry() instanceof LineString ||
              feature.getGeometry() instanceof MultiLineString
            ) {
              legendLayer.geomType = `line`;
            } else {
              legendLayer.geomType = `polygon`;
            }
          }
        }

        if (layer.get('isDrawLayer') || legendLayer.geomType) {
          const vectorContext = toContext(canvas.getContext('2d'), {
            size: [30, 30],
          });
          if (legendLayer.style instanceof Style) {
            if (
              legendLayer.style.getImage() != null &&
              legendLayer.style.getImage().getSize() != null
            ) {
              vectorContext.setStyle(legendLayer.style);
            }
          } else {
            const styles = createDefaultStyle(features[0], null);
            if (styles && styles.length) {
              vectorContext.setStyle(styles[0]);
            }
          }
          if (layer.get('isDrawLayer')) {
            const polyCoords = [];
            polyCoords.push([3, 3]);
            polyCoords.push([27, 3]);
            polyCoords.push([27, 27]);
            polyCoords.push([3, 27]);
            polyCoords.push([3, 3]);
            vectorContext.drawGeometry(new Polygon([polyCoords]));

            vectorContext.drawGeometry(new Point([11, 19]));

            const lineCoords = [];
            lineCoords.push([20, 6]);
            lineCoords.push([24, 6]);
            lineCoords.push([22, 6]);
            lineCoords.push([22, 12]);
            vectorContext.drawGeometry(new LineString(lineCoords));
          } else {
            if (legendLayer.geomType) {
              // icon feature depending on type
              if (legendLayer.geomType === 'point') {
                vectorContext.drawGeometry(new Point([15, 15]));
              } else if (legendLayer.geomType === 'polygon') {
                const polyCoords = [];
                polyCoords.push([3, 3]);
                polyCoords.push([27, 3]);
                polyCoords.push([27, 27]);
                polyCoords.push([3, 27]);
                polyCoords.push([3, 3]);
                vectorContext.drawGeometry(new Polygon([polyCoords]));
              } else {
                const lineCoords = [];
                lineCoords.push([3, 27]);
                lineCoords.push([20, 15]);
                lineCoords.push([15, 10]);
                lineCoords.push([27, 3]);
                vectorContext.drawGeometry(new LineString(lineCoords));
              }
            }
          }
          const canvasContents = canvas.toDataURL('image/png', 1);
          layer.set('imageDataUrl', canvasContents);
          layer.set('isPrefixLegendImg', true);
          layer.set('oneClassOnly', false);
          canvas.remove();
        }
      }
    }
  }

  verifyLayerGroupVisibility(
    layerGroup: LayerGroup,
    mapId: string
  ): [boolean, boolean, boolean, boolean] {
    let childrenLayersAllVisible = true;
    let childrenLayersAllNotVisible = true;
    let atLeastOneIsVisible = false;
    let atLeastOneIsNotVisible = false;

    if (layerGroup) {
      if (
        this.sideBarMode === 'Couches' &&
        layerGroup
          .getLayers()
          .getArray()
          .filter((baseLayer) => baseLayer.get('baseLayer') !== true).length ===
        0
      ) {
        layerGroup.set('visibleChecked', false);
        layerGroup.set('disabled', true);
        layerGroup.set('isEmpty', true);
        return [
          childrenLayersAllVisible,
          childrenLayersAllNotVisible,
          atLeastOneIsVisible,
          atLeastOneIsNotVisible,
        ];
      }
      layerGroup.set('visibleChecked', false);
      layerGroup.set('isEmpty', false);
      layerGroup.set('disabled', false);

      layerGroup
        .getLayers()
        .getArray()
        .forEach((baseLayer: LayerGroup | VectorTileLayer) => {
          if (baseLayer instanceof LayerGroup) {
            const tmpRes = this.verifyLayerGroupVisibility(baseLayer, mapId);
            childrenLayersAllVisible = childrenLayersAllVisible && tmpRes[0];
            childrenLayersAllNotVisible =
              childrenLayersAllNotVisible && tmpRes[1];
            atLeastOneIsVisible = atLeastOneIsVisible || tmpRes[2];
            atLeastOneIsNotVisible = atLeastOneIsNotVisible || tmpRes[3];
          } else if (
            this.sideBarMode === 'Légende' ||
            baseLayer.get('baseLayer') !== true
          ) {
            const layerMaxScale = baseLayer.get('maxscaledenominator')
              ? baseLayer.get('maxscaledenominator')
              : Infinity;
            const layerMinScale = baseLayer.get('minscaledenominator')
              ? baseLayer.get('minscaledenominator')
              : 0;

            if (typeof baseLayer.get('visibleChecked') !== 'boolean') {
              baseLayer.set('visibleChecked', !!baseLayer.getVisible());
            }

            if (this.map && mapId) {
              // Vérification de la visibilité de couche par rapport à la résolution courante
              const mapViewResolution = this.map[mapId]
                ? this.map[mapId].getView().getResolution()
                : this.map[
                  this.mapIdService.getId()
                    ? this.mapIdService.getId()
                    : 'main'
                  ]
                  .getView()
                  .getResolution();
              const mapProjection = this.map[mapId]
                ? this.map[mapId].getView().getProjection()
                : this.map[
                  this.mapIdService.getId()
                    ? this.mapIdService.getId()
                    : 'main'
                  ]
                  .getView()
                  .getProjection();
              const mapViewScale =
                mapViewResolution *
                this.getPixelsPerProjectionUnits(mapProjection);
              if (
                layerMinScale <= mapViewScale &&
                mapViewScale <= layerMaxScale
              ) {
                // La couche est visible
                baseLayer.set('visibleAtScale', true);
                if (baseLayer.get('visibleChecked') === true) {
                  baseLayer.setVisible(true);
                  baseLayer.set('LayerLegendScaleDisplay', true);
                }
              } else {
                // La couche n'est pas visible
                baseLayer.set('visibleAtScale', false);
                baseLayer.set('LayerLegendScaleDisplay', false);
                baseLayer.setVisible(false);
              }

              // Vérification visibilité de la couche par rapport à l'extent courant
              const layerExtent = baseLayer.getExtent();
              if (
                layerExtent &&
                layerExtent instanceof Array &&
                layerExtent.length
              ) {
                const mapViewExtent = this.map[mapId]
                  .getView()
                  .calculateExtent();
                // Si l'extent de la couche est dans celui de la carte ou intersect celui de la carte, on l'affiche
                baseLayer.set(
                  'visibleAtExtent',
                  containsExtent(mapViewExtent, layerExtent) ||
                  intersects(mapViewExtent, layerExtent)
                    ? true
                    : false
                );
              } else {
                baseLayer.set('visibleAtExtent', true);
              }
            }

            childrenLayersAllVisible =
              childrenLayersAllVisible &&
              baseLayer.get('visibleChecked') === true;
            childrenLayersAllNotVisible =
              childrenLayersAllNotVisible &&
              baseLayer.get('visibleChecked') === false;
            atLeastOneIsVisible =
              atLeastOneIsVisible || baseLayer.get('visibleChecked') === true;
            atLeastOneIsNotVisible =
              atLeastOneIsNotVisible ||
              baseLayer.get('visibleChecked') === false;
          }
        });

      if (
        this.sideBarMode === 'Légende' &&
        layerGroup
          .getLayers()
          .getArray()
          .filter((subBaseLayer) => {
            return (
              subBaseLayer.get('visibleChecked') &&
              ((!(subBaseLayer instanceof LayerGroup) &&
                  subBaseLayer.get('visibleAtExtent')) ||
                (!(subBaseLayer instanceof LayerGroup) &&
                  subBaseLayer.get('visibleAtScale')) ||
                (subBaseLayer instanceof LayerGroup &&
                  !subBaseLayer.get('isEmpty')))
            );
          }).length === 0
      ) {
        childrenLayersAllVisible = false;
        childrenLayersAllNotVisible = true;
        atLeastOneIsVisible = false;
        atLeastOneIsNotVisible = true;
        layerGroup.set('visibleChecked', false);
        layerGroup.set('disabled', true);
        layerGroup.set('isEmpty', true);
      }

      if (childrenLayersAllVisible || childrenLayersAllNotVisible) {
        layerGroup.set(
          'visibleChecked',
          childrenLayersAllVisible ? true : false
        );
        layerGroup.set('allChildrenHaveSameVisibility', true);
      } else if (atLeastOneIsVisible || atLeastOneIsNotVisible) {
        layerGroup.set('visibleChecked', true);
        layerGroup.set('allChildrenHaveSameVisibility', false);
      }
    }

    return [
      childrenLayersAllVisible,
      childrenLayersAllNotVisible,
      atLeastOneIsVisible,
      atLeastOneIsNotVisible,
    ];
  }

  public getFeatureTools(): Observable<FeatureCollection['properties']['extension']['Tools']> {
    return this.featureTools$;
  }

  public setFeatureTools(
    tools: FeatureCollection['properties']['extension']['Tools']
  ) {
    this.featureTools$.next(tools);
  }

  public setMapId(mapSection: string, id: string): void {
    this.mapIds[mapSection] = id;
  }

  public getMapId(mapSection: string): string {
    return this.mapIds[mapSection];
  }

  public generateMaximalPrintingFeature(
    mapSection = 'main'
  ): Feature<Geometry> {
    const map = this.map[this.getMapId(mapSection)]
      ? this.map[this.getMapId(mapSection)]
      : this.map[this.getMapId('visualiseur')];
    let maximalFeature: Feature<Geometry> = null;

    const currentMapExtent = map.getView().calculateExtent(map.getSize());
    const coordsCenter = map.getView().getCenter();
    if (currentMapExtent && currentMapExtent.length === 4) {
      const widthMapScreen = currentMapExtent[2] - currentMapExtent[0];
      const heightMapScreen = currentMapExtent[3] - currentMapExtent[1];
      const ratioScreen = widthMapScreen / heightMapScreen;

      let finalHeight = 0;
      let finalWidth = 0;
      let minMaxPointCoords: Array<Array<number>> = null;

      if (this.shapeRatioPrint < ratioScreen) {
        finalHeight = heightMapScreen;
        finalWidth = heightMapScreen * (1 / this.shapeRatioPrint);
        minMaxPointCoords = [
          [coordsCenter[0] - finalWidth / 2, currentMapExtent[1]],
          [coordsCenter[0] + finalWidth / 2, currentMapExtent[3]],
        ];
      } else if (this.shapeRatioPrint > ratioScreen) {
        finalHeight = widthMapScreen * this.shapeRatioPrint;
        finalWidth = widthMapScreen;
        minMaxPointCoords = [
          [currentMapExtent[0], coordsCenter[1] - finalHeight / 2],
          [currentMapExtent[2], coordsCenter[1] + finalHeight / 2],
        ];
      } else {
        finalHeight = heightMapScreen;
        finalWidth = widthMapScreen;
        // featureCoords = currentMapExtent;
      }

      // Il reste à calculer les deux autres points
      // Actuellement, minMaxPointCoords contient le Point min et le Point Max de la geometry (diagonale)
      // Il faut dont calculer les deux autres points
      const featureCoords = [];

      featureCoords.push(
        ...[
          [minMaxPointCoords[0][0], minMaxPointCoords[1][1]],
          minMaxPointCoords[1],
          [minMaxPointCoords[1][0], minMaxPointCoords[0][1]],
          minMaxPointCoords[0],
        ]
      );

      const geom = new Polygon([featureCoords]);
      maximalFeature = new Feature(coordsCenter);
      maximalFeature.setGeometry(geom);
    }

    return maximalFeature;
  }

  public startImpressionRequest(
    impressType: 'pdf' | 'jpg' | 'png' = 'png',
    context: FeatureCollection,
    modelImpress?: unknown, // PrintModel
    mapSection = 'main',
    options?: OptionImpression
  ): void {
    // On récupère le modèle d'impression choisi.
    // TODO - Vérifier si présence obligatoire ou valeur par défaut nécessaire.
    let modeleImpression = null;
    if (!modelImpress) {
      if (
        this.selectedCarrouselOption &&
        this.selectedCarrouselOption.indexCarrousel != null &&
        this.selectedCarrouselOption.indexCarrousel >= 0 &&
        this.selectedCarrouselOption.indexOption != null &&
        this.selectedCarrouselOption.indexOption >= 0

      ) {
        const carrouselItem =
          this.carrouselItems[this.selectedCarrouselOption.indexCarrousel];
        modeleImpression =
          carrouselItem[this.selectedCarrouselOption.indexOption];
      }
    } else {
      modeleImpression = modelImpress;
    }

    // Puis on récupère la figure d'impression choisie.
    // Si aucune n'est sélectionnée, on calcul le maximum possible.
    let impressExtent = this.drawnFeaturePrint
      ? this.drawnFeaturePrint.getGeometry().getExtent()
      : null;

    if (!impressExtent) {
      // On calcule le maximum possible par rapport à la page courante.
      if (modeleImpression) {
        this.shapeRatioPrint =
          this.imageFormat && modeleImpression.ratio[this.imageFormat]
            ? modeleImpression.ratio[this.imageFormat]
            : 1;
      }
      if (this.shapeRatioPrint) {
        const maximalFeature = this.generateMaximalPrintingFeature(mapSection);
        if (maximalFeature) {
          impressExtent = maximalFeature.getGeometry().getExtent();
        }
      }
    }

    if (impressExtent) {
      // Enfin, on poste la requête d'impression.

      if (modeleImpression.file === 'modele3.tpl') {
        this.keyMapCheckbox = false;
      }

      this.impressionEnCours = true;
      this.postMapImpressRequest(
        context,
        modeleImpression,
        impressExtent,
        impressType,
        options
      )
        .then((responsePostImpressionRequest: Blob) => {
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(responsePostImpressionRequest);
          link.download =
            'impr_' +
            Math.random().toString(36).substr(2, 9) +
            '.' +
            impressType;
          link.dispatchEvent(
            new MouseEvent(`click`, {
              bubbles: true,
              cancelable: true,
              view: window,
            })
          );
          this.impressionEnCours = false;
        })
        .catch((errorPostImpressRequest) => {
          this.impressionEnCours = false;
          console.error(errorPostImpressRequest);
        });
      this.keyMapCheckbox = true;
    } else {
      console.error('Impossible de récupérer l\'extent pour l\'impression');
    }
  }

  public postMapImpressRequest(
    context: FeatureCollection,
    modeleImpression: any,
    impressExtent: [number, number, number, number],
    impressType: 'pdf' | 'png' | 'jpg' = 'pdf',
    options: OptionImpression
  ): Promise<Blob> {
    if (context && context.properties && context.properties.bbox) {
      context.properties.bbox = impressExtent;
    }
    return this.http
      .post(
        `${this.environment.serverUrl}core/print/export?account=${
          ['fr', 'en'].includes(window.location.pathname.split('/')[1])
            ? window.location.pathname.split('/')[2]
            : window.location.pathname.split('/')[1]
        }`,
        {
          context: context ? context : null,
          comment: this.mapCommentPrint ? this.mapCommentPrint : null,
          title: this.mapTitlePrint ? this.mapTitlePrint : null,
          subtitle: this.mapSubtitlePrint ? this.mapSubtitlePrint : null,
          orientation: this.orientation ? this.orientation : 'landscape',
          pagesize: this.imageFormat ? this.imageFormat : 'A4',
          pageNumber: this.pageNumber ? this.pageNumber : 1,
          model: modeleImpression ? modeleImpression.file : null,
          scalingCheckbox: this.scalingCheckbox ? this.scalingCheckbox : null,
          keyMapCheckbox: this.keyMapCheckbox ? this.keyMapCheckbox : null,
          mentionCheckbox: this.mentionCheckbox ? this.mentionCheckbox : null,
          mentionText: this.mentionText ? this.mentionText : null,
          legendCheckbox: this.legendCheckbox ? this.legendCheckbox : null,
          extent: impressExtent,
          type: impressType,
          options: options ? options : null
        },
        {
          responseType: 'blob',
        }
      )
      .toPromise();
  }

  // ajustement d'une emprise (extent) pour coller au ratio d'une
  // fenêtre en pixel (size)
  public adjustExtentToSize(extent: Extent, size: Size, buf: number) {
    const extentRatio = getWidth(extent) / getHeight(extent);
    const maxDim = Math.min(getWidth(extent), getHeight(extent));
    const viewPortRatio = size[0] / size[1];
    let newExtent = extent;
    if (viewPortRatio > extentRatio) {
      const newWidth = getHeight(extent) * viewPortRatio;
      const xmin = (extent[0] + extent[2]) / 2.0 - newWidth / 2.0;
      const xmax = xmin + newWidth;
      newExtent = [xmin, extent[1], xmax, extent[3]];
    } else {
      const newHeight = getWidth(extent) / viewPortRatio;
      const ymin = (extent[1] + extent[3]) / 2.0 - newHeight / 2.0;
      const ymax = ymin + newHeight;
      newExtent = [extent[0], ymin, extent[2], ymax];
    }
    newExtent = buffer(newExtent, maxDim * buf);
    return newExtent;
  }

  private getPixelsPerProjectionUnits(p: Projection): number {
    // meter unit crs as default
    let res = INCHES_PER_UNIT.m * DOTS_PER_INCH;
    if (p.getUnits() == 'degrees') {
      res = INCHES_PER_UNIT.dd * DOTS_PER_INCH;
    }
    return res;
  }
}
