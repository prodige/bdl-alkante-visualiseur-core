import { EnvCoreService } from './../env.core.service';
import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import {ProjectionLike} from 'ol/proj';
import './extensions/ol.extensions';

/**
 * Service to get the current id (declared by the host).
 * Inject the service in the class that have to use it
 * and access the current id with the getId function.
 * You can also use the setId to enable the subcomponent to access the id in the host.
 * @example
 * constructor(
 *   @Host()
 *   @Optional() // use optional to mal the service optional
 *   private mousePositionProjectionService: MousePositionProjectionService
 * )
 * ngOnInit() {
 *   // Get the current map id
 *   const mousePositionProjection = this.mousePositionProjectionService.get();
 * }
 */
@Injectable({
  providedIn: 'root'
})
export class MousePositionProjectionService {

  /**
   * The current id
   */
  private mousePositionProjection: ReplaySubject<ProjectionLike> = new ReplaySubject<ProjectionLike>( 1 );

  constructor(
    private environment: EnvCoreService
  ) {
    this.mousePositionProjection.next(this.environment.defaultMousePositionProjection );
  }

  get(): Observable<ProjectionLike> {
    return this.mousePositionProjection;
  }

  set( projParam: ProjectionLike ) {
    this.mousePositionProjection.next(projParam);
  }

}
