import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { DrawLayerService } from '../../../../services/draw-layer.service';

@Component({
  selector: 'alk-export-draws',
  templateUrl: './export-draws.component.html',
  styleUrls: ['./export-draws.component.scss']
})
export class ExportDrawsComponent implements OnInit, OnDestroy {

  mapToolSubscription: Subscription;

  constructor(
    private coreService: VisualiseurCoreService,
    private drawLayerService: DrawLayerService
  ) { }

  ngOnInit() {
    this.mapToolSubscription = this.coreService.getMapTool().subscribe(
      tool => {
        if (tool === 'exportDrawsShp' || tool === 'exportDrawsKml') {
          this.drawLayerService.exportAnnotations(tool);
        }
      }, error => console.error(error)
    );
  }

  ngOnDestroy() {
    if (this.mapToolSubscription) {
      this.mapToolSubscription.unsubscribe();
    }
  }
}
