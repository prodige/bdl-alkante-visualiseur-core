import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportDrawsComponent } from './export-draws.component';

describe('ExportDrawsComponent', () => {
  let component: ExportDrawsComponent;
  let fixture: ComponentFixture<ExportDrawsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportDrawsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportDrawsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
