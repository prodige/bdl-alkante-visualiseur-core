import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnotatingModalComponent } from './annotating-modal.component';

describe('AnnotatingModalComponent', () => {
  let component: AnnotatingModalComponent;
  let fixture: ComponentFixture<AnnotatingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnotatingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnotatingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
