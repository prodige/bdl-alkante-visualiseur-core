import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { FeatureCollection } from '../../../../../lib/models/context';

@Component({
  selector: 'alk-annotating-modal',
  templateUrl: './annotating-modal.component.html',
  styleUrls: ['./annotating-modal.component.scss']
})
export class AnnotatingModalComponent implements OnInit, OnDestroy {

  toolName: string;
  annotatingOptionSubscription: Subscription;
  annotatingOption: string;
  annotationSnappingSubscription: Subscription;
  annotatingSnapping: boolean;
  contextSubscription: Subscription;
  tools: FeatureCollection['properties']['extension']['Tools'];
  measuringOption: string;
  type = null;

  mapToolSubscription: Subscription;
  mapTool: string;

  constructor(
    public activeModal: NgbActiveModal,
    public coreService: VisualiseurCoreService) { }

  ngOnInit() {
    this.mapToolSubscription = this.coreService.getMapTool().subscribe(tool => {
      this.mapTool = tool;
    }, error => console.error(error));
    this.contextSubscription = this.coreService.getContext().subscribe(context => {
      if(!context){
        return false;
      }
      this.tools = context.properties.extension.Tools;
      if (this.tools.Annotation.Attribut) {
        this.type = 'modifyAttributes';
      } else {
        this.type = 'modifyDraw';
      }
      this.coreService.setAnnotatingOption(this.type);
    }, error => console.error(error));
    this.annotatingOptionSubscription = this.coreService.getAnnotatingOption().subscribe(option => {
      this.annotatingOption = option;
    }, error => console.error(error));
    this.annotationSnappingSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
      this.annotatingSnapping = snap;
    }, error => console.error(error));
  }

  ngOnDestroy(): void {
    if (this.mapToolSubscription) {
      this.mapToolSubscription.unsubscribe();
    }
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.annotatingOptionSubscription) {
      this.annotatingOptionSubscription.unsubscribe();
    }
    if (this.annotationSnappingSubscription) {
      this.annotationSnappingSubscription.unsubscribe();
    }
  }

  setToolName(name: string) {
    this.toolName = name;
  }

  setType(typeSelected: string) {
    this.coreService.setAnnotatingOption(typeSelected);
  }
}
