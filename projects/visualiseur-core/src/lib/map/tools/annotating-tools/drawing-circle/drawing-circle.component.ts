import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { MapService } from './../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { DrawLayerService } from './../../../../services/draw-layer.service';

import Map from 'ol/Map';
import { Draw } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-drawing-circle',
  templateUrl: './drawing-circle.component.html',
  styleUrls: ['./drawing-circle.component.scss']
})
export class DrawingCircleComponent implements OnInit, OnDestroy {

  map: Map;
  drawLayer: VectorLayer<VectorSource<Geometry>>;
  drawCircle: Draw;

  drawLayerSubscription: Subscription;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.drawLayerService.generateNewDrawLayer(this.mapIdService.getId() || 'main');
    this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          this.drawLayer = vectorLayer;
          this.drawLayer.setMap(this.map);

          this.drawCircle = new Draw({
            type: 'Circle',
            source: this.drawLayer.getSource(),
            style: this.drawLayerService.getDefaultDrawStyle()
          });

          this.drawCircle.on('drawstart', (event) => {
          });
          this.drawCircle.on('drawend', (event) => {
            const feature = event.feature;
            this.drawLayerService.setHasFeature(true);
          });

          this.drawCircle.setActive(true);
          this.map.addInteraction(this.drawCircle);
        } else if ( this.drawCircle ) {
          this.map.removeInteraction(this.drawCircle);
          this.drawCircle.setActive(false);
          this.drawCircle = null;
        }
      }
    );
  }

  ngOnDestroy() {
    if ( this.drawLayerSubscription ) {
      this.drawLayerSubscription.unsubscribe();
    }

    if ( this.drawCircle ) {
      this.drawCircle.setActive(false);
      this.map.removeInteraction(this.drawCircle);
    }
  }

}
