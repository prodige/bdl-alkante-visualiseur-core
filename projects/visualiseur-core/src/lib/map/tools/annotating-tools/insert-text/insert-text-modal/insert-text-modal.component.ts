import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Text, Fill, Style, Stroke, Circle } from 'ol/style';
import VectorLayer from 'ol/layer/Vector';
import { Select } from 'ol/interaction';
import Feature from 'ol/Feature';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-insert-text-modal',
  templateUrl: './insert-text-modal.component.html',
  styleUrls: ['./insert-text-modal.component.scss']
})
export class InsertTextModalComponent implements OnInit {

  color: string;
  sizes = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
  fonts = ['Times', 'Courier', 'Verdana', 'Comic', 'Arial'];
  insertForm = new FormGroup({
    font: new FormControl('', Validators.required),
    size: new FormControl('', Validators.required),
    color: new FormControl(''),
    text: new FormControl('', Validators.required)
  });
  selectFeature: Select;
  selectedFeature: Feature<any>;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.selectedFeature = this.selectFeature.getFeatures().item(0);
    this.selectFeature.getFeatures().clear();
    this.color = 'rgba(0, 0, 0, 1)';
    if ( this.selectedFeature ) {
      this.insertForm.get('font').setValue(this.selectedFeature.get('font') || this.fonts[0]);
      this.insertForm.get('size').setValue(this.selectedFeature.get('size') || this.sizes[0]);
      this.color = this.selectedFeature.get('color') ? this.selectedFeature.get('color') : 'rgba(0, 0, 0, 1)';
      this.insertForm.get('color').setValue(this.selectedFeature.get('color') ? this.selectedFeature.get('color') : 'rgba(0, 0, 0, 1)');
      this.insertForm.get('text').setValue(this.selectedFeature.get('text'));
    }
  }

  closing(mode: string) {
    this.activeModal.close(true);
    if ( mode === 'save' ) {
      const params = {
        text: this.insertForm.get('text').value,
        textAlign: 'center',
        textBaseline: 'middle',
        font: this.insertForm.get('size').value + 'px ' + this.insertForm.get('font').value,
        fill: new Fill({
          color: this.color
        })
      }
      
      if(this.selectedFeature.getStyle() instanceof Array){
        (this.selectedFeature.getStyle() as Style[]).forEach(style => {
          style.setText(new Text(params));
        })
      }
      else {
        (this.selectedFeature.getStyle() as Style).setText(  new Text(params));
      }
     

      this.selectedFeature.setProperties({
        font: this.insertForm.get('font').value,
        size: this.insertForm.get('size').value,
        color: this.color,
        text: this.insertForm.get('text').value
      });

    }
  }

  setSelectFeature(selectFeature: Select): void {
    this.selectFeature = selectFeature;
  }

  setVectorLayer(vectorLayer: VectorLayer<VectorSource<Geometry>>): void {
    this.vectorLayer = vectorLayer;
  }

  setStyleText(police: string, taille: number, color: string, texte: string) {
    this.insertForm.get('font').setValue(this.selectedFeature.get('font'));
    this.insertForm.get('size').setValue(this.selectedFeature.get('size'));
    this.insertForm.get('color').setValue(this.color);
    this.insertForm.get('text').setValue(this.selectedFeature.get('text'));
  }

}
