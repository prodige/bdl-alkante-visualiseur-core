import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertTextModalComponent } from './insert-text-modal.component';

describe('InsertTextModalComponent', () => {
  let component: InsertTextModalComponent;
  let fixture: ComponentFixture<InsertTextModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertTextModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertTextModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
