import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { MapService } from './../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { DrawLayerService } from './../../../../services/draw-layer.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InsertTextModalComponent } from './insert-text-modal/insert-text-modal.component';


import Map from 'ol/Map';
import { Select } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-insert-text',
  templateUrl: './insert-text.component.html',
  styleUrls: ['./insert-text.component.scss']
})
export class InsertTextComponent implements OnInit, OnDestroy {

  map: Map;
  selectFeature: Select;
  drawLayerSubscription: Subscription;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          this.vectorLayer = vectorLayer;
          this.selectFeature = new Select({
            layers: [vectorLayer],
            style: this.drawLayerService.getDefaultSelectStyle()
          });
          this.selectFeature.setActive(true);
          this.map.addInteraction(this.selectFeature);

          this.selectFeature.on('select', (event) => {
            if ( event.selected.length > 0 ) {
              this.openInsertTextModal();
            } else {
              this.selectFeature.getFeatures().clear();
            }
          });
        } else if ( this.selectFeature ) {
          this.map.removeInteraction(this.selectFeature);
          this.selectFeature.setActive(false);
          this.selectFeature = null;
        }
      }
    );

  }

  ngOnDestroy() {
    if ( this.drawLayerSubscription ) {
      this.drawLayerSubscription.unsubscribe();
    }

    if ( this.selectFeature ) {
      this.selectFeature.setActive(false);
      this.map.removeInteraction(this.selectFeature);
    }
  }

  openInsertTextModal() {
    const modalRef = this.modalService.open(InsertTextModalComponent, {
      size: 'lg',
      centered: true,
      container: '#modals'
    });
    if (this.selectFeature && modalRef.componentInstance.setSelectFeature) {
      modalRef.componentInstance.setSelectFeature(this.selectFeature);
      modalRef.componentInstance.setVectorLayer(this.vectorLayer);
    }
  }

}
