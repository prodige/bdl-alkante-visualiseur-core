import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import VectorLayer from 'ol/layer/Vector';
import { Select } from 'ol/interaction';
import { Feature } from 'ol/';

import LayerGroup from 'ol/layer/Group';
import Map from 'ol/Map';
import { Style } from 'ol/style';
import { MapService } from '../../../../map.service';
import { MapIdService } from '../../../../map-id.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DrawLayerService } from '../../../../../../lib/services/draw-layer.service';
import { VisualiseurCoreService } from '../../../../../../lib/visualiseur-core.service';
import { Subscription } from 'rxjs';
import GeoJSON from 'ol/format/GeoJSON';
import { GenerateStyleDrawsModalComponent } from '../../style-draws/style-draws-modal/generate-style-draws-modal/generate-style-draws-modal.component';
import { FeatureCollection } from '../../../../../models/context';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-buffers-modal',
  templateUrl: './buffers-modal.component.html',
  styleUrls: ['./buffers-modal.component.scss']
})
export class BuffersModalComponent implements OnInit, OnDestroy {

  map: Map;
  mapId: string;

  subscriptions: Subscription = new Subscription();

  polygonSubscription: Subscription;
  contextSubscription: Subscription;
  context: FeatureCollection = null;
  rootLayerGroupSubscription: Subscription;
  color: string;
  bufferFeatures: Array<Feature<any>>;
  selectFeature: Select;
  selectedFeature: Feature<Geometry>;
  selectedFeatures: Array<Feature<Geometry>> = null;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  bufferForm = new FormGroup({
    name: new FormControl('', Validators.required),
    radius: new FormControl(
      {
        value: '',
        disabled: this.context && this.context.properties.extension.Tools.Annotation.BufferGeneral
      }, Validators.required)
  });
  polygonCanvas = null;
  rootLayerGroup: LayerGroup;

  constructor(
    private drawLayerService: DrawLayerService,
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal) { }

  ngOnInit() {
    
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.subscriptions.add(this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;
    }));
    
    this.bufferFeatures = [];
    this.selectedFeature = this.selectFeature ? this.selectFeature.getFeatures().item(0) : null;
    this.rootLayerGroupSubscription = this.coreService.getRootLayerGroup().subscribe(
      (rootLayerGroup: LayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      }
    );
    this.polygonSubscription = this.drawLayerService.getPolygonStyleOptions().subscribe(polygonOptions => {
      this.polygonCanvas = Object.assign({}, (polygonOptions as Style));
      if (this.polygonCanvas.settedStyle.pattern) {
        this.drawLayerService.changeColInUri(
          [this.polygonCanvas.settedStyle.pattern],
          this.polygonCanvas.settedStyle.color
          );
      }
    });
    this.contextSubscription = this.coreService.getContext().subscribe(context => {
      if(!context){
        return false;
      }
      this.context = context;
      if (context.properties.extension.Tools.Annotation.BufferGeneral) {
        this.bufferForm.get('radius').setValue(context.properties.extension.Tools.Annotation.BufferGeneralDistance);
      }
    }, error => console.error(error));
    this.color = '#ffffff';
  }

  ngOnDestroy(): void {
    if (this.rootLayerGroupSubscription) {
      this.rootLayerGroupSubscription.unsubscribe();
    }
    if (this.polygonSubscription) {
      this.polygonSubscription.unsubscribe();
    }
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
  }

  closing(mode: string) {
    this.activeModal.close(true);
    if ( mode === 'save' ) {
      this.bufferFeatures = [];
      if (this.selectedFeature) {
        this.drawLayerService.createBuffer(this.selectedFeature, this.polygonCanvas, this.bufferForm, this.bufferFeatures, this.map);
        this.selectFeature.getFeatures().clear();
      } else {
        if (this.selectedFeatures[0]) {
          this.selectedFeatures.forEach((dataFeature: any) => {
            const feature = (new GeoJSON()).readFeature(dataFeature.feature as Feature<Geometry>);
            for (const [key, value] of Object.entries(dataFeature)) {
              if (key !== 'feature') {
                feature.set(key, value);
              }
            }
            this.selectedFeature = feature;
            this.drawLayerService.createBuffer(this.selectedFeature, this.polygonCanvas, this.bufferForm, this.bufferFeatures, this.map);
          });
        } else {
          this.selectedFeatures.forEach(dataFeature => {
            this.selectedFeature = dataFeature;
            this.drawLayerService.createBuffer(this.selectedFeature, this.polygonCanvas, this.bufferForm, this.bufferFeatures, this.map);
          });
        }
      }
      this.drawLayerService.saveBuffer(this.map, this.bufferForm, this.bufferFeatures);
    }
  }

  generateStyle() {
    const modalRef = this.modalService.open(GenerateStyleDrawsModalComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'window-map-tool',
      container: '#modals',
      backdropClass: 'backdrop-tool',
      keyboard: false
    });
    modalRef.componentInstance.geometryType = 'Polygon';
    modalRef.componentInstance.isBufferCreating = true;
    if (this.polygonCanvas.settedStyle) {
      modalRef.componentInstance.selectPattern(this.polygonCanvas.settedStyle.pattern);
      modalRef.componentInstance.color = this.polygonCanvas.settedStyle.color;
      modalRef.componentInstance.styleForm.get('color').setValue(this.polygonCanvas.settedStyle.color);
      modalRef.componentInstance.background = this.polygonCanvas.settedStyle.background;
      modalRef.componentInstance.styleForm.get('background').setValue(this.polygonCanvas.settedStyle.background);
      modalRef.componentInstance.styleForm.get('size').setValue(this.polygonCanvas.settedStyle.size);
    }
    if (this.selectedFeature) {
      if (this.selectFeature && modalRef.componentInstance.setSelectFeature) {
        modalRef.componentInstance.setSelectFeature(this.selectFeature);
        modalRef.componentInstance.setVectorLayer(this.vectorLayer);
      }
    }
    modalRef.result.then(close => {
      if (close.pattern) {
        this.polygonCanvas.settedStyle = close;
      }
    });
  }

  setSelectFeature(selectFeature: Select): void {
    this.selectFeature = selectFeature;
  }

  setSelectedFeatures(features: Array<Feature<Geometry>>) {
    this.selectedFeatures = features;
  }

  setVectorLayer(vectorLayer: VectorLayer<VectorSource<Geometry>>): void {
    this.vectorLayer = vectorLayer;
  }
}
