import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuffersModalComponent } from './buffers-modal.component';

describe('BuffersModalComponent', () => {
  let component: BuffersModalComponent;
  let fixture: ComponentFixture<BuffersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuffersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuffersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
