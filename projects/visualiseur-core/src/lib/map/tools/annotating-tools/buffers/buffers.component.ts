import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { MapService } from './../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { DrawLayerService } from './../../../../services/draw-layer.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BuffersModalComponent } from './buffers-modal/buffers-modal.component';

import { Feature } from 'ol/';
import Map from 'ol/Map';
import { Style } from 'ol/style';
import { Select } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import LayerGroup from 'ol/layer/Group';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';

@Component({
  selector: 'alk-buffers',
  templateUrl: './buffers.component.html',
  styleUrls: ['./buffers.component.scss']
})
export class BuffersComponent implements OnInit, OnDestroy {

  map: Map;
  mapId: string;
  selectFeature: Select;

  subscriptions: Subscription = new Subscription();

  mapToolSubscription: Subscription;
  selectedTool: string;

  drawLayerSubscription: Subscription;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;

  bufferForm = new FormGroup({
    name: new FormControl('', Validators.required),
    radius: new FormControl('', Validators.required)
  });

  bufferGeneralNameIndexSubscription: Subscription;
  bufferGeneralNameIndex: number = 0;

  contextSubscription: Subscription;
  a=VectorSource
  polygonSubscription: Subscription;
  polygonCanvas = null;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

    this.subscriptions.add(this.drawLayerService.getBufferGeneralNameIndex().subscribe(index => {
      this.bufferGeneralNameIndex = index;
      this.bufferForm.get('name').setValue('zone tampon globale ' + this.bufferGeneralNameIndex.toString());
    }));

    this.subscriptions.add(this.coreService.getContext().subscribe(context => {
      if(!context){
        return false;
      }
      this.bufferForm.get('radius').setValue(context.properties.extension.Tools.Annotation.BufferGeneralDistance);
    }));

    this.mapId = (this.mapIdService && this.mapIdService.getId) ? this.mapIdService.getId() : 'visualiseur' || 'main';
    this.subscriptions.add(this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;

      if (this.map) {
        this.subscriptions.add(this.coreService.getMapTool().subscribe(
          tool => {
            this.selectedTool = tool;
            this.drawLayerService.generateNewBufferLayerGroup(this.mapId);
            this.subscriptions.add(this.drawLayerService.getDrawLayer().subscribe(
              ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
                if ( vectorLayer ) {
    
                  if (this.selectedTool === 'buffers') {
                    this.vectorLayer = vectorLayer;
                    this.selectFeature = new Select({
                      layers: [vectorLayer],
                      style: this.drawLayerService.getDefaultDrawStyle()
                    });
                    this.selectFeature.setActive(true);
                    this.map.addInteraction(this.selectFeature);
    
                    this.selectFeature.on('select', (event) => {
                      if ( event.selected.length > 0 ) {
                        this.openBuffersModal();
                      } else {
                        this.selectFeature.getFeatures().clear();
                      }
                    });
                  } else if (this.selectedTool === 'buffersGeneral') {
                    this.polygonSubscription = this.drawLayerService.getPolygonStyleOptions().subscribe(polygonOptions => {
                      const bufferFeatures = [];
                      this.polygonCanvas = Object.assign({}, (polygonOptions as Style));
                      if (this.polygonCanvas.settedStyle.pattern) {
                        this.drawLayerService.changeColInUri(
                          [this.polygonCanvas.settedStyle.pattern],
                          this.polygonCanvas.settedStyle.color
                          );
                      }
                      const selectedFeatures = vectorLayer.getSource().getFeatures();
                      selectedFeatures.forEach(dataFeature => {
                        this.drawLayerService.createBuffer(dataFeature, this.polygonCanvas, this.bufferForm, bufferFeatures, this.map);
                      });
                      this.drawLayerService.saveBuffer(this.map, this.bufferForm, bufferFeatures);
                      this.drawLayerService.incrementBufferGeneralNameIndex(this.bufferGeneralNameIndex);
                    }, error => console.error(error));
                  }
    
                } else if ( this.selectFeature ) {
                  this.map.removeInteraction(this.selectFeature);
                  this.selectFeature.setActive(false);
                  this.selectFeature = null;
                }
              }
            ));
          }, error => console.error(error)
        ));
      }
    }));
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    if ( this.mapToolSubscription ) {
      this.mapToolSubscription.unsubscribe();
    }
    if ( this.drawLayerSubscription ) {
      this.drawLayerSubscription.unsubscribe();
    }

    if ( this.selectFeature ) {
      this.selectFeature.setActive(false);
      this.map.removeInteraction(this.selectFeature);
    }
  }

  openBuffersModal(selectedFeatures: Array<Feature<Geometry>> = null) {
    const modalRef = this.modalService.open(BuffersModalComponent, {
      size: 'lg',
      centered: true,
      container: '#modals',
      keyboard: false,
      windowClass: 'window-map-tool'
    });
    if (this.selectFeature) {
      modalRef.componentInstance.setSelectFeature(this.selectFeature);
    } else if (selectedFeatures) {
      modalRef.componentInstance.setSelectedFeatures(selectedFeatures);
    }
    modalRef.componentInstance.mapId = this.mapId;
    modalRef.componentInstance.setVectorLayer(this.vectorLayer);
  }

}
