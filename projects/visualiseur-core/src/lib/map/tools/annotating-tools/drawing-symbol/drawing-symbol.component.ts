import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { MapService } from './../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { DrawLayerService } from './../../../../services/draw-layer.service';


import Map from 'ol/Map';
import { Draw, Snap } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import { Style } from 'ol/style';
import { Feature } from 'ol/';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResultModalFicheComponent } from '../../../../interrogating-results/result-modal-fiche/result-modal-fiche.component';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';
import { FeatureCollection } from '../../../../models/context';

@Component({
  selector: 'alk-drawing-symbol',
  templateUrl: './drawing-symbol.component.html',
  styleUrls: ['./drawing-symbol.component.scss']
})
export class DrawingSymbolComponent implements OnInit, OnDestroy {

  map: Map;
  drawLayer: VectorLayer<VectorSource<Geometry>>;
  drawSymbol: Draw;

  contextSubscription: Subscription;
  coreSubscription: Subscription;

  snappingVectorSubscription: Subscription;
  snappingVectorLayer: VectorLayer<VectorSource<Geometry>>;
  snappingInteractionLayer: Snap = null;

  drawLayerSubscription: Subscription;
  pointStyleSubscription: Subscription;
  drawStyle: Style;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private modalService: NgbModal,
    private coreService: VisualiseurCoreService
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.pointStyleSubscription = this.drawLayerService.getPointStyle().subscribe(point => {
      if (point) {
        this.drawStyle = point.clone();
      }
    });

    this.drawLayerService.generateNewDrawLayer(this.mapIdService.getId() || 'main');
    this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          this.drawLayer = vectorLayer;
          this.drawLayer.setMap(this.map);

          this.drawSymbol = new Draw({
            type: 'Point',
            source: this.drawLayer.getSource(),
            style: this.drawStyle ? this.drawStyle : null
          });

          this.drawSymbol.on('drawstart', (event) => {
            this.coreSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
              if (snap) {
                this.drawLayerService.deleteSnappingLayer();
                this.drawLayerService.generateSnappingLayer( ( this.drawLayer.getSource().getFeaturesCollection() as unknown as FeatureCollection), 'Point', true);
                this.snappingVectorSubscription = this.drawLayerService.getSnappingLayer().subscribe(
                  (vectorLayer: VectorLayer<VectorSource<Geometry>>) => {
                    if (vectorLayer) {
                      this.snappingVectorLayer = vectorLayer;
                      this.snappingVectorLayer.setMap(this.map);

                      if (this.snappingInteractionLayer) {
                        this.map.removeInteraction(this.snappingInteractionLayer);
                      }
                      this.snappingInteractionLayer = new Snap({
                        pixelTolerance: 10,
                        source: this.snappingVectorLayer.getSource()
                      });
                      this.snappingInteractionLayer.setActive(true);
                      this.map.addInteraction(this.snappingInteractionLayer);
                    }
                  }
                );
              } else {
                if (this.snappingInteractionLayer) {
                  this.snappingInteractionLayer.setActive(false);
                  this.snappingInteractionLayer = null;
                  this.map.removeInteraction(this.snappingInteractionLayer);
                }
                this.drawLayerService.deleteSnappingLayer();
              }
            });
          });

          this.drawSymbol.on('drawend', (event) => {
            const newFeature = event.feature;
            if ( newFeature ) {
              (newFeature as Feature<any>).setStyle(this.drawStyle ? this.drawStyle : null);
              this.drawLayerService.setHasFeature(true);
              this.coreSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
                if (snap) {
                  this.drawLayerService.deleteSnappingLayer();
                  this.drawLayerService.generateSnappingLayer( ( this.drawLayer.getSource().getFeaturesCollection() as unknown as FeatureCollection), 'Point', true);
                }
              });

              this.contextSubscription = this.coreService.getContext().subscribe(context => {
                if (context && context.properties.extension.Tools.Annotation.Attribut &&
                  context.properties.extension.Tools.Annotation.AttributName.length > 0) {

                  const currentLayerResults = {
                    data: {
                      feature: newFeature
                    },
                    layer: this.drawLayer,
                    layerName: this.drawLayer.get('name'),
                    layerQueryFields: context.properties.extension.Tools.Annotation.AttributName,
                    nbResults: 1
                  };

                  const modalRef = this.modalService.open(ResultModalFicheComponent, {
                    container: '#modals',
                    centered: true,
                    size: 'lg',
                    backdrop: 'static'
                  });

                  modalRef.componentInstance.editing = true;
                  modalRef.componentInstance.currentLayerResults = currentLayerResults;
                  modalRef.componentInstance.currentSelectedFeature = newFeature.getProperties();
                  modalRef.componentInstance.modalReady = true;

                }
              });
            }
          });

          this.drawSymbol.setActive(true);
          this.map.addInteraction(this.drawSymbol);
        } else if ( this.drawSymbol ) {
          this.map.removeInteraction(this.drawSymbol);
          this.drawSymbol.setActive(false);
          this.drawSymbol = null;
        }
      }
    );
  }

  ngOnDestroy() {
    if ( this.drawLayerSubscription ) {
      this.drawLayerSubscription.unsubscribe();
    }
    if (this.coreSubscription) {
      this.coreSubscription.unsubscribe();
    }
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if ( this.pointStyleSubscription ) {
      this.pointStyleSubscription.unsubscribe();
    }
    if ( this.drawSymbol ) {
      this.drawSymbol.setActive(false);
      this.map.removeInteraction(this.drawSymbol);
    }
    if (this.snappingInteractionLayer) {
      this.snappingInteractionLayer.setActive(false);
      this.map.removeInteraction(this.snappingInteractionLayer);
    }
  }

}
