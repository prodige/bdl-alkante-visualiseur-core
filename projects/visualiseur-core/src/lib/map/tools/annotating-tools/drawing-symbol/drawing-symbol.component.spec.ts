import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingSymbolComponent } from './drawing-symbol.component';

describe('DrawingSymbolComponent', () => {
  let component: DrawingSymbolComponent;
  let fixture: ComponentFixture<DrawingSymbolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingSymbolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingSymbolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
