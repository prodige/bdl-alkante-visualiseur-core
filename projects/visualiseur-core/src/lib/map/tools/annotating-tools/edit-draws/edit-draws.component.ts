import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { MapService } from '../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { Subscription } from 'rxjs';

import Map from 'ol/Map';

@Component({
  selector: 'alk-edit-draws',
  templateUrl: './edit-draws.component.html',
  styleUrls: ['./edit-draws.component.scss']
})
export class EditDrawsComponent implements OnInit, OnDestroy {

  map: Map;
  annotatingOptionSubscription: Subscription;
  typeSelect: string;

  constructor(
    private mapService: MapService,
    public mapIdService: MapIdService,
    private coreService: VisualiseurCoreService
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.annotatingOptionSubscription = this.coreService.getAnnotatingOption().subscribe(opt => {
      if (this.typeSelect !== opt) {
        this.typeSelect = opt;
      }
    }, error => console.error(error));

  }

  ngOnDestroy() {
    if (this.annotatingOptionSubscription) {
      this.annotatingOptionSubscription.unsubscribe();
    }
  }

}
