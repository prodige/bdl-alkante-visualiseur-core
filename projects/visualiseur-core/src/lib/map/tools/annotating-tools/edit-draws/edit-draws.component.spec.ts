import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDrawsComponent } from './edit-draws.component';

describe('EditDrawsComponent', () => {
  let component: EditDrawsComponent;
  let fixture: ComponentFixture<EditDrawsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDrawsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDrawsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
