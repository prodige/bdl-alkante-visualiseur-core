import {
  Component,
  OnInit,
  Host,
  Optional,
  OnDestroy,
  Input,
  ViewChild,
} from "@angular/core";
import { MapService } from "../../../../map.service";
import { MapIdService } from "../../../../map-id.service";
import { VisualiseurCoreService } from "../../../../../visualiseur-core.service";
import { Subscription } from "rxjs";

import Map from "ol/Map";
import { Select, Modify, Draw, Snap, Translate } from "ol/interaction";
import VectorLayer from "ol/layer/Vector";
import { DrawLayerService } from "../../../../../services/draw-layer.service";
import { Feature } from "ol/";
import { AlertConfirmPromptModalComponent } from "../../../../../modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Geometry } from "ol/geom";
import VectorSource from "ol/source/Vector";

@Component({
  selector: "alk-remove-draw",
  templateUrl: "./remove-draw.component.html",
  styleUrls: ["./remove-draw.component.scss"],
})
export class RemoveDrawComponent implements OnInit, OnDestroy {
  map: Map;
  mapId: string;

  subscriptions: Subscription = new Subscription();
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  selectFeature: Select;

  @ViewChild(AlertConfirmPromptModalComponent, {read: null, static: true})
  confirmModal: AlertConfirmPromptModalComponent;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    public activeModal: NgbActiveModal,
    private mapIdService: MapIdService
  ) {}

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.subscriptions.add(
      this.mapService.getMapReady(this.mapId).subscribe((map) => {
        this.map = map;
        if (this.map) {
          this.subscriptions.add(
            this.drawLayerService
              .getDrawLayer()
              .subscribe((vectorLayer: VectorLayer<VectorSource<Geometry>>) => {
                if (vectorLayer) {
                  this.vectorLayer = vectorLayer;

                  if (this.selectFeature) {
                    this.map.removeInteraction(this.selectFeature);
                    this.selectFeature.setActive(false);
                    this.selectFeature = null;
                  }

                  this.selectFeature = new Select({
                    layers: [this.vectorLayer],
                  });
                  this.selectFeature.setActive(true);

                  this.map.addInteraction(this.selectFeature);
                  this.refreshInteraction(this.selectFeature);
                } else if (this.selectFeature) {
                  this.map.removeInteraction(this.selectFeature);
                  this.selectFeature.setActive(false);
                  this.selectFeature = null;
                }
              })
          );
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    if (this.selectFeature) {
      this.selectFeature.setActive(false);
      this.map.removeInteraction(this.selectFeature);
    }
    if (
      this.vectorLayer &&
      !this.vectorLayer.getSource().getFeatures().length
    ) {
      this.drawLayerService.setHasFeature(false);
    }
  }

  refreshInteraction(selectFeature: Select) {
    selectFeature.on("select", (event) => {
      if (event.selected.length > 0) {
        this.confirmModal.openModal().then((result) => {
          if (result) {
            this.vectorLayer.getSource().removeFeature(event.selected[0]);
            this.map.removeInteraction(selectFeature);
            this.selectFeature = new Select({ layers: [this.vectorLayer] });
            this.map.addInteraction(this.selectFeature);
            this.refreshInteraction(this.selectFeature);
          }
        });
      } else {
        selectFeature.getFeatures().clear();
      }
    });
  }
}
