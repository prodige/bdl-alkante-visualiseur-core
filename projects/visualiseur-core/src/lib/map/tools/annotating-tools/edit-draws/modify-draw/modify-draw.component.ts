import { Component, OnInit, Host, Optional, OnDestroy, Input } from '@angular/core';
import { MapService } from '../../../../map.service';
import { MapIdService } from '../../../../map-id.service';
import { Subscription } from 'rxjs';

import Map from 'ol/Map';
import { Select, Modify } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import { DrawLayerService } from '../../../../../services/draw-layer.service';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-modify-draw',
  templateUrl: './modify-draw.component.html',
  styleUrls: ['./modify-draw.component.scss']
})
export class ModifyDrawComponent implements OnInit, OnDestroy {

  map: Map;
  mapId: string;

  subscriptions: Subscription = new Subscription();
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  selectFeature: Select;
  modify: Modify;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService) { }

  ngOnInit() {
    
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.subscriptions.add(this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;
      if (this.map) {
        this.subscriptions.add(this.drawLayerService.getDrawLayer().subscribe(
          ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
            if ( vectorLayer ) {

              this.vectorLayer = vectorLayer;
              this.selectFeature = new Select({
                layers: [vectorLayer],
                style: this.drawLayerService.getDefaultSelectStyle()
              });
              this.selectFeature.setActive(true);
              this.map.addInteraction(this.selectFeature);

              this.selectFeature.on('select', (event) => {
                if (this.modify) {
                  this.modify.setActive(false);
                  this.map.removeInteraction(this.modify);
                }
                if ( event.selected.length > 0 ) {
                  this.modify = new Modify({
                    features: this.selectFeature.getFeatures(),
                    style: this.drawLayerService.getDefaultDrawStyle()
                  });
                  this.map.addInteraction(this.modify);
                } else {
                  this.selectFeature.getFeatures().clear();
                }
              });

            } else if ( this.selectFeature ) {
              this.map.removeInteraction(this.selectFeature);
              this.selectFeature.setActive(false);
              this.selectFeature = null;
            }
          }
        ));
      }
    }));

  }

  ngOnDestroy() {
    if (this.modify) {
      this.modify.setActive(false);
      this.map.removeInteraction(this.modify);
    }
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    if (this.selectFeature) {
      this.selectFeature.setActive(false);
      this.map.removeInteraction(this.selectFeature);
    }
  }

}
