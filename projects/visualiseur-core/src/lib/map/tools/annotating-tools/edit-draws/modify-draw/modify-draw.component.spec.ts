import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyDrawComponent } from './modify-draw.component';

describe('ModifyDrawComponent', () => {
  let component: ModifyDrawComponent;
  let fixture: ComponentFixture<ModifyDrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyDrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyDrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
