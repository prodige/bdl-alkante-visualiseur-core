import { Component, OnInit, Host, Optional, OnDestroy, Input } from '@angular/core';
import { VisualiseurCoreService } from '../../../../../visualiseur-core.service';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResultModalFicheComponent } from '../../../../../interrogating-results/result-modal-fiche/result-modal-fiche.component';
import { MapService } from '../../../../map.service';
import { MapIdService } from '../../../../map-id.service';

import Map from 'ol/Map';
import { Select } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import { DrawLayerService } from '../../../../../services/draw-layer.service';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';

@Component({
  selector: 'alk-attributes-draw',
  templateUrl: './attributes-draw.component.html',
  styleUrls: ['./attributes-draw.component.scss']
})
export class AttributesDrawComponent implements OnInit, OnDestroy {

  map: Map;
  mapId: string;

  contextSubscription: Subscription = new Subscription();
  drawLayerSubscription: Subscription = new Subscription();
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  selectFeature: Select;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private modalService: NgbModal,
    private coreService: VisualiseurCoreService
  ) { }

  ngOnInit() {
    
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;
    });

    this.drawLayerSubscription.add(this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          this.vectorLayer = vectorLayer;
          this.selectFeature = new Select({
            layers: [vectorLayer],
            style: this.drawLayerService.getDefaultSelectStyle()
          });
          this.selectFeature.setActive(true);
          this.map.addInteraction(this.selectFeature);

          this.selectFeature.on('select', (event) => {
            this.contextSubscription.add(this.coreService.getContext().subscribe(context => {
              if (context && context.properties.extension.Tools.Annotation.Attribut) {

                const currentLayerResults = {
                  data: {
                    feature: event.selected[0]
                  },
                  layer: this.vectorLayer,
                  layerName: this.vectorLayer.get('name'),
                  layerQueryFields: context.properties.extension.Tools.Annotation.AttributName,
                  nbResults: 1
                };

                const modalRef = this.modalService.open(ResultModalFicheComponent, {
                  container: '#modals',
                  centered: true,
                  size: 'lg',
                  backdrop: 'static'
                });

                modalRef.componentInstance.editing = true;
                modalRef.componentInstance.currentLayerResults = currentLayerResults;
                modalRef.componentInstance.currentSelectedFeature = event.selected[0].getProperties();
                modalRef.componentInstance.modalReady = true;
              }
            }));
          });

        } else if ( this.selectFeature ) {
          this.map.removeInteraction(this.selectFeature);
          this.selectFeature.setActive(false);
          this.selectFeature = null;
        }
      }
    ));
  }

  ngOnDestroy() {
    if (this.drawLayerSubscription) {
      this.drawLayerSubscription.unsubscribe();
    }
    if (this.selectFeature) {
      this.selectFeature.setActive(false);
      this.map.removeInteraction(this.selectFeature);
    }
  }

}
