import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveDrawComponent } from './move-draw.component';

describe('MoveDrawComponent', () => {
  let component: MoveDrawComponent;
  let fixture: ComponentFixture<MoveDrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveDrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveDrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
