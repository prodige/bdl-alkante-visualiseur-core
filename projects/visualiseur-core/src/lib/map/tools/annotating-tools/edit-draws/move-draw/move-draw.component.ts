import { Component, OnInit, Host, Optional, OnDestroy, Input } from '@angular/core';
import { MapService } from '../../../../map.service';
import { MapIdService } from '../../../../map-id.service';
import { VisualiseurCoreService } from '../../../../../visualiseur-core.service';
import { Subscription } from 'rxjs';

import Map from 'ol/Map';
import { Select, Modify, Draw, Snap, Translate } from 'ol/interaction';
import { Options as OptionsTranslate } from 'ol/interaction/Translate';
import VectorLayer from 'ol/layer/Vector';
import { DrawLayerService } from '../../../../../services/draw-layer.service';
import { Feature } from 'ol/';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-move-draw',
  templateUrl: './move-draw.component.html',
  styleUrls: ['./move-draw.component.scss']
})
export class MoveDrawComponent implements OnInit, OnDestroy {

  map: Map;
  mapId: string;

  subscriptions: Subscription = new Subscription();
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  selectFeature: Select;
  translate: Translate;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService) { }

  ngOnInit() {
    
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.subscriptions.add(this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;
      if (this.map) {
        this.subscriptions.add(this.drawLayerService.getDrawLayer().subscribe(
          ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
            if ( vectorLayer ) {
              this.vectorLayer = vectorLayer;
              this.translate = new Translate({
                layers: [vectorLayer],
                hitTolerance: 0,
                style: this.drawLayerService.getDefaultSelectStyle()
              } as OptionsTranslate);
              this.map.addInteraction(this.translate);
    
             
            } else if ( this.selectFeature ) {
            }
          }
        ));
      }
    }));

  }

  ngOnDestroy() {
    if (this.translate) {
      this.translate.setActive(false);
      this.map.removeInteraction(this.translate);
    }
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    if (this.selectFeature) {
      this.selectFeature.setActive(false);
      this.map.removeInteraction(this.selectFeature);
    }
  }

}
