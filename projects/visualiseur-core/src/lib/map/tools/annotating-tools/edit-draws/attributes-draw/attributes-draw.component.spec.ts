import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributesDrawComponent } from './attributes-draw.component';

describe('AttributesDrawComponent', () => {
  let component: AttributesDrawComponent;
  let fixture: ComponentFixture<AttributesDrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributesDrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributesDrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
