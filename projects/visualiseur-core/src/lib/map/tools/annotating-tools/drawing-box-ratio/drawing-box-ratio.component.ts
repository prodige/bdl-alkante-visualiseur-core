import Feature from "ol/Feature";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Polygon, Geometry } from "ol/geom";
import { Component, OnInit, OnDestroy, Host, Optional } from "@angular/core";
import { Subscription, ReplaySubject } from "rxjs";

import { MapService } from "../../../map.service";
import { MapIdService } from "../../../map-id.service";
import { DrawLayerService } from "../../../../services/draw-layer.service";

import Map from "ol/Map";
import { Draw, Modify, Select, Translate } from "ol/interaction";
import VectorLayer from "ol/layer/Vector";
import { Style } from "ol/style";
import { Fill } from "ol/style";
import { Stroke } from "ol/style";
import VectorSource from "ol/source/Vector";

@Component({
  selector: "alk-drawing-box-ratio",
  templateUrl: "./drawing-box-ratio.component.html",
  styleUrls: ["./drawing-box-ratio.component.scss"],
  providers: [MapIdService],
})
export class DrawingBoxRatioComponent implements OnInit, OnDestroy {
  private mapId: string;
  private mapId$: ReplaySubject<string> = new ReplaySubject(null);
  private mapIdSubscription: Subscription;

  private white = [255, 255, 255, 1];
  private blue = [0, 153, 255, 1];
  private width = 3;

  map: Map;
  drawLayer: VectorLayer<VectorSource<Geometry>>;
  drawBoxRatio: Draw;
  translateBoxRatio: Translate;
  selectBoxRatio: Select;

  drawLayerSubscription: Subscription;
  polygonStyleSubscription: Subscription;
  drawStyle: Style;
  polygonStyleOptionsSubscription: Subscription;
  drawStyleOptions: Object;

  tempFeaturePrint: Feature<any>;

  mode: "draw" | "modify" | "remove";

  constructor(
    public mapService: MapService,
    public activeModal: NgbActiveModal,
    private mapIdService: MapIdService,
    private drawLayerService: DrawLayerService
  ) {}

  ngOnInit() {
    this.mapIdSubscription = this.mapId$.subscribe((newMapId) => {
      if (newMapId) {
        this.mapId = newMapId;
        this.mapService
          .getMapReady(this.mapIdService.getId() || this.mapId)
          .subscribe((map) => {
            this.map = map;
          });

        this.polygonStyleSubscription = this.drawLayerService
          .getPolygonStyle()
          .subscribe((polygon) => {
            if (polygon) {
              this.drawStyle = polygon;
            }
          });

        this.polygonStyleOptionsSubscription = this.drawLayerService
          .getPolygonStyleOptions()
          .subscribe((opt) => {
            if (opt) {
              this.drawStyleOptions = opt;
            }
          });

        this.drawLayerService.generateNewDrawLayer(this.mapId);
        this.drawLayerSubscription = this.drawLayerService
          .getDrawLayer()
          .subscribe((vectorLayer: VectorLayer<VectorSource<Geometry>>) => {
            if (vectorLayer) {
              this.drawLayer = vectorLayer;
              this.drawLayer.setMap(this.map);

              if (!this.mapService.drawnFeaturePrint) {
                // On doit générer la feature de base (Maximum ratio / emprise carte)
                this.tempFeaturePrint =
                  this.mapService.generateMaximalPrintingFeature(this.mapId);
                this.drawLayer.getSource().addFeature(this.tempFeaturePrint);
              } else {
                this.tempFeaturePrint = this.mapService.drawnFeaturePrint;
                this.mapService.drawnFeaturePrint = null;
                this.drawLayer.getSource().addFeature(this.tempFeaturePrint);
              }
            }
          });
      }
    });
  }

  public setMapId(mapId: string): void {
    this.mapId$.next(mapId);
  }

  public startDraw(): void {
    this.mode = "draw";
    if (!this.drawBoxRatio) {
      this.drawBoxRatio = new Draw({
        source: this.drawLayer.getSource(),
        type: "Circle",
        geometryFunction: (coordinates, geometry) => {
          const newCoordinates: Array<Array<number>> = [];
          const center = coordinates[0];
          const last = coordinates[1];
          const dx = center[0] - last[0];

          const x = dx;
          const y = dx; /*  * this.mapService.shapeRatioPrint */

          newCoordinates.push([center[0] + x, center[1] + y]);
          newCoordinates.push([center[0] - x, center[1] + y]);
          newCoordinates.push([center[0] - x, center[1] - y]);
          newCoordinates.push([center[0] + x, center[1] - y]);

          newCoordinates.push(newCoordinates[0].slice());

          if (!geometry) {
            geometry = new Polygon([newCoordinates]);
          } else {
            geometry.setCoordinates([newCoordinates]);
          }
          return geometry;
        },
        style: [
          new Style({
            stroke: new Stroke({
              color: this.white,
              width: this.width + 2,
            }),
          }),
          new Style({
            stroke: new Stroke({
              color: this.blue,
              width: this.width,
            }),
          }),
          new Style({
            fill: new Fill({
              color: "rgba(255, 255, 255, 0.85)",
            }),
          }),
        ],
      });

      this.drawBoxRatio.on("drawstart", (event) => {
        if (this.tempFeaturePrint) {
          this.drawLayer.getSource().removeFeature(this.tempFeaturePrint);
        }
      });
      this.drawBoxRatio.on("drawend", (event) => {
        this.tempFeaturePrint = event.feature;
        if (this.tempFeaturePrint) {
          this.tempFeaturePrint.setStyle(
            this.drawStyle ? this.drawStyle : null
          );
          this.tempFeaturePrint.setProperties(
            this.drawStyleOptions ? this.drawStyleOptions : null
          );
          this.drawLayerService.setHasFeature(true);

          // Une seule feature autorisée.
          // this.stopDraw();
        }
      });

      this.drawBoxRatio.setActive(true);
      this.map.addInteraction(this.drawBoxRatio);
    } else {
      this.drawBoxRatio.setActive(true);
    }

    if (this.translateBoxRatio) {
      this.translateBoxRatio.setActive(false);
    }
  }

  public stopDraw(): void {
    this.map.removeInteraction(this.drawBoxRatio);
    this.drawBoxRatio.setActive(false);

    // On arrete et on supprime
  }

  public startModify(): void {
    this.mode = "modify";
    if (!this.translateBoxRatio) {
      this.translateBoxRatio = new Translate({
        layers: [this.drawLayer],
      });

      this.map.addInteraction(this.translateBoxRatio);
    } else {
      this.translateBoxRatio.setActive(true);
    }

    if (this.drawBoxRatio) {
      this.drawBoxRatio.setActive(false);
    }
  }

  public clearLayer() {
    this.drawLayer.getSource().removeFeature(this.tempFeaturePrint);
    this.tempFeaturePrint = null;
    this.mapService.drawnFeaturePrint = null;
  }

  public endEditing(keepFeature: boolean): void {
    // On retourne dans l'état d'avant
    if (this.tempFeaturePrint) {
      this.drawLayer.getSource().removeFeature(this.tempFeaturePrint);
    }

    if (!keepFeature) {
      this.tempFeaturePrint = null;
    } else {
      if (this.tempFeaturePrint) {
        this.mapService.drawnFeaturePrint = this.tempFeaturePrint;
      }
    }

    if (this.drawLayer) {
      this.drawLayer = null;
    }

    this.activeModal.close();
  }

  ngOnDestroy() {
    if (this.drawLayerSubscription) {
      this.drawLayerSubscription.unsubscribe();
    }

    if (this.polygonStyleSubscription) {
      this.polygonStyleSubscription.unsubscribe();
    }

    if (this.polygonStyleOptionsSubscription) {
      this.polygonStyleOptionsSubscription.unsubscribe();
    }

    if (this.mapIdSubscription) {
      this.mapIdSubscription.unsubscribe();
    }

    if (this.drawBoxRatio) {
      this.drawBoxRatio.setActive(false);
      this.map.removeInteraction(this.drawBoxRatio);
    }
  }
}
