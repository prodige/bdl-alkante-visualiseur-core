import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingBoxRatioComponent } from './drawing-box-ratio.component';

describe('DrawingBoxRatioComponent', () => {
  let component: DrawingBoxRatioComponent;
  let fixture: ComponentFixture<DrawingBoxRatioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingBoxRatioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingBoxRatioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
