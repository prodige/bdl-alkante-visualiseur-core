import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleDrawsModalComponent } from './style-draws-modal.component';

describe('StyleDrawsModalComponent', () => {
  let component: StyleDrawsModalComponent;
  let fixture: ComponentFixture<StyleDrawsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleDrawsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleDrawsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
