import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import VectorLayer from 'ol/layer/Vector';
import { Select } from 'ol/interaction';
import { Feature } from 'ol/';
import { FormGroup } from '@angular/forms';
import { GenerateStyleDrawsModalComponent } from './generate-style-draws-modal/generate-style-draws-modal.component';
import { Style } from 'ol/style';
import { DrawLayerService } from '../../../../../services/draw-layer.service';
import { Subscription } from 'rxjs';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-style-draws-modal',
  templateUrl: './style-draws-modal.component.html',
  styleUrls: ['./style-draws-modal.component.scss']
})
export class StyleDrawsModalComponent implements OnInit, OnDestroy {

  styleForm = new FormGroup({});
  selectFeature: Select;
  selectedFeature: Feature<any>;
  selectedFeatureType = null;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  pointStyleSubscription: Subscription;
  pointImage = {
    icon: null,
    color: null,
    size: null
  };
  lineStringStyleSubscription: Subscription;
  lineStringCanvas = null;
  polygonSubscription: Subscription;
  polygonCanvas = null;

  constructor(
    private drawLayerService: DrawLayerService,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.pointStyleSubscription = this.drawLayerService.getPointStyleOptions().subscribe(pointOptions => {
      /* this.pointImage.icon = (point as Style).getImage().glyph_.name;
      this.pointImage.color = (point as Style).getImage().fill_.color_;
      this.pointImage.size = (point as Style).getImage().fontSize_; */
    });
    /* this.pointStyleSubscription = this.drawLayerService.getPointStyle().subscribe(point => {
      this.pointImage.icon = (point as Style).getImage().glyph_.name;
      this.pointImage.color = (point as Style).getImage().fill_.color_;
      this.pointImage.size = (point as Style).getImage().fontSize_;
    }); */
    this.lineStringStyleSubscription = this.drawLayerService.getLineStringStyleOptions().subscribe(lineStringOptions => {
      this.lineStringCanvas = (lineStringOptions as Style);
      if (this.lineStringCanvas.settedStyle.pattern) {
        this.drawLayerService.changeColInUri(
          [this.lineStringCanvas.settedStyle.pattern],
          this.lineStringCanvas.settedStyle.color
          );
      }
    });
    this.polygonSubscription = this.drawLayerService.getPolygonStyleOptions().subscribe(polygonOptions => {
      this.polygonCanvas = (polygonOptions as Style);
      if (this.polygonCanvas.settedStyle.pattern) {
        this.drawLayerService.changeColInUri(
          [this.polygonCanvas.settedStyle.pattern],
          this.polygonCanvas.settedStyle.color
          );
      }
    });
    if ( this.selectedFeature ) {
      this.selectedFeature = this.selectFeature.getFeatures().item(0);
      this.selectedFeatureType = this.selectedFeature.getGeometry().getType();
    }
  }

  ngOnDestroy() {
    if (this.selectFeature) {
      this.selectFeature.setActive(false);
      this.selectFeature = null;
    }
    if (this.pointStyleSubscription) {
      this.pointStyleSubscription.unsubscribe();
    }
    if (this.lineStringStyleSubscription) {
      this.lineStringStyleSubscription.unsubscribe();
    }
    if (this.polygonSubscription) {
      this.polygonSubscription.unsubscribe();
    }
  }

  generateStyle(geometryType: string = this.selectedFeatureType) {
    const modalRef = this.modalService.open(GenerateStyleDrawsModalComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'window-map-tool',
      container: '#modals',
      backdropClass: 'backdrop-tool',
      keyboard: false
    });
    modalRef.componentInstance.setGeometryType(geometryType);
    if (this.selectFeature && modalRef.componentInstance.setSelectFeature) {
      modalRef.componentInstance.setSelectFeature(this.selectFeature);
      modalRef.componentInstance.setVectorLayer(this.vectorLayer);
    } else if (!this.selectFeature) {
      if (geometryType === 'Point' && this.pointImage) {
        modalRef.componentInstance.styleForm.get('form').setValue(this.pointImage.icon);
        modalRef.componentInstance.color = this.pointImage.color;
        modalRef.componentInstance.styleForm.get('color').setValue(this.pointImage.color);
        modalRef.componentInstance.styleForm.get('size').setValue(this.pointImage.size.toString());
      } else if (geometryType === 'LineString' && this.lineStringCanvas) {
        modalRef.componentInstance.selectPattern(this.lineStringCanvas.settedStyle.pattern);
        modalRef.componentInstance.color = this.lineStringCanvas.settedStyle.color;
        modalRef.componentInstance.styleForm.get('color').setValue(this.lineStringCanvas.settedStyle.color);
        modalRef.componentInstance.background = this.lineStringCanvas.settedStyle.background;
        modalRef.componentInstance.styleForm.get('background').setValue(this.lineStringCanvas.settedStyle.background);
        modalRef.componentInstance.styleForm.get('size').setValue(this.lineStringCanvas.settedStyle.size);
      } else if (geometryType === 'Polygon' && this.polygonCanvas) {
        modalRef.componentInstance.selectPattern(this.polygonCanvas.settedStyle.pattern);
        modalRef.componentInstance.color = this.polygonCanvas.settedStyle.color;
        modalRef.componentInstance.styleForm.get('color').setValue(this.polygonCanvas.settedStyle.color);
        modalRef.componentInstance.background = this.polygonCanvas.settedStyle.background;
        modalRef.componentInstance.styleForm.get('background').setValue(this.polygonCanvas.settedStyle.background);
        modalRef.componentInstance.styleForm.get('size').setValue(this.polygonCanvas.settedStyle.size);
      }
    }
  }

  closing() {
    this.activeModal.close(true);
    if (this.selectFeature) {
      this.selectFeature.getFeatures().clear();
    }
  }

  setSelectFeature(selectFeature: Select): void {
    this.selectFeature = selectFeature;
  }

  setVectorLayer(vectorLayer: VectorLayer<VectorSource<Geometry>>): void {
    this.vectorLayer = vectorLayer;
  }

}
