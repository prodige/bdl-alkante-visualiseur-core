import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { DrawLayerService } from '../../../../../lib/services/draw-layer.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Select } from 'ol/interaction';
import { Subscription } from 'rxjs';

import Map from 'ol/Map';
import VectorLayer from 'ol/layer/Vector';
import { MapService } from '../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { StyleDrawsModalComponent } from './style-draws-modal/style-draws-modal.component';
import { GenerateStyleDrawsModalComponent } from './style-draws-modal/generate-style-draws-modal/generate-style-draws-modal.component';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-style-draws',
  templateUrl: './style-draws.component.html',
  styleUrls: ['./style-draws.component.scss']
})
export class StyleDrawsComponent implements OnInit, OnDestroy {

  map: Map;
  selectFeature: Select;
  drawLayerSubscription: Subscription;
  bufferLayerGroupSubscription: Subscription;
  isVectorLayer = false;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.drawLayerService.generateNewDrawLayer(this.mapIdService.getId() || 'main');
    this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          if (vectorLayer.getSource().getFeatures().length === 0) {
            this.openStyleDrawsModal();
          } else {
            this.isVectorLayer = true;
            this.vectorLayer = vectorLayer;
            this.selectFeature = new Select({
              layers: [vectorLayer],
              style: this.drawLayerService.getDefaultSelectStyle()
            });
            this.selectFeature.setActive(true);
            this.map.addInteraction(this.selectFeature);

            this.selectFeature.on('select', (event) => {
              if ( event.selected.length > 0 ) {
                this.openStyleDrawsModal();
              } else {
                this.selectFeature.getFeatures().clear();
              }
            });
          }
        } else if ( this.selectFeature ) {
          this.map.removeInteraction(this.selectFeature);
          this.selectFeature.setActive(false);
          this.selectFeature = null;
        }
      }
    );

    if (!this.isVectorLayer) {
      setTimeout(() => {
        this.openStyleDrawsModal();
      }, 0);
    }
  }

  ngOnDestroy(): void {
    if (this.selectFeature) {
      this.map.removeInteraction(this.selectFeature);
      this.selectFeature.setActive(false);
      this.selectFeature = null;
    }
    if (this.drawLayerSubscription) {
      this.drawLayerSubscription.unsubscribe();
    }
    if (this.bufferLayerGroupSubscription) {
      this.bufferLayerGroupSubscription.unsubscribe();
    }
  }

  openStyleDrawsModal() {
    if (this.selectFeature) {
      const modalRef = this.modalService.open(GenerateStyleDrawsModalComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'window-map-tool',
        container: '#modals',
        keyboard: false
      });
      if (modalRef.componentInstance.setSelectFeature) {
        modalRef.componentInstance.setSelectFeature(this.selectFeature);
        modalRef.componentInstance.setVectorLayer(this.vectorLayer);
      }
    } else {
      const modalRef = this.modalService.open(StyleDrawsModalComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'window-map-tool',
        container: '#modals',
        keyboard: false
      });
    }
  }

}
