import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateStyleDrawsModalComponent } from './generate-style-draws-modal.component';

describe('GenerateStyleDrawsModalComponent', () => {
  let component: GenerateStyleDrawsModalComponent;
  let fixture: ComponentFixture<GenerateStyleDrawsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateStyleDrawsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateStyleDrawsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
