import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleDrawsComponent } from './style-draws.component';

describe('StyleDrawsComponent', () => {
  let component: StyleDrawsComponent;
  let fixture: ComponentFixture<StyleDrawsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleDrawsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleDrawsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
