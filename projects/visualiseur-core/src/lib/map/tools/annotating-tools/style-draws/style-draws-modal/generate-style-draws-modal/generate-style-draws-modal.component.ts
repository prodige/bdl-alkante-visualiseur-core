import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Select } from 'ol/interaction';
import Feature from 'ol/Feature';
import { Style, Fill, Stroke, Icon  } from 'ol/style';
import FontSymbol from 'ol-ext/style/FontSymbol';
import FillPattern from 'ol-ext/style/FillPattern';
import StrokePattern from 'ol-ext/style/StrokePattern';
import VectorLayer from 'ol/layer/Vector';
import { DrawLayerService } from '../../../../../../services/draw-layer.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import VectorSource from 'ol/source/Vector';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'alk-generate-style-draws-modal',
  templateUrl: './generate-style-draws-modal.component.html',
  styleUrls: ['./generate-style-draws-modal.component.scss']
})
export class GenerateStyleDrawsModalComponent implements OnInit, OnDestroy {

  icon = '';
  geometryType: string;
  color;
  selectedColor$: Subject<string> = new Subject();
  selectedColorSubscription: Subscription;
  polygonSubscription: Subscription;
  background: string;
  pointSizes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
  polygonSizes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  patterns: Array<{name: string, data: any, pattern: FillPattern}> = [];
  selectedPattern: any;
  styleForm = new FormGroup({
    size: new FormControl('', Validators.required),
    color: new FormControl(''),
    background: new FormControl(''),
    form: new FormControl('', Validators.required),
  });
  selectFeature: Select;
  selectedFeature: Feature<any>;
  selectedFeatures: Array<Feature<any>> = null;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  symbol: FontSymbol;
  isBufferCreating = false;

  constructor(
    public activeModal: NgbActiveModal,
    private drawLayerService: DrawLayerService

  ) { }

  ngOnInit() {
    if (!this.selectedFeature && this.selectFeature) {
      this.selectedFeature = this.selectFeature.getFeatures().item(0);
      this.selectFeature.getFeatures().clear();
    }
    this.patterns = Object.entries(FillPattern.prototype.patterns).map( ([key, value]) => {
      return {
        name: key,
        data: value,
        pattern: new FillPattern({pattern: key}).getImage().toDataURL()
      };
    });

    if ( this.selectedFeatures && this.selectedFeatures.length ) {
      let feature;
      for ( feature of this.selectedFeatures) {
        if (feature) {
          let polygonCanvas = null;
          this.polygonSubscription = this.drawLayerService.getPolygonStyleOptions().subscribe(polygonOptions => {
            polygonCanvas = Object.assign({}, (polygonOptions as any));
            if (polygonCanvas.settedStyle.pattern) {
              this.drawLayerService.changeColInUri(
                [polygonCanvas.settedStyle.pattern],
                polygonCanvas.settedStyle.color
                );
            }

            this.geometryType = feature.getGeometry().getType();
            if (this.geometryType === 'Point') {
              if (feature.getStyle()) {
                this.styleForm.get('size').setValue(feature.getStyle().getImage() ? feature.getStyle().getImage().fontSize_.toString() : null);
                this.color = feature.getStyle().getImage() ? feature.getStyle().getImage().getFill().getColor() : null;
                this.styleForm.get('color').setValue(feature.getStyle().getImage() ? feature.getStyle().getImage().getFill().getColor() : null);
                this.onIconPickerSelect(feature.getStyle().getImage() ? feature.getStyle().getImage().glyph_.name : null);
              } else {
                this.styleForm.get('size').setValue(polygonCanvas.settedStyle.size);
                this.color = polygonCanvas.settedStyle.color;
                this.styleForm.get('color').setValue(polygonCanvas.settedStyle.color);
                this.onIconPickerSelect('fa-circle');
              }
            } else {
              const canvas = feature.getProperties().settedStyle;
              if (canvas) {
                this.selectPattern(canvas.pattern);
                this.styleForm.get('size').setValue(canvas.size);
                this.color = canvas.color;
                this.styleForm.get('color').setValue(canvas.color);
                this.background = canvas.background;
                this.styleForm.get('background').setValue(canvas.background);
              } else {
                this.selectPattern(polygonCanvas.settedStyle.pattern);
                this.styleForm.get('size').setValue(polygonCanvas.settedStyle.size);
                this.color = polygonCanvas.settedStyle.color;
                this.styleForm.get('color').setValue(polygonCanvas.settedStyle.color);
                this.background = polygonCanvas.settedStyle.background;
                this.styleForm.get('background').setValue(polygonCanvas.settedStyle.background);
              }
            }
          });
          break;
        }
      }
    } else if ( !this.selectedPattern && this.selectedFeature ) {
      this.geometryType = this.selectedFeature.getGeometry().getType();
      if (this.geometryType === 'Point') {
        console.error('à tester et débuger')
        /* TODO 22/12/2021 code testé il n'y a pas de variable fontSize_ dans getImage
        this.styleForm.get('size').setValue( ( this.selectedFeature.getStyle() as Style ).getImage()
          ? (this.selectedFeature.getStyle() as Style).getImage().fontSize_.toString()
          : null);*/
        this.color = (this.selectedFeature.getStyle() as Style).getImage() ? (this.selectedFeature.getStyle() as Style).getFill().getColor() : null;
        this.styleForm.get('color').setValue((this.selectedFeature.getStyle() as Style).getImage()
          ? (this.selectedFeature.getStyle() as Style).getFill().getColor()
          : null);
        
        /*  à test glyph_ introuvale dans doc openlayer this.onIconPickerSelect((this.selectedFeature.getStyle() as Style).getImage()? (this.selectedFeature.getStyle() as Style).getImage().getGlyph.name : null); */
      } else {
        const canvas = this.selectedFeature.getProperties().settedStyle;
        if (canvas) {
          this.selectPattern(canvas.pattern);
          this.styleForm.get('size').setValue(canvas.size);
          this.color = canvas.color;
          this.styleForm.get('color').setValue(canvas.color);
          this.background = canvas.background;
          this.styleForm.get('background').setValue(canvas.background);
        } else {
          this.styleForm.get('size').setValue(this.selectedFeature.get('size') ? this.selectedFeature.get('size').toString() : 1);
          this.color = this.selectedFeature.get('color');
          this.styleForm.get('color').setValue(this.selectedFeature.get('color'));
          this.styleForm.get('form').setValue(this.selectedFeature.get('form'));
        }
      }
    }
    this.selectedColorSubscription = this.selectedColor$.pipe(debounceTime(300)).subscribe(color => {
      this.drawLayerService.changeColInUri([this.selectedPattern], color);
    });
    if (this.color) {
      this.drawLayerService.changeColInUri([this.selectedPattern], this.color);
      this.selectColor(this.color);
    }
  }

  ngOnDestroy() {
    if (this.selectedColorSubscription) {
      this.selectedColorSubscription.unsubscribe();
    }
    if (this.polygonSubscription) {
      this.polygonSubscription.unsubscribe();
    }
  }

  setGeometryType(geometryType: string) {
    this.geometryType = geometryType;
  }

  onIconPickerSelect(icon: string): void {
    this.styleForm.get('form').setValue(icon);
  }

  selectPattern(pattern: any) {
    this.selectedPattern = Object.assign({}, pattern);
    this.styleForm.get('form').setValue(this.selectedPattern);
    if (this.color) {
      this.drawLayerService.changeColInUri([this.selectedPattern], this.color);
    }
  }

  selectColor(color: string) {
    this.selectedColor$.next(color);
  }

  saveBufferNewStyle() {
    const newSettedStyle = {
      size: this.styleForm.get('size').value,
      color: this.color,
      background: this.background,
      pattern: this.selectedPattern
    };
    this.activeModal.close(newSettedStyle);
  }

  generateStyle(param: string): void {

    let currentText = null;
    if (this.selectedFeature) {
      currentText = this.selectedFeature.getStyle() ? (this.selectedFeature.getStyle() as Style).getText() : null;
    }

    let newStyle = null;
    switch(param) {
      case 'Point':
        this.symbol = {
          glyph: this.styleForm.get('form').value.replace('fa ', ''),
          fontSize: this.styleForm.get('size').value > 1 ? '0.1' : this.styleForm.get('size').value,
          fontStyle: 'normal',
          color: this.color,
          fill: new Fill({
            color: this.color
          }),
          stroke: new Stroke({
            color: '#fff',
            width: 2
          }),
          radius: 20
        };
        newStyle = new Style({
          image: new FontSymbol(this.symbol),
          text: currentText ? currentText : null
        });
        this.savePointStyle(newStyle, currentText);
        break;
      case 'LineString':
        newStyle = new Style({
          stroke: new StrokePattern({
            width: 8,
            pattern: this.selectedPattern.name,
            ratio: 2,
            color: this.color,
            offset: 0,
            scale: parseInt(this.styleForm.get('size').value, 10),
            fill: new Fill ({
              color: this.background
            }),
            size: 1,
            spacing: 10,
            angle: 0
          }),
          text: currentText ? currentText : null
        });
        this.saveLineStringStyle(newStyle, currentText);
        break;
      case 'Polygon':
        newStyle = new Style({
          fill: new FillPattern({
            pattern: this.selectedPattern.name,
            ratio: 2,
            icon: new Icon ({
              src: 'data/target.png'
            }),
            color: this.color,
            offset: 0,
            scale: parseInt(this.styleForm.get('size').value, 10),
            fill: new Fill ({
              color: this.background
            }),
            size: 1,
            spacing: 10,
            angle: 0
          }),
          stroke: new Stroke({
            color: this.color,
            width: 3
          }),
          text: currentText ? currentText : null
        });
        this.savePolygonStyle(newStyle, currentText);
        break;
    }

    this.activeModal.close();
  }

  savePointStyle(newStyle: Style, currentText) {
    const newSettedStyle = {
      icon: this.styleForm.get('form').value.replace('fa ', ''),
      size: this.styleForm.get('size').value,
      color: this.color
    };
    if (!this.selectFeature && !this.selectedFeatures) {
      const pointStyle = newStyle;
      pointStyle.setText(null);
      this.drawLayerService.setPointStyle(pointStyle);
      this.drawLayerService.setPointStyleOptions({
        settedStyle: newSettedStyle
      });
    } else if (this.selectedFeatures) {
      this.selectedFeatures.forEach(feature => { this.setStyleToObject(newStyle, newSettedStyle, feature); });
      this.setStyleToObject(newStyle, newSettedStyle, this.vectorLayer);
    } else if (this.selectedFeature) {
      this.setStyleToObject(newStyle, newSettedStyle, this.selectedFeature);
    }
  }

  saveLineStringStyle(newStyle: Style, currentText) {
    const newSettedStyle = {
      size: parseInt(this.styleForm.get('size').value, 10),
      color: this.color,
      background: this.background,
      pattern: this.selectedPattern,
      text: currentText ? currentText : null
    };
    if (!this.selectFeature && !this.selectedFeatures) {
      const lineStringStyle = newStyle;
      this.drawLayerService.setLineStringStyle(lineStringStyle);
      this.drawLayerService.setLineStringStyleOptions({
        settedStyle: newSettedStyle
      });
    } else if (this.selectedFeatures) {
      this.selectedFeatures.forEach(feature => { this.setStyleToObject(newStyle, newSettedStyle, feature); });
      this.setStyleToObject(newStyle, newSettedStyle, this.vectorLayer);
    } else if (this.selectedFeature) {
      this.setStyleToObject(newStyle, newSettedStyle, this.selectedFeature);
    }
  }

  savePolygonStyle(newStyle: Style, currentText) {
    const newSettedStyle = {
      size: parseInt(this.styleForm.get('size').value, 10),
      color: this.color,
      background: this.background,
      pattern: this.selectedPattern,
      text: currentText ? currentText : null
    };
    if (!this.selectFeature && !this.selectedFeatures) {
      const polyginStyle = newStyle;
      this.drawLayerService.setPolygonStyle(polyginStyle);
      this.drawLayerService.setPolygonStyleOptions({
        settedStyle: newSettedStyle
      });
    } else if (this.selectedFeatures) {
      this.selectedFeatures.forEach(feature => { this.setStyleToObject(newStyle, newSettedStyle, feature); });
      this.setStyleToObject(newStyle, newSettedStyle, this.vectorLayer);
    } else if (this.selectedFeature) {
      this.setStyleToObject(newStyle, newSettedStyle, this.selectedFeature);
    }
  }

  setStyleToObject(newStyle: Style, newSettedStyle, objet) {
    objet.setStyle(newStyle);
    objet.setProperties({
      settedStyle: newSettedStyle
    });
  }

  setSelectFeature(selectFeature: Select): void {
    this.selectFeature = selectFeature;
  }

  setSelectedFeature(selectedFeature: Feature<any>): void {
    this.selectedFeature = selectedFeature;
  }

  setSelectedFeatures(features: Array<Feature<any>>) {
    this.selectedFeatures = features;
  }

  setVectorLayer(vectorLayer: VectorLayer<VectorSource<Geometry>>): void {
    this.vectorLayer = vectorLayer;
  }

  closing(): void {
    this.activeModal.close(true);
    if (this.selectFeature) {
      this.selectFeature.getFeatures().clear();
    }
  }

}
