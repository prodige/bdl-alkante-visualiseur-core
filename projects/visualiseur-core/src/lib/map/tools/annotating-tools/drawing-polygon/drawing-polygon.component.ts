import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { MapService } from './../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { DrawLayerService } from './../../../../services/draw-layer.service';

import Map from 'ol/Map';
import { Draw, Snap } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import { Feature } from 'ol/';
import { Style } from 'ol/style';
import { ResultModalFicheComponent } from '../../../../interrogating-results/result-modal-fiche/result-modal-fiche.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';
import { FeatureCollection } from '../../../..//models/context';

@Component({
  selector: 'alk-drawing-polygon',
  templateUrl: './drawing-polygon.component.html',
  styleUrls: ['./drawing-polygon.component.scss']
})
export class DrawingPolygonComponent implements OnInit, OnDestroy {

  map: Map;
  drawLayer: VectorLayer<VectorSource<Geometry>>;
  drawPolygon: Draw;

  contextSubscription: Subscription;
  coreSubscription: Subscription;

  snappingVectorSubscription: Subscription;
  snappingVectorLayer: VectorLayer<VectorSource<Geometry>>;
  snappingInteractionLayer: Snap = null;

  drawLayerSubscription: Subscription;
  polygonStyleSubscription: Subscription;
  drawStyle: Style;
  polygonStyleOptionsSubscription: Subscription;
  drawStyleOptions: Object;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private modalService: NgbModal,
    private coreService: VisualiseurCoreService
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.polygonStyleSubscription = this.drawLayerService.getPolygonStyle().subscribe(polygon => {
      if (polygon) {
        this.drawStyle = polygon.clone();
      }
    });

    this.polygonStyleOptionsSubscription = this.drawLayerService.getPolygonStyleOptions().subscribe(opt => {
      if (opt) {
        this.drawStyleOptions = opt;
      }
    });
    this.drawLayerService.generateNewDrawLayer(this.mapIdService.getId() || 'main');
    this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          this.drawLayer = vectorLayer;
          this.drawLayer.setMap(this.map);

          const white = [255, 255, 255, 1];
          const blue = [0, 153, 255, 1];
          const width = 3;

          this.drawPolygon = new Draw({
            type: 'Polygon',
            style : this.drawLayerService.getDefaultDrawStyle(),
            source: this.drawLayer.getSource()
          });

          this.drawPolygon.on('drawstart', (event) => {
            this.coreSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
              if (snap) {
                this.drawLayerService.deleteSnappingLayer();
                this.drawLayerService.generateSnappingLayer( (this.drawLayer.getSource().getFeaturesCollection() as unknown as FeatureCollection), 'Polygon', true);
                this.snappingVectorSubscription = this.drawLayerService.getSnappingLayer().subscribe(
                  (vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
                    if (vectorLayer) {
                      this.snappingVectorLayer = vectorLayer;
                      this.snappingVectorLayer.setMap(this.map);

                      if (this.snappingInteractionLayer) {
                        this.map.removeInteraction(this.snappingInteractionLayer);
                      }
                      this.snappingInteractionLayer = new Snap({
                        pixelTolerance: 10,
                        source: this.snappingVectorLayer.getSource()
                      });
                      this.snappingInteractionLayer.setActive(true);
                      this.map.addInteraction(this.snappingInteractionLayer);
                    }
                  }
                );
              } else {
                if (this.snappingInteractionLayer) {
                  this.snappingInteractionLayer.setActive(false);
                  this.snappingInteractionLayer = null;
                  this.map.removeInteraction(this.snappingInteractionLayer);
                }
                this.drawLayerService.deleteSnappingLayer();
              }
            });
          });
          this.drawPolygon.on('drawend', (event) => {
            const newFeature = event.feature;
            if ( newFeature ) {
              (newFeature as Feature<any>).setStyle(this.drawStyle ? this.drawStyle : null);
              (newFeature as Feature<any>).setProperties(this.drawStyleOptions ? this.drawStyleOptions : null);
              this.drawLayerService.setHasFeature(true);
              this.coreSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
                if (snap) {
                  this.drawLayerService.deleteSnappingLayer();
                  this.drawLayerService.generateSnappingLayer((this.drawLayer.getSource().getFeaturesCollection() as unknown as FeatureCollection), 'Polygon', true);
                }
              });
              this.contextSubscription = this.coreService.getContext().subscribe(context => {
                if (context && context.properties.extension.Tools.Annotation.Attribut &&
                  context.properties.extension.Tools.Annotation.AttributName.length > 0) {

                  const currentLayerResults = {
                    data: {
                      feature: newFeature
                    },
                    layer: this.drawLayer,
                    layerName: this.drawLayer.get('name'),
                    layerQueryFields: context.properties.extension.Tools.Annotation.AttributName,
                    nbResults: 1
                  };

                  const modalRef = this.modalService.open(ResultModalFicheComponent, {
                    container: '#modals',
                    centered: true,
                    size: 'lg',
                    backdrop: 'static'
                  });

                  modalRef.componentInstance.editing = true;
                  modalRef.componentInstance.currentLayerResults = currentLayerResults;
                  modalRef.componentInstance.currentSelectedFeature = newFeature.getProperties();
                  modalRef.componentInstance.modalReady = true;

                }
              });
            }
          });

          this.drawPolygon.setActive(true);
          this.map.addInteraction(this.drawPolygon);
        } else if ( this.drawPolygon ) {
          this.map.removeInteraction(this.drawPolygon);
          this.drawPolygon.setActive(false);
          this.drawPolygon = null;
        }
      }
    );
  }

  ngOnDestroy() {
    if ( this.drawLayerSubscription ) {
      this.drawLayerSubscription.unsubscribe();
    }
    if ( this.polygonStyleSubscription ) {
      this.polygonStyleSubscription.unsubscribe();
    }
    if ( this.polygonStyleOptionsSubscription ) {
      this.polygonStyleOptionsSubscription.unsubscribe();
    }
    if ( this.drawPolygon ) {
      this.drawPolygon.setActive(false);
      this.map.removeInteraction(this.drawPolygon);
    }
    if (this.snappingInteractionLayer) {
      this.snappingInteractionLayer.setActive(false);
      this.map.removeInteraction(this.snappingInteractionLayer);
    }
  }

}
