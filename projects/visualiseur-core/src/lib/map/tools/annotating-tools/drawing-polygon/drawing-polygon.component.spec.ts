import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingPolygonComponent } from './drawing-polygon.component';

describe('DrawingPolygonComponent', () => {
  let component: DrawingPolygonComponent;
  let fixture: ComponentFixture<DrawingPolygonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingPolygonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingPolygonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
