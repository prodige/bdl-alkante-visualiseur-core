import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingLinestringComponent } from './drawing-linestring.component';

describe('DrawingLinestringComponent', () => {
  let component: DrawingLinestringComponent;
  let fixture: ComponentFixture<DrawingLinestringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingLinestringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingLinestringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
