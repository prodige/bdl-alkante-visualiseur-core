import { Component, OnInit, Host, Optional, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { MapService } from './../../../map.service';
import { MapIdService } from '../../../map-id.service';
import { DrawLayerService } from './../../../../services/draw-layer.service';

import Map from 'ol/Map';
import { Draw, Snap } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import { Style } from 'ol/style';
import { ResultModalFicheComponent } from '../../../../interrogating-results/result-modal-fiche/result-modal-fiche.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Geometry } from 'ol/geom';
import Feature from 'ol/Feature';

import VectorSource from 'ol/source/Vector';
import { FeatureCollection } from '../../../../models/context';

@Component({
  selector: 'alk-drawing-linestring',
  templateUrl: './drawing-linestring.component.html',
  styleUrls: ['./drawing-linestring.component.scss']
})
export class DrawingLinestringComponent implements OnInit, OnDestroy {

  map: Map;
  drawLayer: VectorLayer<VectorSource<Geometry>>;
  drawLineString: Draw;

  contextSubscription: Subscription;
  coreSubscription: Subscription;

  snappingVectorSubscription: Subscription;
  snappingVectorLayer: VectorLayer<VectorSource<Geometry>>;
  snappingInteractionLayer: Snap = null;

  drawLayerSubscription: Subscription;
  lineStringStyleSubscription: Subscription;
  drawStyle: Style;
  lineStringStyleOptionsSubscription: Subscription;
  drawStyleOptions: Object;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private modalService: NgbModal,
    private coreService: VisualiseurCoreService
  ) { }

  ngOnInit() {
    this.map = this.mapService.getMap(this.mapIdService.getId() || 'main');

    this.lineStringStyleSubscription = this.drawLayerService.getLineStringStyle().subscribe(lineString => {
      if (lineString) {
        this.drawStyle = new Style({
          stroke: lineString.getStroke()
        });
      }
    });

    this.lineStringStyleOptionsSubscription = this.drawLayerService.getLineStringStyleOptions().subscribe(opt => {
      if (opt) {
        this.drawStyleOptions = opt;
      }
    });

    this.drawLayerService.generateNewDrawLayer(this.mapIdService.getId() || 'main');
    this.drawLayerSubscription = this.drawLayerService.getDrawLayer().subscribe(
      ( vectorLayer: VectorLayer<VectorSource<Geometry>> ) => {
        if ( vectorLayer ) {
          this.drawLayer = vectorLayer;
          this.drawLayer.setMap(this.map);

          this.drawLineString = new Draw({
            type: 'LineString',
            source: this.drawLayer.getSource(),
            style: this.drawLayerService.getDefaultDrawStyle()
          });

          this.drawLineString.on('drawstart', (event) => {
            this.coreSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
              if (snap) {
                this.drawLayerService.deleteSnappingLayer();
                this.drawLayerService.generateSnappingLayer(this.drawLayer.getSource().getFeaturesCollection() as unknown as FeatureCollection, 'LineString', true);
                this.snappingVectorSubscription = this.drawLayerService.getSnappingLayer().subscribe(
                  (vectorLayer: VectorLayer<VectorSource<Geometry>>) => {
                    if (vectorLayer) {
                      this.snappingVectorLayer = vectorLayer;
                      this.snappingVectorLayer.setMap(this.map);

                      if (this.snappingInteractionLayer) {
                        this.map.removeInteraction(this.snappingInteractionLayer);
                      }
                      this.snappingInteractionLayer = new Snap({
                        pixelTolerance: 10,
                        source: this.snappingVectorLayer.getSource()
                      });
                      this.snappingInteractionLayer.setActive(true);
                      this.map.addInteraction(this.snappingInteractionLayer);
                    }
                  }
                );
              } else {
                if (this.snappingInteractionLayer) {
                  this.snappingInteractionLayer.setActive(false);
                  this.snappingInteractionLayer = null;
                  this.map.removeInteraction(this.snappingInteractionLayer);
                }
                this.drawLayerService.deleteSnappingLayer();
              }
            });
          });
          this.drawLineString.on('drawend', (event) => {
            const newFeature = event.feature;
            if ( newFeature ) {
              newFeature.setStyle(this.drawStyle ? this.drawStyle : null);
              newFeature.setProperties(this.drawStyleOptions ? this.drawStyleOptions : null);
              this.drawLayerService.setHasFeature(true);
              this.coreSubscription = this.coreService.getAnnotatingSnapping().subscribe(snap => {
                if (snap) {
                  this.drawLayerService.deleteSnappingLayer();
                  this.drawLayerService.generateSnappingLayer(this.drawLayer.getSource().getFeaturesCollection() as unknown as FeatureCollection, 'LineString', true);
                }
              });
              this.contextSubscription = this.coreService.getContext().subscribe(context => {
                
                if (context && context.properties.extension.Tools.Annotation.Attribut &&
                  context.properties.extension.Tools.Annotation.AttributName.length > 0) {

                  const currentLayerResults = {
                    data: {
                      feature: newFeature
                    },
                    layer: this.drawLayer,
                    layerName: this.drawLayer.get('name'),
                    layerQueryFields: context.properties.extension.Tools.Annotation.AttributName,
                    nbResults: 1
                  };

                  const modalRef = this.modalService.open(ResultModalFicheComponent, {
                    container: '#modals',
                    centered: true,
                    size: 'lg',
                    backdrop: 'static'
                  });

                  modalRef.componentInstance.editing = true;
                  modalRef.componentInstance.currentLayerResults = currentLayerResults;
                  modalRef.componentInstance.currentSelectedFeature = newFeature.getProperties();
                  modalRef.componentInstance.modalReady = true;

                }
              });
            }
          });

          this.drawLineString.setActive(true);
          this.map.addInteraction(this.drawLineString);
        } else if ( this.drawLineString ) {
          this.map.removeInteraction(this.drawLineString);
          this.drawLineString.setActive(false);
          this.drawLineString = null;
        }
      }
    );
  }

  ngOnDestroy() {
    if ( this.drawLayerSubscription ) {
      this.drawLayerSubscription.unsubscribe();
    }
    if ( this.lineStringStyleSubscription ) {
      this.lineStringStyleSubscription.unsubscribe();
    }
    if ( this.lineStringStyleOptionsSubscription ) {
      this.lineStringStyleOptionsSubscription.unsubscribe();
    }
    if ( this.drawLineString ) {
      this.drawLineString.setActive(false);
      this.map.removeInteraction(this.drawLineString);
    }
    if (this.snappingInteractionLayer) {
      this.snappingInteractionLayer.setActive(false);
      this.map.removeInteraction(this.snappingInteractionLayer);
    }
  }

}
