import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreEditingModalComponent } from './pre-editing-modal.component';

describe('PreEditingModalComponent', () => {
  let component: PreEditingModalComponent;
  let fixture: ComponentFixture<PreEditingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreEditingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreEditingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
