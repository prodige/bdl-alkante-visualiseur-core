import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Type,
} from "@angular/core";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import ImageWMS from "ol/source/ImageWMS";
import LayerGroup from "ol/layer/Group";
import GeoJSON from "ol/format/GeoJSON";
import { DrawLayerService } from "../../../../../services/draw-layer.service";
import {
  ReplaySubject,
  Subscription,
  forkJoin,
  observable,
  Observable,
  from,
} from "rxjs";
import { Draw, Select, Modify, Translate, Snap } from "ol/interaction";
import { Map, Collection } from "ol/";
import { MapService } from "../../../../map.service";
import { Feature } from "ol/";
import ol_interaction_DrawHole from "ol-ext/interaction/DrawHole";
import WKTReader from "jsts/org/locationtech/jts/io/WKTReader";
import WKTWriter from "jsts/org/locationtech/jts/io/WKTWriter";
import Polygonizer from "jsts/org/locationtech/jts/operation/polygonize/Polygonizer";
import GeometryFactory from "jsts/org/locationtech/jts/geom/GeometryFactory";
import Coordinate from "jsts/org/locationtech/jts/geom/Coordinate";
import UnionOp from "jsts/org/locationtech/jts/operation/union/UnionOp";
import RelateOp from "jsts/org/locationtech/jts/operation/relate/RelateOp";
import BufferOp from "jsts/org/locationtech/jts/operation/buffer/BufferOp";
import { WKT } from "ol/format";
import Polygon from "jsts/org/locationtech/jts/geom/Polygon";
import {
  FeatureCollection,
  EditionConfig,
} from "../../../../../models/context";
import { VisualiseurCoreService } from "../../../../../visualiseur-core.service";
import {
  EditingToolService,
  EditionSession,
  FeaturesInfo,
} from "../../../../../services/editing-tool.service";
import { ResultModalFicheComponent } from "../../../../../interrogating-results/result-modal-fiche/result-modal-fiche.component";
import { MergeFicheModalComponent } from "../../../../../modals/merge-fiche-modal/merge-fiche-modal.component";
import lineSplit, { Splitter } from "@turf/line-split";
import { AlertConfirmPromptModalComponent } from "../../../../../modals/alert-confirm-prompt-modal/alert-confirm-prompt-modal.component";
import MultiPolygon from "ol/geom/MultiPolygon";
import MultiLineString from "ol/geom/MultiLineString";
import LineString from "ol/geom/LineString";
import MultiPoint from "ol/geom/MultiPoint";
import { ImportFileModalComponent } from "../../../../../modals/import-file-modal/import-file-modal.component";
import { concat } from "rxjs";
import { every, tap } from "rxjs/operators";
import { MapIdService } from "../../../../map-id.service";
import ImageSource from "ol/source/Image";
import VectorSource from "ol/source/Vector";
import { Geometry, Point } from "ol/geom";

@Component({
  selector: "alk-editing-modal",
  templateUrl: "./editing-modal.component.html",
  styleUrls: ["./editing-modal.component.scss"],
})
export class EditingModalComponent implements OnInit, OnDestroy {
  saveGlobal = false;

  editionConfig: EditionConfig;
  editableLayer: ImageLayer<ImageSource>;
  editableFeaturesStructure: any;

  selectedSnappingLayerFeatures: FeatureCollection;
  selectedEditableLayerFeatures: FeatureCollection;

  rootLayerGroupSubscription: Subscription;
  rootLayerGroup: LayerGroup;

  editableVectorSubscription: Subscription;
  editableVectorLayer: VectorLayer<VectorSource<Geometry>>;

  snappingVectorSubscription: Subscription;
  snappingVectorLayer: VectorLayer<VectorSource<Geometry>>;

  editingSessionSubscription: Subscription;
  editingSession: EditionSession = null;

  format = new WKT();
  geojson = new GeoJSON();
  forkJoin: Subscription;

  snappingInteractionLayer: Snap = null;
  tolerance = 10;
  draw: Draw = null;

  selectFeature: Select = null;
  selectMultiFeature: Select = null;
  selectCut: Select = null;
  selectToAddGeom: Select = null;
  selectedFeatureToAdd: Collection<Feature<any>>;
  modifySelect: Modify = null;
  translateFeature = null;

  drawHole = null;
  drawSplit: Draw = null;
  drawGeomToFeature: Draw = null;

  snapSplit: Snap = null;
  featToCut: Feature<Geometry> = null;

  private mapId: string;
  mapId$: ReplaySubject<string> = new ReplaySubject(null);
  mapIdSubscription: Subscription;
  map: Map;

  enumsSubscription: Subscription;

  msgError = "";

  layerType: string;
  nbSelectedFeatures = 0;
  @ViewChild("cancelConfirm", { read: null, static: true })
  cancelConfirm: AlertConfirmPromptModalComponent;
  @ViewChild("saveConfirm", { read: null, static: true })
  saveConfirm: AlertConfirmPromptModalComponent;
  @ViewChild("featureError", { read: null, static: true })
  featureError: AlertConfirmPromptModalComponent;
  @ViewChild("featureDeleted", { read: null, static: true })
  featureDeleted: AlertConfirmPromptModalComponent;
  @ViewChild("copyToast", { read: null, static: true }) copyToast: ElementRef;
  @ViewChild("saveError", { read: null, static: true })
  saveError: AlertConfirmPromptModalComponent;

  isToastVisible = false;

  isLoading = false;

  editionMode = {
    draw: false,
    select: false,
    edit: false,
    translate: false,
    hole: false,
    merge: false,
    selectCut: false,
    drawGeomToFeature: false,
    multiSelect: false,
  };

  constructor(
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    public mapService: MapService,
    private drawLayerService: DrawLayerService,
    private editingToolService: EditingToolService,
    private coreService: VisualiseurCoreService,
    private mapIdService: MapIdService
  ) {}

  ngOnInit() {
    this.rootLayerGroupSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      });

    this.editingSessionSubscription = this.editingToolService
      .getEditingSession()
      .subscribe((editionSession) => {
        this.editingSession = editionSession;
      });

    this.mapIdSubscription = this.mapId$.subscribe((newMapId) => {
      if (newMapId && this.mapId !== newMapId) {
        this.mapId = newMapId;

        this.map = this.mapService.getMap(
          this.mapIdService.getId() || this.mapId || "main"
        );

        // Création du vecteur source pour les figures en lecture seules
        this.drawLayerService.generateEditLayer(
          this.selectedEditableLayerFeatures,
          this.editingSession,
          this.mapIdService.getId() || this.mapId || "main"
        );
        if (this.selectedSnappingLayerFeatures) {
          this.drawLayerService.generateSnappingLayer(
            this.selectedSnappingLayerFeatures,
            this.layerType
          );
        }
        this.editableVectorSubscription = this.drawLayerService
          .getEditLayer()
          .subscribe((vectorLayer: VectorLayer<VectorSource<Geometry>>) => {
            if (this.layerType) {
              if (this.layerType === "MULTIPOINT") {
                this.layerType = "MultiPoint";
              }

              // Ajout de la couche de dessin de forme (point, ligne, polygon)
              if (vectorLayer) {
                this.editableVectorLayer = vectorLayer;
                this.editableVectorLayer.setMap(this.map);

                // Ajout de l'interaction permettant de dessiner
                this.draw = new Draw({
                  source: this.editableVectorLayer.getSource(),
                  type:
                    this.layerType === "Linestring"
                      ? ("LineString" as any)
                      : this.layerType,
                  style: this.drawLayerService.getDefaultDrawStyle(),
                });

                // La seule interaction activée par défaut
                this.editionMode.draw = true;
                this.draw.on("drawstart", (event) => {});
                this.draw.on("drawend", (event) => {
                  if (event && event.feature) {
                    this.displayFeatureInfo("add", event.feature);
                    // this.updateEditedFeatures('add', event.feature);
                  }
                });
                this.draw.setActive(true);
                this.map.addInteraction(this.draw);

                // Ajout du select Feature pour le mode édition (désactivé par défaut)
                this.selectFeature = new Select({
                  layers: [this.editableVectorLayer],
                  style: this.drawLayerService.getDefaultDrawStyle(),
                });
                this.nbSelectedFeatures = this.selectFeature
                  .getFeatures()
                  .getLength();
                this.selectFeature.on("select", (event: any) => {
                  this.displayFeatureInfo(
                    "modify",
                    this.selectFeature.getFeatures().getArray()[0]
                  );
                  this.nbSelectedFeatures = this.selectFeature
                    .getFeatures()
                    .getLength();
                });
                // Sélection mutliple
                this.initMultiSelect();

                // ajout du select cut pour le mode édition
                this.initSelectCut();

                // ajout du select pour ajouter une geométrie à une feature
                // this.initSelectAddGeom();

                // ajout du draw pour dessiner une geométrie et l'ajouter à la feature selectionnée
                this.initDrawGeomToFeature();

                this.selectFeature.setActive(false);
                this.map.addInteraction(this.selectFeature);

                // Ajout de l'interaction permettant la modification d'une feature
                this.modifySelect = new Modify({
                  // features: this.selectFeature.getFeatures()
                  source: this.editableVectorLayer.getSource(),
                  style: this.drawLayerService.getDefaultDrawStyle(),
                });
                this.modifySelect.on("modifystart", (event) => {});
                this.modifySelect.on("modifyend", (event) => {
                  event.features.getArray().forEach((feature) => {
                    this.updateEditedFeatures(
                      "modify",
                      feature as Feature<Geometry>
                    );
                  });
                });
                this.modifySelect.setActive(false);
                this.map.addInteraction(this.modifySelect);

                // Ajout de l'interaction permettant le déplacement d'une feature
                this.translateFeature = new Translate({
                  layers: [this.editableVectorLayer],
                });
                this.translateFeature.on("translatestart", (event) => {});
                this.translateFeature.on("translateend", (event) => {
                  event.features.getArray().forEach((feature) => {
                    this.updateEditedFeatures("modify", feature);
                  });
                });
                this.translateFeature.setActive(false);
                this.map.addInteraction(this.translateFeature);

                // Ajout de l'interaction permettant de faire un trou dans une feature
                this.drawHole = new ol_interaction_DrawHole({
                  layers: [this.editableVectorLayer],
                });
                this.drawHole.on("drawholestart", (event) => {});
                this.drawHole.on("drawholeend", (event) => {
                  event.features.getArray().forEach((feature) => {
                    this.updateEditedFeatures("modify", feature);
                  });
                });
                this.drawHole.on("modifystart", (event) => {});
                this.drawHole.on("modifyend", (event) => {
                  event.features.forEach((feature) => {
                    this.updateEditedFeatures("modify", feature);
                  });
                });
                this.drawHole.setActive(false);
                this.map.addInteraction(this.drawHole);

                // Ajout de l'interaction permettant de splitter une feature
                this.drawSplit = new Draw({
                  type: "LineString",
                });
                this.drawSplit.on("drawstart", (event: any) => {
                  this.selectFeature.setActive(false);
                  this.selectMultiFeature.setActive(false);
                  this.selectFeature.getFeatures().clear();
                  this.selectMultiFeature.getFeatures().clear();
                });
                this.drawSplit.on("drawend", (event: any) => {
                  const featLine = event.feature;
                  const format = new WKT();
                  let newFeaturesWKT: Array<string> = [];

                  switch (this.layerType) {
                    case "Polygon":
                      newFeaturesWKT = this.splitPolygon(
                        this.featToCut,
                        featLine
                      );
                      break;
                    case "MultiPolygon":
                      newFeaturesWKT = this.splitPolygon(
                        this.featToCut,
                        featLine
                      );
                      break;
                    case "Linestring":
                      newFeaturesWKT = this.splitLineString(
                        this.featToCut,
                        featLine
                      );
                      break;
                    case "MultiLineString":
                      newFeaturesWKT = this.splitLineString(
                        this.featToCut,
                        featLine
                      );
                      break;
                    default:
                      break;
                  }

                  if (newFeaturesWKT.length > 1) {
                    const featToCutProperties = this.featToCut.getProperties();

                    delete featToCutProperties.geometry;
                    delete featToCutProperties.gid;

                    if (
                      this.editableVectorLayer
                        .getSource()
                        .hasFeature(this.featToCut)
                    ) {
                      this.editableVectorLayer
                        .getSource()
                        .removeFeature(this.featToCut);
                      this.updateEditedFeatures("delete", this.featToCut);
                    }
                    for (const wkt of newFeaturesWKT) {
                      const newFeature = this.format.readFeature(wkt);
                      newFeature.setProperties(featToCutProperties);
                      this.editableVectorLayer
                        .getSource()
                        .addFeature(newFeature);
                      this.updateEditedFeatures("add", newFeature);
                    }
                  }

                  this.stopInteractions();
                });
                this.drawSplit.setActive(false);
                this.map.addInteraction(this.drawSplit);

                this.snapSplit = new Snap({
                  source: this.editableVectorLayer.getSource(),
                });

                this.snappingVectorSubscription = this.drawLayerService
                  .getSnappingLayer()
                  .subscribe((vLayer: VectorLayer<VectorSource<Geometry>>) => {
                    if (vLayer) {
                      this.snappingVectorLayer = vLayer;
                      this.snappingVectorLayer.setMap(this.map);

                      this.snappingInteractionLayer = new Snap({
                        pixelTolerance: this.tolerance,
                        source: this.snappingVectorLayer.getSource(),
                      });
                      this.snappingInteractionLayer.setActive(true);
                      this.map.addInteraction(this.snappingInteractionLayer);
                    }
                  });
                this.snapSplit.setActive(false);
                this.map.addInteraction(this.snapSplit);
              }
            }
          });
      }
    });
  }

  ngOnDestroy() {
    this.layerType = null;
    this.stopInteractions();

    // TODO pourquoi c'est si long ?
    this.editableVectorLayer.setMap(null);
    if (this.rootLayerGroup) {
      const rootLayers = this.rootLayerGroup.getLayers();
      rootLayers.getArray().forEach((layer, index) => {
        if (layer.get("isEditLayer")) {
          rootLayers.remove(layer);
        }
      });
      this.rootLayerGroup.setLayers(rootLayers);
    }
    // this.map.removeLayer(this.editableVectorLayer);

    if (this.snappingVectorLayer) {
      this.snappingVectorLayer.setMap(null);
    }
    if (this.rootLayerGroupSubscription) {
      this.rootLayerGroupSubscription.unsubscribe();
    }
    if (this.editableVectorSubscription) {
      this.editableVectorSubscription.unsubscribe();
    }
    if (this.snappingVectorSubscription) {
      this.snappingVectorSubscription.unsubscribe();
    }
    if (this.mapIdSubscription) {
      this.mapIdSubscription.unsubscribe();
    }

    if (this.editingSessionSubscription) {
      this.editingSessionSubscription.unsubscribe();
    }

    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }
  }

  public drawFeatures(): void {
    this.stopInteractions();
    this.editionMode.draw = true;
    this.draw.setActive(true);
  }

  public selectFeatures(): void {
    this.stopInteractions();
    this.editionMode.select = true;
    this.selectFeature.setActive(true);
  }

  public enabledMultiselect(): void {
    this.stopInteractions();
    this.editionMode.multiSelect = true;
    this.selectMultiFeature.setActive(true);
  }

  /**  */
  public enabledSelectCut(): void {
    this.stopInteractions();
    this.editionMode.selectCut = true;
    this.selectCut.setActive(true);
  }

  /**  */
  public enabledDrawGeomToFeature(): void {
    this.editionMode.drawGeomToFeature = true;
    this.selectedFeatureToAdd = this.selectMultiFeature.getFeatures();
    if (this.selectedFeatureToAdd.get("length") > 0) {
      this.drawGeomToFeature.setActive(true);
      this.selectMultiFeature.setActive(false);

      this.editionMode.multiSelect = false;
    }
  }

  public translateFeatures(): void {
    this.stopInteractions();
    this.editionMode.translate = true;
    this.translateFeature.setActive(true);
  }

  public editFeatures(): void {
    this.stopInteractions();
    this.editionMode.edit = true;
    this.modifySelect.setActive(true);
  }

  public mergeFeatures(): boolean {
    const selectedFeatures = this.selectMultiFeature.getFeatures();

    const featuresList = [];
    selectedFeatures.forEach((feature) => {
      featuresList.push(feature);
    });
    if (selectedFeatures.getLength() >= 2) {
      let featureA = null;
      let newFeature = null;

      selectedFeatures.forEach((feature) => {
        if (featureA) {
          const wkt = this.mergePolygon(featureA, feature);
          newFeature = wkt ? this.format.readFeature(wkt) : null;
        } else {
          featureA = feature;
        }

        if (newFeature) {
          Object.keys(feature.getProperties()).forEach((key) => {
            if (key !== "geometry" && key !== "gid") {
              newFeature.set(key, { read: null, static: true });
            }
          });

          featureA = newFeature;
        }
      });

      this.stopInteractions();

      if (!newFeature) {
        return false;
      }

      const layerQueryFieldsNames = this.editableLayer
        .get("LayerQuery")
        .Fields.map((layerQueryField) => layerQueryField.FieldName);
      const datas = Object.entries(
        newFeature.getProperties() as { [key: string]: any }
      )
        .filter(([key, value]) => layerQueryFieldsNames.includes(key))
        .reduce((accum, [key, value]) => {
          accum[key] = value;
          return accum;
        }, {});

      const layerFields = this.editableLayer.get("LayerQuery").Fields;
      layerFields.forEach((field) => {
        const structure = this.editableFeaturesStructure.filter(
          (feat) => feat.column_name === field.FieldName
        );
        if (structure.length > 0) {
          field.structure = structure[0];
        }
      });

      const currentLayerResults = {
        data: [],
        layer: this.editableLayer,
        layerName: this.editableLayer.get("name"),
        layerQueryFields: layerFields,
        nbResults: 1,
      };

      currentLayerResults.data.push(
        Object.assign({}, datas, {
          newFeature,
        })
      );

      const modalRef = this.modalService.open(MergeFicheModalComponent, {
        container: "#modals",
        centered: true,
        size: "lg",
        backdrop: "static",
      });

      modalRef.componentInstance.editing = true;
      modalRef.componentInstance.currentLayerResults = currentLayerResults;
      modalRef.componentInstance.currentSelectedFeature =
        newFeature.getProperties();
      modalRef.componentInstance.modalReady = true;
      modalRef.componentInstance.featureToMerges = featuresList;

      modalRef.result.then((result) => {
        if (!result.isCancel) {
          featuresList.forEach((featureToDel) => {
            this.updateEditedFeatures("delete", featureToDel);
          });

          this.editableVectorLayer.getSource().addFeature(newFeature);
          this.updateEditedFeatures(
            "add",
            currentLayerResults.data[0].newFeature
          );
        }
      });
    }

    return true;
  }

  public copyFeature(): void {
    const selectedFeatures = this.selectMultiFeature.getFeatures();
    if (selectedFeatures.getLength()) {
      const featToCopy = selectedFeatures.item(0);
      const newFeat = featToCopy.clone();
      newFeat.unset("gid");
      this.editableVectorLayer.getSource().addFeature(newFeat);
      this.updateEditedFeatures("add", newFeat);

      this.stopInteractions();
      this.editionMode.translate = true;
      this.translateFeature.setActive(true);
    }
  }

  public splitFeature() {
    const selectedFeatures = this.selectMultiFeature.getFeatures();
    if (selectedFeatures.getLength()) {
      this.featToCut = selectedFeatures.item(0);
      this.stopInteractions();
      this.snapSplit.setActive(true);
      this.drawSplit.setActive(true);
    }
  }

  public createHole() {
    this.stopInteractions();
    this.editionMode.hole = true;
    this.drawHole.setActive(true);
  }

  public deleteFeatures() {
    // this.stopInteractions();
    const selectedFeatures = this.selectMultiFeature.getFeatures();
    if (selectedFeatures.getLength()) {
      selectedFeatures.forEach((feature: any) => {
        if (this.editableVectorLayer.getSource().hasFeature(feature)) {
          this.updateEditedFeatures("delete", feature);
        }
      });
    }
  }

  public displayFeatureInfo(
    mode: "add" | "modify",
    feature: Feature<Geometry>
  ): boolean {
    if (!feature) {
      return false;
    }

    const layerQueryFieldsNames = this.editableLayer
      .get("LayerQuery")
      .Fields.map((layerQueryField) => layerQueryField.FieldName);
    const datas = Object.entries(
      feature.getProperties() as { [key: string]: any }
    )
      .filter(([key, value]) => layerQueryFieldsNames.includes(key))
      .reduce((accum, [key, value]) => {
        accum[key] = value;
        return accum;
      }, {});

    const layerFields = this.editableLayer.get("LayerQuery").Fields;
    layerFields.forEach((field) => {
      const structure = this.editableFeaturesStructure.filter(
        (feat) => feat.column_name === field.FieldName
      );
      if (structure.length > 0) {
        field.structure = structure[0];
      }
    });

    const currentLayerResults = {
      data: [],
      layer: this.editableLayer,
      layerName: this.editableLayer.get("name"),
      layerQueryFields: layerFields,
      nbResults: 1,
    };

    currentLayerResults.data.push(
      Object.assign({}, datas, {
        feature,
      })
    );

    const modalRef = this.modalService.open(ResultModalFicheComponent, {
      container: "#modals",
      centered: true,
      size: "lg",
      backdrop: "static",
    });

    const gidField = {
      FieldAlias: "gid",
      FieldName: "gid",
      FieldQuery: true,
      FieldSorting: true,
      FieldTooltip: false,
      FieldType: "TXT",
      FieldURLConfig: null,
      structure: this.editableFeaturesStructure.filter(
        (feat) => feat.column_name === "gid"
      )[0],
    };
    if (mode === "add") {
      const indexGid = currentLayerResults.layerQueryFields.indexOf(
        currentLayerResults.layerQueryFields.filter(
          (field) => field.FieldAlias === "gid"
        )[0]
      );

      if (indexGid > -1) {
        currentLayerResults.layerQueryFields.splice(indexGid, 1);
      }
    } else if (
      currentLayerResults.layerQueryFields.filter(
        (field) => field.FieldAlias === "gid"
      ).length === 0 &&
      currentLayerResults.data[0].feature.get("gid")
    ) {
      currentLayerResults.layerQueryFields.splice(0, 0, gidField);
    }

    modalRef.componentInstance.editing = true;
    modalRef.componentInstance.currentLayerResults = currentLayerResults;
    modalRef.componentInstance.currentSelectedFeature = feature.getProperties();
    modalRef.componentInstance.modalReady = true;
    modalRef.result.then(() => {
      this.updateEditedFeatures(mode, currentLayerResults.data[0].feature);
    });

    return true;
  }

  public updateEditedFeatures(
    mode: "add" | "modify" | "delete",
    feature: Feature<Geometry>
  ) {
    const datas = feature.getProperties();
    delete datas.geometry;

    const isPresentAdded = this.editingSession.addedFeatures.length
      ? this.editingSession.addedFeatures.filter(
          (added) =>
            added.id === feature.get("gid") ||
            added.id === (feature as any).ol_uid
        )
      : null;

    const isPresentModified = this.editingSession.modifiedFeatures.length
      ? this.editingSession.modifiedFeatures.filter(
          (modified) =>
            modified.id === feature.get("gid") ||
            modified.id === (feature as any).ol_uid
        )
      : null;

    if (mode === "add") {
      this.editingSession.addedFeatures.push({
        id: (feature as any).ol_uid,
        geometry: this.format.writeFeature(feature),
        properties: datas,
      } as FeaturesInfo);
      this.editingToolService.setEditingSession(this.editingSession);
    } else if (mode === "modify") {
      if (!isPresentAdded || isPresentAdded.length === 0) {
        if (!isPresentModified || isPresentModified.length === 0) {
          this.editingSession.modifiedFeatures.push({
            id: feature.get("gid")
              ? feature.get("gid")
              : (feature as any).ol_uid,
            geometry: this.format.writeFeature(feature),
            properties: datas,
          } as FeaturesInfo);
          this.editingToolService.setEditingSession(this.editingSession);
        } else {
          this.editingSession.modifiedFeatures.splice(
            this.editingSession.modifiedFeatures.indexOf(isPresentModified[0]),
            1
          );
          this.updateEditedFeatures("modify", feature);
        }
      } else {
        this.editingSession.addedFeatures.splice(
          this.editingSession.addedFeatures.indexOf(isPresentAdded[0]),
          1
        );
        this.updateEditedFeatures("add", feature);
      }
    } else {
      if (isPresentAdded && isPresentAdded.length > 0) {
        this.editingSession.addedFeatures.splice(
          this.editingSession.addedFeatures.indexOf(isPresentAdded[0]),
          1
        );
      }
      if (isPresentModified && isPresentModified.length > 0) {
        this.editingSession.modifiedFeatures.splice(
          this.editingSession.modifiedFeatures.indexOf(isPresentModified[0]),
          1
        );
      }
      this.editingSession.deletedFeatures.push({
        id: feature.get("gid") ? feature.get("gid") : (feature as any).ol_uid,
        geometry: this.format.writeFeature(feature),
        properties: datas,
      } as FeaturesInfo);
      this.editingToolService.setEditingSession(this.editingSession);
      if (
        this.editableVectorLayer
          .getSource()
          .getFeatures()
          .filter((feat) => feat === feature).length !== 0
      ) {
        this.editableVectorLayer.getSource().removeFeature(feature);
      }
      this.selectFeature.getFeatures().clear();
      this.selectMultiFeature.getFeatures().clear();
      this.nbSelectedFeatures = 0;
    }
  }

  public cancel() {
    setTimeout(() => {
      this.cancelConfirm.openModal().then((result) => {
        if (result) {
          this.activeModal.close();
          this.editingToolService.setEditingSession(null);
        }
      });
    }, 500);
  }

  public saveLayer() {
    setTimeout(() => {
      this.saveConfirm.openModal().then((result) => {
        if (result) {
          const rootLayers = this.rootLayerGroup.getLayersArray();

          if (this.forkJoin) {
            this.forkJoin.unsubscribe();
          }

          const forkJoinObservables = [];

          if (!this.editingSession) {
            this.editingToolService.setEditingSession(null);
            this.activeModal.close();

            return false;
          }
          const featuresSize =
            this.editingSession.addedFeatures.length +
            this.editingSession.modifiedFeatures.length +
            this.editingSession.deletedFeatures.length;

          if (featuresSize === 0) {
            this.editingToolService.setEditingSession(null);
            this.activeModal.close();

            return false;
          }

          if (this.editingSession && this.editingSession.addedFeatures.length) {
            if (this.saveGlobal) {
              this.editingToolService.setGlobalFeature(
                this.editingSession.addedFeatures[
                  this.editingSession.addedFeatures.length - 1
                ]
              );
            }
            const addedFeatures = [];
            this.editingSession.addedFeatures.forEach((added) => {
              const feat = this.format.readFeature(added.geometry);

              for (const key in added.properties) {
                // remove _fileName key included with image field and not necessary for API
                if (
                  key.includes("_fileName") &&
                  added.properties.hasOwnProperty(key.replace("_fileName", ""))
                ) {
                  delete added.properties[key];
                }
              }

              feat.setProperties(added.properties);
              addedFeatures.push(feat);
            });
            forkJoinObservables.push(
              this.editingToolService.add(
                this.editableLayer.get("layerMetadataUuid"),
                this.geojson.writeFeaturesObject(addedFeatures)
              )
            );
          }

          if (
            this.editingSession &&
            this.editingSession.modifiedFeatures.length
          ) {
            if (this.saveGlobal) {
              this.editingToolService.setGlobalFeature(
                this.editingSession.modifiedFeatures[
                  this.editingSession.modifiedFeatures.length - 1
                ]
              );
            }
            const modifiedFeatures = [];

            this.editingSession.modifiedFeatures.forEach((modified) => {
              for (const key in modified.properties) {
                // remove _fileName key included with image field and not necessary for API
                if (
                  key.includes("_fileName") &&
                  modified.properties.hasOwnProperty(
                    key.replace("_fileName", "")
                  )
                ) {
                  delete modified.properties[key];
                }
              }

              const feat = this.format.readFeature(modified.geometry);
              feat.setProperties({ gid: modified.id });
              feat.setProperties(modified.properties);
              // vérifier que le gid existe
              modifiedFeatures.push(feat);
            });
            forkJoinObservables.push(
              this.editingToolService.modify(
                this.editableLayer.get("layerMetadataUuid"),
                this.geojson.writeFeaturesObject(modifiedFeatures)
              )
            );
          }

          if (
            this.editingSession &&
            this.editingSession.deletedFeatures.length
          ) {
            let gids = "";
            this.editingSession.deletedFeatures.forEach((deleted, index) => {
              if (index === 0) {
                gids += deleted.id;
              } else {
                gids += `,${deleted.id}`;
              }
            });
            forkJoinObservables.push(
              this.editingToolService.delete(
                this.editableLayer.get("layerMetadataUuid"),
                gids
              )
            );
          }

          this.isLoading = true;

          const apiCalls$ = concat(...forkJoinObservables).pipe(
            every((responses) => {
              if (
                responses &&
                responses instanceof Array &&
                responses.length > 0
              ) {
                const resWithError = responses.find((response) => {
                  return !!response.reject;
                });

                if (resWithError) {
                  this.saveError.description = resWithError.msg;
                  this.saveError.openModal();
                  this.isLoading = false;
                  return false;
                } else {
                  rootLayers.forEach((layer: ImageLayer<ImageSource>) => {
                    if (
                      layer.get("layerMetadataUuid") ===
                      this.editableLayer.get("layerMetadataUuid")
                    ) {
                      (layer.getSource() as ImageWMS).updateParams({
                        date: new Date(),
                      });
                      layer.changed();
                    }
                  });

                  if (this.saveGlobal && responses[0].gid) {
                    this.editingSession.addedFeatures[
                      this.editingSession.addedFeatures.length - 1
                    ].properties["gid"] = responses[0].gid;
                    this.editingToolService.setGlobalFeature(
                      this.editingSession.addedFeatures[
                        this.editingSession.addedFeatures.length - 1
                      ]
                    );
                  }

                  this.editingToolService.setEditingSession(null);
                  this.activeModal.close();
                  return true;
                }
              }

              this.isToastVisible = true;
              this.copyToast.nativeElement.className = "toast show";
              setTimeout(() => {
                this.copyToast.nativeElement.className =
                  this.copyToast.nativeElement.className.replace("show", "");
              }, 2000);
              this.isToastVisible = false;

              return false;
            }),
            tap((e) => {})
          );

          apiCalls$.subscribe();
        }
      });
    }, 500);
  }

  public setMapId(newMapId: string): void {
    this.mapId$.next(newMapId);
  }

  public setSaveGlobal(mode) {
    this.saveGlobal = mode;
  }

  public setEditionConfig(configs: EditionConfig[]): void {
    this.editionConfig = configs.find((config) => {
      return config.uuid === this.editableLayer.get("layerMetadataUuid");
    });
  }

  public setEditableLayer(layer: ImageLayer<ImageSource>): void {
    this.editableLayer = layer;
  }

  public setSelectedSnappingLayerFeatures(features: any) {
    this.selectedSnappingLayerFeatures = features;
  }

  public setTolerance(tolerance: number) {
    this.tolerance = tolerance;
  }

  public setSelectedEditableLayerFeatures(features: any) {
    this.selectedEditableLayerFeatures = features;
  }

  public setEditableFeaturesStructure(structure: any) {
    this.editableFeaturesStructure = structure;
  }

  public setLayerType(type: string) {
    this.layerType = type;
  }

  private stopInteractions() {
    this.draw.setActive(false);

    this.selectFeature.getFeatures().clear();
    this.nbSelectedFeatures = this.selectFeature.getFeatures().getLength();
    this.selectFeature.setActive(false);

    this.selectMultiFeature.getFeatures().clear();
    this.nbSelectedFeatures = this.selectMultiFeature.getFeatures().getLength();
    this.selectMultiFeature.setActive(false);

    this.selectCut.getFeatures().clear();
    this.selectCut.setActive(false);

    this.drawGeomToFeature.setActive(false);

    this.modifySelect.setActive(false);
    this.translateFeature.setActive(false);
    this.drawHole.setActive(false);
    this.snapSplit.setActive(false);
    this.drawSplit.setActive(false);

    this.editionMode = {
      draw: false,
      select: false,
      edit: false,
      translate: false,
      hole: false,
      merge: false,
      selectCut: false,
      drawGeomToFeature: false,
      multiSelect: false,
    };
  }

  /**
   * convertie une distance pixel en metres
   * @return une distance en mettre
   */
  private getDistanceMeterFromPixel(
    pixel: number[],
    sizeInPixel: number
  ): number {
    const pixelB = [pixel[0] + sizeInPixel, pixel[1]];
    const pointA = this.map.getCoordinateFromPixel(pixel);
    const pointB = this.map.getCoordinateFromPixel(pixelB);

    const line = new LineString([pointA, pointB]);

    return line.getLength();
  }

  private updateFeatureWithGeom(
    feature: Feature<Geometry>,
    geometry: Geometry
  ) {
    feature.setGeometry(geometry);
    if ((feature.getGeometry() as Point).getCoordinates().length === 0) {
      this.featureDeleted.openModal().then((result) => {
        if (result) {
          this.updateEditedFeatures("delete", feature);
        }
      });
    } else {
      this.updateEditedFeatures("modify", feature);
    }
  }

  /** Supression d'une partie d'un multi-objet */
  private applySelectCut(event): void {
    const coordinate = event.mapBrowserEvent.coordinate;

    let features = this.selectCut.getFeatures();
    features = features ? features : new Collection<Feature<any>>();

    features.forEach((feature) => {
      if (feature.getGeometry().getType() === "MultiPolygon") {
        const polygons = (feature.getGeometry() as MultiPolygon).getPolygons();
        const multiPolygon = new MultiPolygon([]);

        polygons.forEach((polygon) => {
          if (!this.containXY(polygon, coordinate)) {
            multiPolygon.appendPolygon(polygon);
          }
        });

        this.updateFeatureWithGeom(feature, multiPolygon);
      } else if (feature.getGeometry().getType() === "MultiLineString") {
        const bufferSize = this.getDistanceMeterFromPixel(
          event.mapBrowserEvent.pixel,
          10
        );

        const lineStrings = (
          feature.getGeometry() as MultiLineString
        ).getLineStrings();
        const multiLineString = new MultiLineString([]);

        lineStrings.forEach((lineString) => {
          if (!this.intersectXY(lineString, coordinate, bufferSize)) {
            multiLineString.appendLineString(lineString);
          }
        });

        this.updateFeatureWithGeom(feature, multiLineString);
      } else if (feature.getGeometry().getType() === "MultiPoint") {
        const bufferSize = this.getDistanceMeterFromPixel(
          event.mapBrowserEvent.pixel,
          10
        );

        const points = (feature.getGeometry() as MultiPoint).getPoints();
        const multiPoint = new MultiPoint([]);

        points.forEach((point) => {
          if (!this.intersectXY(point, coordinate, bufferSize)) {
            multiPoint.appendPoint(point);
          }
        });

        this.updateFeatureWithGeom(feature, multiPoint);
      }
    });

    this.selectCut.getFeatures().clear();
  }

  /** ajout d'une geometrie à une feature */
  private applySelectGeomToFeature(
    selectedFeatures: Feature<Geometry>[],
    geoms: Feature<Geometry>
  ): void {
    if (selectedFeatures.length === 1) {
      const feature = selectedFeatures[0];
      const newJstsGeom = this.mergePolygon(feature, geoms);

      const newOlGeom = this.format.readGeometry(newJstsGeom);

      feature.setGeometry(newOlGeom);
      this.stopInteractions();

      this.updateEditedFeatures("modify", feature);

      geoms.setGeometry(new MultiPoint([]));
    } else {
      console.error("trop de feature sélectionnée");
    }
  }

  /** */
  private getObjetsFromMultiGeometry(geometrys: Geometry) {
    switch (geometrys.getType()) {
      case "MultiPolygon":
        return (geometrys as MultiPolygon).getPolygons();
      case "MultiLineString":
        return (geometrys as MultiLineString).getLineStrings();
      case "MultiPoint":
        return (geometrys as MultiPoint).getPoints();
      default:
        console.error("geomtry error type : ", geometrys.getType());
        return null;
    }
  }

  private splitLineString(
    featToCut: Feature<any>,
    featLineCut: Feature<any>
  ): Array<string> {
    const newFeaturesWKT: Array<string> = [];

    if (featToCut.getGeometry().getType() === "MultiLineString") {
      featToCut.setGeometry(featToCut.getGeometry().getLineStrings()[0]);
    }

    const geojsonA = this.geojson.writeFeatureObject(featToCut);
    const geojsonB = this.geojson.writeFeatureObject(featLineCut) as Splitter;

    const split = lineSplit<any>(geojsonA, geojsonB);

    split.features.forEach((geojsonFeat) => {
      const feature = this.geojson.readFeature(geojsonFeat);
      newFeaturesWKT.push(this.format.writeFeature(feature));
    });

    return newFeaturesWKT;
  }

  private splitPolygon(
    featToCut: Feature<any>,
    featLineCut: Feature<any>
  ): Array<string> {
    const newFeaturesWKT: Array<string> = [];

    if (featToCut.getGeometry().getType() === "MultiPolygon") {
      featToCut.setGeometry(featToCut.getGeometry().getPolygons()[0]);
    }
    const wktA = this.format.writeFeature(featToCut);
    const wktB = this.format.writeFeature(featLineCut);

    const reader = new WKTReader(new GeometryFactory());
    const writer = new WKTWriter(new GeometryFactory());

    const a: Polygon = reader.read(wktA);
    const b = reader.read(wktB);
    const union = UnionOp.union(a.getExteriorRing(), b);

    const polygonizer = new Polygonizer();
    polygonizer.add(union);

    const polygons = polygonizer.getPolygons();

    for (const i = polygons.iterator(); i.hasNext(); ) {
      const polygon = i.next();
      if (RelateOp.contains(BufferOp.bufferOp(a, 0.0001), polygon)) {
        newFeaturesWKT.push(writer.write(polygon));
      }
    }

    return newFeaturesWKT;
  }

  private mergePolygon(
    featToMergeA: Feature<Geometry>,
    featToMergeB: Feature<Geometry>
  ): string {
    let newFeaturesWKT: string = null;

    try {
      const wktA = this.format.writeFeature(featToMergeA);
      const wktB = this.format.writeFeature(featToMergeB);

      const reader = new WKTReader(new GeometryFactory());
      const writer = new WKTWriter(new GeometryFactory());

      const a = reader.read(wktA);
      const b = reader.read(wktB);

      const union = UnionOp.union(a, b);

      newFeaturesWKT = writer.write(union);
    } catch (error) {
      console.error(error);
    }

    return newFeaturesWKT;
  }

  private containXY(
    geom: Geometry,
    coordinate: number[],
    bufferSize: number = null
  ): boolean {
    const wktA = this.format.writeGeometry(geom);

    const factory = new GeometryFactory();
    const reader = new WKTReader(new GeometryFactory());

    const jstsGeom = reader.read(wktA);

    let point = factory.createPoint(
      new Coordinate(coordinate[0], coordinate[1])
    );

    if (bufferSize) {
      point = BufferOp.bufferOp(point, bufferSize);
    }

    return RelateOp.contains(jstsGeom, point);
  }

  private intersectXY(
    geom: Geometry,
    coordinate: number[],
    bufferSize: number = null
  ): boolean {
    const wktA = this.format.writeGeometry(geom);

    const factory = new GeometryFactory();
    const reader = new WKTReader(new GeometryFactory());

    const jstsGeom = reader.read(wktA);

    let point = factory.createPoint(
      new Coordinate(coordinate[0], coordinate[1])
    );

    if (bufferSize) {
      point = BufferOp.bufferOp(point, bufferSize);
    }

    return RelateOp.intersects(jstsGeom, point);
  }

  private initMultiSelect(): void {
    this.selectMultiFeature = new Select({
      layers: [this.editableVectorLayer],
      style: this.drawLayerService.getDefaultDrawStyle(),
    });

    this.selectMultiFeature.setActive(false);
    this.map.addInteraction(this.selectMultiFeature);
    this.nbSelectedFeatures = this.selectMultiFeature.getFeatures().getLength();
    this.selectMultiFeature.on("select", (event: any) => {
      this.nbSelectedFeatures = this.selectMultiFeature
        .getFeatures()
        .getLength();
    });
  }

  private initSelectCut(): void {
    // ajout du select cut pour le mode édition
    this.selectCut = new Select({
      layers: [this.editableVectorLayer],
      style: this.drawLayerService.getDefaultDrawStyle(),
    });

    this.selectCut.setActive(false);
    this.map.addInteraction(this.selectCut);
    this.selectCut.on("select", (event: any) => {
      this.applySelectCut(event);
    });
  }

  private initSelectAddGeom(): void {
    // ajout du select cut pour le mode édition
    this.selectToAddGeom = new Select({
      layers: [this.editableVectorLayer],
      style: this.drawLayerService.getDefaultDrawStyle(),
    });

    this.selectToAddGeom.setActive(false);
    this.map.addInteraction(this.selectToAddGeom);
    this.selectToAddGeom.on("select", (event: any) => {
      this.editionMode.drawGeomToFeature = true;
      this.selectedFeatureToAdd = { ...event.selected };
      this.drawGeomToFeature.setActive(true);
      this.selectToAddGeom.setActive(false);
    });
  }

  private initDrawGeomToFeature(): void {
    // Ajout de l'interaction permettant de dessiner
    this.drawGeomToFeature = new Draw({
      source: this.editableVectorLayer.getSource(),
      type:
        this.layerType === "Linestring"
          ? ("LineString" as any)
          : this.layerType,
      style: this.drawLayerService.getDefaultDrawStyle(),
    });

    // La seule interaction activée par défaut
    this.drawGeomToFeature.setActive(false);
    this.map.addInteraction(this.drawGeomToFeature);
    this.drawGeomToFeature.on("drawstart", (event) => {});
    this.drawGeomToFeature.on("drawend", (event) => {
      if (event && event.feature) {
        this.applySelectGeomToFeature(
          this.selectMultiFeature.getFeatures().getArray(),
          event.feature
        );
      }
    });
  }

  public loadFile(): void {
    const modalRef = this.modalService.open(ImportFileModalComponent, {
      container: "#modals",
      centered: true,
      size: "lg",
      backdrop: "static",
    });

    modalRef.componentInstance.editableVectorLayer = this.editableVectorLayer;
    modalRef.componentInstance.layerFields =
      this.editableLayer.get("LayerQuery").Fields;

    modalRef.result.then((result) => {
      if (this.editableLayer.get("layerMetadataUuid")) {
        this.enumsSubscription = this.editingToolService
          .getUserDefinedEnum(this.editableLayer.get("layerMetadataUuid"))
          .subscribe((userDefinedEnum) => {
            this.addFeaturesFromGeojson(result, userDefinedEnum);
          });
      }
    });
  }

  public addFeaturesFromGeojson(result, enumeration: string[][]): void {
    const geoJson = new GeoJSON();
    const features = geoJson.readFeatures(result.source.data);
    let isOk = true;
    const featuresToAdd = [];
    const featuresToUpdate = [];

    features.some((feature, index) => {
      const retour = this.checkAndProcessData(feature, enumeration);
      const featureProcess = retour.feature;

      if (!retour.msg && featureProcess) {
        const featureToUp = this.findAndUpdateFeature(
          featureProcess,
          result.gidField
        );
        if (!featureToUp) {
          featureProcess.unset("gid");
          featuresToAdd.push(featureProcess);
        } else {
          featureToUp.setGeometry(feature.getGeometry());

          featuresToUpdate.push(featureToUp);
        }

        return false;
      } else {
        this.msgError = retour.msg;
        console.error("ERROR : ", this.msgError, retour.msg);
        isOk = false;
        return !isOk;
      }
    });

    if (!isOk) {
      this.featureError.description = this.msgError;
      this.featureError.openModal();
    } else {
      featuresToAdd.forEach((feature) => {
        this.editableVectorLayer.getSource().addFeature(feature);
        if (
          this.editionConfig &&
          (this.editionConfig.edition || this.editionConfig.edition_ajout)
        ) {
          this.updateEditedFeatures("add", feature);
        }
      });

      featuresToUpdate.forEach((feature) => {
        this.editableVectorLayer.getSource().removeFeature(feature);
        this.editableVectorLayer.getSource().addFeature(feature);
        if (
          this.editionConfig &&
          (this.editionConfig.edition ||
            this.editionConfig.edition_modification)
        ) {
          this.updateEditedFeatures("modify", feature);
        }
      });
    }
  }

  private checkAndProcessData(
    feature: Feature<Geometry>,
    enumeration: string[][]
  ): { msg: string; feature: Feature<Geometry> } {
    let msg = "";
    const layerQueryFieldsNames = this.editableLayer
      .get("LayerQuery")
      .Fields.map((layerQueryField) => layerQueryField.FieldName);
    const datas = Object.entries(
      feature.getProperties() as { [key: string]: any }
    )
      .filter(([key, value]) => layerQueryFieldsNames.includes(key))
      .reduce((accum, [key, value]) => {
        accum[key] = value;
        return accum;
      }, {});
    const featureValues = Object.entries(datas);

    const layerFields = this.editableLayer.get("LayerQuery").Fields;
    if (featureValues.length !== layerFields.length) {
      return {
        msg: "Les champs du fichier importé sont différents des champs de la donnée.",
        feature: null,
      };
    }

    layerFields.forEach((field, idx) => {
      const structure = this.editableFeaturesStructure.filter(
        (feat) => feat.column_name === field.FieldName
      );
      if (structure.length > 0) {
        layerFields[idx].structure = structure[0];
      }
    });

    let containAllField = true;

    featureValues.some((values) => {
      let isOk = false;
      layerFields.some((field) => {
        if (values[0] === "gid") {
          isOk = true;
          return true;
        } else if (field.FieldName !== values[0]) {
          return false;
        } else {
          isOk = true;
        }

        switch (field.structure.data_type) {
          case "ARRAY":
            if (
              enumeration[field.structure.udt_name] &&
              (!field.structure.is_nullable || values[1] || values[1] === 0)
            ) {
              switch (typeof values[1]) {
                case "bigint":
                case "number":
                  values[1] = (values[1] + "").split(",");
                  break;
                case "string":
                  values[1] = values[1].split(",");
                  break;
                case "object":
                  values[1] = values[1] instanceof Array ? values[1] : null;
                  break;
                default:
                  values[1] = null;
                  break;
              }
              containAllField = !!enumeration[field.structure.udt_name].find(
                (r) => (values[1] as string[]).includes(r)
              );
              msg = containAllField
                ? msg
                : values[1] +
                  " n'est pas une valeur valide dans la liste " +
                  field.structure.udt_name;

              if (!msg) {
                feature.set(field.FieldName, values[1]);
              }
            }
            break;
          default:
            break;
        }

        if (!field.structure.is_nullable) {
          containAllField = containAllField && values[1] !== null;
          msg = containAllField
            ? msg
            : "Le champ " + field.FieldName + " ne doit pas être nul.";
        }

        return isOk;
      });

      return !containAllField;
    });

    return { msg, feature };
  }

  private findAndUpdateFeature(
    featureToUp: Feature<Geometry>,
    idRefField: string
  ): Feature<Geometry> {
    const fieldSearch = idRefField ? idRefField : "gid";
    const retour = this.editableVectorLayer
      .getSource()
      .getFeatures()
      .find((feature) => {
        return (
          feature.get(fieldSearch) + "" === featureToUp.get(fieldSearch) + ""
        );
      });

    if (retour) {
      const layerFields = this.editableLayer.get("LayerQuery").Fields;

      layerFields.forEach((field, idx) => {
        retour.set(field.FieldName, featureToUp.get(field.FieldName));
      });
    }

    return retour ? retour : null;
  }
}
