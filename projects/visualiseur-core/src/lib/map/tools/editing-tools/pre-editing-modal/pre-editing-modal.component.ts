import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EditingModalComponent } from "./editing-modal/editing-modal.component";
import { MapService } from "../../../map.service";
import { VisualiseurCoreService } from "../../../../visualiseur-core.service";
import { Subscription, forkJoin, config } from "rxjs";
import {
  EditingToolService,
  EditionSession,
} from "../../../../services/editing-tool.service";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import Map from "ol/Map";
import Layer from "ol/layer/Layer";
import { MapIdService } from "../../../map-id.service";
import ImageSource from "ol/source/Image";
import VectorSource from "ol/source/Vector";
import { Geometry } from "ol/geom";

@Component({
  selector: "alk-pre-editing-modal",
  templateUrl: "./pre-editing-modal.component.html",
  styleUrls: ["./pre-editing-modal.component.scss"],
})
export class PreEditingModalComponent implements OnInit, OnDestroy {
  saveGlobal = false;

  private mapId: string;
  private map: Map;
  forkJoin: Subscription;

  private rootLayerGroupSubscription: Subscription;
  private editableFeaturesSubscription: Subscription;
  private snappingFeaturesSubscription: Subscription;
  @ViewChild("snapTolerance", {read: null, static: true}) tolerance: ElementRef;

  private editionConfigSubscription: Subscription;
  private editionConfig: any;

  editingSessionSubscription: Subscription;
  editingSession: EditionSession = null;
  isEditingSessionSaved = false;

  editableLayers: Array<ImageLayer<ImageSource>>;
  snappingLayers: Array<Layer<any>>;
  selectedEditableLayer: string | ImageLayer<ImageSource>;
  selectedSnappingLayer: string | Layer<any>;

  isLoading = false;

  constructor(
    public activeModal: NgbActiveModal,
    public mapService: MapService,
    public coreService: VisualiseurCoreService,
    private editingToolService: EditingToolService,
    private modalService: NgbModal,
    public mapIdService: MapIdService
  ) {}

  ngOnInit() {
    this.map = this.mapService.getMap(
      this.mapIdService.getId() || "visualiseur"
    );

    this.coreService.setMapTool("editer");

    this.selectedEditableLayer = "noPresetSelected";
    this.selectedSnappingLayer = "noPresetSelected";
    this.editableLayers = [];
    this.snappingLayers = [];

    this.editionConfigSubscription = this.coreService
      .getContext()
      .subscribe((context) => {
        if (!context) {
          return false;
        }
        this.editionConfig = context.properties.extension.Tools.Edition
          ? context.properties.extension.Tools.Edition.config
          : null;
        this.rootLayerGroupSubscription = this.coreService
          .getRootLayerGroup()
          .subscribe((rootLayerGroup) => {
            if (this.editionConfig) {
              this.editionConfig.forEach((layerConfig) => {
                rootLayerGroup.getLayersArray().forEach((layer) => {
                  if (layer.get("layerMetadataUuid")) {
                    if (
                      layer.get("layerType") === "POSTGIS" &&
                      this.snappingLayers.filter(
                        (snappingLayer) =>
                          layer.get("layerIdx") ===
                          snappingLayer.get("layerIdx")
                      ).length === 0
                    ) {
                      this.snappingLayers.push(layer);
                    }
                    if (layerConfig.uuid === layer.get("layerMetadataUuid")) {
                      this.editableLayers.push(layer as ImageLayer<ImageSource>);
                    }
                  }
                });
              });
            }
          });
      });

    this.editingSessionSubscription = this.editingToolService
      .getEditingSession()
      .subscribe((editionSession) => {
        if (editionSession) {
          this.editingSession = editionSession;
        }
      });
    if (this.editingSession) {
      this.isEditingSessionSaved = true;
    }
  }

  ngOnDestroy() {
    if (this.editionConfigSubscription) {
      this.editionConfigSubscription = null;
    }
    if (this.editingSessionSubscription) {
      this.editingSessionSubscription = null;
    }
    if (this.rootLayerGroupSubscription) {
      this.rootLayerGroupSubscription = null;
    }
    if (this.editableFeaturesSubscription) {
      this.editableFeaturesSubscription = null;
    }
    if (this.snappingFeaturesSubscription) {
      this.snappingFeaturesSubscription = null;
    }
    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }
  }

  startEdition(): void {
    if (
      this.selectedEditableLayer === "noPresetSelected" &&
      this.isEditingSessionSaved
    ) {
      if (
        this.map.getView().getProjection().getExtent() !==
        this.editingSession.mapExtent
      ) {
        this.map.getView().fit(this.editingSession.mapExtent);
        this.mapService.setMapFitEvent(true);
      }
      this.selectedEditableLayer = this.editableLayers.filter(
        (layer) =>
          layer.get("layerMetadataUuid") === this.editingSession.idEditableLayer
      )[0];
      if (this.editingSession.idSnappingLayer) {
        this.selectedSnappingLayer = this.snappingLayers.filter(
          (layer) =>
            layer.get("layerMetadataUuid") ===
            this.editingSession.idSnappingLayer
        )[0];
      }
    }

    if (!this.isEditingSessionSaved) {
      this.editingSession = {
        mapExtent: this.map.getView().getProjection().getExtent(),
        idEditableLayer: (
          this.selectedEditableLayer as ImageLayer<ImageSource>
        ).get("layerMetadataUuid"),
        idSnappingLayer:
          this.selectedSnappingLayer !== "noPresetSelected"
            ? (this.selectedSnappingLayer as Layer<any>).get(
                "layerMetadataUuid"
              )
            : null,
        tolerance: this.tolerance.nativeElement.value,
        addedFeatures: [],
        modifiedFeatures: [],
        deletedFeatures: [],
      };
      this.editingToolService.setEditingSession(this.editingSession);
    }

    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }

    const forkJoinObservables = [];
    forkJoinObservables.push(
      this.editingToolService.getEditableFeatures(
        (this.selectedEditableLayer as ImageLayer<ImageSource>).get(
          "layerMetadataUuid"
        ),
        this.map.getView().calculateExtent(this.map.getSize())
      )
    );
    forkJoinObservables.push(
      this.editingToolService.getEditableFeaturesStructure(
        (this.selectedEditableLayer as ImageLayer<ImageSource>).get(
          "layerMetadataUuid"
        )
      )
    );
    if (this.selectedSnappingLayer !== "noPresetSelected") {
      forkJoinObservables.push(
        this.editingToolService.getSnappingFeatures(
          (this.selectedSnappingLayer as Layer<any>).get("layerMetadataUuid"),
          this.map.getView().calculateExtent(this.map.getSize())
        )
      );
    }

    this.isLoading = true;
    this.forkJoin = forkJoin(forkJoinObservables).subscribe((response: any) => {
      this.isLoading = false;
      if (response[0]) {
        this.editingSession.modifiedFeatures.forEach((modified, index) => {
          let exists = false;
          response[0].features.forEach((feature) => {
            if (feature.properties.gid === modified.id) {
              exists = true;
            }
          });
          if (!exists) {
            this.editingSession.modifiedFeatures.splice(index, 1);
          }
        });

        this.editingSession.deletedFeatures.forEach((deleted, index) => {
          let exists = false;
          response[0].features.forEach((feature) => {
            if (feature.properties.gid === deleted.id) {
              exists = true;
            }
          });
          if (!exists) {
            this.editingSession.deletedFeatures.splice(index, 1);
          }
        });
      }

      const modalRef = this.modalService.open(EditingModalComponent, {
        size: "sm",
        windowClass: "window-tool editing-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });

      if (modalRef.componentInstance.setSaveGlobal) {
        modalRef.componentInstance.setSaveGlobal(this.saveGlobal);
      }
      if (modalRef.componentInstance.setMapId) {
        this.editionConfig.forEach((config) => {
          if (
            config.uuid ===
            (this.selectedEditableLayer as ImageLayer<ImageSource>).get(
              "layerMetadataUuid"
            )
          ) {
            modalRef.componentInstance.setLayerType(
              config.type ? config.type : "Polygon"
            );
          }
        });
        modalRef.componentInstance.setMapId(
          this.mapIdService.getId() || "visualiseur"
        );
        modalRef.componentInstance.setEditableLayer(this.selectedEditableLayer);
        modalRef.componentInstance.setEditionConfig(this.editionConfig);

        if (response[1]) {
          modalRef.componentInstance.setSelectedSnappingLayerFeatures(
            response[2]
          );
          modalRef.componentInstance.setTolerance(
            this.editingSession.tolerance
              ? this.editingSession.tolerance
              : this.tolerance.nativeElement.value
          );
        }
        modalRef.componentInstance.setSelectedEditableLayerFeatures(
          this.editionConfig.edition
            ? response[0]
            : this.editionConfig.edition_ajout
            ? []
            : response[0]
        );
        modalRef.componentInstance.setEditableFeaturesStructure(
          response[1] ? response[1] : null
        );
      }

      this.activeModal.close();
    });
  }

  public setMapId(mapId: string): void {
    this.mapId = mapId;
  }

  public setSaveGlobal(mode: boolean): void {
    this.saveGlobal = mode;
  }
}
