import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alk-measuring-modal',
  templateUrl: './measuring-modal.component.html',
  styleUrls: ['./measuring-modal.component.scss']
})
export class MeasuringModalComponent implements OnInit, OnDestroy {

  measuringOptionSubscription: Subscription;
  measuringOption: string;
  contextSubscription: Subscription;
  tools: any;
  type = 'length';
  @Input() screenMode: 'desktop' | 'mobile' = 'desktop';

  constructor(
    public activeModal: NgbActiveModal,
    private coreService: VisualiseurCoreService) { }

  ngOnInit() {
    this.setType(this.type);
    this.measuringOptionSubscription = this.coreService.getMeasuringOption().subscribe(option => {
      this.measuringOption = option;
    }, error => console.error(error));
    this.contextSubscription = this.coreService.getContext().subscribe(
      (context) => {
        this.tools = context.properties.extension.Tools;
      },
      (error) => console.error(error)
    );
  }

  ngOnDestroy(): void {
    if (this.measuringOptionSubscription) {
      this.measuringOptionSubscription.unsubscribe();
    }
  }

  setType(typeSelected: string) {
    this.coreService.setMeasuringOption(typeSelected);
  }

  setScreenMode(mode: 'desktop' | 'mobile') {
    this.screenMode = mode;
  }

}
