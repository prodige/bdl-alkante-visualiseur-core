import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuringModalComponent } from './measuring-modal.component';

describe('MeasuringModalComponent', () => {
  let component: MeasuringModalComponent;
  let fixture: ComponentFixture<MeasuringModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasuringModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuringModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
