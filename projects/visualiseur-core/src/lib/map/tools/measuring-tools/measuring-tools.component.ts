import {
  Component,
  OnInit,
  Host,
  Optional,
  Input,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { Subscription } from "rxjs";

import { MapService } from "../../map.service";
import { MapIdService } from "../../map-id.service";
import { VisualiseurCoreService } from "../../../visualiseur-core.service";

import Map from "ol/Map";
import { unByKey } from "ol/Observable";
import Overlay from "ol/Overlay";
import { getArea, getLength } from "ol/sphere";
import { LineString, Polygon, Geometry, Point, MultiLineString } from "ol/geom";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { OSM, Vector as VectorSource } from "ol/source";
import { Circle as CircleStyle, Fill, Stroke, Style } from "ol/style";
import Feature from "ol/Feature";
import { Draw } from "ol/interaction";
import Profil from "ol-ext/control/Profile";
import { Services, olExtended } from "geoportal-extensions-openlayers";

@Component({
  selector: "alk-measuring-tools",
  templateUrl: "./measuring-tools.component.html",
  styleUrls: ["./measuring-tools.component.scss"],
})
export class MeasuringToolsComponent implements OnInit, OnDestroy {
  @Input() isHmVisible = true;

  subscriptions: Subscription = new Subscription();
  map: Map;
  mapId: string;
  mapReadyOnce = false;

  // raster = new TileLayer({
  //   source: new OSM()
  // });
  source = new VectorSource({ wrapX: false });
  vector = new VectorLayer({
    source: this.source,
    style: new Style({
      fill: new Fill({
        color: "rgba(255, 255, 255, 0.2)",
      }),
      stroke: new Stroke({
        color: "#ffcc33",
        width: 2,
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: "#ffcc33",
        }),
      }),
    }),
  });
  sketch: Feature<any>;
  mapOverlays: Array<Overlay>;

  isMeasuring = false;
  @ViewChild("helpTooltip", { read: null, static: true })
  helpTooltipElementRef: ElementRef;
  helpTooltip: Overlay;

  @ViewChild("measureTooltip", { read: null, static: true })
  measureTooltipElementRef: ElementRef;
  measureTooltip: Overlay;
  typeSelect: string;
  draw: Draw;
  profil: olExtended.control.ElevationPath = null;
  altiPoint: Feature;

  constructor(
    private elementRef: ElementRef,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.subscriptions.add(
      this.mapService.getMapReady(this.mapId).subscribe((map) => {
        this.map = map;

        if (this.map) {
          this.mapReadyOnce = true;

          Services.getConfig({
            apiKey: "essentiels",
          });

          if (!this.isHmVisible) {
            this.map.getViewport().style.cursor = "pointer";
          }

          this.mapOverlays = [];
          this.vector.setMap(this.map);

          this.map.on("pointermove", (evt: any) => {
            if (evt.dragging) {
              return;
            }
            if (this.helpTooltip) {
              this.helpTooltip.setPosition(evt.coordinate);
              (
                this.helpTooltipElementRef.nativeElement as HTMLElement
              ).removeAttribute("hidden");
              (
                this.helpTooltipElementRef.nativeElement as HTMLElement
              ).classList.remove("hidden");
            }
          });

          this.map.getViewport().addEventListener("mouseout", () => {
            if (this.helpTooltip) {
              (
                this.helpTooltipElementRef.nativeElement as HTMLElement
              ).setAttribute("hidden", "true");
              (
                this.helpTooltipElementRef.nativeElement as HTMLElement
              ).classList.add("hidden");
            }
          });

          this.subscriptions.add(
            this.coreService.getMeasuringOption().subscribe(
              (opt) => {
                if (this.typeSelect !== opt) {
                  this.typeSelect = opt;
                  this.map.removeInteraction(this.draw);
                  this.addInteraction();
                }
              },
              (error) => console.error(error)
            )
          );
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.mapOverlays.forEach((overlay, index) => {
      this.map.removeOverlay(overlay);
    });
    if (this.profil) {
      this.profil.clean();
      this.profil.setActive(false);
      this.map.removeControl(this.profil);
      this.profil.setMap(null);
      this.profil = null;
    }
    this.mapOverlays.length = 0;
    this.map.removeInteraction(this.draw);
    this.vector.setMap(null);
    this.map.getViewport().style.cursor = "";
    this.typeSelect = null;
  }

  formatLength(line): string {
    const distance = getLength(line, {
      projection: this.map.getView().getProjection(),
    });

    const distanceInM = Math.round(distance * 100) / 100;
    const distanceInHa = Math.round((distance / 100) * 100) / 100;
    const distanceInKm = Math.round((distance / 1000) * 100) / 100;

    let ret = "";
    if (this.isHmVisible) {
      ret = `${distanceInM}m | ${distanceInHa}'hm | ${distanceInKm}km`;
    } else {
      ret = `${distanceInM}m | ${distanceInKm}km`;
    }

    return ret;
  }

  formatArea(polygon): string {
    const area = getArea(polygon, {
      projection: this.map.getView().getProjection(),
    });

    const areaInM = Math.round(area * 100) / 100;
    const areaInHa = Math.round((area / 10000) * 100) / 100;
    const areaInKm = Math.round((area / 1000000) * 100) / 100;

    let ret = "";
    if (this.isHmVisible) {
      ret = `${areaInM}m<sup>2</sup> | ${areaInHa}'ha | ${areaInKm}km<sup>2</sup>`;
    } else {
      ret = `${areaInM}m<sup>2</sup> | ${areaInKm}km<sup>2</sup>`;
    }

    return ret;
  }

  addInteraction() {
    if (this.typeSelect !== "altimetry") {
      if (this.profil) {
        this.profil.clean();
        this.profil.setActive(false);
        this.map.removeControl(this.profil);
        this.profil.setMap(null);
        this.profil = null;
      }
      const type = this.typeSelect === "area" ? "Polygon" : "MultiLineString";
      this.draw = new Draw({
        source: this.source,
        type,
        style: new Style({
          fill: new Fill({
            color: "rgba(255, 255, 255, 0.2)",
          }),
          stroke: new Stroke({
            color: "rgba(0, 0, 0, 0.5)",
            lineDash: [10, 10],
            width: 2,
          }),
          image: new CircleStyle({
            radius: 5,
            stroke: new Stroke({
              color: "rgba(0, 0, 0, 0.7)",
            }),
            fill: new Fill({
              color: "rgba(255, 255, 255, 0.2)",
            }),
          }),
        }),
      });
      this.map.addInteraction(this.draw);

      this.createMeasureTooltip();
      this.createHelpTooltip();
    } else {
      this.map.removeOverlay(this.helpTooltip);
      this.map.removeOverlay(this.measureTooltip);
      this.map.updateSize();

      this.profil = new olExtended.control.ElevationPath({
        active: true,
      });
      this.map.addControl(this.profil);
      // this.profil.show();
    }

    let listener;
    this.draw.on("drawstart", (evt) => {
      this.isMeasuring = true;
      // set sketch
      const sketch = evt.feature;
      let tooltipCoord = (
        (evt.feature as Feature<Geometry>).getGeometry() as Point
      ).getCoordinates();
      (this.helpTooltipElementRef.nativeElement as HTMLElement).setAttribute(
        "hidden",
        "true"
      );
      (this.helpTooltipElementRef.nativeElement as HTMLElement).classList.add(
        "hidden"
      );

      listener = sketch.getGeometry().on("change", (event) => {
        const geom = event.target;
        let output;
        if (geom instanceof Polygon) {
          output = this.formatArea(geom);
          tooltipCoord = geom.getInteriorPoint().getCoordinates();
        } else if (geom instanceof LineString) {
          output = this.formatLength(geom);
          tooltipCoord = geom.getLastCoordinate();
        }
        (this.measureTooltipElementRef.nativeElement as HTMLElement).innerHTML =
          output;
        this.measureTooltip.setPosition(tooltipCoord);
      });
    });

    this.draw.on("drawend", (evt) => {
      this.isMeasuring = false;
      let drawnFeature = evt.feature as Feature;
      (this.helpTooltipElementRef.nativeElement as HTMLElement).setAttribute(
        "hidden",
        "true"
      );
      (this.helpTooltipElementRef.nativeElement as HTMLElement).classList.add(
        "hidden"
      );

      const newMeasureTooltipElement = document.createElement("div");
      newMeasureTooltipElement.className = "tooltip tooltip-static";
      newMeasureTooltipElement.innerText = (
        this.measureTooltipElementRef.nativeElement as HTMLElement
      ).innerText;
      (this.elementRef.nativeElement as HTMLElement).prepend(
        newMeasureTooltipElement
      );
      (this.measureTooltip as Overlay).setElement(newMeasureTooltipElement);

      this.measureTooltip.setOffset([0, -7]);
      this.createMeasureTooltip();
      unByKey(listener);
    });
  }

  // Draw a point on the map when mouse fly over profil
  drawPoint(e) {
    if (!this.altiPoint) return;
    if (e.type == "over") {
      // Show point at coord
      this.altiPoint.setGeometry(new Point(e.coord));
      this.altiPoint.setStyle(null);
    } else {
      // hide point
      this.altiPoint.setStyle([]);
    }
  }

  createHelpTooltip() {
    this.helpTooltip = new Overlay({
      element: this.helpTooltipElementRef.nativeElement,
      offset: [15, 0],
      positioning: "center-left",
    });
    this.map.addOverlay(this.helpTooltip);
    this.mapOverlays.push(this.helpTooltip);
  }

  createMeasureTooltip() {
    this.measureTooltip = new Overlay({
      element: this.measureTooltipElementRef.nativeElement,
      offset: [0, -15],
      positioning: "bottom-center",
    });
    this.map.addOverlay(this.measureTooltip);
    this.mapOverlays.push(this.measureTooltip);
    this.map.updateSize();
  }
}
