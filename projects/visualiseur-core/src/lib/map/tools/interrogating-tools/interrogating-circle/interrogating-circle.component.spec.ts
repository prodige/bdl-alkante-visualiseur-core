import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingCircleComponent } from './interrogating-circle.component';

describe('InterrogatingCircleComponent', () => {
  let component: InterrogatingCircleComponent;
  let fixture: ComponentFixture<InterrogatingCircleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingCircleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
