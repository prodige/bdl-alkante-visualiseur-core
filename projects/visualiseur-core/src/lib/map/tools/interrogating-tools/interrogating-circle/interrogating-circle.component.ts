import { Component, OnInit, OnDestroy, Host, Optional } from "@angular/core";

import Map from "ol/Map";
import Feature from "ol/Feature";
import { MapService } from "../../../map.service";
import { MapIdService } from "../../../map-id.service";
import { Draw } from "ol/interaction";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import Circle from "ol/geom/Circle";
import LayerGroup from "ol/layer/Group";
import { InterrogatingService } from "../../../services/interrogating.service";
import { VisualiseurCoreService } from "../../../../visualiseur-core.service";

import { Subscription } from "rxjs";
import { fromCircle } from "ol/geom/Polygon";
import { Geometry } from "ol/geom";

@Component({
  selector: "alk-interrogating-circle",
  templateUrl: "./interrogating-circle.component.html",
  styleUrls: ["./interrogating-circle.component.scss"],
})
export class InterrogatingCircleComponent implements OnInit, OnDestroy {
  map: Map;
  mapId: string;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  drawSymbol: Draw;
  rootLayerGroupSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  interrogationSubscription: Subscription;

  constructor(
    public mapService: MapService,
    public mapIdService: MapIdService,
    public interrogationService: InterrogatingService,
    public coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {
    this.rootLayerGroupSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((rootLayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      });

    // ajouter un evenement sur la carte au clic
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;

      if (this.map) {
        // Initialisation du vectorDrawLayer
        this.vectorLayer = new VectorLayer({
          source: new VectorSource({
            wrapX: false,
            features: [],
          }),
          zIndex: Infinity,
        });

        this.drawSymbol = new Draw({
          type: "Circle",
          source: this.vectorLayer.getSource(),
        });

        this.drawSymbol.on("drawend", (event: any) => {
          this.interrogationService.setResultSearching(true);
          if (this.interrogationSubscription) {
            this.interrogationSubscription.unsubscribe();
          }
          let feature: Feature<Circle> = event.feature;

          if (feature && this.rootLayerGroup) {
            feature = new Feature<any>({
              name: "tooltip",
              geometry: fromCircle(feature.getGeometry()),
            });

            const interrogableLayers = this.rootLayerGroup
              .getLayersArray()
              .filter((layer) => {
                try {
                  if (
                    layer &&
                    layer.getVisible() &&
                    layer.get("visibleAtScale") &&
                    layer.get("visibleAtExtent") &&
                    layer.get("LayerQuery") &&
                    layer.get("LayerQuery").Fields &&
                    layer.get("LayerQuery").Fields.length &&
                    (layer.getSource() as any).getUrl()
                  ) {
                    return true;
                  } else if (
                    layer &&
                    layer.getVisible() &&
                    layer.get("visibleAtScale") &&
                    layer.get("visibleAtExtent") &&
                    layer.get("externalLayer") === true &&
                    layer instanceof VectorLayer &&
                    layer.getSource() instanceof VectorSource
                  ) {
                    return true;
                  }
                } catch (exe) {
                  console.error(exe, layer);
                }
                return false;
              });

            this.interrogationService.setSpinner(true);

            let updateInterrogation = true;
            this.interrogationSubscription = this.interrogationService
              .postInterrogation(
                interrogableLayers as VectorLayer<VectorSource<Geometry>>[],
                feature,
                this.map.getView().getProjection()
              )
              .subscribe({
                next: (results) => {},
                error: (err) => {
                  console.error(
                    "this.interrogationService.postInterrogation error",
                    err
                  );
                },
                complete: () => {},
              });
          }
          // A la fin du traitement, on retire le point
          this.vectorLayer.getSource().clear();
        });

        this.drawSymbol.setActive(true);
        this.map.addInteraction(this.drawSymbol);
      }
    });
  }

  ngOnDestroy() {
    // retirer l'evenement sur la carte au clic
    this.drawSymbol.setActive(false);
    this.map.removeInteraction(this.drawSymbol);
    if (this.rootLayerGroupSubscription) {
      this.rootLayerGroupSubscription.unsubscribe();
    }

    this.map.getOverlays().forEach((overlay, index) => {
      this.map.removeOverlay(overlay);
    });

    if (this.interrogationSubscription) {
      this.interrogationSubscription.unsubscribe();
    }

    this.interrogationService.resetResultOfInterrogation();
  }
}
