import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingPolygonComponent } from './interrogating-polygon.component';

describe('InterrogatingPolygonComponent', () => {
  let component: InterrogatingPolygonComponent;
  let fixture: ComponentFixture<InterrogatingPolygonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingPolygonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingPolygonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
