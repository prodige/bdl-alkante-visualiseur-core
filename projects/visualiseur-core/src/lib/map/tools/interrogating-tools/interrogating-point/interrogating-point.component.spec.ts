import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingPointComponent } from './interrogating-point.component';

describe('InterrogatingPointComponent', () => {
  let component: InterrogatingPointComponent;
  let fixture: ComponentFixture<InterrogatingPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
