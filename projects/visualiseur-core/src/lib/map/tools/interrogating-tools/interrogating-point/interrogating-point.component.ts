import { EnvCoreService } from "./../../../../env.core.service";
import {
  Component,
  OnInit,
  OnDestroy,
  Host,
  Optional,
  Injectable,
  Inject,
  Input,
} from "@angular/core";

import Map from "ol/Map";
import Feature from "ol/Feature";
import { MapService } from "../../../map.service";
import { MapIdService } from "../../../map-id.service";
import { Draw } from "ol/interaction";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import Point from "ol/geom/Point";
import LayerGroup from "ol/layer/Group";
import { InterrogatingService } from "../../../services/interrogating.service";
import { VisualiseurCoreService } from "../../../../visualiseur-core.service";

import { Subscription } from "rxjs";
import Circle from "ol/geom/Circle";
import { fromCircle } from "ol/geom/Polygon";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";
import { ImageWMS, TileWMS } from "ol/source";
import { FilterLayer } from "../../../../models/filter-layer";
import { Geometry } from "ol/geom";

@Injectable()
@Component({
  selector: "alk-interrogating-point",
  templateUrl: "./interrogating-point.component.html",
  styleUrls: ["./interrogating-point.component.scss"],
})
export class InterrogatingPointComponent implements OnInit, OnDestroy {
  @Input() filterLayers: FilterLayer[];

  screenMode: "mobile" | "desktop";
  map: Map;
  mapId: string;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  drawSymbol: Draw;
  rootLayerGroup: LayerGroup;
  mapReadyOnce = false;

  interrogationSubscription: Subscription = new Subscription();
  subscriptions: Subscription = new Subscription();

  constructor(
    public mapService: MapService,
    public mapIdService: MapIdService,
    public interrogationService: InterrogatingService,
    public coreService: VisualiseurCoreService,
    public environment: EnvCoreService
  ) {}

  ngOnInit() {
    this.coreService.getInterrogatingOption().subscribe((mode) => {
      this.screenMode = mode;
    });

    // ajouter un evenement sur la carte au clic
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.subscriptions.add(
      this.mapService.getMapReady(this.mapId).subscribe((map) => {
        this.map = map;

        if (this.map) {
          this.mapReadyOnce = true;
          // Initialisation du vectorDrawLayer
          this.vectorLayer = new VectorLayer({
            source: new VectorSource({
              wrapX: false,
              features: [],
            }),
            zIndex: Infinity,
          });

          this.drawSymbol = new Draw({
            type: "Point",
            source: this.vectorLayer.getSource(),
          });

          this.drawSymbol.on("drawend", (event: any) => {
            this.interrogationService.setResultSearching(true);
            if (this.interrogationSubscription) {
              this.subscriptions.unsubscribe();
            }

            let feature: Feature<any> = event.feature;
            const coordinates = (
              feature.getGeometry() as Point
            ).getCoordinates();
            const featureCircle = new Circle(
              coordinates,
              this.environment.interrogationPointPrecision *
                this.map.getView().getResolution()
            );
            feature = new Feature({
              name: "tooltip",
              geometry: fromCircle(featureCircle),
            });

            if (feature && this.rootLayerGroup) {
              const interrogableLayers = this.rootLayerGroup
                .getLayersArray()
                .filter((layer) => {
                  try {
                    if (
                      layer &&
                      layer.getVisible() &&
                      layer.get("visibleAtScale") &&
                      layer.get("visibleAtExtent") &&
                      layer.get("LayerQuery") &&
                      layer.get("LayerQuery").Fields &&
                      layer.get("LayerQuery").Fields.length &&
                      (layer.getSource() as any).getUrl()
                    ) {
                      return true;
                    } else if (
                      layer &&
                      layer.getVisible() &&
                      layer.get("visibleAtScale") &&
                      layer.get("visibleAtExtent") &&
                      layer.get("externalLayer") === true &&
                      ((layer instanceof ImageLayer &&
                        layer.getSource() instanceof ImageWMS) ||
                        (layer instanceof TileLayer &&
                          layer.getSource() instanceof TileWMS))
                    ) {
                      return true;
                    } else if (
                      layer &&
                      layer.getVisible() &&
                      layer.get("visibleAtScale") &&
                      layer.get("visibleAtExtent") &&
                      layer.get("externalLayer") === true &&
                      layer instanceof VectorLayer &&
                      layer.getSource() instanceof VectorSource
                    ) {
                      return true;
                    }
                  } catch (exception) {
                    console.error(exception, layer);
                  }
                  return false;
                });

              this.interrogationSubscription.add(
                this.interrogationService
                  .postInterrogation(
                    interrogableLayers as VectorLayer<VectorSource<Geometry>>[],
                    feature,
                    this.map.getView().getProjection(),
                    coordinates,
                    this.map,
                    this.screenMode,
                    true,
                    this.filterLayers
                  )
                  .subscribe({
                    next: (results) => {},
                    error: (err) => {
                      console.error(
                        "this.interrogationService.postInterrogation error",
                        err
                      );
                    },
                    complete: () => {},
                  })
              );
            }

            // A la fin du traitement, on retire le point
            this.vectorLayer.getSource().clear();
          });

          this.drawSymbol.setActive(true);
          this.map.addInteraction(this.drawSymbol);
        }
      })
    );

    this.subscriptions.add(
      this.coreService.getRootLayerGroup().subscribe((rootLayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      })
    );
  }

  ngOnDestroy() {
    // retirer l'evenement sur la carte au clic
    this.drawSymbol.setActive(false);
    this.map.removeInteraction(this.drawSymbol);

    this.subscriptions.unsubscribe();

    this.map.getOverlays().forEach((overlay, index) => {
      this.map.removeOverlay(overlay);
    });

    if (this.interrogationSubscription) {
      this.interrogationSubscription.unsubscribe();
    }
    this.interrogationService.resetResultOfInterrogation();
  }
}
