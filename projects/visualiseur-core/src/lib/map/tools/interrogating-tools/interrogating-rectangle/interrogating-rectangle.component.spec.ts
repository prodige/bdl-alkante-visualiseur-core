import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingRectangleComponent } from './interrogating-rectangle.component';

describe('InterrogatingRectangleComponent', () => {
  let component: InterrogatingRectangleComponent;
  let fixture: ComponentFixture<InterrogatingRectangleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingRectangleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingRectangleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
