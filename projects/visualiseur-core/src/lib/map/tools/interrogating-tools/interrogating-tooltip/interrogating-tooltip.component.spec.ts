import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingTooltipComponent } from './interrogating-tooltip.component';

describe('InterrogatingTooltipComponent', () => {
  let component: InterrogatingTooltipComponent;
  let fixture: ComponentFixture<InterrogatingTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
