import { EnvCoreService } from "./../../../../env.core.service";
import {
  Component,
  OnInit,
  OnDestroy,
  Host,
  Optional,
  ViewChild,
  ElementRef,
  Injectable,
} from "@angular/core";

import Map from "ol/Map";
import MapBrowserEvent from "ol/MapBrowserEvent";
import { MapService } from "../../../map.service";
import { MapIdService } from "../../../map-id.service";
import LayerGroup from "ol/layer/Group";
import {
  InterrogatingService,
  InterrogationReturnType,
} from "../../../services/interrogating.service";
import { VisualiseurCoreService } from "../../../../visualiseur-core.service";
import Feature from "ol/Feature";
import Circle from "ol/geom/Circle";

import { Subscription, ReplaySubject } from "rxjs";
import { fromCircle } from "ol/geom/Polygon";
import { Draw } from "ol/interaction";
import Overlay from "ol/Overlay";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import BaseLayer from "ol/layer/Base";
import { Style, Fill, Stroke } from "ol/style";
import { Geometry } from "ol/geom";

@Injectable()
@Component({
  selector: "alk-interrogating-tooltip",
  templateUrl: "./interrogating-tooltip.component.html",
  styleUrls: ["./interrogating-tooltip.component.scss"],
})
export class InterrogatingTooltipComponent implements OnInit, OnDestroy {
  screenMode: "mobile" | "desktop";
  @ViewChild("ficheInfoPopup", { read: null, static: true })
  ficheInfoPopup: ElementRef;
  ficheVisible = false;

  map: Map;
  rootLayerGroupSubscription: Subscription;
  rootLayerGroup: LayerGroup;
  vectorLayer: VectorLayer<VectorSource<Geometry>>;
  drawSymbol: Draw;

  cursorCoordinate: Array<number> = [];
  cursorPixel: Array<number> = [];
  public cursorCoordinate$: ReplaySubject<{
    coordinate: Array<number>;
    pixel: Array<number>;
  }> = new ReplaySubject(1);
  interrogationSubscription: Subscription;
  resultsOfInterrogationSubscription: Subscription;

  popup: Overlay;
  resultsOfInterrogation: InterrogationReturnType = null;

  layerResultIndex: {
    [layerResultName: string]: { index: number; nbResults: number };
  } = {};

  popoverTop = false;
  popoverLeft = false;
  ficheInfoPopupOffsetHeight = 0;
  ficheInfoTableOffsetHeight = 0;

  subscriptions = new Subscription();

  constructor(
    public mapService: MapService,
    public mapIdService: MapIdService,
    public interrogationService: InterrogatingService,
    public coreService: VisualiseurCoreService,
    public environment: EnvCoreService
  ) {}

  ngOnInit() {
    this.subscriptions.add(
      this.coreService.getRootLayerGroup().subscribe((rootLayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      })
    );

    // Initialisation du vectorDrawLayer
    this.vectorLayer = new VectorLayer({
      source: new VectorSource({
        wrapX: false,
        features: [],
      }),
      zIndex: Infinity,
    });

    this.drawSymbol = new Draw({
      type: "Point",
      source: this.vectorLayer.getSource(),
      style: new Style({
        stroke: new Stroke({
          color: [255, 255, 255, 0],
          width: 0,
        }),
        fill: new Fill({
          color: [255, 255, 255, 0],
        }),
      }),
    });

    if (document.getElementById && document.getElementById("visualiseur")) {
      document.getElementById("visualiseur").style.cursor = "pointer";
    }

    this.drawSymbol.on("drawend", (event: any) => {
      this.mapService.removeInformativeDisplayLayer();

      this.interrogationService.setTooltipResultSearching(true);

      this.cursorCoordinate = event.target.sketchCoords_;
      this.cursorPixel = event.target.downPx_;
      /* this.interrogateAtCoordinates({
        coordinate: this.cursorCoordinate,
        pixel: this.cursorPixel,
      }); */

      // A la fin du traitement, on retire le point
      this.vectorLayer.getSource().clear();
    });

    // ajouter un evenement sur la carte au clic
    let mapId = this.mapIdService.getId() || "main";
    this.subscriptions.add(
      this.mapService.getMapReady(mapId).subscribe((map) => {
        this.map = map as Map;

        if (this.map) {
          if (this.drawSymbol) {
            this.drawSymbol.setActive(true);
            this.map.addInteraction(this.drawSymbol);
          }

          // Chaque fois que les résultats de l'interrogation tooltip change, on effectue ce traitement
          this.subscriptions.add(
            this.interrogationService
              .getResultOfInterrogationTooltip()
              .subscribe((results: InterrogationReturnType) => {
                this.interrogationService.setTooltipResultSearching(false);

                if (
                  results &&
                  results.totalResults &&
                  results.totalResults > 0
                ) {
                  if (results.totalResults > 0) {
                    this.ficheVisible = true;
                    // On met la popover à gauche de la coordonnées si celle-ci est dans la moitié droite de la carte
                    this.popoverLeft =
                      this.cursorPixel[0] >
                      this.map.getViewport().offsetWidth / 2;
                    // On met la popover au dessus de la coordonnées si celle-ci est dans la moitié basse de la carte
                    this.popoverTop = !(
                      this.cursorPixel[1] >
                      this.map.getViewport().offsetHeight / 2
                    );

                    this.resultsOfInterrogation = results;
                    this.layerResultIndex = {};
                    this.resultsOfInterrogation.layersResults.forEach(
                      (layerResult) => {
                        this.layerResultIndex[layerResult.layerName] = {
                          index: 0,
                          nbResults: layerResult.nbResults,
                        };
                      }
                    );

                    // Popup showing the position the user clicked
                    if (!this.popup) {
                      this.popup = new Overlay({
                        element: this.ficheInfoPopup.nativeElement,
                        position: this.cursorCoordinate,
                      });
                      this.map.addOverlay(this.popup);
                    }
                  }
                }
              })
          );

          this.drawSymbol.setActive(true);
          this.map.addInteraction(this.drawSymbol);
        }

        this.drawSymbol.setActive(true);
        this.map.addInteraction(this.drawSymbol);
      })
    );
  }

  ngOnDestroy() {
    // retirer l'evenement sur la carte au clic
    this.closePopover();

    this.map.getOverlays().forEach((overlay, index) => {
      this.map.removeOverlay(overlay);
    });

    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    if (document.getElementById && document.getElementById("visualiseur")) {
      document.getElementById("visualiseur").style.cursor = "default";
    }
    this.drawSymbol.setActive(false);
    this.map.removeInteraction(this.drawSymbol);
  }

  getLayerQueryField(
    layer: BaseLayer,
    feature: Feature<Geometry>,
    urlField: any
  ): string {
    let finalUrl =
      (layer.get("LayerQuery").URLConfig
        ? layer.get("LayerQuery").URLConfig
        : "") + (urlField.FieldURLConfig ? urlField.FieldURLConfig : "");
    const fieldsToSearch = finalUrl.split(/[<>]/);

    if (finalUrl.match(/[<>]/) && fieldsToSearch.length > 0) {
      fieldsToSearch.forEach((fieldToSearch) => {
        for (const [key, value] of Object.entries(feature.getProperties())) {
          if (key === fieldToSearch) {
            finalUrl = finalUrl.replace(`<${fieldToSearch}>`, value);
          }
        }
      });
    } else {
      finalUrl += feature[urlField.FieldName];
    }

    return finalUrl;
  }

  closePopover: () => void = () => {
    this.resultsOfInterrogation = null;
    this.interrogationService.setResultOfInterrogationTooltip(
      this.resultsOfInterrogation
    );
    if (this.interrogationSubscription) {
      this.interrogationSubscription.unsubscribe();
    }
    if (this.popup) {
      // On détruit la précédente popover si il en existait une
      this.map.removeOverlay(this.popup);
      this.popup = null;
      this.ficheVisible = false;
    }
    this.ficheInfoPopup.nativeElement.remove();
  };

  getCursorCoordinates: (event: MapBrowserEvent<any>) => void = (
    event: MapBrowserEvent<any>
  ) => {
    this.cursorCoordinate$.next({
      coordinate: event.coordinate,
      pixel: event.pixel,
    });
  };

  interrogateAtCoordinates: (cursorCoordinates: {
    coordinate: Array<number>;
    pixel: Array<number>;
  }) => void = (cursorCoordinates: {
    coordinate: Array<number>;
    pixel: Array<number>;
  }) => {
    this.closePopover();

    const featureCircle = new Circle(
      cursorCoordinates.coordinate,
      this.environment.interrogationTooltipPrecision *
        (this.map as Map).getView().getResolution()
    );
    const feature = new Feature({
      name: "tooltip",
      geometry: fromCircle(featureCircle),
    });

    if (feature && this.rootLayerGroup) {
      const interrogableLayers = this.rootLayerGroup
        .getLayersArray()
        .filter((layer) => {
          try {
            if (
              layer &&
              layer.getVisible() &&
              layer.get("visibleAtScale") &&
              layer.get("visibleAtExtent") &&
              layer.get("LayerQuery") &&
              layer.get("LayerQuery").Fields &&
              layer.get("LayerQuery").Fields.length &&
              (layer.getSource() as any).getUrl()
            ) {
              const layerQuery = layer.get("LayerQuery");
              if (
                layerQuery &&
                layerQuery.hasTooltip &&
                layerQuery.Fields &&
                layerQuery.Fields.filter((field) => field.FieldTooltip).length >
                  0
              ) {
                return true;
              } else if (
                layer &&
                layer.getVisible() &&
                layer.get("visibleAtScale") &&
                layer.get("visibleAtExtent") &&
                layer.get("externalLayer") === true &&
                layer instanceof VectorLayer &&
                layer.getSource() instanceof VectorSource
              ) {
                return true;
              }
            }
          } catch (exe) {
            console.error(exe, layer);
          }
          return false;
        });

      this.subscriptions.add(
        this.interrogationService
          .postInterrogationTooltip(
            interrogableLayers as VectorLayer<VectorSource<Geometry>>[],
            feature,
            (this.map as Map).getView().getProjection()
          )
          .subscribe({
            next: (results) => {},
            error: (err) => {
              console.error(
                "this.interrogationService.postInterrogation error",
                err
              );
            },
            complete: () => {},
          })
      );
    }
  };

  prevFeature(layerResultName): void {
    if (this.layerResultIndex[layerResultName].index === 0) {
      this.layerResultIndex[layerResultName].index =
        this.layerResultIndex[layerResultName].nbResults - 1;
    } else {
      this.layerResultIndex[layerResultName].index--;
    }
  }

  nextFeature(layerResultName): void {
    if (
      this.layerResultIndex[layerResultName].index ===
      this.layerResultIndex[layerResultName].nbResults - 1
    ) {
      this.layerResultIndex[layerResultName].index = 0;
    } else {
      this.layerResultIndex[layerResultName].index++;
    }
  }

  updateTooltipTableSize(
    ficheInfoPopupOffsetHeight: number,
    ficheInfoTableOffsetHeight: number
  ): void {
    this.ficheInfoPopupOffsetHeight = ficheInfoPopupOffsetHeight;
    this.ficheInfoTableOffsetHeight =
      Number.isFinite(ficheInfoTableOffsetHeight) &&
      !Number.isNaN(ficheInfoTableOffsetHeight)
        ? Math.max(ficheInfoTableOffsetHeight, 0)
        : 0;
  }
}
