import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingGrantedRequestComponent } from './interrogating-granted-request.component';

describe('InterrogatingGrantedRequestComponent', () => {
  let component: InterrogatingGrantedRequestComponent;
  let fixture: ComponentFixture<InterrogatingGrantedRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingGrantedRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingGrantedRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
