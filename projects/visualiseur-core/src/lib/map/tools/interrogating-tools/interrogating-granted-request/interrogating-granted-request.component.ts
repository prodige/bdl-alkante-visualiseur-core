import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InterrogatingGrantedRequestModalComponent } from '../../../../modals/interrogating-granted-request-modal/interrogating-granted-request-modal.component';
import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { Subscription } from 'rxjs';
import { MapService } from '../../../map.service';
import { MapIdService } from '../../../map-id.service';
import Map from 'ol/Map';

@Component({
  selector: 'alk-interrogating-granted-request',
  templateUrl: './interrogating-granted-request.component.html',
  styleUrls: ['./interrogating-granted-request.component.scss']
})
export class InterrogatingGrantedRequestComponent implements OnInit, OnDestroy {

  map: Map;
  mapId: string;

  coreServiceSubscription: Subscription;
  @Input() mapHrefPath: string;
  @Output() closeEvent = new EventEmitter();

  constructor(
    protected modalService: NgbModal,
    public activeModal: NgbActiveModal,
    public coreService: VisualiseurCoreService,
    protected mapService: MapService,
    protected mapIdService: MapIdService
  ) { }

  ngOnInit() {

    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;
    });

    this.coreServiceSubscription = this.coreService.getRootLayerGroup().subscribe(
      rootLayerGroup => {
        // Récupération des couches associées à la carte
        if ( rootLayerGroup ) {

          const interrogableLayers = rootLayerGroup.getLayersArray().filter(layer => {
            if ( layer.get('externalLayer') === true ) {
              return false;
            }
            try {
              if (
                layer &&
                layer.get('LayerQuery') && layer.get('LayerQuery').Fields && layer.get('LayerQuery').Fields.length &&
                (layer.getSource() as any).getUrl() /*&& layer.get('externalLayer')*/
              ) {
                if ( layer.get('externalLayer') === true ) {
                  return false;
                }
                return true;
              }
            } catch (exe) {
              console.error(exe, layer);
            }
            return false;
          }).map( layer => {
            return {
              layerId: layer.get('layerName'),
              layerName: layer.get('name'),
              url: layer.getSource().get('url'),
              layer
            };
          });

          if ( interrogableLayers.length ) {
            const modalRef = this.modalService.open(InterrogatingGrantedRequestModalComponent, {
              size: 'lg',
              centered: true,
              backdrop: 'static'
            });
            (modalRef.componentInstance as InterrogatingGrantedRequestModalComponent).modalReady = true;
            (modalRef.componentInstance ).possibleLayers = interrogableLayers;
            (modalRef.componentInstance as InterrogatingGrantedRequestModalComponent).mapHrefPathNoRouter = this.mapHrefPath;

            modalRef.result.then(close => {
              // Quand on ferme la popup, fermer également celle-ci
              this.coreService.setMapTool(null);
              this.modalService.dismissAll();
              this.closeEvent.emit();
            }).catch( () => {
              this.coreService.setMapTool(null);
              this.modalService.dismissAll();
              this.closeEvent.emit();
            });
          } else {
            this.modalService.dismissAll();
            this.closeEvent.emit();
          }
        }
      }
    );

  }
  ngOnDestroy() {
    this.activeModal.close();
    this.closeEvent.emit();
    if ( this.coreServiceSubscription ) {
      this.coreServiceSubscription.unsubscribe();
    }

    this.map.getOverlays().forEach((overlay, index) => {
      this.map.removeOverlay(overlay);
    });
  }

}
