import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VisualiseurCoreService } from '../../../../visualiseur-core.service';
import { InterrogatingService } from '../../../services/interrogating.service';

export type InterrogatingType = 'interrogatingPoint' | 'interrogatingCircle' | 'interrogatingRectangle' | 'interrogatingPolygon';

@Component({
  selector: 'alk-interrogating-modal',
  templateUrl: './interrogating-modal.component.html',
  styleUrls: ['./interrogating-modal.component.scss']
})
export class InterrogatingModalComponent implements OnInit, OnDestroy {

  toolName: string;
  measuringOption: string;
  type: InterrogatingType = 'interrogatingPoint';

  mapToolSubscription: Subscription;
  mapTool: string;
/* 
  resultSearching = false;
  resultSearchingSubscription: Subscription
  resultAvailable = false;
  resultAvailableSubscription: Subscription;

  toastResponse: string = null;
  @ViewChild('copyToast') copyToast: ElementRef; */

  constructor(
    public activeModal: NgbActiveModal,
    private coreService: VisualiseurCoreService/* ,
    private interrogatingService: InterrogatingService */
  ) { }

  ngOnInit() {
    this.mapToolSubscription = this.coreService.getMapTool().subscribe(
      tool => {
        this.mapTool = tool;
      },
      error => console.error(error)
    );

    /* this.resultAvailableSubscription = this.interrogatingService.getResultOfInterrogation().subscribe(
      ( result ) => {
        if ( result && result.totalResults > 0 ) {
          this.resultAvailable = true;
        } else {
          this.resultAvailable = false;
        }
      }
    );

    this.resultSearchingSubscription = this.interrogatingService.getResultSearching().subscribe(
      ( result ) => {
        this.resultSearching = result;
        if (!this.resultSearching) {
          setTimeout(() => {
            if (!this.resultAvailable) {
              this.toastResponse = `Pas de résultats`;
              this.copyToast.nativeElement.className = 'toast show';
              setTimeout(() => {
                this.copyToast.nativeElement.className = this.copyToast.nativeElement.className.replace('show', '');
              }, 100000);
            }
          }, 500);
        }
      }
    ); */
  }

  ngOnDestroy() {
    if ( this.mapToolSubscription ) {
      this.mapToolSubscription.unsubscribe();
    }
  }

  setToolName(name: string) {
    this.toolName = name;
  }

}
