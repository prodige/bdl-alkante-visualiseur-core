import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogatingModalComponent } from './interrogating-modal.component';

describe('InterrogatingModalComponent', () => {
  let component: InterrogatingModalComponent;
  let fixture: ComponentFixture<InterrogatingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterrogatingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogatingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
