import { Directive, ElementRef, Input, OnChanges, HostListener, SimpleChanges } from '@angular/core';

// tslint:disable-next-line: directive-selector
@Directive({ selector: '[indeterminate]' })
export class IndeterminateDirective implements OnChanges {
  @Input() indeterminate: boolean = null;
  @Input() checked: boolean = null;

  constructor(private elem: ElementRef) {}

  public ngOnChanges(change: SimpleChanges) {
    this.elem.nativeElement.checked = this.checked;
    this.elem.nativeElement.indeterminate = this.elem.nativeElement.checked && !!this.indeterminate;
  }

  @HostListener('change')
  onChanges() {
    this.elem.nativeElement.checked = this.elem.nativeElement.checked || this.checked;
    this.elem.nativeElement.indeterminate = this.elem.nativeElement.checked && !!this.indeterminate;
  }
}
