export interface OptionChart{
  templateId: string;
  labels?: {[key: string]: string};
  colors?: string[];
  columns?: number|string;
  type: 'bar'|'pie';
  angleValueOnchart?: number;
}

