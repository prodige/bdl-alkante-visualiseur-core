export interface FilterItem{
  id: number;
  name: String;
}

export interface FilterLayer {
  views: Array<FilterItem>;
  selecteds: Array<FilterItem>;
  field: string;
  name: string;
  layerId: string;
}

