/**
 *
 * exemple:
 * ```
 * {
 *     fields: {terrain: 'très grand', recolte: {ete2020: 50, printemps2020: 54, hivers2025: 68}},
 *     labels: {terrain: 'Taille du terrain'},
 *     graph: {recolte: {
 *       templateId: 'graph-recolte',
 *       colors: ['#7845', '#4578'],
 *       labels: {ete2020: 'été 2020', printemps2020: 'printemps 2020', hivers2025: 'hivers 2025'}
 *     }},
 *     view:{
 *       projection: "urn:ogc:def:crs:EPSG::2154",
 *       center: [563401.85717836,6792415.9857373],
 *       zoom: 12
 *     },
 *     header: "Titre du pdf",
 *     footer: "Pied de page du pdf"
 * }
 * ```
 */
import {OptionChart} from './option-chart';
import {featureCollection} from './context';
import Properties = featureCollection.Properties;



export interface OptionImpression{
  /** Champs afficher dans l'impression */
  fields?: {[key: string]: unknown};

  /** Labels pour remplacer les noms des champs */
  labels?: {[key: string]: string};

  /** correspondance entre l'id des graph dans le template et les champs  */
  graph?: {[key: string]: OptionChart};

  /** Pour centrer la vue de la carte sur un point */
  view?: ImpressionView;

  /** entête */
  header: string;

  /** pied de page */
  footer: string;
}

export interface ImpressionView{
  center?: number[];
  extend?: number[];
  zoom?: number;
  projection: string;
}

