export interface ConnexionConfig {
    email: string;
    login: string;
    name: string;
    firstName: string;
    userInternet: boolean;
    URLS: UrlCAS;
}

export interface UrlCAS {
    admincarto: string;
    caslogin: string;
    caslogout: string;
    catalogue: string;
    telecarto: string;
}
