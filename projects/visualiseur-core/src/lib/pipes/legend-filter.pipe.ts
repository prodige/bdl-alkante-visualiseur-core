import { Pipe, PipeTransform } from '@angular/core';
import BaseLayer from 'ol/layer/Base';

interface AlkLegend{
  layer: BaseLayer;
}

@Pipe({
  name: 'legendFilter'
})
export class LegendFilterPipe implements PipeTransform {

  transform(legends: AlkLegend[]): unknown {
    if (!legends || !(legends instanceof Array)){
      return false;
    }

    return legends.filter((legend) => {
      const baseLayer = legend.layer as BaseLayer;
      return (
        baseLayer.get('filterVisibility') !== false &&
        baseLayer.get('visibleAtExtent') &&
        baseLayer.get('visibleAtScale') &&
        baseLayer.get('visible') &&
        baseLayer.get('displayClass')
      );
    });


  }

}
