import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transforme un nombre en label en fonction d'un opérateur et d'une valeur
 * exemple : 32 | numberToLabel:'<':50:'mon label'
 *
 */
@Pipe({
  name: 'numberToLabel'
})
export class NumberToLabelPipe implements PipeTransform {

  transform(value: number|string, operator: '<'|'>'|'<='|'>='|'===', value2: number|string, label: string): string|number {
    let retour = value;

    const nb = typeof value === 'string' ?  parseInt(value, 10) : value;
    const nb2 = typeof value2 === 'string' ?  parseInt(value2, 10) : value2;

    switch (operator){
      case '<':
        retour = nb < nb2 ? label : retour;
        break;
      case '<=':
        retour = nb <= nb2 ? label : retour;
        break;
      case '>':
        retour = nb > nb2 ? label : retour;
        break;
      case '>=':
        retour = nb >= nb2 ? label : retour;
        break;
      case '===':
        retour = nb === nb2 ? label : retour;
        break;
      default:

    }

    return retour;
  }

}
