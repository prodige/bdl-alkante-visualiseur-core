import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'informationSheetFilter'
})
export class InformationSheetFilterPipe implements PipeTransform {

  transform(value: number|string|boolean, digit: number = null, local: string = 'fr-Fr'): number|string {
    if ( typeof digit === 'string' ){
      digit = parseInt(digit, 10);
    }

    if (!value && value !== 0 && value !== false) {
      return 'Données indisponibles';
    } else if (value === true || value === false) {
      return value ? 'Oui' : 'Non';
    } else if (isFinite(value as number)) {
      if (typeof value === 'string'){
        value = parseFloat(value);
      }
      if (digit || digit === 0) {
        value = parseFloat( (value).toFixed(digit));
      }

      return value.toLocaleString(local);
    } else {
      return value;
    }
  }

}
