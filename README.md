# VisualiseurCore

## Developpement

See ```./cicd/dev/README.md.

## Build & Release

To build a new version, i.e. push from branch _develop_ to _master_, use the jenkins task [prodige_bdl-alkante-visualiseur-core_build](https://jenkins.alkante.al/view/Prodige/job/prodige_bdl-alkante-visualiseur-core_build/). No parameter is needed.

To publish the version as a package available for other project, use the jenkins task [prodige_bdl-alkante-visualiseur-core_release](https://jenkins.alkante.al/view/Prodige/job/prodige_bdl-alkante-visualiseur-core_release/) and give the new package version number which is also used to tag the master branch.





# TODO Change/Update this part

## Paramétrage des variables d'environnement
Les variables d'environnement se placent dans les fichiers intitulés par exemple env.service.ts ou env.core.service.ts.
Plus tard, lorsque le projet sera compilé, pour éviter de recompiler mais tout en voulant changer les valeurs de ces dites variables, le fichier à modifier sera : env.js, dans le dossier du projet compilé. 
Un exemple de surcharge des valeurs est disponible dans le fichier : /src/env.js

Info 1 : La surcharge de l'environnement est possible uniquement si la variable est déjà présente dans un fichier type "env.service". Exemple : 

```javascript
// ---------- ---------- ---------- env.service.ts :

class EnvService {

  public var1 = 'A';
  public var2 = false;
  public var3  = 5;

}

// ---------- ---------- ---------- env.js :
// Autorisé :
$ENV.var1 = 'B';

// Non autorisé :
$ENV.var4 = 'Z';
```

Info 2 : Le paramétrage de l'url du CAS se fait côté serveur. (`${environment.serverUrl}prodige/config`)

## Générer la traduction
```console
npm run extract-i18n
```

## Pour builder la version multilingue de l'application 
```console
npm run build-locale
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

